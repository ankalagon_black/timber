package test.test;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.opencv.core.Core.*;
import static org.opencv.core.CvType.*;
import static org.opencv.imgproc.Imgproc.*;

public class TestController {
    public ImageView originalImage;
    public ImageView processedImage;

    public Slider minDist;
    public Slider p1;
    public Slider p2;
    public Slider minRadius;
    public Slider maxRadius;

    private Image image;

    public void setController(Image image) throws IOException {
        System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
        this.image = image;

        minDist.setValue(105);
        p1.setValue(100);
        p2.setValue(12);
        minRadius.setValue(31);
        maxRadius.setValue(105);

        recompute(minDist.getValue(), p1.getValue(), p2.getValue(), (int) minRadius.getValue(), (int) maxRadius.getValue());

        minDist.valueProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if(!minDist.isValueChanging()) recompute(newValue.doubleValue(), p1.getValue(), p2.getValue(), (int) minRadius.getValue(), (int) maxRadius.getValue());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        p1.valueProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if(!p1.isValueChanging()){
                    recompute(minDist.getValue(), newValue.doubleValue(), p2.getValue(), (int) minRadius.getValue(), (int) maxRadius.getValue());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        p2.valueProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if(!p2.isValueChanging()){
                    recompute(minDist.getValue(), p1.getValue(), newValue.doubleValue(), (int) minRadius.getValue(), (int) maxRadius.getValue());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        minRadius.valueProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if(!minRadius.isValueChanging()) recompute(minDist.getValue(), p1.getValue(), p2.getValue(), newValue.intValue(), (int) maxRadius.getValue());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        maxRadius.valueProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if(!maxRadius.isValueChanging()) recompute(minDist.getValue(), p1.getValue(), p2.getValue(), (int) minRadius.getValue(), newValue.intValue());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void recompute(double minDistance, double p1, double p2, int minRadius, int maxRadius) throws IOException {
        Mat original = imageToMat(image);

        Mat gray = new Mat();
        cvtColor(original, gray, COLOR_BGR2GRAY);

        GaussianBlur(gray, gray, new Size(5,5), 0, 0);
        dilate(gray, gray, Mat.ones(3, 3, CV_8UC1));

        Mat circles = new Mat(), mask = new Mat(original.rows(), original.cols(), CV_8UC1);
        Imgproc.HoughCircles(gray, circles, Imgproc.HOUGH_GRADIENT,
                1.0,
                minDistance, p1, p2, minRadius, maxRadius);

        for (int x = 0; x < circles.cols(); x++) {
            double[] c = circles.get(0, x);
            Point center = new Point(Math.round(c[0]), Math.round(c[1]));
            int radius = (int) Math.round(c[2]);
            Imgproc.circle(mask, center, radius, new Scalar(GC_PR_FGD ,GC_PR_FGD ,GC_PR_FGD ), -1);
            Imgproc.circle(original, center, radius, new Scalar(0,0,0), 3, 8, 0 );
        }

        originalImage.setImage(matToImage(original));

        grabCut(original, mask, new Rect(), new Mat(), new Mat(), 1, GC_INIT_WITH_MASK );

        for (int i = 0; i < mask.rows(); i++) {
            for (int j = 0; j < mask.cols(); j++) {

                if(mask.get(i, j)[0] == 0 || mask.get(i,j)[0] == 2) {
                    //mask.put(i , j , 0,0,0);
                }
                else if(mask.get(i,j)[0] == 1 || mask.get(i,j)[0] == 3) {
                    mask.put(i , j , 255,255,255);
                }
            }
        }

        processedImage.setImage(matToImage(mask));
    }


//    private Mat getSharpened(Mat original) {
//        original = original.clone();
//
//        Mat laplacian = new Mat();
//        Imgproc.filter2D(original, laplacian, CV_8UC1, getKernel());
//
//        return laplacian;
//    }
//
//    private Mat getKernel() {
//        int[][] arr = {
//                {1,  1, 1},
//                {1, -8, 1},
//                {1,  1, 1}
//        };
//
//        Mat kernel = new Mat();
//        kernel.create( 3, 3, CV_8UC1 );
//
//        for (int i=0; i < 3; i++) for(int j=0; j < 3; j++) kernel.put(i,j, arr[i][j]);
//
//        return kernel;
//    }


    private void out(Mat mat){
        for (int i = 0; i < 100; i++) System.out.println(Arrays.toString(mat.get(i, i)) + " ");
    }

    private Mat imageToMat(Image image) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "jpg", byteArrayOutputStream);
        byteArrayOutputStream.flush();
        return Imgcodecs.imdecode(new MatOfByte(byteArrayOutputStream.toByteArray()), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
    }

    private Image matToImage(Mat matrix)throws IOException {
        MatOfByte mob = new MatOfByte();
        Imgcodecs.imencode(".jpg", matrix, mob);
        return SwingFXUtils.toFXImage(ImageIO.read(new ByteArrayInputStream(mob.toArray())), null);
    }

    //        Mat grad_x = new Mat(), grad_y = new Mat();
//        Mat abs_grad_x = new Mat(), abs_grad_y = new Mat();
//
//        Imgproc.Sobel(gray, grad_x, CV_16S , 1, 0);
//        Imgproc.Sobel(gray, grad_y, CV_16S, 0, 1);
//
//        Core.convertScaleAbs(grad_x, abs_grad_x);
//        Core.convertScaleAbs(grad_y, abs_grad_y);
//
//        Mat weight = new Mat();
//        Core.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, weight);
//
//        Mat res = new Mat(), res_abs = new Mat();
//        Imgproc.Laplacian( gray, res, CV_16S, 3, 2, 0);
//        // converting back to CV_8U
//        Core.convertScaleAbs( res, res_abs );

//        Mat dst = new Mat(), detected_edges = new Mat();
//        Imgproc.Canny( blurred, detected_edges, 1,3 );
//
//        original.copyTo( dst, detected_edges);



//        Mat circles = new Mat();
//        Imgproc.HoughCircles(dist, circles, Imgproc.HOUGH_GRADIENT, 1.0,
//                (double)gray.rows()/16, // change this value to detect circles with different distances to each other
//                100.0, 30.0, 1, 30); // change the last two parameters
//        // (min_radius & max_radius) to detect larger circles
//        for (int x = 0; x < circles.cols(); x++) {
//            double[] c = circles.get(0, x);
//            Point center = new Point(Math.round(c[0]), Math.round(c[1]));
//            // circle center
//            Imgproc.circle(dist, center, 1, new Scalar(0,100,100), 3, 8, 0 );
//            // circle outline
//            int radius = (int) Math.round(c[2]);
//            Imgproc.circle(dist, center, radius, new Scalar(255,0,255), 3, 8, 0 );
//        }
    // (min_radius & max_radius) to detect larger circles

//        Mat dst = Mat.zeros(canny_output.size(), CV_8UC3);
//        for( int i = 0; i < 100; i++ )
//        {
//            Mat mat = contours.get(i);
//            for (int j = 0; j < mat.rows(); j++) {
//                System.out.print(Arrays.toString(mat.get(j, 0)) + " ");
//            }
//            System.out.println();
//        }
    //        List<MatOfPoint> contours = new ArrayList<>();
//        Mat hierarchy = new Mat();
//        Imgproc.findContours( canny_output, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
//Mat canny_output = new Mat();
//        Imgproc.Canny( gray, canny_output,  150, 250);
//
//    Mat circles = new Mat();
//        Imgproc.HoughCircles(canny_output, circles, Imgproc.HOUGH_GRADIENT, 2.0, 40, 100, 200, 30, 200);
//
//        for (int x = 0; x < circles.cols(); x++) {
//        double[] c = circles.get(0, x);
//        Point center = new Point(Math.round(c[0]), Math.round(c[1]));
//        // circle center
//        Imgproc.circle(canny_output, center, 1, new Scalar(0, 100, 100), 3, 8, 0);
//        // circle outline
//        int radius = (int) Math.round(c[2]);
//        Imgproc.circle(canny_output, center, radius, new Scalar(255, 0, 255), 3, 8, 0);
//    }
//Mat sharpened = getSharpened(original), gray = new Mat();
//        Imgproc.cvtColor(sharpened, gray, COLOR_BGR2GRAY);
//        Imgproc.threshold(gray, gray, 0, 255, THRESH_OTSU);
//
//        Mat dist = new Mat();
//        Imgproc.distanceTransform(gray, dist, DIST_L2, 3);
//        Imgproc.threshold(dist, dist, 5, 255, THRESH_BINARY);
//
//        dilate(dist, dist, Mat.ones(3, 3, CV_8UC1));
//
//        dist.convertTo(dist, CV_8UC1);
//
//        List<MatOfPoint> points = new ArrayList<>();
//        findContours(dist, points, new Mat(), RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
//
//        Mat skins = Mat.zeros(dist.size(), CV_32SC1);
//
//        for (int i = 0; i < points.size(); i++) drawContours(skins, points, i, new Scalar(255, 255, 255), 6);
//
//        originalImage.setImage(matToImage(skins));
//
////        circle(skins, new Point(5,5), 3, new Scalar(255, 255, 255), -1);
////
////        watershed(original, skins);
////
////        Mat mark = Mat.zeros(skins.size(), CV_8UC1);
////        skins.convertTo(mark, CV_8UC1);
////        bitwise_not(mark, mark);
//
//        skins.convertTo(skins, CV_8UC3);
//        Imgproc.cvtColor( skins, skins, Imgproc.COLOR_GRAY2BGR);
//
//        //Mat res = new Mat();
//        Core.add(original, skins, original);
//
//        processedImage.setImage(matToImage(original));
    //        Mat blurred = new Mat();
//        Imgproc.GaussianBlur(original, blurred, new Size(3,3), 0,0, BORDER_DEFAULT);
//
//        Mat gray = new Mat();
//        Imgproc.cvtColor(blurred, gray, COLOR_BGR2GRAY);
//
//        //todo determine how parameters influence result
//
//        Mat gradX = new Mat();
//        Imgproc.Sobel(gray, gradX,  CV_16S, 1, 0);
//
//        Mat gradY = new Mat();
//        Imgproc.Sobel(gray, gradY, CV_16S, 0,1);
//
//        gradX.convertTo(gradX, CV_8UC1);
//        gradY.convertTo(gradY, CV_8UC1);
//
//        Mat grad = new Mat();
//        Core.addWeighted(gradX, 0.5, gradY, 0.5,0, grad);
//
//        Mat detected_edges = new Mat();
//        Imgproc.Canny( gray, detected_edges, 20,100 );
//
//        System.out.println(detected_edges);
//
//        processedImage.setImage(matToImage(detected_edges));
    //        Mat dist = new Mat();
//        Imgproc.distanceTransform(binary, dist, DIST_L2, 3);
//        dilate(dist, dist, Mat.ones(3, 3, CV_8UC1));
//        dist.convertTo(dist, CV_8UC1);
//
//        originalImage.setImage(matToImage(dist));

//        List<MatOfPoint> contours = new ArrayList<>();
//        Mat hierarchy = new Mat();
//        Imgproc.findContours( dist, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
//
//        Mat skins = Mat.zeros(gray.size(), CV_32SC1);
//        for (int i = 0; i < contours.size(); i++) drawContours(skins, contours, i, new Scalar(i + 1,i+1,i+1), -1);


    //originalImage.setImage(matToImage(skins));
    //    private Mat getSharpened(Mat original) {
//        original = original.clone();
//        original.convertTo(original, CV_32F);
//
//        Mat laplacian = new Mat();
//        Imgproc.filter2D(original, laplacian, CV_32F, getKernel());
//
//        Mat result = new Mat();
//        Core.subtract(laplacian, original, result);
//
//        Core.normalize(result, result, 0, 255, Core.NORM_MINMAX, CvType.CV_8UC1);
//
//        return result;
//    }
    //        Mat binary = new Mat();
//        threshold(gray, binary, 0, 255, THRESH_OTSU);


//        Mat laplacian = getSharpened(original);
//        Laplacian(original, laplacian, CV_32F, 3, 1,0);
//
//        laplacian.convertTo(laplacian, CV_8UC1);
//
//
//        Mat result = new Mat();
//        Core.add(original, laplacian, result);
//

//
//        Mat binary = new Mat();
//        threshold(gray, binary, 0, 255,THRESH_OTSU);
//
//        Mat dist = new Mat();
//        distanceTransform(binary, dist, DIST_L2, 3);
//
//        dist.convertTo(dist, CV_8UC1);

//        List<MatOfPoint> contours = new ArrayList<>();
//        Mat hierarchy = new Mat();
//        Imgproc.findContours( dist, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
//
//        Mat skins = Mat.zeros(gray.size(), CV_32SC1);
//        for (int i = 0; i < contours.size(); i++) drawContours(skins, contours, i, new Scalar(i + 1,i+1,i+1), -1);
    //Mat yellow = new Mat();
    //subtract(original, new Scalar(255,255,0), yellow);
}
