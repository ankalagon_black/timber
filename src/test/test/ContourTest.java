package test.test;

import javafx.geometry.Point2D;
import main.core.data_types.markers.contour_branch.Contour;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ContourTest {

    @Test
    public void calculateArea() {
        List<Point2D> point2DS = new ArrayList<>();

        point2DS.add(new Point2D(0,0));
        point2DS.add(new Point2D(0,2));
        point2DS.add(new Point2D(2,2));
        point2DS.add(new Point2D(2,0));

        //point2DS.add(new Point2D(2,5));
        //point2DS.add(new Point2D(0,5));
        //point2DS.add(new Point2D(0,7));
        //point2DS.add(new Point2D(4,7));
        //point2DS.add(new Point2D(4,0));

        Contour contour = new Contour(0, 0, 0, point2DS);
        contour.calculateArea();

        System.out.println(contour.getArea());
    }
}