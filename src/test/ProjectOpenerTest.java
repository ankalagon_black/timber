package test;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Action;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import io.reactivex.schedulers.Schedulers;
import main.Main;
import main.algorithms.ClassifierDetector;
import main.algorithms.PrepareTrainingData;
import main.algorithms.WoodLogsDetector;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.junctions.CircleMarkersJunction;
import org.junit.Test;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.*;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.Callable;

public class ProjectOpenerTest {

    @Test
    public void createPositiveAndNegative() {
        PrepareTrainingData prepareTrainingData = new PrepareTrainingData(new File("D:\\18\\sources"));

        prepareTrainingData.prepare(new File("D:\\18\\test_samples"));
    }

    @Test
    public void cascade() {
        System.load(new File("./libraries/opencv_java341.dll").getAbsolutePath());


        File cascade = new File("D:\\18\\test_samples\\cascades\\cascade.xml"),
                stage1 = new File("D:\\18\\test_samples\\second data\\set1\\133 500\\cascade.xml"),
                stage4 = new File("D:\\18\\test_samples\\second data\\set4\\690 1500 4 0.05\\cascade.xml"),
                stage5 = new File("D:\\18\\test_samples\\second data\\set5\\980 1500 4 0.05\\cascade.xml"),
                oldCascade24 = new File("D:\\18\\test_samples\\first data\\24 24 fa-0.43 11 stages ratio 360-900\\cascade.xml"),
                oldCascade30 = new File("D:\\18\\test_samples\\first data\\30 30 fa-0.43 11 stages ratio 360-900\\cascade.xml");

        ClassifierDetector detector1 = new ClassifierDetector(cascade);
        ClassifierDetector detector2 = new ClassifierDetector(oldCascade24);

        File testSources = new File("D:\\18\\test_samples\\test_samples");
        File testResults = new File("D:\\18\\test_samples\\test_results");

        File[] files = testSources.listFiles();

        for (File file: files) {
            Mat imageSource = Imgcodecs.imread(file.getAbsolutePath());
            if(imageSource.channels() == 4) Imgproc.cvtColor(imageSource, imageSource, Imgproc.COLOR_BGRA2BGR);
            Mat grayScaleImage = new Mat();
            Imgproc.cvtColor(imageSource, grayScaleImage, Imgproc.COLOR_BGR2GRAY);

            List<Rect> rois;

            rois = detector2.detectRoi(grayScaleImage, 40, 300, 12);
            for (Rect i: rois) Imgproc.rectangle(imageSource, new Point(i.x, i.y), new Point(i.x + i.width, i.y + i.height), new Scalar(255, 0, 0), 4);

            rois = detector1.detectRoi(grayScaleImage, 40, 300, 12);
            for (Rect i: rois) Imgproc.rectangle(imageSource, new Point(i.x, i.y), new Point(i.x + i.width, i.y + i.height), new Scalar(255, 0, 255), 4);


            File output = new File(testResults, file.getName());
            Imgcodecs.imwrite(output.getAbsolutePath(), imageSource);
        }
    }

    @Test
    public void sample() {
        try {
            System.out.println(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            System.out.println(new File("").getAbsolutePath());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}