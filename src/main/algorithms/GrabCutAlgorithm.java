package main.algorithms;

import main.core.utils.ViewUtils;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.core.CvType.CV_8UC3;
import static org.opencv.imgproc.Imgproc.*;

public class GrabCutAlgorithm {
    public static final String CONVEX_HULL = "convex_hull.jpg",
            PR_FG = "pr_fg.jpg", FG = "fg.jpg", INNER = "inner.jpg",
            DISTANCE_TRANSFORM = "dt.jpg", DT_THRESH = "dt_thresh.jpg",
            MASK_BEFORE = "mask_before.jpg", MASK_AFTER = "mask_after.jpg";

    private Mat imageSource;
    private List<Point> convexHull;
    private List<Rect> rois;

    public GrabCutAlgorithm(Mat imageSource, List<Point> convexHull, List<Rect> rois) {
        this.imageSource = imageSource;
        this.convexHull = convexHull;
        this.rois = rois;
    }


    public Mat segmentImage(File outputFolder) {
        Mat mask = prepareMask(outputFolder);

        if(outputFolder != null) {
            File output = new File(outputFolder, MASK_BEFORE);
            Imgcodecs.imwrite(output.getAbsolutePath(), maskToColored(mask));
        }

        Imgproc.grabCut(imageSource, mask, new Rect(), new Mat(), new Mat(), 5, Imgproc.GC_INIT_WITH_MASK);

        if (outputFolder != null) {
            File output = new File(outputFolder, MASK_AFTER);
            Imgcodecs.imwrite(output.getAbsolutePath(), maskToColored(mask));
        }

        return mask;
    }

    private Mat prepareMask(File outputFolder) {
        Mat mask = new Mat(imageSource.rows(), imageSource.cols(), CV_8UC1, new Scalar(GC_BGD));

        MatOfPoint matOfPoint = new MatOfPoint();
        matOfPoint.fromList(convexHull);
        drawContours(mask,  Collections.singletonList(matOfPoint), 0, new Scalar(GC_PR_BGD), -1);

        if(outputFolder != null) {
            File output = new File(outputFolder, CONVEX_HULL);
            Imgcodecs.imwrite(output.getAbsolutePath(), maskToColored(mask));
        }

        for (Rect i: rois) {
            rectangle(mask, new Point(i.x, i.y), new Point(i.x + i.width, i.y + i.height), new Scalar(GC_PR_FGD), -1);
        }

        if(outputFolder != null) {
            File output = new File(outputFolder, PR_FG);
            Imgcodecs.imwrite(output.getAbsolutePath(), maskToColored(mask));
        }

        List<Rect> innerRois = new ArrayList<>();

        filterRois(mask, innerRois, outputFolder);
        applyDistanceTransform(mask, innerRois, outputFolder);

        return mask;
    }

    private void filterRois(Mat mask, List<Rect> innerRois, File outputFolder) {
        Point center = ViewUtils.getConvexHullCenter(convexHull);
        double radius = ViewUtils.getConvexHullInscribedCircleRadius(center, convexHull);
        double rSq = radius * radius;

        innerRois.addAll(rois.stream().filter(rect -> {
            Point rectCenter = new Point(rect.x + rect.width / 2, rect.y + rect.height / 2);
            Point distance = new Point(rectCenter.x - center.x, rectCenter.y - center.y);
            return distance.dot(distance) <= rSq;
        }).collect(Collectors.toList()));

        double confidenceRadius = 0.30 * radius;
        double crSq = confidenceRadius * confidenceRadius;

        List<Rect> confidenceRois = innerRois.stream().filter(rect -> {
             Point rectCenter = new Point(rect.x + rect.width / 2, rect.y + rect.height / 2);
             Point distance = new Point(rectCenter.x - center.x, rectCenter.y - center.y);
             return distance.dot(distance) <= crSq;
         }).collect(Collectors.toList());

        for (Rect i: confidenceRois) {
            Point p = new Point(i.x + i.width / 2, i.y + i.height / 2);
            double r = i.width / 2;
            circle(mask, p, (int) r, new Scalar(GC_FGD), -1);
        }

        if(outputFolder != null) {
            File output = new File(outputFolder, FG);

            Mat maskCopy = mask.clone();
            circle(maskCopy, center, (int) radius, new Scalar(GC_FGD), 4);
            circle(maskCopy, center, (int) confidenceRadius, new Scalar(GC_FGD), 4);

            Imgcodecs.imwrite(output.getAbsolutePath(), maskToColored(maskCopy));
        }
    }

    private void applyDistanceTransform(Mat mask, List<Rect> innerRois, File outputFolder) {
        Mat dt = new Mat(imageSource.rows(), imageSource.cols(), CV_8UC1, new Scalar(255));

        for (Rect i: innerRois) {
            Point p = new Point(i.x + i.width / 2, i.y + i.height / 2);
            double r = Math.sqrt((i.width * i.width + i.height * i.height) / 4);
            circle(dt, p, (int) r, new Scalar(0), -1);
        }

        if(outputFolder != null) {
            File output = new File(outputFolder, INNER);
            Imgcodecs.imwrite(output.getAbsolutePath(), dt);
        }

        distanceTransform(dt, dt, DIST_L2, 3);
        dt.convertTo(dt, CV_8UC1);

        if(outputFolder != null) {
            File output = new File(outputFolder, DISTANCE_TRANSFORM);
            Imgcodecs.imwrite(output.getAbsolutePath(), dt);
        }

        threshold(dt, dt, 12, 255, THRESH_TOZERO_INV);
        threshold(dt, dt, 3, 255, THRESH_BINARY);

        if(outputFolder != null) {
            File output = new File(outputFolder, DT_THRESH);
            Imgcodecs.imwrite(output.getAbsolutePath(), dt);
        }

        mask.setTo(new Scalar(GC_BGD), dt);
    }

    private Mat maskToColored(Mat mask) {
        Mat coloredMask = new Mat(mask.rows(), mask.cols(), CV_8UC3, new Scalar(0, 0, 0));

        for (int i = 0; i < mask.rows(); i++) {
            for (int j = 0; j < mask.cols(); j++) {
                switch ((int) mask.get(i, j)[0]) {
                    case GC_PR_BGD:
                        coloredMask.put(i, j, 255, 0, 0);
                        break;
                    case GC_PR_FGD:
                        coloredMask.put(i, j, 255, 0, 255);
                        break;
                    case GC_FGD:
                        coloredMask.put(i, j, 0, 255, 0);
                        break;
                }
            }
        }

        return coloredMask;
    }
}
