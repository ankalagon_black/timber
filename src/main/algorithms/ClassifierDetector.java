package main.algorithms;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassifierDetector {

    private CascadeClassifier cascadeClassifier = new CascadeClassifier();

    public ClassifierDetector(File cascade) {
        cascadeClassifier.load(cascade.getAbsolutePath());
    }

    public List<Rect> detectRoi(Mat grayScaleImage, double min, double max, int precision) {
        MatOfRect matOfRect = new MatOfRect();

        cascadeClassifier.detectMultiScale(grayScaleImage, matOfRect, 1.1, precision, 0, new Size(min, min), new Size(max, max));

        Rect[] rois = matOfRect.toArray();
        List<Rect> roisList = new ArrayList<>();

        if(rois != null) roisList.addAll(Arrays.asList(rois));

        return roisList;
    }
}
