package main.algorithms;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.core.data_types.settings.Settings;
import main.core.utils.ViewUtils;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.BiConsumer;

import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.imgproc.Imgproc.GC_FGD;
import static org.opencv.imgproc.Imgproc.GC_PR_FGD;
import static org.opencv.imgproc.Imgproc.circle;

public class WoodLogsDetector {
    private Mat imageSource;

    public WoodLogsDetector(Image image) throws UnsatisfiedLinkError, IOException {
        imageSource = ViewUtils.imageToMat(image);
        if(imageSource.channels() == 4) Imgproc.cvtColor(imageSource, imageSource, Imgproc.COLOR_BGRA2BGR);
    }

    public WoodLogsDetector(File imageFile) {
        imageSource = Imgcodecs.imread(imageFile.getAbsolutePath());
        if(imageSource.channels() == 4) Imgproc.cvtColor(imageSource, imageSource, Imgproc.COLOR_BGRA2BGR);
    }


    public List<Rect> performInitialClassification(File cascade, double min, double max, LogicalContour logicalContour) {
        Mat grayScaleImage = new Mat();
        Imgproc.cvtColor(imageSource, grayScaleImage, Imgproc.COLOR_BGR2GRAY);

        ClassifierDetector classifierDetector = new ClassifierDetector(cascade);

        List<Point> convexHull = getConvexHull(logicalContour);
        grayScaleImage = createConvexHullGrayScale(grayScaleImage, convexHull);

        return classifierDetector.detectRoi(grayScaleImage, min ,max, 12);
    }

    public ICResults performInitialClassification(File cascade, double min, double max) {
        Mat grayScaleImage = new Mat();
        Imgproc.cvtColor(imageSource, grayScaleImage, Imgproc.COLOR_BGR2GRAY);

        ClassifierDetector classifierDetector = new ClassifierDetector(cascade);
        List<Rect> result = classifierDetector.detectRoi(grayScaleImage, min, max, 12);
        LogicalContour logicalContour = new LogicalContour(getConvexHull(result));

        return new ICResults(result, logicalContour);
    }


    private Mat createConvexHullGrayScale(Mat grayScaleImage, List<Point> convexHull) {
        Mat mask = new Mat(grayScaleImage.rows(), grayScaleImage.cols(), CV_8UC1, new Scalar(0));

        MatOfPoint matOfPoint = new MatOfPoint();
        matOfPoint.fromList(convexHull);

        List<MatOfPoint> contours = Collections.singletonList(matOfPoint);
        Imgproc.drawContours(mask, contours, 0, new Scalar(255), -1);

        Mat result = new Mat();

        grayScaleImage.copyTo(result, mask);

        Imgcodecs.imwrite("D:\\18\\test_samples\\img.jpg", result);

        return result;
    }

    private List<Point2D> getConvexHull(List<Rect> rois) {
        List<Point> inputList = rois.stream().collect(ArrayList::new, (points, rect) -> {
            points.add(new Point(rect.x, rect.y));
            points.add(new Point(rect.x + rect.width, rect.y));
            points.add(new Point(rect.x, rect.y + rect.height));
            points.add(new Point(rect.x + rect.width, rect.y + rect.height));
        }, ArrayList::addAll);

        MatOfPoint input = new MatOfPoint();
        input.fromList(inputList);

        MatOfInt hull = new MatOfInt();
        Imgproc.convexHull(input, hull);

        List<Point2D> convexHull = new ArrayList<>();

        for (int i = 0; i < hull.rows(); i++) {
            int value = (int) hull.get(i, 0)[0];
            Point p = inputList.get(value);
            convexHull.add(new Point2D(p.x, p.y));
        }

        return convexHull;
    }

    private List<Point> getConvexHull(LogicalContour logicalContour) {
        MatOfPoint input = ViewUtils.getPointsAbs(logicalContour);

        MatOfInt hull = new MatOfInt();
        Imgproc.convexHull(input, hull);

        List<Point> convexHull = new ArrayList<>();
        for (int i = 0; i < hull.rows(); i++) {
            int value = (int) hull.get(i, 0)[0];
            double[] point = input.get(value, 0);
            convexHull.add(new Point(point[0], point[1]));
        }

        return convexHull;
    }


    public Rect detectInArea(File cascade, LogicalCircleMarker marker, double min, double max) {
        int rx = (int) (marker.getX() - (max / 2)), ry = (int) (marker.getY() - (max / 2)), size = (int) max;
        Rect rect = ViewUtils.truncate(new Rect(rx, ry, size, size), new Rect(0,0, imageSource.width(), imageSource.height()));

        Mat subImage = imageSource.submat(rect);

        Mat grayScaleImage = new Mat();
        Imgproc.cvtColor(subImage, grayScaleImage, Imgproc.COLOR_BGR2GRAY);

        ClassifierDetector classifierDetector = new ClassifierDetector(cascade);
        List<Rect> rois = classifierDetector.detectRoi(grayScaleImage, min ,max, 12);

        int centerX = (int) (marker.getX() - rx), centerY = (int) (marker.getY() - ry);

        return rois.stream().min((o1, o2) -> {
            double rcx1 = o1.x + o1.width / 2 - centerX,
                    rcy1 = o1.y + o1.height / 2 - centerY;

            double rcx2 = o2.x + o2.width / 2 - centerX,
                    rcy2 = o2.y + o2.height / 2 - centerY;

            return Double.compare(rcx1 * rcx1 + rcy1 * rcy1, rcx2 * rcx2 + rcy2 * rcy2);
        }).orElse(null);
    }

    public static class ICResults {
        public List<Rect> rois;
        public LogicalContour logicalContour;

        public ICResults(List<Rect> rois, LogicalContour logicalContour) {
            this.rois = rois;
            this.logicalContour = logicalContour;
        }
    }


//    private Rect toRect(LogicalCircleMarker marker) {
//        int x = (int) (marker.getX() - marker.getRadius()), y = (int) (marker.getY() - marker.getRadius());
//        int r2 = (int) (marker.getRadius() * 2);
//        return new Rect(x,y, r2, r2);
//    }
//    private void createRois(Rect[] result, CircleMarkersJunction junction, File outputFolder) {
//        if(result != null) {
//            if(junction != null) {
//                for (Rect i: result) {

//                }
//            }
//
//            if(outputFolder != null) {
//                File output = new File(outputFolder, ROIS);
//
//                Mat mat = imageSource.clone();
//
//                for (Rect i: result) {
//                    Imgproc.rectangle(mat, new Point(i.x, i.y), new Point(i.x + i.width, i.y + i.height), new Scalar(255, 0, 255), 4);
//                }
//
//                Imgcodecs.imwrite(output.getAbsolutePath(), mat);
//            }
//        }
//    }
    //    public void improveClassification(CircleMarkersJunction junction, File outputFolder) {
//        List<Point> convexHull = getConvexHull(junction.getLogicalContour());
//        List<Rect> rois = junction.getMarkers().stream().collect(ArrayList::new, (rects, lcm) -> rects.add(toRect(lcm)), ArrayList::addAll);
//
//        GrabCutAlgorithm grabCutAlgorithm = new GrabCutAlgorithm(imageSource, convexHull, rois);
//        Mat mask = grabCutAlgorithm.segmentImage(outputFolder);
//
//        ListIterator<LogicalCircleMarker> listIterator = junction.getMarkers().listIterator();
//        while (listIterator.hasNext()) {
//            LogicalCircleMarker marker = listIterator.next();
//            Rect rect = toRect(marker);
//
//            Mat subMask = mask.submat(rect);
//
//            double total = subMask.rows() * subMask.cols();
//
//            double positive = 0;
//            for (int i = 0; i < subMask.rows(); i++) {
//                for (int j = 0; j < subMask.cols(); j++) {
//                    switch ((int) subMask.get(i, j)[0]) {
//                        case GC_PR_FGD:
//                        case GC_FGD:
//                            positive += 1;
//                            break;
//                    }
//                }
//            }
//
//            if(positive / total < 0.17) listIterator.remove();
//        }
//
//        if(outputFolder != null) {
//            File output = new File(outputFolder, RESULT);
//
//            Mat results = imageSource.clone();
//
//            for (LogicalCircleMarker i: junction.getMarkers()) {
//                Point p = new Point(i.getX(), i.getY());
//                circle(results, p, (int) i.getRadius(), new Scalar(255, 0, 255), 4);
//            }
//
//            Imgcodecs.imwrite(output.getAbsolutePath(), results);
//        }
//    }
//
//    public void improveClassification(CircleMarkersJunction junction) {
//        improveClassification(junction, null);
//    }
    //        File output = new File(outputFolder, "rois_circle.jpg");
//
//        Mat mat = subImage.clone();
//
//        for (Rect i: rois) {
//            Imgproc.rectangle(mat, new Point(i.x, i.y), new Point(i.x + i.width, i.y + i.height), new Scalar(255, 0, 0), 4);
//        }
//
//        Imgproc.rectangle(mat, new Point(roi.x, roi.y), new Point(roi.x + roi.width, roi.y + roi.height), new Scalar(255, 0, 255), 4);
//        Imgcodecs.imwrite(output.getAbsolutePath(), mat);

    //List<Point> points = getConvexHull(rois);
//        marker.setRadius(ViewUtils.getConvexHullInscribedCircleRadius(ViewUtils.getConvexHullCenter(points), points));
}
