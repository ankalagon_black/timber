package main.algorithms;

import com.google.common.io.Files;
import javafx.geometry.Point2D;
import main.core.data_types.common.Project;
import main.core.data_types.common.Truck;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.core.utils.ViewUtils;
import main.repository.xml.project.ProjectOpener;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.opencv.core.CvType.CV_8UC1;

public class PrepareTrainingData {
    public static final String POSITIVE_DIR = "positive", NEGATIVE_DIR = "negative";
    public static final String INFO = "info.txt", BG = "bg.txt";
    public static final String PROJECT_FILE = "truck.tpf";

    private File source;

    public PrepareTrainingData(File source) {
        this.source = source;
    }

    public void prepare(File samplesFolder) {
        System.load(new File("./libraries/opencv_java341.dll").getAbsolutePath());

        File positiveDir = new File(samplesFolder, POSITIVE_DIR), negativeDir = new File(samplesFolder, NEGATIVE_DIR);
        if(!positiveDir.exists()) positiveDir.mkdir();
        if(!negativeDir.exists()) negativeDir.mkdir();

        File[] files = source.listFiles();
        if(files != null) {
            File info = new File(samplesFolder, INFO);
            File bg = new File(samplesFolder, BG);

            BufferedWriter infoWriter = null, bgWriter = null;
            try {
                infoWriter = new BufferedWriter(new FileWriter(info));
                bgWriter = new BufferedWriter(new FileWriter(bg));
            } catch (IOException e) {
                e.printStackTrace();
            }

            for (File projectFolder: files) {

                File projectFile = new File(projectFolder, PROJECT_FILE);

                ProjectOpener projectOpener = new ProjectOpener(projectFile);
                Project project = new Project();
                try {
                    projectOpener.open(project);
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }

                Truck truck = project.getTruck();

                for (AbstractJunction<LogicalCircleMarker> junction: truck.getJunctions()) {
                    File originalFile = junction.getOriginalFile();

                    if(junction.hasLogicalContour()) {
                        Mat junctionSource = Imgcodecs.imread(originalFile.getAbsolutePath());

                        Mat aabb = junctionSource.submat(getAABB(junction.getLogicalContour()));

                        List<MatOfPoint> contours = Collections.singletonList(ViewUtils.getPoints(junction.getLogicalContour()));

                        Imgproc.drawContours(aabb, contours, 0, new Scalar(0), -1);

                        File negativeModification = new File(negativeDir, originalFile.getName().replace(" ", ""));

                        Imgcodecs.imwrite(negativeModification.getAbsolutePath(), junctionSource);

                        write(bgWriter, negativeModification.getAbsolutePath());
                    }

                    if(junction.getMarkersCount() != 0) {
                        File originalFileCopy = new File(positiveDir, originalFile.getName().replace(" ", ""));

                        try {
                            Files.copy(originalFile, originalFileCopy);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        StringBuilder builder = new StringBuilder(originalFileCopy.getAbsolutePath().replace(samplesFolder.getAbsolutePath(), "") + " " + junction.getMarkersCount() + " ");
                        for (LogicalCircleMarker marker : junction.getMarkers()) {
                            int x = (int) (marker.getX() - marker.getRadius()), y = (int) (marker.getY() - marker.getRadius()),
                                    size = (int) (marker.getRadius() * 2);

                            builder = builder.append(x).append(" ").append(y).append(" ").append(size).append(" ").append(size).append(" ");
                        }

                        write(infoWriter, builder.toString());
                    }
                }

                System.out.println(projectFile.getAbsolutePath() + " is ready");
            }

            try {
                infoWriter.close();
                bgWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void write(BufferedWriter bufferedWriter, String str) {
        try {
            bufferedWriter.write(str + "\r\n");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Rect getAABB(LogicalContour logicalContour) {
        double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE,
                maxX = Double.MIN_VALUE, maxY = Double.MIN_VALUE;

        for (Point2D point2D: logicalContour.getPoints()) {
            double x = point2D.getX(), y = point2D.getY();

            if(x < minX) minX = x;
            if(y < minY) minY = y;

            if(x > maxX) maxX = x;
            if(y > maxY) maxY = y;
        }

        return new Rect((int) logicalContour.getX(), (int) logicalContour.getY(), (int) (maxX - minX), (int) (maxY - minY));
    }
}
