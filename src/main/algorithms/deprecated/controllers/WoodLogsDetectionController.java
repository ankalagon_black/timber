//package main.wood_logs_detection.controllers;
//
//import javafx.collections.FXCollections;
//import javafx.collections.ListChangeListener;
//import javafx.collections.ObservableList;
//import javafx.event.ActionEvent;
//import javafx.fxml.FXMLLoader;
//import javafx.fxml.Initializable;
//import javafx.scene.Node;
//import javafx.scene.control.*;
//import javafx.scene.image.Image;
//import main.core.data_types.settings.LogParameters;
//import main.core.data_types.markers.circle_markers_branch.CircleMarker;
//import main.core.data_types.markers.ruler_branch.LogicalRuler;
//import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
//import main.core.utils.MyUtils;
//import org.opencv.core.Mat;
//
//import java.io.IOException;
//import java.net.URL;
//import java.util.List;
//import java.util.ResourceBundle;
//
//import static main.core.utils.ViewUtils.imageToMat;
//import static main.core.utils.ViewUtils.matToImage;
//import static org.opencv.imgproc.Imgproc.COLOR_BGRA2BGR;
//import static org.opencv.imgproc.Imgproc.cvtColor;
//
//public class WoodLogsDetectionController implements Initializable {
//    private static final int HOUGH_CIRCLES_STEP = 0, GRAB_CUT_STEP = 1, VALIDATION_STEP = 2;
//
//    public DialogPane dialogPane;
//
//    private HoughCirclesController houghCirclesController;
//    private GrabCutController grabCutController;
//
//    private int step;
//
//    private Image image;
//    private LogicalRuler logicalRuler;
//    private LogParameters logParameters;
//
//    private CircleMarkerSettingsWrapper circleMarkerSettings;
//
//    //private ObservableList<JunctionCircleMarker> results = FXCollections.observableArrayList();
//
//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//        Button finish = (Button) dialogPane.lookupButton(MyUtils.FINISH);
//        finish.setDisable(true);
//
////        results.addListener((ListChangeListener<JunctionCircleMarker>) c -> {
////            finish.setDisable(results.isEmpty());
////        });
//
//        finish.addEventFilter(ActionEvent.ACTION, event -> {
//            Alert alert = new Alert(Alert.AlertType.WARNING, "Вы действительно хотите завершить процесс?", MyUtils.YES, MyUtils.NO);
//            alert.setHeaderText("Внимание");
//            alert.setTitle("Подтверждение действия");
//            alert.showAndWait().ifPresent(buttonType -> {
//                if(buttonType.equals(MyUtils.NO)) event.consume();
//            });
//        });
//
//        Button cancel = (Button) dialogPane.lookupButton(MyUtils.CANCEL);
//        cancel.addEventFilter(ActionEvent.ACTION, event -> {
//            Alert alert = new Alert(Alert.AlertType.WARNING, "Вы действительно хотите выйти?", MyUtils.YES, MyUtils.NO);
//            alert.setHeaderText("Внимание");
//            alert.setTitle("Подтверждение действия");
//            alert.showAndWait().ifPresent(buttonType -> {
//                if(buttonType.equals(MyUtils.NO)) event.consume();
//            });
//        });
//
//        Button contin = (Button) dialogPane.lookupButton(MyUtils.CONTINUE);
//        contin.addEventFilter(ActionEvent.ACTION, event -> {
//            switchToTheNextStep();
//            event.consume();
//        });
//    }
//
//    public void setController(Image image, LogicalRuler logicalRuler, LogParameters logParameters, CircleMarkerSettingsWrapper circleMarkerSettings) throws IOException {
//        this.logicalRuler = logicalRuler;
//        this.logParameters = logParameters;
//        this.circleMarkerSettings = circleMarkerSettings;
//
//        Mat img = imageToMat(image);
//        if(img.channels() == 4) cvtColor(img, img, COLOR_BGRA2BGR);
//        this.image = matToImage(img);
//
//        showHoughtCircles(this.image);
//        step = HOUGH_CIRCLES_STEP;
//    }
//
//    private void switchToTheNextStep() {
//        switch (step) {
//            case HOUGH_CIRCLES_STEP:
//                List<CircleMarker> circleMarkers = houghCirclesController.getCandidates();
//                if(circleMarkers.size() > 2) {
//                    showGrabCut(circleMarkers);
//                    step = GRAB_CUT_STEP;
//                    houghCirclesController = null;
//                }
//                else {
//                    Alert alert = new Alert(Alert.AlertType.WARNING, "Для перехода к следующему шагу алгоритм должен определить хотя бы 3 бревна.",
//                            new ButtonType("Ок", ButtonBar.ButtonData.OK_DONE));
//                    alert.setTitle("Предупреждение");
//                    alert.setHeaderText(null);
//                    alert.show();
//                }
//                break;
//            case GRAB_CUT_STEP:
//                Image segmentedImage = grabCutController.getSegmentedImage();
//                //results.addAll(grabCutController.getForegroundMarkers(circleMarkerSettings, logicalRuler, logParameters));
//                showHoughtCircles(segmentedImage);
//                step = VALIDATION_STEP;
//                break;
//            case VALIDATION_STEP:
//                //results.addAll(houghCirclesController.getSummary());
//                houghCirclesController.refresh();
//                break;
//        }
//    }
//
//    private void showHoughtCircles(Image toProcess) {
//        FXMLLoader fxmlLoader = new FXMLLoader();
//        fxmlLoader.setLocation(getClass().getResource("/main/deprecated/resources/hough_circles.fxml"));
//
//        try {
//            Node node = fxmlLoader.load();
//
//            houghCirclesController = fxmlLoader.getController();
//            houghCirclesController.setController(image, toProcess, logicalRuler, logParameters, circleMarkerSettings, results);
//
//            dialogPane.setContent(node);
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void showGrabCut(List<CircleMarker> circleMarkers) {
//        FXMLLoader fxmlLoader = new FXMLLoader();
//        fxmlLoader.setLocation(getClass().getResource("/main/deprecated/resources/grab_cut.fxml"));
//
//        try {
//            Node node = fxmlLoader.load();
//
//            dialogPane.setContent(node);
//
//            grabCutController = fxmlLoader.getController();
//            grabCutController.setController(circleMarkers, image);
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
////    public ObservableList<JunctionCircleMarker> getSummary() {
////        return results;
////    }
//}
