//package main.wood_logs_detection.controllers;
//
//import javafx.collections.FXCollections;
//import javafx.collections.ListChangeListener;
//import javafx.collections.ObservableList;
//import javafx.scene.control.Label;
//import javafx.scene.control.Slider;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import main.core.data_types.settings.LogParameters;
//import main.core.data_types.markers.circle_markers_branch.CircleMarker;
//import main.core.data_types.markers.ruler_branch.LogicalRuler;
//import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
//import main.core.views.MarkerImageView;
//import main.junctions.junction_markers.JunctionCircleMarker;
//import main.algorithms.deprecated.algorithms.CandidatesHoughCircles;
//import main.algorithms.deprecated.algorithms.HoughCircles;
//import main.algorithms.deprecated.algorithms.ValidationHoughCircles;
//
//import java.io.IOException;
//import java.util.List;
//
//public class HoughCirclesController {
//
//    public ImageView imageView;
//
//    public Slider minDist;
//    public Slider param;
//    public MarkerImageView markersImageView;
//    public Label determinedMarkers;
//    public Label newMarkers;
//
//    private HoughCircles houghCircles;
//
//    private int initMinRadius, initMaxRadius;
//    private double initMinDist, initParam;
//
//    public void setController(Image toShow, Image toProcess,
//                              LogicalRuler logicalRuler, LogParameters logParameters, CircleMarkerSettingsWrapper settings,
//                              ObservableList<JunctionCircleMarker> determinedMarkers) throws IOException {
//        if(determinedMarkers.isEmpty()) houghCircles = new CandidatesHoughCircles(toShow, toProcess);
//        else {
//            ObservableList<JunctionCircleMarker> newMarkers = FXCollections.observableArrayList();
//            houghCircles = new ValidationHoughCircles(toShow, toProcess, newMarkers, determinedMarkers, logicalRuler, logParameters, settings);
//
//            //markersImageView.setMarkerFactory(param -> new JunctionCircleMarkerView(param, markersImageView));
//            //markersImageView.setMarkers(newMarkers);
//
//            this.determinedMarkers.setText(String.format("Подтвержденных маркеров: %d", determinedMarkers.size()));
//            determinedMarkers.addListener((ListChangeListener<JunctionCircleMarker>) c -> {
//                boolean isStructural = false;
//                while (c.next()) {
//                    if(c.wasRemoved() || c.wasAdded()) {
//                        isStructural = true;
//                        break;
//                    }
//                }
//
//                if(isStructural) this.determinedMarkers.setText(String.format("Подтвержденных маркеров: %d", determinedMarkers.size()));
//            });
//
//            newMarkers.addListener((ListChangeListener<JunctionCircleMarker>) c -> {
//                boolean isStructural = false;
//                while (c.next()) {
//                    if(c.wasRemoved() || c.wasAdded()) {
//                        isStructural = true;
//                        break;
//                    }
//                }
//
//                if(isStructural) this.newMarkers.setText(String.format("Новых маркеров: %d", newMarkers.size()));
//            });
//        }
//
//        initMinRadius = (int) (logParameters.getMinDiameter().getValue() * logicalRuler.getPixelPerLogical().getValue() / 2);
//        initMaxRadius = (int) (logParameters.getMaxDiameter().getValue() * logicalRuler.getPixelPerLogical().getValue() / 2);
//
//        initMinDist = initMaxRadius - 2 * initMinRadius;
//        initParam = initMaxRadius + 2 * initMinRadius;
//
//        minDist.setMin(initMinRadius * 2);
//        minDist.setMax(initMaxRadius * 2);
//        minDist.setValue(initMinDist);
//
//        param.setMin(initMinRadius * 2);
//        param.setMax(initMaxRadius * 2);
//        param.setValue(initParam);
//
//        minDist.valueProperty().addListener((observable, oldValue, newValue) -> {
//            if(!minDist.isValueChanging()) {
//                imageView.setImage(houghCircles.run(newValue.doubleValue(), param.getValue(), param.getValue() / 2, initMinRadius, initMaxRadius));
//            }
//        });
//        param.valueProperty().addListener((observable, oldValue, newValue) -> {
//            if(!param.isValueChanging()) {
//                imageView.setImage(houghCircles.run(minDist.getValue(), newValue.doubleValue(), newValue.doubleValue() / 2, initMinRadius, initMaxRadius));
//            }
//        });
//
//        imageView.setImage(houghCircles.run(initMinDist, initParam, initParam / 2, initMinRadius, initMaxRadius));
//    }
//
//    public List<CircleMarker> getCandidates() {
//        return ((CandidatesHoughCircles) houghCircles).getSummary();
//    }
//
//    public List<JunctionCircleMarker> getSummary() {
//        return ((ValidationHoughCircles) houghCircles).getSummary();
//    }
//
//    public void refresh() {
//        ((ValidationHoughCircles) houghCircles).refresh();
//        imageView.setImage(houghCircles.run(initMinDist, initParam, initParam / 2, initMinRadius, initMaxRadius));
//    }
//}
