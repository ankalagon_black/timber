//package main.wood_logs_detection.controllers;
//
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import main.core.data_types.settings.LogParameters;
//import main.core.data_types.markers.ruler_branch.LogicalRuler;
//import main.core.views.MarkerImageView;
//import org.opencv.core.*;
//import main.algorithms.deprecated.data_types.GrabMaskCircleMarker;
//
//import java.io.IOException;
//import java.util.List;
//
//import static main.core.utils.ViewUtils.imageToMat;
//import static main.core.utils.ViewUtils.matToImage;
//import static org.opencv.core.CvType.CV_8UC3;
//import static org.opencv.imgproc.Imgproc.*;
//
//public class ValidationController {
//    public MarkerImageView markerImageView;
//    public ImageView imageView;
//
//    private Mat mask;
//
//    public void setController(Image image, Mat mask, List<GrabMaskCircleMarker> circleMarkers, LogicalRuler logicalRuler, LogParameters logParameters) {
//        imageView.setImage(image);
//
//        this.mask = mask;
//
//        //markerImageView.setMarkerFactory(param -> new JunctionCircleMarkerView(param, markerImageView));
//        //markerImageView.setMarkers(FXCollections.observableArrayList());
//
////        for (CircleMarker i: circleMarkers) {
////            markerImageView.getMarkers().add(new JunctionCircleMarker(i.getX(), i.getY(), i.getRadius(), logicalRuler, logParameters, -1,
////                    new CircleMarkerSettingsWrapper()));
////        }
//    }
//
//    private Image validatedImage;
//
//    public void validate() {
//        try {
//            Mat original = imageToMat(imageView.getImage());
//
//            Mat imageMask = new Mat(mask.rows(), mask.cols(), CV_8UC3, new Scalar(0, 0, 0));
//
//            for (int i = 0; i < mask.rows(); i++) {
//                for (int j = 0; j < mask.cols(); j++) {
//
//                    switch ((int) mask.get(i, j)[0]) {
//                        case GC_BGD:
//                        case GC_PR_BGD:
//                        case GC_FGD:
//                            imageMask.put(i, j, 255, 255, 255);
//                            break;
//                    }
//                }
//            }
//
////            for (JunctionCircleMarker i: markerImageView.getMarkers()) {
////                Imgproc.circle(imageMask, new Point(i.getX(), i.getY()), (int) (i.getRadius() + i.getRadius() * 0.2), new Scalar(255, 255, 255), -1);
////            }
//
//            Core.subtract(original, imageMask, original);
//
//            validatedImage = matToImage(original);
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public Image getValidatedImage() {
//        return validatedImage;
//    }
//
////    public ObservableList<JunctionCircleMarker> getCircleMarkers() {
////        return markerImageView.getList("da");
////    }
//}
