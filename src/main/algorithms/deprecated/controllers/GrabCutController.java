//package main.wood_logs_detection.controllers;
//
//import javafx.event.ActionEvent;
//import javafx.scene.control.Button;
//import javafx.scene.control.Slider;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import main.core.data_types.markers.circle_markers_branch.CircleMarker;
//import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
//import main.core.views.MarkerImageView;
//import main.algorithms.deprecated.algorithms.GrabCut;
//
//import java.io.IOException;
//import java.util.List;
//
//public class GrabCutController {
//    public ImageView imageView;
//    public MarkerImageView markerImageView;
//
//    public Button confirm;
//    public Slider slider;
//    public Slider complexity;
//
//    private GrabCut grabCut;
//    private CircleMarkerSettingsWrapper settings;
//
//    public void setController(List<CircleMarker> circleMarkers, Image image) throws IOException {
//        grabCut = new GrabCut(circleMarkers, image);
////        settings = new CircleMarkerSettingsWrapper();
////        settings.setColor(Color.GREEN);
//
//        grabCut.prepareScene(settings);
//
//        imageView.setImage(grabCut.getPreparedScene());
//
//        //markerImageView.setMarkerFactory(param -> new RangeCircleMarkerView(param, markerImageView));
//        //markerImageView.setMarkers(grabCut.getForegroundMarkers());
//
//        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
//            if(!slider.isValueChanging()) {
//                imageView.setImage(grabCut.applyDistanceTransform(slider.getValue()));
//            };
//        });
//    }
//
//    public Image getSegmentedImage() {
//        return grabCut.run((int) complexity.getValue());
//    }
//
////    public List<JunctionCircleMarker> getForegroundMarkers(CircleMarkerSettingsWrapper circleMarkerSettings, LogicalRuler logicalRuler, LogParameters logParameters) {
////        return grabCut.getForegroundMarkers().stream().collect(ArrayList::new, (junctionCircleMarkers, i) -> {
////            junctionCircleMarkers.add(new JunctionCircleMarker(i.getX(), i.getY(), i.getRadius(), logicalRuler, logParameters, -1, circleMarkerSettings));
////        }, ArrayList::addAll);
////    }
//
//
//    public void onConfirmPositions(ActionEvent actionEvent) {
//        if(slider.isDisabled()) {
//            confirm.setText("Вернуться к позиционированию");
//            slider.setDisable(false);
//            settings.setEnabled(false);
//
//            imageView.setImage(grabCut.applyDistanceTransform(slider.getValue()));
//        }
//        else {
//            confirm.setText("Подтвердить позиции");
//            slider.setDisable(true);
//            settings.setEnabled(true);
//
//            imageView.setImage(grabCut.getPreparedScene());
//        }
//    }
//}
