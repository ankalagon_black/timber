//package main.wood_logs_detection.data_types;
//
//import javafx.beans.property.IntegerProperty;
//import javafx.beans.property.SimpleIntegerProperty;
//import main.core.data_types.markers.circle_markers_branch.CircleMarker;
//
//import static org.opencv.imgproc.Imgproc.GC_PR_BGD;
//import static org.opencv.imgproc.Imgproc.GC_PR_FGD;
//
//public class GrabMaskCircleMarker extends CircleMarker {
//
//    public GrabMaskCircleMarker(double x, double y, double radius) {
//        super(x, y, radius);
//        setMaskValue(GC_PR_BGD);
//    }
//
//    public GrabMaskCircleMarker(CircleMarker circleMarker) {
//        super(circleMarker);
//        setMaskValue(GC_PR_BGD);
//    }
//
//
//    private IntegerProperty maskValue = new SimpleIntegerProperty();
//
//    public int getMaskValue() {
//        return maskValue.get();
//    }
//
//    public IntegerProperty maskValueProperty() {
//        return maskValue;
//    }
//
//    public void setMaskValue(int maskValue) {
//        this.maskValue.set(maskValue);
//    }
//}
