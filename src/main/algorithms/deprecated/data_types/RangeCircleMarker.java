//package main.wood_logs_detection.data_types;
//
//import javafx.beans.property.DoubleProperty;
//import javafx.beans.property.ObjectProperty;
//import javafx.beans.property.SimpleDoubleProperty;
//import javafx.beans.property.SimpleObjectProperty;
//import javafx.geometry.Point2D;
//import main.core.data_types.markers.circle_markers_branch.CircleMarker;
//import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
//
//public class RangeCircleMarker extends CircleMarker {
//
//    public RangeCircleMarker(CircleMarker circleMarker, Point2D rangeCenter, double range, CircleMarkerSettingsWrapper settings) {
//        super(circleMarker);
//        setRangeCenter(rangeCenter);
//        setRange(range);
//        setSettings(settings);
//    }
//
//
//    private ObjectProperty<Point2D> rangeCenter = new SimpleObjectProperty<>();
//
//    public Point2D getRangeCenter() {
//        return rangeCenter.get();
//    }
//
//    public ObjectProperty<Point2D> rangeCenterProperty() {
//        return rangeCenter;
//    }
//
//    public void setRangeCenter(Point2D rangeCenter) {
//        this.rangeCenter.set(rangeCenter);
//    }
//
//
//    private DoubleProperty range = new SimpleDoubleProperty();
//
//    public double getRange() {
//        return range.get();
//    }
//
//    public DoubleProperty rangeProperty() {
//        return range;
//    }
//
//    public void setRange(double range) {
//        this.range.set(range);
//    }
//
//
//    private ObjectProperty<CircleMarkerSettingsWrapper> settings = new SimpleObjectProperty<>();
//
//    public CircleMarkerSettingsWrapper getSettings() {
//        return settings.get();
//    }
//
//    public ObjectProperty<CircleMarkerSettingsWrapper> settingsProperty() {
//        return settings;
//    }
//
//    public void setSettings(CircleMarkerSettingsWrapper settings) {
//        this.settings.set(settings);
//    }
//}
