//package main.wood_logs_detection.views;
//
//import javafx.scene.control.Skin;
//import main.core.views.MarkerImageView;
//import main.core.views.marker_views.AbstractCircleMarkerView;
//import main.algorithms.deprecated.data_types.RangeCircleMarker;
//import main.algorithms.deprecated.views.skins.RangeCircleMarkerViewSkin;
//
//public class RangeCircleMarkerView extends AbstractCircleMarkerView<RangeCircleMarker> {
//
//    public RangeCircleMarkerView(RangeCircleMarker marker, MarkerImageView markerImageView) {
//        super(marker, markerImageView);
//    }
//
//    @Override
//    protected Skin<?> createDefaultSkin() {
//        return new RangeCircleMarkerViewSkin(this);
//    }
//}
