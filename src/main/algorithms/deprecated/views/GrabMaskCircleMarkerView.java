//package main.wood_logs_detection.views;
//
//import javafx.scene.control.Skin;
//import main.core.views.MarkerImageView;
//import main.core.views.marker_views.AbstractCircleMarkerView;
//import main.algorithms.deprecated.data_types.GrabMaskCircleMarker;
//import main.algorithms.deprecated.views.skins.GrabMaskCircleMarkerViewSkin;
//
//public class GrabMaskCircleMarkerView extends AbstractCircleMarkerView<GrabMaskCircleMarker> {
//
//    public GrabMaskCircleMarkerView(GrabMaskCircleMarker marker, MarkerImageView markerImageView) {
//        super(marker, markerImageView);
//    }
//
//    @Override
//    protected Skin<?> createDefaultSkin() {
//        return new GrabMaskCircleMarkerViewSkin(this);
//    }
//}
