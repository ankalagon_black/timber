//package main.wood_logs_detection.views.skins;
//
//import javafx.geometry.Point2D;
//import javafx.scene.control.ContextMenu;
//import javafx.scene.control.MenuItem;
//import javafx.scene.input.MouseEvent;
//import javafx.scene.paint.Color;
//import main.core.views.skins.marker_views.CircleMarkerViewSkin;
//import main.algorithms.deprecated.views.GrabMaskCircleMarkerView;
//import main.algorithms.deprecated.views.behaviors.GrabMaskCircleMarkerViewBehavior;
//
//import static org.opencv.imgproc.Imgproc.*;
//
//public class GrabMaskCircleMarkerViewSkin extends CircleMarkerViewSkin<GrabMaskCircleMarkerView, GrabMaskCircleMarkerViewBehavior> {
//    private static final String MASK_VALUE = "MASK_VALUE";
//
//    private ContextMenu contextMenu = null;
//
//    public GrabMaskCircleMarkerViewSkin(GrabMaskCircleMarkerView control) {
//        super(control, new GrabMaskCircleMarkerViewBehavior(control));
//
//        getCircle().setStrokeWidth(2);
//        getCircle().setFill(Color.TRANSPARENT);
//
//        setMaskValue(getSkinnable().getMarker().getMaskValue());
//
//        registerChangeListener(getSkinnable().getMarker().maskValueProperty(), MASK_VALUE);
//    }
//
//    @Override
//    protected void handleControlPropertyChanged(String propertyReference) {
//        super.handleControlPropertyChanged(propertyReference);
//        switch (propertyReference) {
//            case MASK_VALUE:
//                setMaskValue(getSkinnable().getMarker().getMaskValue());
//                break;
//        }
//    }
//
//    private void setMaskValue(int maskValue) {
//        switch (maskValue) {
//            case GC_FGD:
//                getCircle().setStroke(Color.GREEN);
//                break;
//            case GC_PR_FGD:
//                getCircle().setStroke(Color.RED);
//                break;
//            case GC_BGD:
//                getCircle().setStroke(Color.BLUE);
//                break;
//        }
//    }
//
//    public void hideContextMenu() {
//        if(this.contextMenu != null) contextMenu.hide();
//    }
//
//    public void onSecondaryButtonClicked(MouseEvent mouseEvent) {
//        ContextMenu contextMenu;
//
//        switch (getSkinnable().getMarker().getMaskValue()) {
//            case GC_FGD:
//            case GC_BGD:
//                MenuItem removeMark = new MenuItem();
//                removeMark.setText("Убрать отметку");
//                removeMark.setOnAction(event -> {
//                    getSkinnable().getMarker().setMaskValue(GC_PR_FGD);
//                });
//
//                contextMenu = new ContextMenu(removeMark);
//                break;
//            default:
//                MenuItem markAsFGD = new MenuItem(), markAsBGD = new MenuItem();
//
//                markAsFGD.setText("Отметить как правильное");
//                markAsFGD.setOnAction(event -> getSkinnable().getMarker().setMaskValue(GC_FGD));
//
//                markAsBGD.setText("Отметить как неправильное");
//                markAsBGD.setOnAction(event -> getSkinnable().getMarker().setMaskValue(GC_BGD));
//
//                contextMenu = new ContextMenu(markAsFGD, markAsBGD);
//                break;
//        }
//
//        contextMenu.show(getSkinnable(), mouseEvent.getScreenX(), mouseEvent.getScreenY());
//        this.contextMenu = contextMenu;
//    }
//
//}
