//package main.wood_logs_detection.views.skins;
//
//import javafx.geometry.Point2D;
//import javafx.scene.Cursor;
//import javafx.scene.control.ContextMenu;
//import javafx.scene.control.MenuItem;
//import javafx.scene.input.MouseEvent;
//import javafx.scene.paint.Color;
//import main.core.data_types.markers.Marker;
//import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
//import main.core.views.skins.marker_views.CircleMarkerViewSkin;
//import main.algorithms.deprecated.data_types.RangeCircleMarker;
//import main.algorithms.deprecated.views.RangeCircleMarkerView;
//import main.algorithms.deprecated.views.behaviors.RangeCircleMarkerBehavior;
//
//import static main.core.utils.ViewUtils.eventToPoint;
//
//public class RangeCircleMarkerViewSkin extends CircleMarkerViewSkin<RangeCircleMarkerView, RangeCircleMarkerBehavior<RangeCircleMarkerView>> {
//    private final String CIRCLE_MARKER_SETTINGS = "CIRCLE_MARKER_SETTINGS";
//    private final String COLOR = "COLOR";
//    private final String ENABLED = "ENABLED";
//    private final String VISIBLE  ="VISIBLE";
//
//    private Point2D rangeCenter;
//
//    private ContextMenu contextMenu;
//
//    public RangeCircleMarkerViewSkin(RangeCircleMarkerView control) {
//        super(control, new RangeCircleMarkerBehavior<>(control));
//
//        getStackPane().setCursor(Cursor.OPEN_HAND);
//
//        getCircle().setStrokeWidth(2);
//        getCircle().setFill(Color.TRANSPARENT);
//
//        RangeCircleMarker rangeCircleMarker = getSkinnable().getMarker();
//        CircleMarkerSettingsWrapper settings = rangeCircleMarker.getMarkerSettings();
//
//        setColor(settings.getColor());
//        setEnabled(settings.isEnabled());
//        setVisible(settings.isVisible());
//
//        registerChangeListener(rangeCircleMarker.settingsWrapperProperty(), CIRCLE_MARKER_SETTINGS);
//
//        registerChangeListener(settings.colorProperty(), COLOR);
//        registerChangeListener(settings.enabledProperty(), ENABLED);
//        registerChangeListener(settings.visibleProperty(), VISIBLE);
//    }
//
//
//    @Override
//    protected void handleControlPropertyChanged(String propertyReference) {
//        super.handleControlPropertyChanged(propertyReference);
//        switch (propertyReference) {
//            case CIRCLE_MARKER_SETTINGS:
//                CircleMarkerSettingsWrapper settings = getSkinnable().getMarker().getMarkerSettings();
//
//                setColor(settings.getColor());
//                setEnabled(settings.isEnabled());
//                setVisible(settings.isVisible());
//
//                registerChangeListener(settings.colorProperty(), COLOR);
//                registerChangeListener(settings.enabledProperty(), ENABLED);
//                registerChangeListener(settings.visibleProperty(), VISIBLE);
//                break;
//            case COLOR:
//                setColor(getSkinnable().getMarker().getMarkerSettings().getColor());
//                break;
//            case ENABLED:
//                setEnabled(getSkinnable().getMarker().getMarkerSettings().isEnabled());
//                break;
//            case VISIBLE:
//                setVisible(getSkinnable().getMarker().getMarkerSettings().isVisible());
//                break;
//        }
//    }
//
//    private void setColor(Color color) {
//        getCircle().setStroke(color);
//    }
//
//    private void setEnabled(boolean enabled) {
//        getSkinnable().setDisable(!enabled);
//    }
//
//    private void setVisible(boolean visible) {
//        getSkinnable().setVisible(visible);
//    }
//
//
//    @Override
//    protected void updatePosition() {
//        super.updatePosition();
//        rangeCenter = getSkinnable().getMarker().getRangeCenter().multiply(getImageViewScale().getScaledToActualRatio());
//    }
//
//    @Override
//    public void onMoved(MouseEvent mouseEvent, Point2D difference) {
//        xCurrent += difference.getX();
//        yCurrent += difference.getY();
//
//        Point2D radiusVector = new Point2D(xCurrent, yCurrent).subtract(rangeCenter);
//
//        double length = radiusVector.magnitude();
//
//        if(length > getSkinnable().getMarker().getRange()) {
//            double penetration = length - getSkinnable().getMarker().getRange();
//            Point2D correction = radiusVector.multiply(1 / length).multiply(-penetration);
//
//            xCurrent += correction.getX();
//            yCurrent += correction.getY();
//        }
//
//        Point2D offset = getCircleCenterOffsetFromStackPane();
//
//        getNode().setLayoutX(xCurrent - offset.getX());
//        getNode().setLayoutY(yCurrent - offset.getY());
//
//        Marker marker = getSkinnable().getMarker();
//
//        marker.setX(xCurrent * getImageViewScale().getActualToScaledRatio());
//        marker.setY(yCurrent * getImageViewScale().getActualToScaledRatio());
//    }
//
//
//    public void onMouseMovedInResizeRange(MouseEvent mouseEvent) {
//        Point2D point2D = eventToPoint(mouseEvent).subtract(getSkinnable().getWidth() / 2, getSkinnable().getHeight() / 2);
//
//        double degree = Math.toDegrees(Math.atan2(-point2D.getY(), point2D.getX()));
//
//        if(22.5 >= degree && degree > -22.5) getStackPane().setCursor(Cursor.E_RESIZE);
//        else if(67.5 >= degree && degree > 22.5) getStackPane().setCursor(Cursor.NE_RESIZE);
//        else if(112.5 >= degree && degree > 67.5) getStackPane().setCursor(Cursor.N_RESIZE);
//        else if(157.5 >= degree && degree > 112.5) getStackPane().setCursor(Cursor.NW_RESIZE);
//        else if((180 >= degree && degree > 157.5) || (-180 <= degree && degree < -157.5)) getStackPane().setCursor(Cursor.W_RESIZE);
//        else if(-157.5 <= degree && degree < -112.5) getStackPane().setCursor(Cursor.SW_RESIZE);
//        else if(-112.5 <= degree && degree < -67.5) getStackPane().setCursor(Cursor.S_RESIZE);
//        else if(-67.5 <= degree && degree <= -22.5) getStackPane().setCursor(Cursor.SE_RESIZE);
//    }
//
//    public void onMouseMovedOutsideMovementRange(MouseEvent mouseEvent) {
//        if(!getStackPane().getCursor().equals(Cursor.OPEN_HAND)) getStackPane().setCursor(Cursor.OPEN_HAND);
//    }
//
//    public void hideContextMenu() {
//        if(this.contextMenu != null) contextMenu.hide();
//    }
//
//    public void onSecondaryButtonClicked(MouseEvent mouseEvent) {
//        MenuItem remove = new MenuItem("Удалить");
//        remove.setOnAction(event -> {
//            //getSkinnable().getMarkerImageView().getMarkers().remove(getSkinnable().getMarker());
//        });
//
//        ContextMenu contextMenu = new ContextMenu(remove);
//
//        contextMenu.show(getSkinnable(), mouseEvent.getScreenX(), mouseEvent.getScreenY());
//        this.contextMenu = contextMenu;
//    }
//}
