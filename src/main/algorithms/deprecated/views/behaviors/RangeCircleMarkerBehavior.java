//package main.wood_logs_detection.views.behaviors;
//
//import javafx.event.EventHandler;
//import javafx.scene.input.MouseButton;
//import javafx.scene.input.MouseEvent;
//import main.core.views.behaviors.marker_views.CircleMarkerViewBehavior;
//import main.junctions.views.custom.markers.skins.JunctionCircleMarkerViewSkin;
//import main.algorithms.deprecated.views.RangeCircleMarkerView;
//import main.algorithms.deprecated.views.skins.RangeCircleMarkerViewSkin;
//
//import java.util.ArrayList;
//
//public class RangeCircleMarkerBehavior<C extends RangeCircleMarkerView> extends CircleMarkerViewBehavior<C> {
//
//    private final EventHandler<MouseEvent> mouseMovedHandler = event -> {
//        if(!isInMove() && !isInResize() && getSkin().isInMovementRange(event)) {
//            if(getSkin().isInResizeRange(event)) {
//                getSkin().onMouseMovedInResizeRange(event);
//                return;
//            }
//        }
//
//        getSkin().onMouseMovedOutsideMovementRange(event);
//    };
//
//    private EventHandler<MouseEvent> mouseEventEventHandler = event -> {
//        getSkin().hideContextMenu();
//
//        if(event.getButton().equals(MouseButton.SECONDARY)) {
//            getSkin().onSecondaryButtonClicked(event);
//        }
//    };
//
//    public RangeCircleMarkerBehavior(C control) {
//        super(control, new ArrayList<>());
//        control.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedHandler);
//        control.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEventEventHandler);
//    }
//
//    @Override
//    public void dispose() {
//        super.dispose();
//        getControl().removeEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedHandler);
//        getControl().removeEventHandler(MouseEvent.MOUSE_CLICKED, mouseEventEventHandler);
//    }
//
//    private RangeCircleMarkerViewSkin getSkin() {
//        return ((RangeCircleMarkerViewSkin) getControl().getSkin());
//    }
//}
