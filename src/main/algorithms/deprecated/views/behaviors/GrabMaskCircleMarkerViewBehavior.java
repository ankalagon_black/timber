//package main.wood_logs_detection.views.behaviors;
//
//import javafx.event.EventHandler;
//import javafx.scene.input.MouseButton;
//import javafx.scene.input.MouseEvent;
//import main.core.views.behaviors.marker_views.CircleMarkerViewBehavior;
//import main.algorithms.deprecated.views.GrabMaskCircleMarkerView;
//import main.algorithms.deprecated.views.skins.GrabMaskCircleMarkerViewSkin;
//
//import java.util.ArrayList;
//
//public class GrabMaskCircleMarkerViewBehavior extends CircleMarkerViewBehavior<GrabMaskCircleMarkerView> {
//
//    private EventHandler<MouseEvent> mouseEventEventHandler = event -> {
//        ((GrabMaskCircleMarkerViewSkin) getControl().getSkin()).hideContextMenu();
//
//        if(event.getButton().equals(MouseButton.SECONDARY)) {
//            ((GrabMaskCircleMarkerViewSkin) getControl().getSkin()).onSecondaryButtonClicked(event);
//        }
//    };
//
//    public GrabMaskCircleMarkerViewBehavior(GrabMaskCircleMarkerView control) {
//        super(control, new ArrayList<>());
//        control.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEventEventHandler);
//    }
//
//    @Override
//    public void dispose() {
//        super.dispose();
//        getControl().removeEventHandler(MouseEvent.MOUSE_CLICKED, mouseEventEventHandler);
//    }
//
//    @Override
//    protected boolean isMovementAndResizeAllowed() {
//        return false;
//    }
//}
