//package main.wood_logs_detection.algorithms;
//
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.geometry.Point2D;
//import javafx.scene.image.Image;
//import main.core.data_types.markers.circle_markers_branch.CircleMarker;
//import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
//import org.opencv.core.*;
//import main.algorithms.deprecated.data_types.GrabMaskCircleMarker;
//import main.algorithms.deprecated.data_types.RangeCircleMarker;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//import java.util.ListIterator;
//
//import static main.core.utils.ViewUtils.imageToMat;
//import static main.core.utils.ViewUtils.matToImage;
//import static org.opencv.core.CvType.CV_8UC1;
//import static org.opencv.core.CvType.CV_8UC3;
//import static org.opencv.imgproc.Imgproc.*;
//
//public class GrabCut {
//    private static final double INNER_CIRCLE_COEFF = 0.25;
//    private static final int CIRCLES_OFFSET = 0;
//
//    private List<CircleMarker> candidates;
//    private Mat original;
//
//    public GrabCut(List<CircleMarker> candidates, Image image) throws IOException {
//        this.candidates = candidates;
//        original = imageToMat(image);
//    }
//
//
//    private Point center = new Point(0,0);
//    private double radius = 0;
//    private ObservableList<RangeCircleMarker> foregroundMarkers = FXCollections.observableArrayList();
//    private List<GrabMaskCircleMarker> otherMarkers = new ArrayList<>();
//
//    private List<Point> convexHull;
//    private Image preparedScene;
//    private Mat preparedSceneMat;
//
//    public ObservableList<RangeCircleMarker> getForegroundMarkers() {
//        return foregroundMarkers;
//    }
//
//    public Image getPreparedScene() {
//        return preparedScene;
//    }
//
//
//    public void prepareScene(CircleMarkerSettingsWrapper circleMarkerExtendedSettings) {
//        convexHull = getConvexHull();
//        otherMarkers = getMaskMarkers(convexHull);
//
//        Mat scene = new Mat();
//        original.copyTo(scene);
//
//        MatOfPoint matOfPoint = new MatOfPoint();
//        matOfPoint.fromList(convexHull);
//        drawContours(scene, Collections.singletonList(matOfPoint), 0, new Scalar(0, 0 ,255), 4);
//
//        circle(scene, center, (int) (radius * INNER_CIRCLE_COEFF), new Scalar(0, 255, 0), 4);
//        circle(scene, center, (int) radius, new Scalar(255, 255, 255), 4);
//
//        ListIterator<GrabMaskCircleMarker> iterator = otherMarkers.listIterator();
//
//        while (iterator.hasNext()) {
//            GrabMaskCircleMarker i = iterator.next();
//
//            switch (i.getMaskValue()) {
//                case GC_FGD:
//                    foregroundMarkers.add(new RangeCircleMarker(i, new Point2D(center.x, center.y), radius, circleMarkerExtendedSettings));
//                    iterator.remove();
//                    break;
//                case GC_PR_FGD:
//                    circle(scene, new Point(i.getX(), i.getY()), (int) i.getRadius(), new Scalar(255, 255, 255), 2);
//                    break;
//                case GC_PR_BGD:
//                    circle(scene, new Point(i.getX(), i.getY()), (int) i.getRadius(), new Scalar(0, 0, 255), 2);
//                    break;
//            }
//        }
//
//        preparedSceneMat = scene;
//        preparedScene = matToImage(scene);
//    }
//
//    private List<GrabMaskCircleMarker> getMaskMarkers(List<Point> convexHull) {
//        center = getCenterPoint(convexHull);
//        radius = findMinInCircleRadius(convexHull, center);
//
//        double fgBorder = radius * INNER_CIRCLE_COEFF;
//
//        double radiusSq = radius * radius,
//                fgBorderSq = fgBorder * fgBorder;
//
//        return candidates.stream().collect(ArrayList::new, (grabMaskCircleMarkers1, circleMarker) -> {
//            GrabMaskCircleMarker grabMaskCircleMarker = new GrabMaskCircleMarker(circleMarker);
//
//            double x = circleMarker.getX() - center.x, y = circleMarker.getY() - center.y, distSq = x * x + y * y;
//            if(distSq < radiusSq) {
//                if(distSq < fgBorderSq) grabMaskCircleMarker.setMaskValue(GC_FGD);
//                else grabMaskCircleMarker.setMaskValue(GC_PR_FGD);
//            }
//
//            grabMaskCircleMarkers1.add(grabMaskCircleMarker);
//        }, ArrayList::addAll);
//    }
//
//    private List<Point> getConvexHull() {
//        MatOfPoint input = new MatOfPoint();
//        input.fromList(candidates.stream().collect(ArrayList::new, (points, circleMarker) ->
//                points.add(new Point(circleMarker.getX(), circleMarker.getY())), ArrayList::addAll));
//
//        MatOfInt hull = new MatOfInt();
//        convexHull(input, hull);
//
//        List<Point> points = new ArrayList<>();
//
//        for (int i = 0; i < hull.rows(); i++) {
//            int value = (int) hull.get(i, 0)[0];
//            CircleMarker circleMarker = candidates.get(value);
//            points.add(new Point(circleMarker.getX(), circleMarker.getY()));
//        }
//
//        return points;
//    }
//
//    private Point getCenterPoint(List<Point> convexHull) {
//        Point innerPoint = new Point();
//        for (Point i: convexHull) {
//            innerPoint.x += i.x;
//            innerPoint.y += i.y;
//        }
//
//        int size = convexHull.size();
//
//        innerPoint.x /= (double) size;
//        innerPoint.y /= (double) size;
//
//        Point center = new Point();
//        double totalS = 0;
//
//        for (int i = 0; i < size; i++) {
//            Point a = convexHull.get(i),
//                    b = convexHull.get((i + 1) % size);
//            double square = Math.abs((b.y - innerPoint.y) * (a.x - innerPoint.x) - (a.y - innerPoint.y) * (b.x - innerPoint.x)) / 2;
//
//            center.x += square * (innerPoint.x + a.x + b.x) / 3;
//            center.y += square * (innerPoint.y + a.y + b.y) / 3;
//
//            totalS += square;
//        }
//
//        center.x /= totalS;
//        center.y /= totalS;
//
//        return center;
//    }
//
//    private double findMinInCircleRadius(List<Point> convexHull, Point center) {
//        int size = convexHull.size();
//        double min = Double.MAX_VALUE;
//        for (int i = 0; i < size; i++) {
//            Point a = convexHull.get(i),
//                    b = convexHull.get((i + 1) % size);
//            Point norm = new Point(b.y - a.y, -b.x + a.x);
//            double length = Math.sqrt(norm.dot(norm));
//            norm.x /= length;
//            norm.y /= length;
//
//            Point vector = new Point(a.x - center.x, a.y - center.y);
//            double radius = vector.dot(norm);
//            if(radius < min) min = radius;
//        }
//
//        return min;
//    }
//
//
//    private Mat gaps;
//
//    public Image applyDistanceTransform(double threshold) {
//        gaps = new Mat(original.rows(), original.cols(), CV_8UC1, new Scalar(255));
//
//        for (RangeCircleMarker i: foregroundMarkers)
//            circle(gaps, new Point(i.getX(), i.getY()), (int) i.getRadius() + CIRCLES_OFFSET, new Scalar(0), -1);
//
//        distanceTransform(gaps, gaps, DIST_L2, 3);
//
//        gaps.convertTo(gaps, CV_8UC1);
//
//        threshold(gaps, gaps, threshold, 255, THRESH_TOZERO_INV);
//
//        Mat gapsRGBA = new Mat(original.rows(), original.cols(), CV_8UC3);
//        gapsRGBA.setTo(new Scalar(255,0,255), gaps);
//
//        Core.addWeighted(preparedSceneMat, 1, gapsRGBA, 0.2, 0, gapsRGBA);
//
//        return matToImage(gapsRGBA);
//    }
//
//    private Mat getMask(List<Point> convexHull) {
//        Mat mask = new Mat(original.rows(), original.cols(), CV_8UC1, new Scalar(GC_BGD));
//
//        MatOfPoint matOfPoint = new MatOfPoint();
//        matOfPoint.fromList(convexHull);
//        drawContours(mask, Collections.singletonList(matOfPoint), 0, new Scalar(GC_PR_BGD), -1);
//
//        for (GrabMaskCircleMarker i: otherMarkers)
//            circle(mask, new Point(i.getX(), i.getY()), (int) i.getRadius(), new Scalar(i.getMaskValue()), -1);
//
//        for (RangeCircleMarker i: foregroundMarkers)
//            circle(mask, new Point(i.getX(), i.getY()), (int) i.getRadius(), new Scalar(GC_FGD), -1);
//
//        return mask;
//    }
//
//
//    public Image run(int iterCount) {
//        Mat mask = getMask(convexHull);
//
//        mask.setTo(new Scalar(GC_BGD), gaps);
//
//        grabCut(original, mask, new Rect(), new Mat(), new Mat(), iterCount, GC_INIT_WITH_MASK );
//
//        Mat imageMask = new Mat(mask.rows(), mask.cols(), CV_8UC3, new Scalar(255, 255, 255));
//
//        for (int i = 0; i < mask.rows(); i++) {
//            for (int j = 0; j < mask.cols(); j++) {
//                switch ((int) mask.get(i, j)[0]) {
//                    case GC_PR_FGD:
//                        imageMask.put(i, j, 0, 0, 0);
//                        break;
//                }
//            }
//        }
//
//        return matToImage(original.setTo(new Scalar(0,0,0), imageMask));
//    }
//}
//
////        try {
////            Mat original = imageToMat(imageView.getImage());
////            resultMask = new Mat(original.rows(), original.cols(), CV_8UC1, new Scalar(GC_PR_BGD));
////
////            for (GrabMaskCircleMarker i: markerImageView.getNewMarkers()) {
////                Point point = new Point(i.getX(), i.getY());
////                Imgproc.circle(resultMask, point, (int) i.getRadius(), new Scalar(i.getMaskValue()), -1);
////            }
////
////
////
////            ListIterator<GrabMaskCircleMarker> iterator = markerImageView.getNewMarkers().listIterator();
////            while (iterator.hasNext()) {
////                GrabMaskCircleMarker marker = iterator.next();
////                if(marker.getMaskValue() == GC_BGD) iterator.remove();
////                else if(marker.getMaskValue() != GC_FGD) {
////                    int x = (int) marker.getX(), y = (int) marker.getY();
////                    int r = (int) marker.getRadius();
////
////                    int rowStart = Math.max(y - r, 0),
////                            rowEnd = Math.min(y + r, resultMask.rows()),
////                            colStart = Math.max(x - r, 0),
////                            colEnd = Math.min(x + r, resultMask.cols());
////
////                    Mat area = resultMask.submat(rowStart, rowEnd, colStart, colEnd);
////                    int rows = area.rows(), cols = area.cols(), threshold = (int) ((double) rows * (double) cols * 0.2), accumulator = 0;
////
////                    boolean finished = false;
////                    for (int i = 0; i < rows; i++) {
////                        for (int j = 0; j < cols; j++) {
////                            int value = (int) area.get(i, j)[0];
////                            if (value == GC_BGD || value == GC_PR_BGD) {
////                                accumulator += 1;
////
////                                if (accumulator > threshold) {
////                                    iterator.remove();
////                                    finished = true;
////                                    break;
////                                }
////                            }
////                        }
////                        if(finished) break;
////                    }
////                }
////            }
////        }
////        catch (IOException e){
////            e.printStackTrace();
////        }
