//package main.wood_logs_detection.algorithms;
//
//import javafx.scene.image.Image;
//import main.core.data_types.markers.circle_markers_branch.CircleMarker;
//import org.opencv.core.Mat;
//import org.opencv.core.Point;
//import org.opencv.core.Scalar;
//import org.opencv.imgproc.Imgproc;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.List;
//import java.util.ListIterator;
//
//import static main.core.utils.ViewUtils.matToImage;
//
//public class CandidatesHoughCircles extends HoughCircles<CircleMarker> {
//
//    private List<CircleMarker> results;
//
//    public CandidatesHoughCircles(Image toShow, Image toProcess) throws IOException {
//        super(toShow, toProcess);
//    }
//
//    @Override
//    protected Image processCandidates(List<CircleMarker> candidates) {
//        filterCandidates(candidates);
//
//        Mat result = new Mat();
//        toShow.copyTo(result);
//
//        for (CircleMarker i : candidates) Imgproc.circle(result, new Point(i.getX(), i.getY()), (int) i.getRadius(), new Scalar(0, 0, 255), 4);
//
//        this.results = candidates;
//
//        return matToImage(result);
//    }
//
//    private void filterCandidates(List<CircleMarker> circleMarkers) {
//        HashMap<CircleMarker, CircleMarker> intersectionSet = new HashMap<>();
//
//        int size = circleMarkers.size();
//        for (int i = 0; i < size; i++) {
//            CircleMarker from = circleMarkers.get(i);
//            for (int j = i + 1; j < size; j++) {
//                CircleMarker to = circleMarkers.get(j);
//                double x = to.getX() - from.getX(),
//                        y = to.getY() - from.getY(),
//                        dist = from.getRadius() + to.getRadius();
//
//                if(x * x + y * y <= 0.49 * dist * dist) {
//                    CircleMarker c1 = intersectionSet.get(from);
//                    if(c1 == null) intersectionSet.put(from, to);
//                    else if(c1.getRadius() > to.getRadius()) intersectionSet.put(from, to);
//
//                    CircleMarker c2 = intersectionSet.get(to);
//                    if(c2 == null) intersectionSet.put(to, from);
//                    else if(c2.getRadius() > from.getRadius()) intersectionSet.put(to, from);
//                }
//            }
//        }
//
//
//        ListIterator<CircleMarker> iterator = circleMarkers.listIterator();
//
//        while (iterator.hasNext()) {
//            CircleMarker circleMarker = iterator.next();
//
//            CircleMarker c = intersectionSet.get(circleMarker);
//            if(c != null) {
//                if(circleMarker.getRadius() > c.getRadius()) iterator.remove();
//            }
//        }
//    }
//
//    @Override
//    public List<CircleMarker> getResults() {
//        return results;
//    }
//}
