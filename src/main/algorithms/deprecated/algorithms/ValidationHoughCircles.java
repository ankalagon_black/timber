//package main.wood_logs_detection.algorithms;
//
//import javafx.collections.ObservableList;
//import javafx.scene.image.Image;
//import main.core.data_types.settings.LogParameters;
//import main.core.data_types.markers.circle_markers_branch.CircleMarker;
//import main.core.data_types.markers.ruler_branch.LogicalRuler;
//import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
//import main.junctions.junction_markers.JunctionCircleMarker;
//import org.opencv.core.Mat;
//import org.opencv.core.Point;
//import org.opencv.core.Scalar;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.ListIterator;
//import java.util.function.BiConsumer;
//
//import static main.core.utils.ViewUtils.matToImage;
//import static org.opencv.core.CvType.CV_8UC3;
//import static org.opencv.imgproc.Imgproc.circle;
//
//public class ValidationHoughCircles extends HoughCircles<JunctionCircleMarker> {
//
//    private ObservableList<JunctionCircleMarker> newMarkers;
//    private ObservableList<JunctionCircleMarker> determinedMarkers;
//
//    private LogicalRuler logicalRuler;
//    private LogParameters logParameters;
//    private CircleMarkerSettingsWrapper settings;
//
//    public ValidationHoughCircles(Image toShow, Image toProcess,
//                                  ObservableList<JunctionCircleMarker> newMarkers, ObservableList<JunctionCircleMarker> determinedMarkers,
//                                  LogicalRuler logicalRuler, LogParameters logParameters, CircleMarkerSettingsWrapper settings) throws IOException {
//        super(toShow, toProcess);
//        this.newMarkers = newMarkers;
//        this.determinedMarkers = determinedMarkers;
//        this.logicalRuler = logicalRuler;
//        this.logParameters = logParameters;
//        this.settings = settings;
//
//        for (JunctionCircleMarker i: determinedMarkers) circle(this.toShow, new Point(i.getX(), i.getY()), (int) i.getRadius(), new Scalar(0, 255, 0), 2);
//    }
//
//    public void refresh() {
//        for (JunctionCircleMarker i: newMarkers) circle(this.toShow, new Point(i.getX(), i.getY()), (int) i.getRadius(), new Scalar(0, 255, 0), 2);
//        newMarkers.clear();
//    }
//
//    @Override
//    protected Image processCandidates(List<CircleMarker> candidates) {
//        filterCandidates(candidates);
//
//        newMarkers.clear();
//        newMarkers.addAll(candidates.stream().collect(ArrayList::new, (BiConsumer<ArrayList<JunctionCircleMarker>, CircleMarker>) (objects, i) -> {
//            objects.add(new JunctionCircleMarker(i.getX(), i.getY(), i.getRadius(), logicalRuler, logParameters, -1, settings)); }, ArrayList::addAll));
//
//        return matToImage(toShow);
//    }
//
//    private void filterCandidates(List<CircleMarker> circleMarkers) {
//        HashMap<CircleMarker, CircleMarker> intersectionSet = new HashMap<>();
//
//        int sizeC = circleMarkers.size(), sizeD = determinedMarkers.size();
//        for (int i = 0; i < sizeC; i++) {
//            CircleMarker from = circleMarkers.get(i);
//
//            for (int j = 0; j < sizeD; j++) {
//                CircleMarker to = determinedMarkers.get(j);
//
//                double x = to.getX() - from.getX(),
//                        y = to.getY() - from.getY(),
//                        dist = from.getRadius() + to.getRadius();
//
//                if(x * x + y * y <= 0.49 * dist * dist) {
//                    CircleMarker c1 = intersectionSet.get(from);
//                    if(c1 == null) intersectionSet.put(from, to);
//                    else if(c1.getRadius() > to.getRadius()) intersectionSet.put(from, to);
//
//                    CircleMarker c2 = intersectionSet.get(to);
//                    if(c2 == null) intersectionSet.put(to, from);
//                    else if(c2.getRadius() > from.getRadius()) intersectionSet.put(to, from);
//                }
//            }
//        }
//
//
//        ListIterator<CircleMarker> iterator = circleMarkers.listIterator();
//
//        while (iterator.hasNext()) {
//            CircleMarker circleMarker = iterator.next();
//
//            CircleMarker c = intersectionSet.get(circleMarker);
//            if(c != null) {
//                if(circleMarker.getRadius() > c.getRadius()) iterator.remove();
//            }
//        }
//    }
//
//    @Override
//    public List<JunctionCircleMarker> getSummary() {
//        Mat mask = new Mat(toProcess.rows(), toProcess.cols(), CV_8UC3);
//        for (JunctionCircleMarker i: newMarkers) {
//            circle(mask, new Point(i.getX(), i.getY()), (int) (i.getRadius() * 1.2), new Scalar(255, 255, 255), -1);
//        }
//
//        toProcess.setTo(new Scalar(0,0,0), mask);
//
//        return newMarkers;
//    }
//}
