//package main.wood_logs_detection.algorithms;
//
//import javafx.scene.image.Image;
//import main.core.data_types.markers.circle_markers_branch.CircleMarker;
//import org.opencv.core.*;
//import org.opencv.imgproc.Imgproc;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//import static main.core.utils.ViewUtils.imageToMat;
//import static main.core.utils.ViewUtils.matToImage;
//import static org.opencv.core.CvType.CV_8UC1;
//import static org.opencv.imgproc.Imgproc.*;
//
//public abstract class HoughCircles<T extends CircleMarker> {
//    protected Mat toShow;
//    protected Mat toProcess;
//
//    public HoughCircles(Image toShow, Image toProcess) throws IOException {
//        this.toShow = imageToMat(toShow);
//        this.toProcess = imageToMat(toProcess);
//    }
//
//    public final Image run(double minDist, double p1, double p2, int minRadius, int maxRadius) {
//        Mat gray = new Mat();
//        cvtColor(toProcess, gray, COLOR_BGR2GRAY);
//
//        GaussianBlur(gray, gray, new Size(5, 5), 0, 0);
//        dilate(gray, gray, Mat.ones(3, 3, CV_8UC1));
//
//        Mat circles = new Mat();
//        Imgproc.HoughCircles(gray, circles, Imgproc.HOUGH_GRADIENT, 2.0, minDist, p1, p2, minRadius, maxRadius);
//
//        List<CircleMarker> candidates = new ArrayList<>(circles.cols());
//        int cols = circles.cols();
//        for (int i = 0; i < cols; i++) {
//            double[] c = circles.get(0, i);
//            if (c[2] != 0) candidates.add(new CircleMarker(c[0], c[1], c[2]));
//        }
//
//        return processCandidates(candidates);
//    }
//
//    protected abstract Image processCandidates(List<CircleMarker> candidates);
//
//    public abstract List<T> getResults();
//
//    public Image getToProcessAsImage() {
//        return matToImage(toProcess);
//    }
//}
