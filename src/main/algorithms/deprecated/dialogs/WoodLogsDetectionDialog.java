//package main.wood_logs_detection.dialogs;
//
//import javafx.collections.ObservableList;
//import javafx.fxml.FXMLLoader;
//import javafx.scene.control.Dialog;
//import javafx.scene.image.Image;
//import main.core.data_types.settings.LogParameters;
//import main.core.data_types.markers.ruler_branch.LogicalRuler;
//import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
//import main.core.utils.MyUtils;
//import main.algorithms.deprecated.controllers.WoodLogsDetectionController;
//import main.junctions.junction_markers.JunctionCircleMarker;
//
//import java.io.File;
//import java.io.IOException;
//
//public class WoodLogsDetectionDialog extends Dialog<ObservableList<JunctionCircleMarker>> {
//
//    public WoodLogsDetectionDialog(Image image, LogicalRuler logicalRuler, LogParameters logParameters,
//                                   CircleMarkerSettingsWrapper circleMarkerSettings) throws IOException {
//        System.load(new File("./libraries/opencv_java341.dll").getAbsolutePath());
//
//        setTitle("Автоматизированное опеределение бревен");
//
//        FXMLLoader fxmlLoader = new FXMLLoader();
//        fxmlLoader.setLocation(getClass().getResource("/main/deprecated/resources/deprecated.fxml"));
//
//        setDialogPane(fxmlLoader.load());
//
//        WoodLogsDetectionController controller = fxmlLoader.getController();
//
//        boolean initialEnabled = circleMarkerSettings.isEnabled();
//        circleMarkerSettings.setEnabled(true);
//
//        controller.setController(image, logicalRuler, logParameters, circleMarkerSettings);
//
//        setResultConverter(param -> {
//            circleMarkerSettings.setEnabled(initialEnabled);
//
//            if(param.equals(MyUtils.FINISH)) return controller.getSummary();
//            else return null;
//        });
//    }
//}
