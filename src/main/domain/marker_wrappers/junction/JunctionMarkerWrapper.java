package main.domain.marker_wrappers.junction;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import main.core.data_types.markers.Marker;
import main.domain.marker_settings_wrappers.MarkerSettingsWrapper;

public class JunctionMarkerWrapper {

    public JunctionMarkerWrapper(Marker marker, int index, MarkerSettingsWrapper markerSettingsWrapper) {
        this.marker = marker;
        this.index.set(index);
        this.markerSettingsWrapper = markerSettingsWrapper;
    }


    private Marker marker;

    public Marker getMarker() {
        return marker;
    }


    private IntegerProperty index = new SimpleIntegerProperty();

    public int getIndex() {
        return index.get();
    }

    public IntegerProperty indexProperty() {
        return index;
    }

    public void setIndex(int index) {
        this.index.set(index);
    }


    private MarkerSettingsWrapper markerSettingsWrapper;

    public MarkerSettingsWrapper getMarkerSettingsWrapper() {
        return markerSettingsWrapper;
    }
}
