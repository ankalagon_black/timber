package main.domain.marker_wrappers.junction;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.WeakListChangeListener;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Summary;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.domain.marker_settings_wrappers.ContourSettingsWrapper;
import main.domain.marker_wrappers.base.LogicalContourWrapper;
import main.repository.database.CubageDatabase;
import main.truck.data_types.proxys.CubageProxy;

import java.sql.SQLException;

public class JunctionContourWrapper {
    private AbstractJunction junction;
    private CubageDatabase cubageDatabase;

    public JunctionContourWrapper(LogicalContourWrapper contourWrapper, ContourSettingsWrapper wrapper, AbstractJunction junction, CubageDatabase cubageDatabase) {
        this.contourWrapper = contourWrapper;
        this.wrapper = wrapper;
        this.junction = junction;
        this.cubageDatabase = cubageDatabase;

        calculateGeometryVolume();

        contourWrapper.getLogicalContour().areaProperty().addListener(new WeakChangeListener<>(numberChangeListener));
        junction.lengthProperty().addListener(new WeakChangeListener<>(changeListener));
        junction.coefficientProperty().addListener(new WeakChangeListener<>(changeListener));
        junction.getMarkers().addListener(new WeakListChangeListener<>(markersListener));
    }

    private void calculateGeometryVolume() {
        Summary results = junction.getSummary();

        if(junction.getLength() != null && junction.getLogicalContour() != null) {
            results.setGeometryVolume((junction.getCoefficient() / 100.0) * junction.getLogicalContour().getLogicalArea() * junction.getLength().getValue());

            CubageProxy cubageProxy = null;
            if(junction.getMarkersCount() != 0) {
                double adg = results.getGeometryVolume() / junction.getMarkersCount();
                try {
                    cubageProxy = cubageDatabase.getAverageDiameterStandard(junction.getCubageType(), junction.getLength(), adg);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if(cubageProxy != null) results.setAverageDiameterGeometry(cubageProxy.getDiameterStandard());
            else results.setAverageDiameterGeometry(new LogicalValue());
        }
        else {
            results.setGeometryVolume(0);
            results.setAverageDiameterGeometry(new LogicalValue());
        }
    }


    private LogicalContourWrapper contourWrapper;

    public LogicalContourWrapper get() {
        return contourWrapper;
    }


    private ContourSettingsWrapper wrapper;

    public ContourSettingsWrapper getWrapper() {
        return wrapper;
    }


    public Summary getResults() {
        return junction.getSummary();
    }


    private ChangeListener<Number> numberChangeListener = (observable, oldValue, newValue) -> {
        calculateGeometryVolume();
    };
    private ChangeListener<Object> changeListener = (observable, oldValue, newValue) -> calculateGeometryVolume();
    private ListChangeListener<Object> markersListener = c -> {
        while (c.next()) {
            if(c.wasRemoved() || c.wasAdded()) {
                calculateGeometryVolume();
                break;
            }
        }
    };
}
