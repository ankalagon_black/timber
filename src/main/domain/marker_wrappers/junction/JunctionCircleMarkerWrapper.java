package main.domain.marker_wrappers.junction;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
import main.domain.marker_wrappers.base.LogicalCircleMarkerWrapper;

public class JunctionCircleMarkerWrapper {

    public JunctionCircleMarkerWrapper(LogicalCircleMarkerWrapper logicalCircleMarkerWrapper,
                                       int index,
                                       CircleMarkerSettingsWrapper markerSettingsWrapper) {
        this.logicalCircleMarkerWrapper = logicalCircleMarkerWrapper;
        this.index.set(index);
        this.markerSettingsWrapper = markerSettingsWrapper;
    }


    private LogicalCircleMarkerWrapper logicalCircleMarkerWrapper;

    public LogicalCircleMarkerWrapper getLogicalCircleMarkerWrapper() {
        return logicalCircleMarkerWrapper;
    }


    private IntegerProperty index = new SimpleIntegerProperty();

    public int getIndex() {
        return index.get();
    }

    public IntegerProperty indexProperty() {
        return index;
    }

    public void setIndex(int index) {
        this.index.set(index);
    }


    private CircleMarkerSettingsWrapper markerSettingsWrapper;

    public CircleMarkerSettingsWrapper getMarkerSettingsWrapper() {
        return markerSettingsWrapper;
    }
}
