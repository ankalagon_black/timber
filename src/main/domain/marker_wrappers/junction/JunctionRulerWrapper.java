package main.domain.marker_wrappers.junction;

import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.domain.marker_settings_wrappers.RulerSettingsWrapper;

public class JunctionRulerWrapper {

    public JunctionRulerWrapper(LogicalRuler logicalRuler, RulerSettingsWrapper rulerSettingsWrapper) {
        this.logicalRuler = logicalRuler;
        this.rulerSettingsWrapper = rulerSettingsWrapper;
    }

    private LogicalRuler logicalRuler;

    public LogicalRuler getLogicalRuler() {
        return logicalRuler;
    }


    private RulerSettingsWrapper rulerSettingsWrapper;

    public RulerSettingsWrapper getRulerSettingsWrapper() {
        return rulerSettingsWrapper;
    }
}
