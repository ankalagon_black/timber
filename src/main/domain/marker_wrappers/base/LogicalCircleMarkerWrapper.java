package main.domain.marker_wrappers.base;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.geometry.Point2D;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.LogParameters;

public class LogicalCircleMarkerWrapper {

    private LogicalCircleMarker logicalCircleMarker;

    private LogicalRuler logicalRuler;
    private LogParameters logParameters;

    public LogicalCircleMarkerWrapper(LogicalCircleMarker logicalCircleMarker, LogicalRuler logicalRuler, LogParameters logParameters) {
        this.logicalCircleMarker = logicalCircleMarker;

        this.logicalRuler = logicalRuler;
        this.logParameters = logParameters;

        if(logicalRuler.isInitialized() && logicalCircleMarker.getRadius() == 0) {
            logicalCircleMarker.setRadius(logParameters.getMinDiameterInPixels(logicalRuler) / 2);
            logicalCircleMarker.setLogicalDiameter(computeLogicalDiameter(logicalCircleMarker.getRadius(), logicalRuler));
        }

        WeakChangeListener<Boolean> initializedWeakChangeListener = new WeakChangeListener<>(initializedChangeListener);
        WeakChangeListener<Number> numberWeakChangeListener = new WeakChangeListener<>(numberChangeListener);

        WeakChangeListener<LogicalValue> logicalValueWeakChangeListener = new WeakChangeListener<>(logicalValueChangeListener);
        WeakChangeListener<Point2D> point2DWeakChangeListener = new WeakChangeListener<>(point2DChangeListener);


        logicalRuler.initializedProperty().addListener(initializedWeakChangeListener);
        logicalCircleMarker.radiusProperty().addListener(numberWeakChangeListener);

        logicalRuler.startProperty().addListener(point2DWeakChangeListener);
        logicalRuler.endProperty().addListener(point2DWeakChangeListener);
        logicalRuler.logicalLengthProperty().addListener(logicalValueWeakChangeListener);
    }


    public LogicalCircleMarker getLogicalCircleMarker() {
        return logicalCircleMarker;
    }


    private LogicalValue computeLogicalDiameter(double radius, LogicalRuler logicalRuler) {
        return new LogicalValue(2 * radius * logicalRuler.getLogicalPerPixel().getValue());
    }


    private ChangeListener<Boolean> initializedChangeListener = (observable, oldValue, newValue) -> {
        if (!oldValue && newValue && logicalCircleMarker.getRadius() == 0) logicalCircleMarker.setRadius(logParameters.getMinDiameterInPixels(logicalRuler)/ 2);
    };

    private ChangeListener<Number> numberChangeListener = (observable, oldValue, newValue) -> {
        LogicalValue newLogicalDiameter = computeLogicalDiameter(newValue.doubleValue(), logicalRuler),
                actualLogicalDiameter = logParameters.truncate(newLogicalDiameter);

        logicalCircleMarker.setLogicalDiameter(actualLogicalDiameter);

        if(!actualLogicalDiameter.equals(newLogicalDiameter)) {
            double actualRadius = actualLogicalDiameter.getValue() * logicalRuler.getPixelPerLogical().getValue() / 2;
            if(actualRadius != 0) logicalCircleMarker.setRadius(actualRadius);
        }
    };
    private ChangeListener<LogicalValue> logicalValueChangeListener = (observable, oldValue, newValue) -> {
        logicalCircleMarker.setLogicalDiameter(computeLogicalDiameter(logicalCircleMarker.getRadius(), logicalRuler));
    };
    private ChangeListener<Point2D> point2DChangeListener = (observable, oldValue, newValue) -> {
        if(logicalRuler.isInitialized()) logicalCircleMarker.setLogicalDiameter(computeLogicalDiameter(logicalCircleMarker.getRadius(), logicalRuler));
    };
}
