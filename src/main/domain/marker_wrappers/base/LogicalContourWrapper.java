package main.domain.marker_wrappers.base;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.WeakListChangeListener;
import javafx.geometry.Point2D;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.utils.MyUtils;

import java.util.List;

public class LogicalContourWrapper {
    private LogicalContour logicalContour;

    private LogicalRuler logicalRuler;

    public LogicalContourWrapper(LogicalContour logicalContour, LogicalRuler logicalRuler) {
        this.logicalContour = logicalContour;

        this.logicalRuler = logicalRuler;

        WeakChangeListener<Number> areaWeakChangeListener = new WeakChangeListener<>(areaChangeListener);

        WeakChangeListener<Point2D> rulerPointWeakChangeListener = new WeakChangeListener<>(rulerPointChangeListener);
        WeakChangeListener<LogicalValue> logicalValueWeakChangeListener = new WeakChangeListener<>(logicalValueChangeListener);

        WeakListChangeListener<Point2D> weakListChangeListener = new WeakListChangeListener<>(listChangeListener);

        logicalContour.getPoints().addListener(weakListChangeListener);
        logicalContour.areaProperty().addListener(areaWeakChangeListener);

        logicalRuler.startProperty().addListener(rulerPointWeakChangeListener);
        logicalRuler.endProperty().addListener(rulerPointWeakChangeListener);
        logicalRuler.logicalLengthProperty().addListener(logicalValueWeakChangeListener);
    }

    public LogicalContour getLogicalContour() {
        return logicalContour;
    }


    private double computeLogicalArea() {
        if(logicalContour.getPoints().size() == 2) {
            List<Point2D> points = logicalContour.getPoints();

            Point2D vecContour = points.get(1).subtract(points.get(0)),
                    vecRuler = logicalRuler.getEnd().subtract(logicalRuler.getEnd());

            return Math.abs(MyUtils.crossProduct(vecContour, vecRuler));
        }
        else  {
            LogicalValue v = logicalRuler.getLogicalPerPixel();
            return logicalContour.getArea() * v.getValue() * v.getValue();
        }
    }


    private ListChangeListener<Point2D> listChangeListener = c -> {
        while (c.next()) {
            if(c.wasReplaced()) {
                logicalContour.recomputeBaseline();
                logicalContour.calculateArea();
            }
            else if(c.wasAdded()) {
                Point2D point2D = c.getAddedSubList().get(0);

                Point2D baseline = logicalContour.getBaseline(), newBaseline = new Point2D(Math.min(baseline.getX(), point2D.getX()), Math.min(baseline.getY(), point2D.getY()));

                if(baseline.getX() != newBaseline.getX()) logicalContour.setX(logicalContour.getX() - (baseline.getX() - newBaseline.getX()));
                if(baseline.getY() != newBaseline.getY()) logicalContour.setY(logicalContour.getY() - (baseline.getY() - newBaseline.getY()));

                logicalContour.setBaseline(newBaseline);

                logicalContour.calculateArea();
            }
            else if(c.wasRemoved()) {
                Point2D baseline = logicalContour.getBaseline(), newBaseline = logicalContour.recomputeBaseline();

                if(baseline.getX() != newBaseline.getX()) logicalContour.setX(logicalContour.getX() + (newBaseline.getX() - baseline.getX()));
                if(baseline.getY() != newBaseline.getY()) logicalContour.setY(logicalContour.getY() + (newBaseline.getY() - baseline.getY()));

                logicalContour.calculateArea();
            }
        }
    };
    private ChangeListener<Number> areaChangeListener = (observable, oldValue, newValue) -> {
        logicalContour.setLogicalArea(computeLogicalArea());
    };
    private ChangeListener<LogicalValue> logicalValueChangeListener = (observable, oldValue, newValue) -> {
        logicalContour.setLogicalArea(computeLogicalArea());
    };
    private ChangeListener<Point2D> rulerPointChangeListener = (observable, oldValue, newValue) -> {
        if(logicalRuler.isInitialized()) logicalContour.setLogicalArea(computeLogicalArea());
    };
}