package main.domain.marker_settings_wrappers;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import main.core.data_types.settings.markers_settings.CircleMarkerSettings;

public class CircleMarkerSettingsWrapper {

    public CircleMarkerSettingsWrapper(CircleMarkerSettings markerSettings, boolean enabled, boolean visible) {
        this.circleMarkerSettings = markerSettings;
        setEnabled(enabled);
        setVisible(visible);
    }


    private CircleMarkerSettings circleMarkerSettings;

    public CircleMarkerSettings getCircleMarkerSettings() {
        return circleMarkerSettings;
    }


    private BooleanProperty enabled = new SimpleBooleanProperty();

    public boolean isEnabled() {
        return enabled.get();
    }

    public BooleanProperty enabledProperty() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled.set(enabled);
    }


    private BooleanProperty visible = new SimpleBooleanProperty();

    public boolean isVisible() {
        return visible.get();
    }

    public BooleanProperty visibleProperty() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible.set(visible);
    }
}
