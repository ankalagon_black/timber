package main.domain.marker_settings_wrappers;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import main.core.data_types.settings.markers_settings.MarkerSettings;

public class MarkerSettingsWrapper {

    public MarkerSettingsWrapper(MarkerSettings markerSettings, boolean enabled, boolean visible) {
        this.markerSettings = markerSettings;
        setEnabled(enabled);
        setVisible(visible);
    }


    private MarkerSettings markerSettings;

    public MarkerSettings getMarkerSettings() {
        return markerSettings;
    }


    private BooleanProperty enabled = new SimpleBooleanProperty();

    public boolean isEnabled() {
        return enabled.get();
    }

    public BooleanProperty enabledProperty() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled.set(enabled);
    }


    private BooleanProperty visible = new SimpleBooleanProperty();

    public boolean isVisible() {
        return visible.get();
    }

    public BooleanProperty visibleProperty() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible.set(visible);
    }
}
