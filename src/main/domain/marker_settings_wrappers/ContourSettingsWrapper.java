package main.domain.marker_settings_wrappers;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import main.core.data_types.settings.markers_settings.ContourSettings;

public class ContourSettingsWrapper {

    public ContourSettingsWrapper(ContourSettings contourSettings, boolean enabled, boolean visible) {
        this.contourSettings = contourSettings;
        setEnabled(enabled);
        setVisible(visible);
        setHighlightInnerAreaProxy(false);
    }


    private ContourSettings contourSettings;

    public ContourSettings get() {
        return contourSettings;
    }


    private BooleanProperty enabled = new SimpleBooleanProperty();

    public boolean isEnabled() {
        return enabled.get();
    }

    public BooleanProperty enabledProperty() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled.set(enabled);
    }


    private BooleanProperty highlightInnerAreaProxy = new SimpleBooleanProperty();

    public boolean isHighlightInnerAreaProxy() {
        return highlightInnerAreaProxy.get();
    }

    public BooleanProperty highlightInnerAreaProxyProperty() {
        return highlightInnerAreaProxy;
    }

    public void setHighlightInnerAreaProxy(boolean highlightInnerAreaProxy) {
        this.highlightInnerAreaProxy.set(highlightInnerAreaProxy);
    }


    private BooleanProperty visible = new SimpleBooleanProperty();

    public boolean isVisible() {
        return visible.get();
    }

    public BooleanProperty visibleProperty() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible.set(visible);
    }
}
