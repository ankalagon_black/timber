package main.domain.marker_settings_wrappers;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import main.core.data_types.settings.markers_settings.RulerSettings;

public class RulerSettingsWrapper {

    public RulerSettingsWrapper(RulerSettings rulerSettings, boolean enabled, boolean visible) {
        this.rulerSettings = rulerSettings;
        setEnabled(enabled);
        setVisible(visible);
    }


    private RulerSettings rulerSettings;

    public RulerSettings getRulerSettings() {
        return rulerSettings;
    }


    private BooleanProperty enabled = new SimpleBooleanProperty();

    public boolean isEnabled() {
        return enabled.get();
    }

    public BooleanProperty enabledProperty() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled.set(enabled);
    }


    private BooleanProperty visible = new SimpleBooleanProperty();

    public boolean isVisible() {
        return visible.get();
    }

    public BooleanProperty visibleProperty() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible.set(visible);
    }
}
