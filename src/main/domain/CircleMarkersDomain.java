package main.domain;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.WeakListChangeListener;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Summary;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.junctions.data_types.LCMCubage;
import main.junctions.data_types.LCMCubageWrapper;
import main.junctions.data_types.ResultRow;
import main.repository.database.CubageDatabase;
import main.truck.data_types.proxys.CubageProxy;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class CircleMarkersDomain {
    private CircleMarkersJunction junction;
    private CubageDatabase cubageDatabase;

    private ObservableList<LCMCubageWrapper> lcmCubageWrappers = FXCollections.observableArrayList();

    private ListChangeListener<LogicalCircleMarker> markerListChangeListener = c -> {
        while (c.next()) {
            if(c.wasRemoved()) {
                List<LogicalCircleMarker> removed = (List<LogicalCircleMarker>) c.getRemoved();

                for (LogicalCircleMarker i: removed) lcmCubageWrappers.stream().filter(lcmCubageWrapper ->
                        lcmCubageWrapper.getLogicalCircleMarker().equals(i)).findFirst().ifPresent(this::removeLCMCubageWrapper);
            }
            else if(c.wasAdded()) {
                List<LogicalCircleMarker> added = (List<LogicalCircleMarker>) c.getAddedSubList();

                for (LogicalCircleMarker i: added) lcmCubageWrappers.add(createLCMCubageWrapper(i, lcmCubageWrappers.size()));
            }
        }
    };
    private WeakListChangeListener<LogicalCircleMarker> markerWeakListChangeListener = new WeakListChangeListener<>(markerListChangeListener);

    private ListChangeListener<LCMCubageWrapper> listChangeListener = c -> {
        int minIndex = Integer.MAX_VALUE;
        while (c.next()) {
            if (c.wasRemoved() && c.getFrom() < minIndex) minIndex = c.getFrom();
        }

        if(minIndex != -1) {
            int size = lcmCubageWrappers.size();
            for (int i = minIndex; i < size; i++)
                lcmCubageWrappers.get(i).setIndex(i);
        }
    };

    private HashMap<LCMCubageWrapper, ChangeListener<LCMCubage>> listenerHashMap = new HashMap<>();


    public CircleMarkersDomain(CircleMarkersJunction junction, CubageDatabase cubageDatabase) {
        this.junction = junction;
        this.cubageDatabase = cubageDatabase;

        junction.getSummary().resetMarkersResults();

        ObservableList<LogicalCircleMarker> logicalCircleMarkers = junction.getMarkers();

        lcmCubageWrappers.addAll(logicalCircleMarkers.stream().collect(ArrayList::new, (BiConsumer<ArrayList<LCMCubageWrapper>, LogicalCircleMarker>)
                (arrayList, logicalCircleMarker) -> arrayList.add(createLCMCubageWrapper(logicalCircleMarker, arrayList.size())), ArrayList::addAll));
        lcmCubageWrappers.addListener(listChangeListener);
        logicalCircleMarkers.addListener(markerWeakListChangeListener);
    }


    public CircleMarkersJunction getJunction() {
        return junction;
    }

    public ObservableList<LCMCubageWrapper> getLcmCubageWrappers() {
        return lcmCubageWrappers;
    }

    public ObservableList<ResultRow> getResultRows() {
        Summary results = junction.getSummary();

        Map<Double, List<ResultRow>> map = lcmCubageWrappers.stream().collect(ArrayList::new, (BiConsumer<List<ResultRow>, LCMCubageWrapper>) (resultRows, wrapper) -> {
            LCMCubage c = wrapper.getLcmCubage();
            double percentage = 0;
            if(results.getVolumeStandard() != 0) percentage = c.getVolumeStandard() / results.getVolumeStandard();
            resultRows.add(new ResultRow(c.getDiameterStandard(), c.getCubage(), 1, c.getVolumeStandard(), percentage));
            }, List::addAll).
                stream().
                collect(Collectors.groupingBy(resultRow -> resultRow.getDiameterStandard().getValue()));

        List<ResultRow> rows = new ArrayList<>();


        map.values().forEach((resultRows) -> {
            LogicalValue diameterStandard = resultRows.get(0).getDiameterStandard();
            double cubage = resultRows.get(0).getCubage();
            ResultRow resultRow = new ResultRow(diameterStandard, cubage, resultRows.size(), 0, 0);

            for (ResultRow i: resultRows) {
                resultRow.addSumVolumeStandard(i.getSumVolumeStandard());
                resultRow.addPercentage(i.getPercentage());
            }

            rows.add(resultRow);
        });

        return FXCollections.observableArrayList(rows);
    }


    private LCMCubageWrapper createLCMCubageWrapper(LogicalCircleMarker logicalCircleMarker, int index) {
        LCMCubageWrapper wrapper = new LCMCubageWrapper(logicalCircleMarker, junction, cubageDatabase, index);

        ChangeListener<LCMCubage> wrapperListener = (observable, oldValue, newValue) -> updateSummary(newValue.getDiameter(), oldValue.getDiameter(), newValue.getVolumeStandard(), oldValue.getVolumeStandard());
        listenerHashMap.put(wrapper, wrapperListener);

        wrapper.lcmCubageProperty().addListener(wrapperListener);

        addToSummary(wrapper.getLcmCubage().getDiameter(), wrapper.getLcmCubage().getVolumeStandard(), index);

        return wrapper;
    }

    private void removeLCMCubageWrapper(LCMCubageWrapper lcmCubageWrapper) {
        lcmCubageWrapper.lcmCubageProperty().removeListener(listenerHashMap.remove(lcmCubageWrapper));
        lcmCubageWrappers.remove(lcmCubageWrapper);

        removeFromSummary(lcmCubageWrapper.getLcmCubage().getDiameter(), lcmCubageWrapper.getLcmCubage().getVolumeStandard(), lcmCubageWrappers.size() + 1);
    }


    private void addToSummary(LogicalValue diameter, double volumeStandard, int nBeforeAddition) {
        Summary results = junction.getSummary();

        if(nBeforeAddition != 0) {
            double multi = nBeforeAddition * nBeforeAddition + nBeforeAddition;
            results.setAverageDiameter(results.getAverageDiameter().add(diameter.multiply(nBeforeAddition).subtract(results.getAverageDiameter().multiply(nBeforeAddition)).multiply(1 / multi)));
        }
        else results.setAverageDiameter(results.getAverageDiameter().add(diameter));

        results.setVolumeStandard(results.getVolumeStandard() + volumeStandard);

        double averageDiameter = results.getVolumeStandard() / junction.getMarkersCount();
        CubageProxy cubageProxy = null;
        try {
            cubageProxy = cubageDatabase.getAverageDiameterStandard(junction.getCubageType(), junction.getLength(), averageDiameter);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(cubageProxy != null) results.setAverageDiameterStandard(cubageProxy.getDiameterStandard());
        else results.setAverageDiameterStandard(new LogicalValue());
    }

    private void updateSummary(LogicalValue newDiameter, LogicalValue oldDiameter, double newVS, double oldVS) {
        Summary results = junction.getSummary();

        results.setAverageDiameter(results.getAverageDiameter().add(newDiameter.subtract(oldDiameter).multiply(1 / (double) lcmCubageWrappers.size())));

        if(newVS - oldVS != 0) {
            results.setVolumeStandard(results.getVolumeStandard() + newVS - oldVS);

            double averageDiameter = results.getVolumeStandard() / junction.getMarkersCount();
            CubageProxy cubageProxy = null;
            try {
                cubageProxy = cubageDatabase.getAverageDiameterStandard(junction.getCubageType(), junction.getLength(), averageDiameter);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (cubageProxy != null) results.setAverageDiameterStandard(cubageProxy.getDiameterStandard());
            else results.setAverageDiameterStandard(new LogicalValue());
        }
    }

    private void removeFromSummary(LogicalValue diameter, double volumeStandard, int nBeforeRemoving) {
        Summary results = junction.getSummary();

        if(nBeforeRemoving != 1) {
            double multi = nBeforeRemoving * nBeforeRemoving - nBeforeRemoving;
            results.setAverageDiameter(results.getAverageDiameter().subtract(diameter.multiply(nBeforeRemoving).subtract(results.getAverageDiameter().multiply(nBeforeRemoving)).multiply(1 / multi)));
        }
        else results.setAverageDiameter(results.getAverageDiameter().subtract(diameter));

        results.setVolumeStandard(results.getVolumeStandard() - volumeStandard);

        CubageProxy cubageProxy = null;
        if(junction.getMarkersCount() != 0) {
            double averageDiameter = results.getVolumeStandard() / junction.getMarkersCount();
            try {
                cubageProxy = cubageDatabase.getAverageDiameterStandard(junction.getCubageType(), junction.getLength(), averageDiameter);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(cubageProxy != null) results.setAverageDiameterStandard(cubageProxy.getDiameterStandard());
        else results.setAverageDiameterStandard(new LogicalValue());
    }
}
