package main.repository.database;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTableConfig;
import com.j256.ormlite.table.TableUtils;
import main.core.data_types.base.LogicalValue;
import main.truck.data_types.proxys.CubageProxy;
import main.repository.database.data_types.CubageRecord;
import main.repository.network.data_types.CubageRow;

import java.sql.SQLException;
import java.util.*;

public class CubageDatabase {
    private ConnectionSource connectionSource;

    private Dao<CubageRecord, Integer> cubageTable;

    public CubageDatabase(ConnectionSource connectionSource) throws SQLException {
        this.connectionSource = connectionSource;

        cubageTable = DaoManager.createDao(connectionSource, CubageRecord.class);

        initializeTables();
    }

    private void initializeTables() throws SQLException {
        if(!cubageTable.isTableExists()) TableUtils.createTable(connectionSource, CubageRecord.class);
    }


    public void addCubageRecord(CubageRow cubageRow) {
        try {
            cubageTable.create(CubageRow.toCubageRecord(cubageRow));
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<CubageProxy> getCubageTable() {
        try {
            QueryBuilder<CubageRecord, Integer> queryBuilder = cubageTable.queryBuilder().orderByRaw("cubageType, length, diameter, diameterStandard, cubage");
            return queryBuilder.query().stream().collect(ArrayList::new, (cubageProxies, cubageRecord) -> {
                cubageProxies.add(new CubageProxy(cubageRecord.getCubageType(),
                        new LogicalValue(cubageRecord.getLength()),
                        new LogicalValue(cubageRecord.getDiameter()),
                        new LogicalValue(cubageRecord.getDiameterStandard()),
                        cubageRecord.getCubage()));
            }, ArrayList::addAll);
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }


    public CubageProxy getCubageProxy(Integer cubageType, LogicalValue length, LogicalValue diameter) {
        if(cubageType == null || length == null) return null;

        Map<String, Object> map = new HashMap<>();
        map.put("cubageType", cubageType);
        map.put("length", length.getValue());
        map.put("diameter", diameter.getRounded(3, LogicalValue.METERS));

        try {
            List<CubageRecord> records = cubageTable.queryForFieldValues(map);
            if(!records.isEmpty()) return new CubageProxy(records.get(0));
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public CubageProxy getAverageDiameterStandard(Integer cubageType, LogicalValue length, Double volume) throws SQLException {
        if(cubageType == null || length == null) return null;

        QueryBuilder<CubageRecord, Integer> queryBuilder = cubageTable.queryBuilder();
        queryBuilder.where()
                .eq("cubageType", cubageType).and()
                .eq("length", length.getValue()).
                and().ge("cubage", volume);
        queryBuilder.orderBy("cubage", true).limit(1);

        try {
            List<CubageRecord> records = queryBuilder.query();
            if(!records.isEmpty()) return new CubageProxy(records.get(0));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    public void clear() {
        try {
            TableUtils.clearTable(connectionSource, cubageTable.getDataClass());
            cubageTable.executeRawNoArgs("ALTER TABLE " + DatabaseTableConfig.extractTableName(cubageTable.getDataClass()) + " ALTER COLUMN id RESTART WITH 1" + ";");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void close(){
        try {
            connectionSource.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
