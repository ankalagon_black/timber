package main.repository.database.data_types;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "cubage_records")
public class CubageRecord {

    public CubageRecord(){}

    public CubageRecord(int cubageType, double length, double diameter, double diameterStandard, double cubage){
        this.cubageType = cubageType;
        this.length = length;
        this.diameter = diameter;
        this.diameterStandard = diameterStandard;
        this.cubage = cubage;
    }

    @DatabaseField(generatedId = true)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @DatabaseField
    private int cubageType;

    public int getCubageType() {
        return cubageType;
    }

    public void setCubageType(int cubageType) {
        this.cubageType = cubageType;
    }

    @DatabaseField
    private double length;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @DatabaseField
    private double diameter;

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(Double diameter) {
        this.diameter = diameter;
    }

    @DatabaseField
    private double diameterStandard;

    public double getDiameterStandard() {
        return diameterStandard;
    }

    public void setDiameterStandard(double diameterStandard) {
        this.diameterStandard = diameterStandard;
    }

    @DatabaseField
    private double cubage;

    public double getCubage() {
        return cubage;
    }

    public void setCubage(double cubage) {
        this.cubage = cubage;
    }
}
