package main.repository.database.data_types;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "cubage_types")
public class CubageTypeRecord {

    public CubageTypeRecord() {}

    public CubageTypeRecord(int cubageType, boolean selected) {
        setCubageType(cubageType);
        setSelected(selected);
    }

    @DatabaseField(generatedId = true)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @DatabaseField
    private int cubageType;

    public int getCubageType() {
        return cubageType;
    }

    public void setCubageType(int cubageType) {
        this.cubageType = cubageType;
    }


    @DatabaseField
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
