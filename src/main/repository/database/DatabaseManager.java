package main.repository.database;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import java.io.File;
import java.sql.SQLException;

public class DatabaseManager {
    public static final String  DATABASE_NAME = "timber_data";

    public CubageDatabase getCubageDatabase() throws SQLException {
        File database = new File(".", DATABASE_NAME);
        String databaseUrl = "jdbc:h2:" + database.getPath();
        ConnectionSource connectionSource = new JdbcConnectionSource(databaseUrl);
        return new CubageDatabase(connectionSource);
    }

    public ClassifierDatabase getClassifierDatabase() throws SQLException {
        File database = new File(".", DATABASE_NAME);
        String databaseUrl = "jdbc:h2:" + database.getPath();
        ConnectionSource connectionSource = new JdbcConnectionSource(databaseUrl);
        return new ClassifierDatabase(connectionSource);
    }
}
