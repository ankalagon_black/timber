package main.repository.database;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.StatementBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.CubageType;
import main.core.data_types.common.Length;
import main.repository.database.data_types.CubageTypeRecord;
import main.repository.database.data_types.LengthRecord;
import main.truck.data_types.proxys.CubageTypeProxy;
import main.truck.data_types.proxys.LengthProxy;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClassifierDatabase {
    private ConnectionSource connectionSource;

    private Dao<CubageTypeRecord, Integer> cubageTypesTable;
    private Dao<LengthRecord, Integer> lengthsTable;

    public ClassifierDatabase(ConnectionSource connectionSource) throws SQLException {
        this.connectionSource = connectionSource;

        cubageTypesTable = DaoManager.createDao(connectionSource, CubageTypeRecord.class);
        lengthsTable = DaoManager.createDao(connectionSource, LengthRecord.class);

        initialize();
    }

    private void initialize() throws SQLException {
        if(!cubageTypesTable.isTableExists()) TableUtils.createTable(connectionSource, CubageTypeRecord.class);
        if(!lengthsTable.isTableExists()) TableUtils.createTable(connectionSource, LengthRecord.class);
    }


    public void updateTables(List<CubageType> cubageTypes, List<Length> lengths) {
        try {
            List<Integer> c = cubageTypes.stream().collect(ArrayList::new, (integers, cubageType) -> integers.add(cubageType.getId()), ArrayList::addAll);

            DeleteBuilder deleteBuilder = cubageTypesTable.deleteBuilder();
            deleteBuilder.where().not().in("cubageType", c.toArray());
            deleteBuilder.delete();

            for (CubageType i: cubageTypes) {
                if(cubageTypesTable.queryForEq("cubageType", i.getId()).isEmpty()) cubageTypesTable.create(new CubageTypeRecord(i.getId(), false));
            }

            List<Double> l = lengths.stream().collect(ArrayList::new, (doubles, length) -> doubles.add(length.getLength().getValue()), ArrayList::addAll);

            deleteBuilder = lengthsTable.deleteBuilder();
            deleteBuilder.where().not().in("length", l.toArray());
            deleteBuilder.delete();

            for (Length i: lengths) {
                if(lengthsTable.queryForEq("length", i.getLength().getValue()).isEmpty()) lengthsTable.create(new LengthRecord(i.getLength().getValue(), false));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public List<CubageTypeProxy> getCubageTypes() {
        try {
            return cubageTypesTable.queryForAll().stream().collect(ArrayList::new, (cubageTypeProxies, cubageTypeRecord) -> {
                cubageTypeProxies.add(new CubageTypeProxy(cubageTypeRecord.getCubageType(), cubageTypeRecord.isSelected()));
            }, ArrayList::addAll);
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void setCubageTypes(List<CubageTypeProxy> cubageTypes) {
        cubageTypes.forEach(cubageTypeProxy -> {
            try {
                UpdateBuilder updateBuilder = cubageTypesTable.updateBuilder();

                updateBuilder.updateColumnValue("selected", cubageTypeProxy.isSelected());
                updateBuilder.where().eq("cubageType", cubageTypeProxy.getCubageType());
                updateBuilder.update();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }


    public List<LengthProxy> getLengths() {
        try {
            return lengthsTable.queryForAll().stream().collect(ArrayList::new, (lengthProxies, lengthRecord) -> {
                lengthProxies.add(new LengthProxy(new LogicalValue(lengthRecord.getLength()), lengthRecord.isSelected()));
            }, ArrayList::addAll);
        }
        catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void  setLengths(List<LengthProxy> lengths) {
        lengths.forEach(lengthProxy -> {
            try {
                UpdateBuilder updateBuilder = lengthsTable.updateBuilder();

                updateBuilder.updateColumnValue("selected", lengthProxy.isSelected());
                updateBuilder.where().eq("length", lengthProxy.getLength().getValue());
                updateBuilder.update();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void close(){
        try {
            connectionSource.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    cubageTypesTable.create(new CubageTypeRecord(LogParameters.CUBAGE_TYPE_1, true));
//            cubageTypesTable.create(new CubageTypeRecord(LogParameters.CUBAGE_TYPE_2, false));
//            cubageTypesTable.create(new CubageTypeRecord(LogParameters.CUBAGE_TYPE_3, false));
//            cubageTypesTable.create(new CubageTypeRecord(LogParameters.CUBAGE_TYPE_4, false));
    //            lengthsTable.create(new LengthRecord(LogParameters.LENGTH_380.getValue(), false));
//            lengthsTable.create(new LengthRecord(LogParameters.LENGTH_390.getValue(), false));
//            lengthsTable.create(new LengthRecord(LogParameters.LENGTH_400.getValue(), true));
//            lengthsTable.create(new LengthRecord(LogParameters.LENGTH_410.getValue(), false));
//            lengthsTable.create(new LengthRecord(LogParameters.LENGTH_600.getValue(), false));
}
