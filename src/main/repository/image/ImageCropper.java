package main.repository.image;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ImageCropper {
    private Image sourceImage;

    public ImageCropper(Image sourceImage){
        this.sourceImage = sourceImage;
    }

    public Image crop(Rectangle rectangle){
        int width = (int) rectangle.getWidth(), height = (int) rectangle.getHeight();
        WritableImage writableImage = new WritableImage(width, height);

        PixelWriter writer = writableImage.getPixelWriter();
        PixelReader reader = sourceImage.getPixelReader();

        int x = (int) rectangle.getX(), y = (int) rectangle.getY();
        int maxX = x + width, maxY = y + height;

        for (int i = x; i < maxX; i++){
            for (int j = y; j < maxY; j++){
                Color color = reader.getColor(i, j);
                writer.setColor(i - x, j - y, color);
            }
        }

        return writableImage;
    }
}
