package main.repository.image;

import com.google.common.io.Files;
import main.core.data_types.common.Project;
import main.core.data_types.junctions.AbstractJunction;
import main.repository.loaders.DirectoryDataLoader;
import main.core.utils.MyUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ModifiedImageManager {

    public void open(File projectFile) {
        File directory = new DirectoryDataLoader().getModifiedImagesDirectory(projectFile);

        if(!directory.exists()) directory.mkdir();
        else if(directory.getName().equals(DirectoryDataLoader.MODIFIED_IMAGES_TEMP)) MyUtils.deleteContent(directory);
    }

    public void save(Project project, File file) {
        File directory = new DirectoryDataLoader().getModifiedImagesDirectory(file);

        if(project.hasProjectFile() && project.getProjectFile().equals(file)) {
            File[] files = directory.listFiles();
            if(files != null){
                for (File i: files){
                    if(project.getTruck().getJunctions().stream().
                            noneMatch(junction -> junction.hasModifiedFile() && junction.getModifiedFile().equals(i)))
                        file.delete();
                }
            }
        }
        else {
            if(!directory.exists()) directory.mkdir();

            for (AbstractJunction i : project.getTruck().getJunctions()) {
                if (i.hasModifiedFile()) {
                    File from = i.getModifiedFile(),
                            to = new File(directory, from.getName());
                    try {
                        Files.copy(from, to);
                        i.setModifiedFile(to);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if(directory.getName().equals(DirectoryDataLoader.MODIFIED_IMAGES_TEMP)) MyUtils.deleteContent(directory);
        }
    }

    public File createModifiedFile(Project project, File originalFile, BufferedImage bufferedImage){
        File modifiedFile = new DirectoryDataLoader().getModifiedImageFile(project.getProjectFile(), originalFile);
        new ImageSaver(modifiedFile).save(bufferedImage);
        return modifiedFile;
    }
}
