package main.repository.image;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import javafx.scene.image.Image;
import main.core.utils.MyUtils;

public class ImageCache {

    private LoadingCache<String, Image> cache = CacheBuilder.newBuilder().maximumSize(10).weakValues().build(new CacheLoader<String, Image>() {
        @Override
        public Image load(String s) throws Exception {
            return new Image(MyUtils.getLocalURL(s), true);
        }
    });

    public static class Holder {
        public static final ImageCache imageCache = new ImageCache();
    }

    public static LoadingCache<String, Image> get(){
        return Holder.imageCache.cache;
    }
}
