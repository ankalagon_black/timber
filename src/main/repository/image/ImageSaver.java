package main.repository.image;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageSaver {
    private File file;

    public ImageSaver(File file){
        this.file = file;
    }

    public void save(BufferedImage bufferedImage){
        try {
            if (file.getName().matches(".*(\\.jpg)$")) {
                BufferedImage imageRGB = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.OPAQUE);

                Graphics2D graphics = imageRGB.createGraphics();
                graphics.drawImage(bufferedImage, 0, 0, null);
                graphics.dispose();

                ImageIO.write(imageRGB, "jpg", file);
            }
            else ImageIO.write(bufferedImage, "png", file);
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
