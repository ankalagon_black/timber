package main.repository.xml.classifier;

import main.core.data_types.common.Classifier;
import main.core.data_types.settings.Settings;
import main.repository.xml.RootReader;
import main.repository.xml.classifier.structure.InConverter;
import main.repository.xml.classifier.structure.common.ClassifierRootXML;

import javax.xml.bind.JAXBException;
import java.io.File;

public class ClassifierOpener {
    private File file;

    public ClassifierOpener(File file){
        this.file = file;
    }

    public void open(Classifier classifier, Settings settings) throws JAXBException {
        RootReader rootReader = new RootReader(file);

        new InConverter((ClassifierRootXML) rootReader.read(ClassifierRootXML.class)).set(classifier, settings);
    }
}
