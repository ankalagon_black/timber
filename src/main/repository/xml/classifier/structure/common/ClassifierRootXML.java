package main.repository.xml.classifier.structure.common;

import main.repository.xml.classifier.structure.elements.BreedXML;
import main.repository.xml.classifier.structure.elements.CubageTypeXML;
import main.repository.xml.classifier.structure.elements.LengthXML;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "root")
public class ClassifierRootXML {

    private List<CubageTypeXML> cubageTypes;

    @XmlElementWrapper(name = "СписокВидовКубатурника")
    @XmlElement(name = "Кубатурник")
    public List<CubageTypeXML> getCubageTypes() {
        return cubageTypes;
    }

    public void setCubageTypes(List<CubageTypeXML> cubageTypes) {
        this.cubageTypes = cubageTypes;
    }


    private List<LengthXML> lengths;

    @XmlElementWrapper(name = "СписокДлинБревен")
    @XmlElement(name = "Длина")
    public List<LengthXML> getLengths() {
        return lengths;
    }

    public void setLengths(List<LengthXML> lengths) {
        this.lengths = lengths;
    }


    private List<BreedXML> breeds;

    @XmlElementWrapper(name = "СписокПород")
    @XmlElement(name = "Порода")
    public List<BreedXML> getBreeds() {
        return breeds;
    }

    public void setBreeds(List<BreedXML> breeds) {
        this.breeds = breeds;
    }


    private AutoSelectMarkersTypeXML autoSelectMarkersType;

    @XmlElement(name = "АвтоВыборВидаМаркетов")
    public AutoSelectMarkersTypeXML getAutoSelectMarkersType() {
        return autoSelectMarkersType;
    }

    public void setAutoSelectMarkersType(AutoSelectMarkersTypeXML autoSelectMarkersType) {
        this.autoSelectMarkersType = autoSelectMarkersType;
    }


    private AutoSelectCubageTypeXML autoSelectCubageType;

    @XmlElement(name = "АвтоВыборВидаКубатурника")
    public AutoSelectCubageTypeXML getAutoSelectCubageType() {
        return autoSelectCubageType;
    }

    public void setAutoSelectCubageType(AutoSelectCubageTypeXML autoSelectCubageType) {
        this.autoSelectCubageType = autoSelectCubageType;
    }


    private AutoSelectLengthXML autoSelectLength;

    @XmlElement(name = "АвтоВыборДлиныБревна")
    public AutoSelectLengthXML getAutoSelectLength() {
        return autoSelectLength;
    }

    public void setAutoSelectLength(AutoSelectLengthXML autoSelectLength) {
        this.autoSelectLength = autoSelectLength;
    }
}
