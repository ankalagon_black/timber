package main.repository.xml.classifier.structure;

import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Breed;
import main.core.data_types.common.Classifier;
import main.core.data_types.common.CubageType;
import main.core.data_types.common.Length;
import main.core.data_types.settings.LogParameters;
import main.core.data_types.settings.Settings;
import main.repository.xml.classifier.structure.common.AutoSelectCubageTypeXML;
import main.repository.xml.classifier.structure.common.AutoSelectLengthXML;
import main.repository.xml.classifier.structure.common.AutoSelectMarkersTypeXML;
import main.repository.xml.classifier.structure.common.ClassifierRootXML;

import java.util.ArrayList;
import java.util.List;

public class InConverter {
    private ClassifierRootXML rootXML;

    public InConverter(ClassifierRootXML rootXML) {
        this.rootXML = rootXML;
    }

    public void set(Classifier classifier, Settings settings) {
        List<CubageType> cubageTypes = rootXML.getCubageTypes().stream().collect(ArrayList::new, (cubageTables1, cubageTableXML) -> cubageTables1.add(new CubageType(cubageTableXML.getId(), cubageTableXML.getText())), ArrayList::addAll);
        List<Length> lengths = rootXML.getLengths().stream().collect(ArrayList::new, (lengths1, lengthXML) -> lengths1.add(new Length(new LogicalValue(lengthXML.getId(), LogicalValue.CENTIMETERS), lengthXML.getText())), ArrayList::addAll);
        List<Breed> breeds = rootXML.getBreeds().stream().collect(ArrayList::new, (breeds1, breedXML) -> breeds1.add(new Breed(breedXML.getId(), breedXML.getText())), ArrayList::addAll);

        classifier.setCubageTypes(cubageTypes);
        classifier.setLengths(lengths);
        classifier.setBreeds(breeds);

        AutoSelectMarkersTypeXML markersType = rootXML.getAutoSelectMarkersType();
        if(markersType != null) {
            switch (markersType.getId()) {
                case 1:
                    settings.setJunctionMode(Settings.MARKERS);
                    break;
                case 2:
                    settings.setJunctionMode(Settings.CIRCLE_MARKERS);
                    break;
            }
        }

        LogParameters logParameters = settings.getLogParameters();

        AutoSelectCubageTypeXML cubageType = rootXML.getAutoSelectCubageType();
        if(cubageType != null) {
            logParameters.setCubageType(cubageType.getId());
        }

        AutoSelectLengthXML length = rootXML.getAutoSelectLength();
        if(length != null) {
            logParameters.setLength(new LogicalValue(length.getId(), LogicalValue.CENTIMETERS));
        }
    }
}
