package main.repository.xml.project;

import main.core.data_types.common.Project;
import main.repository.image.ModifiedImageManager;
import main.repository.xml.RootWriter;
import main.repository.xml.project.structure.OutConverter;
import main.repository.xml.project.structure.v2.common.ProjectRootXML;

import javax.xml.bind.JAXBException;
import java.io.File;

public class ProjectSaver {
    public static final String CURRENT_VERSION = "2.5", LEGACY = "1.1";

    private File file;

    public ProjectSaver(File file){
        this.file = file;
    }

    public void save(Project project) throws JAXBException {
        new ModifiedImageManager().save(project, file);

        OutConverter outConverter = new OutConverter(file);

        new RootWriter(file).write(outConverter.getRoot(project), ProjectRootXML.class);
    }
}
