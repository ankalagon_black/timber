package main.repository.xml.project;

import main.core.data_types.common.Project;
import main.repository.image.ModifiedImageManager;
import main.repository.xml.RootReader;
import main.repository.xml.project.structure.InConverter;
import main.repository.xml.project.structure.deprecated.InConverter_v1_1;
import main.repository.xml.project.structure.deprecated.v1_1.RootXML;
import main.repository.xml.project.structure.v2.common.ProjectRootXML;

import java.io.File;

public class ProjectOpener {
    private File file;

    public ProjectOpener(File file){
        this.file = file;
    }

    public void open(Project project) throws Exception {
        new ModifiedImageManager().open(file);

        XPathFinder xPathFinder = new XPathFinder(file);

        String version = xPathFinder.findVersion();
        RootReader rootReader = new RootReader(file);

        if(version == null) return;

        switch (version){
            case ProjectSaver.CURRENT_VERSION:
                InConverter inConverter = new InConverter(file, (ProjectRootXML) rootReader.read(ProjectRootXML.class));
                inConverter.set(project);
                break;
            case ProjectSaver.LEGACY:
                InConverter_v1_1 inConverter_v1_1 = new InConverter_v1_1(file, (RootXML) rootReader.read(RootXML.class));
                inConverter_v1_1.set(project);
                break;
            default:
                throw new Exception("document version isn't supported by this verison of program");
        }
    }
}
