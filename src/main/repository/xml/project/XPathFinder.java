package main.repository.xml.project;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;

public class XPathFinder {
    private Document document;
    private XPath xpath;

    public XPathFinder(File file) throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        document = builder.parse(file);

        XPathFactory xPathfactory = XPathFactory.newInstance();
        xpath = xPathfactory.newXPath();

    }

    public String findVersion() throws XPathExpressionException {
        XPathExpression expression = xpath.compile("/root/@version");
        return (String) expression.evaluate(document, XPathConstants.STRING);
    }
}
