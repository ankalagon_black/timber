package main.repository.xml.project.structure.deprecated;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Project;
import main.core.data_types.common.Summary;
import main.core.data_types.common.Truck;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.junctions.MarkersJunction;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.circle_markers_branch.CircleMarker;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.HttpsSettings;
import main.core.data_types.settings.LogParameters;
import main.core.data_types.settings.Settings;
import main.core.data_types.settings.markers_settings.CircleMarkerSettings;
import main.core.data_types.settings.markers_settings.ContourSettings;
import main.core.data_types.settings.markers_settings.MarkerSettings;
import main.core.data_types.settings.markers_settings.RulerSettings;
import main.repository.xml.project.structure.deprecated.v1_1.*;
import main.repository.xml.project.structure.v2.base.FileXML;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class InConverter_v1_1 {
    private File file;
    private RootXML rootXML;

    public InConverter_v1_1(File file, RootXML rootXML){
        this.file = file;
        this.rootXML = rootXML;
    }

    public void set(Project project) throws Exception {
        setSettings(project.getSetting());
        setWorkingDirectory(project);
        setTruck(project.getTruck(), project.getSetting());
        project.setProjectFile(file);
    }

    private void setSettings(Settings settings) {
        SettingsXML settingsXML = rootXML.getMarkerSettings();

        settings.setJunctionMode(settingsXML.isUseDiameterMarkers() ? Settings.CIRCLE_MARKERS : Settings.MARKERS);
        settings.setLogParameters(getLogParameters(settingsXML.getLogParameters()));
        settings.setHttpsSettings(getHttpsSettings(settingsXML.getHttpsSettings()));
    }

    private LogParameters getLogParameters(LogParametersXML xml) {
        LogParameters out = new LogParameters();

        out.setLength(new LogicalValue(xml.getLength()));
        out.setMinDiameter(new LogicalValue(xml.getMinDiameter()));
        out.setMaxDiameter(new LogicalValue(xml.getMaxDiameter()));

        return out;
    }

    private HttpsSettings getHttpsSettings(HttpsSettingsXML httpsSettingsXML) {
        HttpsSettings out = new HttpsSettings();

        out.setIp(httpsSettingsXML.getIp());
        out.setGateway(httpsSettingsXML.getGateway());

        return out;
    }


    private void setWorkingDirectory(Project project) {
        String path = rootXML.getWorkingDirectory();

        File workingDirectory = new File(path);
        if(workingDirectory.exists() && workingDirectory.getAbsolutePath().equals(file.getParentFile().getAbsolutePath())) project.setWorkingDirectory(workingDirectory);
        else project.setWorkingDirectory(file.getParentFile());
    }


    private void setTruck(Truck truck, Settings settings) throws Exception {
        TruckXML truckXML = rootXML.getTruck();
        truck.setTruckNumber(truckXML.getNumber());
        truck.setCabin(getFile(truckXML.getCabin()));
        truck.setJunctions(getJunctions(truckXML.getJunctions(), settings));
    }

    private List<AbstractJunction> getJunctions(List<JunctionXML> junctionXMLS, Settings settings) {
        List<AbstractJunction> junctions = new ArrayList<>();

        switch (settings.getJunctionMode()) {
            case Settings.MARKERS:
                junctions.addAll(junctionXMLS.stream().collect(ArrayList::new, (abstractJunctions, junctionXML) -> {
                    try {
                        abstractJunctions.add(getMarkersJunction(junctionXML, settings.getLogParameters()));
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }, ArrayList::addAll));
                break;
            case Settings.CIRCLE_MARKERS:
                junctions.addAll(junctionXMLS.stream().collect(ArrayList::new, (abstractJunctions, junctionXML) -> {
                    try {
                        abstractJunctions.add(getCircleMarkersJunction(junctionXML, settings.getLogParameters()));
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }, ArrayList::addAll));
                break;
        }

        return junctions;
    }

    private MarkersJunction getMarkersJunction(JunctionXML xml, LogParameters logParameters) throws Exception {

        MarkerSettings settings = new MarkerSettings(Color.web(xml.getTextColor()), xml.getFontSize());

        RulerXML ruler = xml.getRuler();

        RulerSettings rulerSettings = new RulerSettings(Color.web(ruler.getTextColor()), ruler.getTextFontSize(), Color.web(ruler.getRulerColor()));

        double xStart = ruler.getxStart(), yStart = ruler.getyStart();
        double xEnd = ruler.getxEnd(), yEnd = ruler.getyEnd();

        double minX = Math.min(xStart, xEnd), minY = Math.min(yStart, yEnd),
                width = Math.max(xStart, xEnd) - minX, height = Math.max(yStart, yEnd) - minY;

        LogicalRuler logicalRuler = new LogicalRuler(minX, minY, new Point2D(0,0), new Point2D(width, height), new LogicalValue(ruler.getLengthInMeters()));

        File modifiedFile = null;
        if(xml.hasModified()) modifiedFile = getFileByRelativePath(xml.getModified());

        LogicalValue length = new LogicalValue(logParameters.getLength());

        return new MarkersJunction(getFile(xml.getOriginal()), modifiedFile, xml.getNumber(),
                null, length, null, null, 100,
                new Summary(), logicalRuler, rulerSettings,
                null, new ContourSettings(), getMarkers(xml.getMarkers()), settings);
    }

    private List<Marker> getMarkers(List<MarkerXML> markerXMLS) {
        return markerXMLS.stream().collect(ArrayList::new, (markers, markerXML) -> markers.add(new Marker(markerXML.getX(), markerXML.getY())), ArrayList::addAll);
    }

    private CircleMarkersJunction getCircleMarkersJunction(JunctionXML xml, LogParameters logParameters) throws Exception {

        Color color = Color.web(xml.getTextColor());
        CircleMarkerSettings settings = new CircleMarkerSettings(color, xml.getFontSize(), color);

        RulerXML ruler = xml.getRuler();

        RulerSettings rulerSettings = new RulerSettings(Color.web(ruler.getTextColor()), ruler.getTextFontSize(), Color.web(ruler.getRulerColor()));

        double xStart = ruler.getxStart(), yStart = ruler.getyStart();
        double xEnd = ruler.getxEnd(), yEnd = ruler.getyEnd();

        double minX = Math.min(xStart, xEnd), minY = Math.min(yStart, yEnd),
                width = Math.max(xStart, xEnd) - minX, height = Math.max(yStart, yEnd) - minY;

        LogicalRuler logicalRuler = new LogicalRuler(minX, minY, new Point2D(0,0), new Point2D(width, height), new LogicalValue(ruler.getLengthInMeters()));

        File modifiedFile = null;
        if(xml.hasModified()) modifiedFile = getFileByRelativePath(xml.getModified());

        LogicalValue length = new LogicalValue(logParameters.getLength());


        return new CircleMarkersJunction(getFile(xml.getOriginal()), modifiedFile, xml.getNumber(),
                null, length, null, null, 100,
                new Summary(), null, new ContourSettings(),
                logicalRuler, rulerSettings,
                getLogicalCircleMarker(xml.getMarkers()), settings);
    }

    private List<LogicalCircleMarker> getLogicalCircleMarker(List<MarkerXML> markerXMLS) {
        return markerXMLS.stream().collect(ArrayList::new,
                (logicalCircleMarkers, markerXML) -> logicalCircleMarkers.add(new LogicalCircleMarker(markerXML.getX(), markerXML.getY(), markerXML.getRadius(), new LogicalValue(markerXML.getLogicalDiameter()))), ArrayList::addAll);
    }


    private File getFile(FileXML file) throws Exception {
        File out;

        if(file.hasRelativePath()){
            out = getFileByRelativePath(file.getRelativePath());
            if(out.exists()) return out;
        }

        out = new File(file.getAbsolutePath());
        if(out.exists()) return out;

        throw new Exception("One of junctions files with path (" + file.getAbsolutePath() + ") doesn't exist.");
    }

    private File getFileByRelativePath(String relativePath){
        return new File(file.getParentFile(), relativePath);
    }
}
