package main.repository.xml.project.structure.deprecated.v1_1;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class SettingsXML {

    private boolean useDiameterMarkers;

    @XmlAttribute
    public boolean isUseDiameterMarkers() {
        return useDiameterMarkers;
    }

    public void setUseDiameterMarkers(boolean useDiameterMarkers) {
        this.useDiameterMarkers = useDiameterMarkers;
    }


    private LogParametersXML logParameters;

    @XmlElement(name = "LogParameters")
    public LogParametersXML getLogParameters() {
        return logParameters;
    }

    public void setLogParameters(LogParametersXML logParameters) {
        this.logParameters = logParameters;
    }


    private HttpsSettingsXML httpsSettings;

    @XmlElement(name = "HttpsSettings")
    public HttpsSettingsXML getHttpsSettings() {
        return httpsSettings;
    }

    public void setHttpsSettings(HttpsSettingsXML httpsSettings) {
        this.httpsSettings = httpsSettings;
    }
}
