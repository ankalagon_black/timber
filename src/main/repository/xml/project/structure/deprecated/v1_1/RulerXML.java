package main.repository.xml.project.structure.deprecated.v1_1;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class RulerXML {

    private double lengthInMeters;

    @XmlAttribute
    public double getLengthInMeters() {
        return lengthInMeters;
    }

    public void setLengthInMeters(double lengthInMeters) {
        this.lengthInMeters = lengthInMeters;
    }


    private String rulerColor;

    @XmlAttribute
    public String getRulerColor() {
        return rulerColor;
    }

    public void setRulerColor(String rulerColor) {
        this.rulerColor = rulerColor;
    }


    private String textColor;

    @XmlAttribute
    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }


    private int textFontSize;

    @XmlAttribute
    public int getTextFontSize() {
        return textFontSize;
    }

    public void setTextFontSize(int textFontSize) {
        this.textFontSize = textFontSize;
    }


    private boolean visible;

    @XmlAttribute
    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }


    private double xStart;

    @XmlAttribute
    public double getxStart() {
        return xStart;
    }

    public void setxStart(double xStart) {
        this.xStart = xStart;
    }


    private double yStart;

    @XmlAttribute
    public double getyStart() {
        return yStart;
    }

    public void setyStart(double yStart) {
        this.yStart = yStart;
    }


    private double xEnd;

    @XmlAttribute
    public double getxEnd() {
        return xEnd;
    }

    public void setxEnd(double xEnd) {
        this.xEnd = xEnd;
    }


    private double yEnd;

    public double getyEnd() {
        return yEnd;
    }

    public void setyEnd(double yEnd) {
        this.yEnd = yEnd;
    }
}
