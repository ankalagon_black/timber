package main.repository.xml.project.structure.deprecated.v1_1;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class LogParametersXML {

    private double length;

    @XmlAttribute
    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }


    private double minDiameter;

    @XmlAttribute
    public double getMinDiameter() {
        return minDiameter;
    }

    public void setMinDiameter(double minDiameter) {
        this.minDiameter = minDiameter;
    }


    private double maxDiameter;

    @XmlAttribute
    public double getMaxDiameter() {
        return maxDiameter;
    }

    public void setMaxDiameter(double maxDiameter) {
        this.maxDiameter = maxDiameter;
    }
}
