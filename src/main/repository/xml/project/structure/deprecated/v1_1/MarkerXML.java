package main.repository.xml.project.structure.deprecated.v1_1;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class MarkerXML {
    private double x;

    @XmlAttribute
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }


    private double y;

    @XmlAttribute
    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }


    private double diameter;

    @XmlAttribute
    public double getRadius() {
        return diameter;
    }

    public void setRadius(double diameter) {
        this.diameter = diameter;
    }


    private double logicalDiameter;

    @XmlAttribute
    public double getLogicalDiameter() {
        return logicalDiameter;
    }

    public void setLogicalDiameter(double logicalDiameter) {
        this.logicalDiameter = logicalDiameter;
    }
}
