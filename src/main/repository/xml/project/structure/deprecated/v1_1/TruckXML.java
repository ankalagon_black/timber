package main.repository.xml.project.structure.deprecated.v1_1;

import main.repository.xml.project.structure.v2.base.FileXML;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType
public class TruckXML {

    private String number;

    @XmlAttribute
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


    private FileXML cabin;

    @XmlElement(name = "Cabin")
    public FileXML getCabin() {
        return cabin;
    }

    public void setCabin(FileXML cabin) {
        this.cabin = cabin;
    }


    private List<JunctionXML> junctions;

    @XmlElement(name = "MarkersJunction")
    @XmlElementWrapper(name = "Junctions")
    public List<JunctionXML> getJunctions() {
        return junctions;
    }

    public void setJunctions(List<JunctionXML> junctions) {
        this.junctions = junctions;
    }
}