package main.repository.xml.project.structure.deprecated.v1_1;

import main.repository.xml.project.structure.v2.base.FileXML;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType
public class JunctionXML {
    private String number;

    @XmlAttribute
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


    private String color;

    @XmlAttribute
    public String getTextColor() {
        return color;
    }

    public void setTextColor(String color) {
        this.color = color;
    }


    private int fontSize;

    @XmlAttribute
    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }


    private FileXML original;

    @XmlElement(name = "Original")
    public FileXML getOriginal() {
        return original;
    }

    public void setOriginal(FileXML original) {
        this.original = original;
    }


    private String modified;

    @XmlElement(name = "Modified")
    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public boolean hasModified(){
        return modified != null;
    }


    private List<MarkerXML> markers;

    @XmlElement(name = "Marker")
    @XmlElementWrapper(name = "Markers")
    public List<MarkerXML> getMarkers() {
        return markers;
    }

    public void setMarkers(List<MarkerXML> markers) {
        this.markers = markers;
    }


    private RulerXML ruler;

    @XmlElement(name = "Ruler")
    public RulerXML getRuler() {
        return ruler;
    }

    public void setRuler(RulerXML ruler) {
        this.ruler = ruler;
    }
}
