//package main.repository.xml.project.structure.deprecated.v1;
//
//import javax.xml.bind.annotation.XmlAttribute;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;
//
//@XmlType
//@XmlRootElement(name = "root")
//public class Root {
//    private String version;
//
//    @XmlAttribute
//    public String getVersion() {
//        return version;
//    }
//
//    public void setVersion(String version) {
//        this.version = version;
//    }
//
//
//    private Truck truck;
//
//    @XmlElement(name = "Truck")
//    public Truck getTruck() {
//        return truck;
//    }
//
//    public void setTruck(Truck truck) {
//        this.truck = truck;
//    }
//
//
//    private Settings settings;
//
//    @XmlElement(name = "Settings")
//    public Settings getMarkerSettings() {
//        return settings;
//    }
//
//    public void setSettingsWrapper(Settings settings) {
//        this.settings = settings;
//    }
//
//
//    private String workingDirectory;
//
//    @XmlElement(name = "WorkingDirectory")
//    public String getWorkingDirectory() {
//        return workingDirectory;
//    }
//
//    public void setWorkingDirectory(String workingDirectory) {
//        this.workingDirectory = workingDirectory;
//    }
//}
