//package main.repository.xml.project.structure.deprecated.v1;
//
//import javax.xml.bind.annotation.XmlAttribute;
//import javax.xml.bind.annotation.XmlType;
//
//@XmlType
//public class Marker {
//    private double x;
//
//    @XmlAttribute
//    public double getX() {
//        return x;
//    }
//
//    public void setX(double x) {
//        this.x = x;
//    }
//
//
//    private double y;
//
//    @XmlAttribute
//    public double getY() {
//        return y;
//    }
//
//    public void setY(double y) {
//        this.y = y;
//    }
//
//
//    private double diameter;
//
//    @XmlAttribute
//    public double getDiameter() {
//        return diameter;
//    }
//
//    public void setDiameter(double diameter) {
//        this.diameter = diameter;
//    }
//}
