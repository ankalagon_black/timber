//package main.repository.xml.project.structure.deprecated.v1;
//
//import main.repository.xml.project.structure.v2.bar.FileXML;
//
//import javax.xml.bind.annotation.XmlAttribute;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlElementWrapper;
//import javax.xml.bind.annotation.XmlType;
//import java.util.List;
//
//@XmlType
//public class Truck {
//
//    private String number;
//
//    @XmlAttribute
//    public String getNumber() {
//        return number;
//    }
//
//    public void setNumber(String number) {
//        this.number = number;
//    }
//
//
//    private FileXML cabin;
//
//    @XmlElement(name = "Cabin")
//    public FileXML getCabin() {
//        return cabin;
//    }
//
//    public void setCabin(FileXML cabin) {
//        this.cabin = cabin;
//    }
//
//
//    private List<Junction> junctions;
//
//    @XmlElement(name = "MarkersJunction")
//    @XmlElementWrapper(name = "Junctions")
//    public List<Junction> getJunctions() {
//        return junctions;
//    }
//
//    public void setJunctions(List<Junction> junctions) {
//        this.junctions = junctions;
//    }
//}
