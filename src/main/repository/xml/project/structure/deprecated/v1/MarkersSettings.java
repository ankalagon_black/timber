//package main.repository.xml.project.structure.deprecated.v1;
//
//import javax.xml.bind.annotation.XmlAttribute;
//import javax.xml.bind.annotation.XmlType;
//
//@XmlType
//public class MarkersSettings {
//    private boolean useDiameterMarkers;
//
//    @XmlAttribute
//    public boolean isUseDiameterMarkers() {
//        return useDiameterMarkers;
//    }
//
//    public void setUseDiameterMarkers(boolean useDiameterMarkers) {
//        this.useDiameterMarkers = useDiameterMarkers;
//    }
//
//
//    private double minDiameterInMeters;
//
//    @XmlAttribute
//    public double getMinDiameterInMeters() {
//        return minDiameterInMeters;
//    }
//
//    public void setMinDiameterInMeters(double minDiameterInMeters) {
//        this.minDiameterInMeters = minDiameterInMeters;
//    }
//
//
//    private double maxDiameterInMeters;
//
//    @XmlAttribute
//    public double getMaxDiameterInMeters() {
//        return maxDiameterInMeters;
//    }
//
//    public void setMaxDiameterInMeters(double maxDiameterInMeters) {
//        this.maxDiameterInMeters = maxDiameterInMeters;
//    }
//}
