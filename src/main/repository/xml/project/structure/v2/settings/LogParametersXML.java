package main.repository.xml.project.structure.v2.settings;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class LogParametersXML {

    private Double length;

    @XmlAttribute
    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }


    private Integer cubageType;

    @XmlAttribute
    public Integer getCubageType() {
        return cubageType;
    }

    public void setCubageType(Integer cubageType) {
        this.cubageType = cubageType;
    }


    private double minDiameter;

    @XmlAttribute
    public double getMinDiameter() {
        return minDiameter;
    }

    public void setMinDiameter(double minDiameter) {
        this.minDiameter = minDiameter;
    }


    private double maxDiameter;

    @XmlAttribute
    public double getMaxDiameter() {
        return maxDiameter;
    }

    public void setMaxDiameter(double maxDiameter) {
        this.maxDiameter = maxDiameter;
    }
}
