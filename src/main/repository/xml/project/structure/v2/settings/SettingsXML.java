package main.repository.xml.project.structure.v2.settings;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class SettingsXML {
    private int junctionMode;

    @XmlAttribute
    public int getJunctionMode() {
        return junctionMode;
    }

    public void setJunctionMode(int junctionMode) {
        this.junctionMode = junctionMode;
    }


    private LogParametersXML logParameters;

    @XmlElement(name = "LogParameters")
    public LogParametersXML getLogParameters() {
        return logParameters;
    }

    public void setLogParameters(LogParametersXML logParameters) {
        this.logParameters = logParameters;
    }


    private HttpsSettingsXML httpsSettings;

    @XmlElement(name = "HttpsSettings")
    public HttpsSettingsXML getHttpsSettings() {
        return httpsSettings;
    }

    public void setHttpsSettings(HttpsSettingsXML httpsSettings) {
        this.httpsSettings = httpsSettings;
    }
}
