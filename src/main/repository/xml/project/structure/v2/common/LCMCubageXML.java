package main.repository.xml.project.structure.v2.common;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class LCMCubageXML {

    private double diameterStandard;

    @XmlAttribute
    public double getDiameterStandard() {
        return diameterStandard;
    }

    public void setDiameterStandard(double diameterStandard) {
        this.diameterStandard = diameterStandard;
    }


    private double cubage;

    @XmlAttribute
    public double getCubage() {
        return cubage;
    }

    public void setCubage(double cubage) {
        this.cubage = cubage;
    }


    private double volumeStandard;

    @XmlAttribute
    public double getVolumeStandard() {
        return volumeStandard;
    }

    public void setVolumeStandard(double volumeStandard) {
        this.volumeStandard = volumeStandard;
    }
}
