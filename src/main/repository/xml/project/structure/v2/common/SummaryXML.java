package main.repository.xml.project.structure.v2.common;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class SummaryXML {

    private double volumeStandard;

    @XmlAttribute
    public double getVolumeStandard() {
        return volumeStandard;
    }

    public void setVolumeStandard(double volumeStandard) {
        this.volumeStandard = volumeStandard;
    }


    private double geometryVolume;

    @XmlAttribute
    public double getGeometryVolume() {
        return geometryVolume;
    }

    public void setGeometryVolume(double geometryVolume) {
        this.geometryVolume = geometryVolume;
    }


    private double averageDiameter;

    @XmlAttribute
    public double getAverageDiameter() {
        return averageDiameter;
    }

    public void setAverageDiameter(double averageDiameter) {
        this.averageDiameter = averageDiameter;
    }


    private double averageDiameterStandard;

    @XmlAttribute
    public double getAverageDiameterStandard() {
        return averageDiameterStandard;
    }

    public void setAverageDiameterStandard(double averageDiameterStandard) {
        this.averageDiameterStandard = averageDiameterStandard;
    }


    private double averageDiameterGeometry;

    @XmlAttribute
    public double getAverageDiameterGeometry() {
        return averageDiameterGeometry;
    }

    public void setAverageDiameterGeometry(double averageDiameterGeometry) {
        this.averageDiameterGeometry = averageDiameterGeometry;
    }
}
