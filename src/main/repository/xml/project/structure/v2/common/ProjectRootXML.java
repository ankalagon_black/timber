package main.repository.xml.project.structure.v2.common;

import main.repository.xml.project.structure.v2.settings.SettingsXML;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "root")
public class ProjectRootXML {
    private String version;

    @XmlAttribute
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    private TruckXML truck;

    @XmlElement(name = "Truck")
    public TruckXML getTruck() {
        return truck;
    }

    public void setTruck(TruckXML truck) {
        this.truck = truck;
    }


    private SettingsXML settings;

    @XmlElement(name = "Settings")
    public SettingsXML getSettings() {
        return settings;
    }

    public void setSettings(SettingsXML settings) {
        this.settings = settings;
    }


    private String workingDirectory;

    @XmlElement(name = "WorkingDirectory")
    public String getWorkingDirectory() {
        return workingDirectory;
    }

    public void setWorkingDirectory(String workingDirectory) {
        this.workingDirectory = workingDirectory;
    }
}
