package main.repository.xml.project.structure.v2.common;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class ResultRowXML {

    private double diameterStandard;

    @XmlAttribute
    public double getDiameterStandard() {
        return diameterStandard;
    }

    public void setDiameterStandard(double diameterStandard) {
        this.diameterStandard = diameterStandard;
    }


    private double cubage;

    @XmlAttribute
    public double getCubage() {
        return cubage;
    }

    public void setCubage(double cubage) {
        this.cubage = cubage;
    }


    private int count;

    @XmlAttribute
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    private double sumVolumeStandard;

    @XmlAttribute
    public double getSumVolumeStandard() {
        return sumVolumeStandard;
    }

    public void setSumVolumeStandard(double sumVolumeStandard) {
        this.sumVolumeStandard = sumVolumeStandard;
    }


    private double percentage;

    @XmlAttribute
    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }
}
