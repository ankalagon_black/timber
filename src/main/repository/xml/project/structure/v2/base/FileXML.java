package main.repository.xml.project.structure.v2.base;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class FileXML {
    private String absolutePath;

    @XmlAttribute(name = "absolute")
    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }


    private String relativePath;

    @XmlAttribute(name = "relative")
    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public boolean hasRelativePath(){
        return relativePath != null;
    }
}
