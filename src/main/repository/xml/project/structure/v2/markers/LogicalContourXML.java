package main.repository.xml.project.structure.v2.markers;

import main.repository.xml.project.structure.v2.base.PointXML;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType
public class LogicalContourXML {
    private double x;

    @XmlAttribute
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }


    private double y;

    @XmlAttribute
    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }


    private List<PointXML> points;

    @XmlElement(name = "Point")
    @XmlElementWrapper(name = "Points")
    public List<PointXML> getPoints() {
        return points;
    }

    public void setPoints(List<PointXML> points) {
        this.points = points;
    }


    private double area;

    @XmlAttribute
    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    private double logicalArea;

    @XmlAttribute
    public double getLogicalArea() {
        return logicalArea;
    }

    public void setLogicalArea(double logicalArea) {
        this.logicalArea = logicalArea;
    }
}
