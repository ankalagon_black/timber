package main.repository.xml.project.structure.v2.markers;

import main.repository.xml.project.structure.v2.base.PointXML;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class LogicalRulerXML {

    private double x;

    @XmlAttribute
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }


    private double y;

    @XmlAttribute
    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }


    private PointXML start;

    @XmlElement
    public PointXML getStart() {
        return start;
    }

    public void setStart(PointXML start) {
        this.start = start;
    }


    private PointXML end;

    @XmlElement
    public PointXML getEnd() {
        return end;
    }

    public void setEnd(PointXML end) {
        this.end = end;
    }


    private double logicalLength;

    @XmlAttribute
    public double getLogicalLength() {
        return logicalLength;
    }

    public void setLogicalLength(double logicalLength) {
        this.logicalLength = logicalLength;
    }
}
