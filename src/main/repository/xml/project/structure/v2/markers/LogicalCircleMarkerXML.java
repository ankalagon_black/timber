package main.repository.xml.project.structure.v2.markers;

import main.repository.xml.project.structure.v2.common.LCMCubageXML;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class LogicalCircleMarkerXML {
    private double x;

    @XmlAttribute
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }


    private double y;

    @XmlAttribute
    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }


    private double radius;

    @XmlAttribute
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    private double logicalDiameter;

    @XmlAttribute
    public double getLogicalDiameter() {
        return logicalDiameter;
    }

    public void setLogicalDiameter(double logicalDiameter) {
        this.logicalDiameter = logicalDiameter;
    }


    private LCMCubageXML lcmCubage;

    @XmlElement(name = "Cubage")
    public LCMCubageXML getLcmCubage() {
        return lcmCubage;
    }

    public void setLcmCubage(LCMCubageXML lcmCubage) {
        this.lcmCubage = lcmCubage;
    }
}
