package main.repository.xml.project.structure.v2.junctions;

import main.repository.xml.classifier.structure.elements.BreedXML;
import main.repository.xml.classifier.structure.elements.CubageTypeXML;
import main.repository.xml.classifier.structure.elements.LengthXML;
import main.repository.xml.project.structure.v2.base.FileXML;
import main.repository.xml.project.structure.v2.common.ResultRowXML;
import main.repository.xml.project.structure.v2.common.SummaryXML;
import main.repository.xml.project.structure.v2.markers.LogicalCircleMarkerXML;
import main.repository.xml.project.structure.v2.markers.LogicalContourXML;
import main.repository.xml.project.structure.v2.markers.LogicalRulerXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.CircleMarkerSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.ContourSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.RulerSettingsXML;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType
public class CircleMarkersJunctionXML extends AbstractJunctionXML {

    public CircleMarkersJunctionXML() {}

//    public CircleMarkersJunctionXML(FileXML original, String modified,
//                                    String number, CubageTypeXML cubageType, LengthXML length, BreedXML breed, String commentary, double coefficient,
//                                    SummaryXML results,
//                                    LogicalRulerXML logicalRuler, RulerSettingsXML rulerSettings,
//                                    LogicalContourXML logicalContour, ContourSettingsXML contourSettings,
//                                    List<LogicalCircleMarkerXML> logicalCircleMarkers, CircleMarkerSettingsXML circleMarkerSettings) {
//        super(original, modified,
//                number, cubageType, length, breed, commentary, coefficient,
//                results,
//                logicalRuler, rulerSettings, logicalContour, contourSettings);
//        initialize(logicalCircleMarkers, circleMarkerSettings);
//    }

    public CircleMarkersJunctionXML(AbstractJunctionXML abstractJunctionXML, List<LogicalCircleMarkerXML> logicalCircleMarkers, CircleMarkerSettingsXML circleMarkerSettings, List<ResultRowXML> resultRows) {
        super(abstractJunctionXML);
        initialize(logicalCircleMarkers, circleMarkerSettings, resultRows);
    }


    private void initialize(List<LogicalCircleMarkerXML> logicalCircleMarkers, CircleMarkerSettingsXML circleMarkerSettings, List<ResultRowXML> resultRows) {
        setMarkers(logicalCircleMarkers);
        setCircleMarkerSettings(circleMarkerSettings);
        setResultRows(resultRows);
    }


    private List<LogicalCircleMarkerXML> logicalCircleMarkers;

    @XmlElement(name = "CircleMarker")
    @XmlElementWrapper(name = "CircleMarkers")
    public List<LogicalCircleMarkerXML> getMarkers() {
        return logicalCircleMarkers;
    }

    public void setMarkers(List<LogicalCircleMarkerXML> markers) {
        this.logicalCircleMarkers = markers;
    }


    private CircleMarkerSettingsXML circleMarkerSettings;

    @XmlElement(name = "CircleMarkerSettings")
    public CircleMarkerSettingsXML getCircleMarkerSettings() {
        return circleMarkerSettings;
    }

    public void setCircleMarkerSettings(CircleMarkerSettingsXML circleMarkerSettings) {
        this.circleMarkerSettings = circleMarkerSettings;
    }


    private List<ResultRowXML> resultRows;

    @XmlElement(name = "ResultRow")
    @XmlElementWrapper(name = "ResultRows")
    public List<ResultRowXML> getResultRows() {
        return resultRows;
    }

    public void setResultRows(List<ResultRowXML> resultRows) {
        this.resultRows = resultRows;
    }
}
