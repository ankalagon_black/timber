package main.repository.xml.project.structure.v2.junctions;

import main.repository.xml.classifier.structure.elements.BreedXML;
import main.repository.xml.classifier.structure.elements.CubageTypeXML;
import main.repository.xml.classifier.structure.elements.LengthXML;
import main.repository.xml.project.structure.v2.base.FileXML;
import main.repository.xml.project.structure.v2.common.SummaryXML;
import main.repository.xml.project.structure.v2.markers.LogicalContourXML;
import main.repository.xml.project.structure.v2.markers.LogicalRulerXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.ContourSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.RulerSettingsXML;

import javax.xml.bind.annotation.*;

@XmlType
@XmlSeeAlso({MarkersJunctionXML.class, CircleMarkersJunctionXML.class})
public class AbstractJunctionXML {

    public AbstractJunctionXML(){}

    public AbstractJunctionXML(FileXML original, String modified,
                               String number, CubageTypeXML cubageType, LengthXML length, BreedXML breed, String commentary, double coefficient,
                               SummaryXML results,
                               LogicalRulerXML logicalRuler, RulerSettingsXML rulerSettings,
                               LogicalContourXML logicalContour, ContourSettingsXML contourSettings) {
        setOriginal(original);
        setModified(modified);
        setNumber(number);
        setCubageType(cubageType);
        setLength(length);
        setBreed(breed);
        setCommentary(commentary);
        setCoefficient(coefficient);

        setSummary(results);

        setLogicalRuler(logicalRuler);
        setRulerSettings(rulerSettings);

        setLogicalContour(logicalContour);
        setContourSettings(contourSettings);
    }

    public AbstractJunctionXML(AbstractJunctionXML xml) {
        this(xml.getOriginal(), xml.getModified(),
                xml.getNumber(), xml.getCubageType(), xml.getLength(), xml.getBreed(), xml.getCommentary(), xml.getCoefficient(),
                xml.getSummary(),
                xml.getLogicalRuler(), xml.getRulerSettings(),
                xml.getLogicalContour(), xml.getContourSettings());
    }

    private FileXML original;

    @XmlElement(name = "Original")
    public FileXML getOriginal() {
        return original;
    }

    public void setOriginal(FileXML original) {
        this.original = original;
    }


    private String modified;

    @XmlElement(name = "Modified")
    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public boolean hasModified(){
        return modified != null;
    }


    private String number;

    @XmlAttribute
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


    private CubageTypeXML cubageType;

    @XmlElement(name = "CubageType")
    public CubageTypeXML getCubageType() {
        return cubageType;
    }

    public void setCubageType(CubageTypeXML cubageType) {
        this.cubageType = cubageType;
    }


    private LengthXML length;

    @XmlElement(name = "Length")
    public LengthXML getLength() {
        return length;
    }

    public void setLength(LengthXML length) {
        this.length = length;
    }


    private BreedXML breed;

    @XmlElement(name = "Breed")
    public BreedXML getBreed() {
        return breed;
    }

    public void setBreed(BreedXML breed) {
        this.breed = breed;
    }


    private String commentary;

    @XmlElement(name = "Commentary")
    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }


    private double coefficient;

    @XmlAttribute
    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }


    private SummaryXML summary;

    @XmlElement(name = "Summary")
    public SummaryXML getSummary() {
        return summary;
    }

    public void setSummary(SummaryXML summary) {
        this.summary = summary;
    }


    private LogicalRulerXML logicalRuler;

    @XmlElement(name = "Ruler")
    public LogicalRulerXML getLogicalRuler() {
        return logicalRuler;
    }

    public void setLogicalRuler(LogicalRulerXML logicalRuler) {
        this.logicalRuler = logicalRuler;
    }


    private RulerSettingsXML rulerSettings;

    @XmlElement(name = "RulerSettings")
    public RulerSettingsXML getRulerSettings() {
        return rulerSettings;
    }

    public void setRulerSettings(RulerSettingsXML rulerSettings) {
        this.rulerSettings = rulerSettings;
    }


    private LogicalContourXML logicalContour;

    @XmlElement(name = "Contour")
    public LogicalContourXML getLogicalContour() {
        return logicalContour;
    }

    public void setLogicalContour(LogicalContourXML logicalContour) {
        this.logicalContour = logicalContour;
    }


    private ContourSettingsXML contourSettings;

    @XmlElement(name = "ContourSettings")
    public ContourSettingsXML getContourSettings() {
        return contourSettings;
    }

    public void setContourSettings(ContourSettingsXML contourSettings) {
        this.contourSettings = contourSettings;
    }
}
