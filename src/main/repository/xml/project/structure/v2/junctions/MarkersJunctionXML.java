package main.repository.xml.project.structure.v2.junctions;

import main.repository.xml.classifier.structure.elements.BreedXML;
import main.repository.xml.classifier.structure.elements.CubageTypeXML;
import main.repository.xml.classifier.structure.elements.LengthXML;
import main.repository.xml.project.structure.v2.base.FileXML;
import main.repository.xml.project.structure.v2.common.SummaryXML;
import main.repository.xml.project.structure.v2.markers.LogicalContourXML;
import main.repository.xml.project.structure.v2.markers.LogicalRulerXML;
import main.repository.xml.project.structure.v2.markers.MarkerXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.ContourSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.MarkerSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.RulerSettingsXML;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlType
public class MarkersJunctionXML extends AbstractJunctionXML{

    public MarkersJunctionXML() {}

    public MarkersJunctionXML(FileXML original, String modified,
                              String number, CubageTypeXML cubageType, LengthXML length, BreedXML breed, String commentary, double coefficient,
                              SummaryXML results,
                              LogicalRulerXML logicalRuler, RulerSettingsXML rulerSettings,
                              LogicalContourXML logicalContour, ContourSettingsXML contourSettings,
                              List<MarkerXML> markers, MarkerSettingsXML markerSettings) {
        super(original, modified,
                number, cubageType, length,  breed, commentary, coefficient,
                results,
                logicalRuler, rulerSettings, logicalContour, contourSettings);
        initialize(markers, markerSettings);
    }

    public MarkersJunctionXML(AbstractJunctionXML abstractJunctionXML, List<MarkerXML> markers, MarkerSettingsXML markerSettings) {
        super(abstractJunctionXML);
        initialize(markers, markerSettings);
    }

    private void initialize(List<MarkerXML> markers, MarkerSettingsXML markerSettings) {
        setMarkers(markers);
        setMarkerSettings(markerSettings);
    }


    private List<MarkerXML> markers;

    @XmlElement(name = "Marker")
    @XmlElementWrapper(name = "Markers")
    public List<MarkerXML> getMarkers() {
        return markers;
    }

    public void setMarkers(List<MarkerXML> markers) {
        this.markers = markers;
    }


    private MarkerSettingsXML markerSettings;

    @XmlElement(name = "MarkerSettings")
    public MarkerSettingsXML getMarkerSettings() {
        return markerSettings;
    }

    public void setMarkerSettings(MarkerSettingsXML markerSettings) {
        this.markerSettings = markerSettings;
    }
}
