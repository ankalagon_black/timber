package main.repository.xml.project.structure;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Project;
import main.core.data_types.common.Summary;
import main.core.data_types.common.Truck;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.junctions.MarkersJunction;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.HttpsSettings;
import main.core.data_types.settings.LogParameters;
import main.core.data_types.settings.Settings;
import main.core.data_types.settings.markers_settings.CircleMarkerSettings;
import main.core.data_types.settings.markers_settings.ContourSettings;
import main.core.data_types.settings.markers_settings.MarkerSettings;
import main.core.data_types.settings.markers_settings.RulerSettings;
import main.repository.xml.project.structure.v2.base.FileXML;
import main.repository.xml.project.structure.v2.base.PointXML;
import main.repository.xml.project.structure.v2.common.ProjectRootXML;
import main.repository.xml.project.structure.v2.common.SummaryXML;
import main.repository.xml.project.structure.v2.common.TruckXML;
import main.repository.xml.project.structure.v2.junctions.AbstractJunctionXML;
import main.repository.xml.project.structure.v2.junctions.CircleMarkersJunctionXML;
import main.repository.xml.project.structure.v2.junctions.MarkersJunctionXML;
import main.repository.xml.project.structure.v2.markers.LogicalCircleMarkerXML;
import main.repository.xml.project.structure.v2.markers.LogicalContourXML;
import main.repository.xml.project.structure.v2.markers.MarkerXML;
import main.repository.xml.project.structure.v2.markers.LogicalRulerXML;
import main.repository.xml.project.structure.v2.settings.HttpsSettingsXML;
import main.repository.xml.project.structure.v2.settings.LogParametersXML;
import main.repository.xml.project.structure.v2.settings.SettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.CircleMarkerSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.ContourSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.MarkerSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.RulerSettingsXML;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class InConverter {
    private File file;
    private ProjectRootXML rootXML;

    public InConverter(File file, ProjectRootXML rootXML){
        this.file = file;
        this.rootXML = rootXML;
    }

    public void set(Project project) throws Exception {
        setSettings(project.getSetting());
        setWorkingDirectory(project);
        setTruck(project.getTruck(), project.getSetting());
        project.setProjectFile(file);
    }


    private void setSettings(Settings settings){
        SettingsXML settingsXML = rootXML.getSettings();

        settings.setJunctionMode(settingsXML.getJunctionMode());
        settings.setLogParameters(getLogParameters(settingsXML.getLogParameters()));
        settings.setHttpsSettings(getHttpsSettings(settingsXML.getHttpsSettings()));
    }

    private LogParameters getLogParameters(LogParametersXML xml){
        LogParameters out = new LogParameters();

        if(xml.getLength() != null) out.setLength(new LogicalValue(xml.getLength()));
        if(xml.getCubageType() != null) out.setCubageType(xml.getCubageType());
        out.setMinDiameter(new LogicalValue(xml.getMinDiameter()));
        out.setMaxDiameter(new LogicalValue(xml.getMaxDiameter()));

        return out;
    }

    private HttpsSettings getHttpsSettings(HttpsSettingsXML httpsSettingsXML){
        HttpsSettings out = new HttpsSettings();

        out.setIp(httpsSettingsXML.getIp());
        out.setGateway(httpsSettingsXML.getGateway());

        return out;
    }


    private void setWorkingDirectory(Project project){
        String path = rootXML.getWorkingDirectory();

        File workingDirectory = new File(path);
        if(workingDirectory.exists() && workingDirectory.getAbsolutePath().equals(file.getParentFile().getAbsolutePath())) project.setWorkingDirectory(workingDirectory);
        else project.setWorkingDirectory(file.getParentFile());
    }


    private void setTruck(Truck truck, Settings settings) throws Exception {
        TruckXML truckXML = rootXML.getTruck();
        truck.setTruckNumber(truckXML.getNumber());
        truck.setCabin(getFile(truckXML.getCabin()));
        truck.setJunctions(getJunctions(truckXML.getJunctions(), settings));
    }

    private List<AbstractJunction> getJunctions(List<AbstractJunctionXML> junctions, Settings settings) {
        switch (settings.getJunctionMode()) {
            case Settings.MARKERS:
                return junctions.stream().collect(ArrayList::new, (abstractJunctions, abstractJunctionXML) -> {
                    try {
                        abstractJunctions.add(getMarkersJunction(((MarkersJunctionXML) abstractJunctionXML)));
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }, ArrayList::addAll);
            case Settings.CIRCLE_MARKERS:
                return junctions.stream().collect(ArrayList::new, (abstractJunctions, abstractJunctionXML) -> {
                    try {
                        abstractJunctions.add(getCircleMarkersJunction((CircleMarkersJunctionXML) abstractJunctionXML));
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }, ArrayList::addAll);
            default:
                return null;
        }
    }


    private MarkersJunction getMarkersJunction(MarkersJunctionXML xml) throws Exception {
        MarkerSettings settings = getMarkersSettings(xml.getMarkerSettings());
        RulerSettings rulerSettings = getRulerSettings(xml.getRulerSettings());

        LogicalRuler logicalRuler = getLogicalRuler(xml.getLogicalRuler());

        File modifiedFile = null;
        if(xml.hasModified()) modifiedFile = getFileByRelativePath(xml.getModified());

        LogicalContour logicalContour = null;
        if(xml.getLogicalContour() != null) logicalContour = getContour(xml.getLogicalContour());

        Integer cubageType = null;
        if(xml.getCubageType() != null) cubageType = xml.getCubageType().getId();

        LogicalValue length = null;
        if (xml.getLength() != null) length = new LogicalValue(xml.getLength().getId(), LogicalValue.CENTIMETERS);

        Integer breed = null;
        if(xml.getBreed() != null) breed = xml.getBreed().getId();

        return new MarkersJunction(
                getFile(xml.getOriginal()), modifiedFile,
                xml.getNumber(), cubageType, length, breed, xml.getCommentary(), xml.getCoefficient(),
                getResults(xml.getSummary()),
                logicalRuler, rulerSettings,
                logicalContour, getContoursSettings(xml.getContourSettings()),
                getMarkers(xml.getMarkers()),
                settings);
    }

    private CircleMarkersJunction getCircleMarkersJunction(CircleMarkersJunctionXML xml) throws Exception {
        CircleMarkerSettings markerSettings = getCircleMarkersSettings(xml.getCircleMarkerSettings());
        RulerSettings rulerSettings = getRulerSettings(xml.getRulerSettings());

        LogicalRuler logicalRuler = getLogicalRuler(xml.getLogicalRuler());

        File modifiedFile = null;
        if(xml.hasModified()) modifiedFile = getFileByRelativePath(xml.getModified());

        LogicalContour logicalContour = null;
        if(xml.getLogicalContour() != null) logicalContour = getContour(xml.getLogicalContour());

        Integer cubageType = null;
        if(xml.getCubageType() != null) cubageType = xml.getCubageType().getId();

        LogicalValue length = null;
        if (xml.getLength() != null) length = new LogicalValue(xml.getLength().getId(), LogicalValue.CENTIMETERS);

        Integer breed = null;
        if(xml.getBreed() != null) breed = xml.getBreed().getId();

        return new CircleMarkersJunction(
                getFile(xml.getOriginal()), modifiedFile,
                xml.getNumber(), cubageType, length, breed, xml.getCommentary(), xml.getCoefficient(),
                getResults(xml.getSummary()),
                logicalContour, getContoursSettings(xml.getContourSettings()),
                logicalRuler, rulerSettings,
                getCircleMarkers(xml.getMarkers()),
                markerSettings);
    }


    private Summary getResults(SummaryXML xml) {
        return new Summary(xml.getVolumeStandard(), xml.getGeometryVolume(),
                new LogicalValue(xml.getAverageDiameter()), new LogicalValue(xml.getAverageDiameterStandard()), new LogicalValue(xml.getAverageDiameterGeometry()));
    }


    private List<Marker> getMarkers(List<MarkerXML> markerXMLS) {
        return markerXMLS.stream().collect(ArrayList::new,
                (markers, markerXML) ->
                        markers.add(new Marker(markerXML.getX(), markerXML.getY())),
                ArrayList::addAll);
    }

    private List<LogicalCircleMarker> getCircleMarkers(List<LogicalCircleMarkerXML> logicalCircleMarkers) {
        return logicalCircleMarkers.stream().collect(ArrayList::new,
                (junctionCircleMarkers, logicalCircleMarkerXML) -> junctionCircleMarkers.add(new LogicalCircleMarker(
                        logicalCircleMarkerXML.getX(),
                        logicalCircleMarkerXML.getY(),
                        logicalCircleMarkerXML.getRadius(),
                        new LogicalValue(logicalCircleMarkerXML.getLogicalDiameter()))), ArrayList::addAll);
    }

    private LogicalContour getContour(LogicalContourXML logicalContourXML) {
        List<Point2D> points = logicalContourXML.getPoints().stream().collect(ArrayList::new, (point2DS, pointXML) ->
                point2DS.add(getPoint(pointXML)), ArrayList::addAll);

        return new LogicalContour(logicalContourXML.getX(), logicalContourXML.getY(),
                logicalContourXML.getArea(), points, logicalContourXML.getLogicalArea());
    }

    private LogicalRuler getLogicalRuler(LogicalRulerXML junctionRulerXML) {
        return new LogicalRuler(junctionRulerXML.getX(),
                junctionRulerXML.getY(),
                getPoint(junctionRulerXML.getStart()),
                getPoint(junctionRulerXML.getEnd()),
                new LogicalValue(junctionRulerXML.getLogicalLength()));
    }


    private MarkerSettings getMarkersSettings(MarkerSettingsXML markerSettingsXML) {
        return new MarkerSettings(Color.web(markerSettingsXML.getTextColor()), markerSettingsXML.getFontSize());
    }

    private CircleMarkerSettings getCircleMarkersSettings(CircleMarkerSettingsXML settingsXML) {
        return new CircleMarkerSettings(
                Color.web(settingsXML.getTextColor()),
                settingsXML.getFontSize(),
                Color.web(settingsXML.getColor()));
    }

    private ContourSettings getContoursSettings(ContourSettingsXML contourSettingsXML) {
        return new ContourSettings(Color.web(contourSettingsXML.getTextColor()),
                contourSettingsXML.getFontSize(),
                Color.web(contourSettingsXML.getColor()),
                contourSettingsXML.isHighlightInnerArea());
    }

    private RulerSettings getRulerSettings(RulerSettingsXML settingsXML) {
        return new RulerSettings( Color.web(settingsXML.getTextColor()),
                settingsXML.getFontSize(),
                Color.web(settingsXML.getColor()));
    }


    private File getFile(FileXML file) throws Exception {
        File out;

        if(file.hasRelativePath()){
            out = getFileByRelativePath(file.getRelativePath());
            if(out.exists()) return out;
        }

        out = new File(file.getAbsolutePath());
        if(out.exists()) return out;

        throw new Exception("One of junctions files with path (" + file.getAbsolutePath() + ") doesn't exist.");
    }

    private File getFileByRelativePath(String relativePath){
        return new File(file.getParentFile(), relativePath);
    }

    private Point2D getPoint(PointXML pointXML) {
        return new Point2D(pointXML.getX(), pointXML.getY());
    }
}
