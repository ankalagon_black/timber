package main.repository.xml.project.structure;

import javafx.geometry.Point2D;
import main.core.data_types.common.*;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.junctions.MarkersJunction;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.HttpsSettings;
import main.core.data_types.settings.LogParameters;
import main.core.data_types.settings.Settings;
import main.core.data_types.settings.markers_settings.CircleMarkerSettings;
import main.core.data_types.settings.markers_settings.ContourSettings;
import main.core.data_types.settings.markers_settings.MarkerSettings;
import main.core.data_types.settings.markers_settings.RulerSettings;
import main.domain.CircleMarkersDomain;
import main.junctions.data_types.LCMCubage;
import main.junctions.data_types.LCMCubageWrapper;
import main.junctions.data_types.ResultRow;
import main.repository.database.CubageDatabase;
import main.repository.database.DatabaseManager;
import main.repository.xml.classifier.structure.elements.BreedXML;
import main.repository.xml.classifier.structure.elements.CubageTypeXML;
import main.repository.xml.classifier.structure.elements.LengthXML;
import main.repository.xml.project.ProjectSaver;
import main.core.utils.MyUtils;
import main.repository.xml.project.structure.v2.base.FileXML;
import main.repository.xml.project.structure.v2.base.PointXML;
import main.repository.xml.project.structure.v2.common.*;
import main.repository.xml.project.structure.v2.junctions.AbstractJunctionXML;
import main.repository.xml.project.structure.v2.junctions.CircleMarkersJunctionXML;
import main.repository.xml.project.structure.v2.junctions.MarkersJunctionXML;
import main.repository.xml.project.structure.v2.markers.LogicalCircleMarkerXML;
import main.repository.xml.project.structure.v2.markers.LogicalContourXML;
import main.repository.xml.project.structure.v2.markers.MarkerXML;
import main.repository.xml.project.structure.v2.markers.LogicalRulerXML;
import main.repository.xml.project.structure.v2.settings.HttpsSettingsXML;
import main.repository.xml.project.structure.v2.settings.LogParametersXML;
import main.repository.xml.project.structure.v2.settings.SettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.CircleMarkerSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.ContourSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.MarkerSettingsXML;
import main.repository.xml.project.structure.v2.settings.markers_settings.RulerSettingsXML;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.regex.Pattern;

public class OutConverter {
    private File file;

    public OutConverter(File file){
        this.file = file;
    }

    public ProjectRootXML getRoot(Project project){
        ProjectRootXML rootXML = new ProjectRootXML();

        CubageDatabase cubageDatabase = null;
        try {
            cubageDatabase = new DatabaseManager().getCubageDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        rootXML.setVersion(ProjectSaver.CURRENT_VERSION);
        rootXML.setSettings(getSettings(project.getSetting()));
        rootXML.setTruck(getTruck(project.getTruck(), project.getSetting(), cubageDatabase, project.getClassifier()));
        rootXML.setWorkingDirectory(project.getWorkingDirectory().getAbsolutePath());

        if (cubageDatabase != null) cubageDatabase.close();

        return rootXML;
    }


    private SettingsXML getSettings(Settings settings) {
        SettingsXML settingsXML = new SettingsXML();

        settingsXML.setJunctionMode(settings.getJunctionMode());
        settingsXML.setLogParameters(getLogParameters(settings.getLogParameters()));
        settingsXML.setHttpsSettings(getHttpsSettings(settings.getHttpsSettings()));

        return settingsXML;
    }

    private LogParametersXML getLogParameters(LogParameters logParameters) {
        LogParametersXML logParametersXML = new LogParametersXML();

        if(logParameters.hasCubageType()) logParametersXML.setCubageType(logParameters.getCubageType());
        if(logParameters.hasLength()) logParametersXML.setLength(logParameters.getLength().getValue());
        logParametersXML.setMinDiameter(logParameters.getMinDiameter().getValue());
        logParametersXML.setMaxDiameter(logParameters.getMaxDiameter().getValue());

        return logParametersXML;
    }

    private HttpsSettingsXML getHttpsSettings(HttpsSettings httpsSettings) {
        HttpsSettingsXML httpsSettingsXML = new HttpsSettingsXML();

        httpsSettingsXML.setIp(httpsSettings.getIp());
        httpsSettingsXML.setGateway(httpsSettings.getGateway());

        return httpsSettingsXML;
    }


    private TruckXML getTruck(Truck truck, Settings settings, CubageDatabase cubageDatabase, Classifier classifier){
        TruckXML truckXML = new TruckXML();

        truckXML.setNumber(truck.getTruckNumber());
        truckXML.setCabin(getFile(truck.getCabin()));
        truckXML.setJunctions(getAbstractJunctions(truck.getJunctions(), settings, cubageDatabase, classifier));

        return truckXML;
    }

    private List<AbstractJunctionXML> getAbstractJunctions(List<AbstractJunction> abstractJunctions, Settings settings, CubageDatabase cubageDatabase, Classifier classifier) {
        switch (settings.getJunctionMode()) {
            case Settings.MARKERS:
                return abstractJunctions.stream().collect(ArrayList::new, (abstractJunctionXMLS, abstractJunction) ->
                        abstractJunctionXMLS.add(getMarkersJunction(((MarkersJunction) abstractJunction), classifier)), ArrayList::addAll);
            case Settings.CIRCLE_MARKERS:
                return abstractJunctions.stream().collect(ArrayList::new, (abstractJunctionXMLS, abstractJunction) ->
                        abstractJunctionXMLS.add(getCircleMarkersJunction(((CircleMarkersJunction) abstractJunction), cubageDatabase, classifier)), ArrayList::addAll);
            default:
                return null;
        }
    }


    private AbstractJunctionXML getAbstractJunction(AbstractJunction abstractJunction, Classifier classifier) {
        AbstractJunctionXML abstractJunctionXML = new AbstractJunctionXML();

        abstractJunctionXML.setOriginal(getFile(abstractJunction.getOriginalFile()));
        if(abstractJunction.hasModifiedFile()) abstractJunctionXML.setModified(getRelativePath(abstractJunction.getModifiedFile()));
        abstractJunctionXML.setNumber(abstractJunction.getNumber());

        if (abstractJunction.hasCubageType()) abstractJunctionXML.setCubageType(getCubageType(classifier.getCubageTypeById(abstractJunction.getCubageType())));
        if (abstractJunction.hasLength()) abstractJunctionXML.setLength(getLength(classifier.getLengthByLogicalValue(abstractJunction.getLength())));
        if (abstractJunction.hasBreed()) abstractJunctionXML.setBreed(getBreed(classifier.getBreedById(abstractJunction.getBreed())));

        abstractJunctionXML.setCommentary(abstractJunction.getCommentary());
        abstractJunctionXML.setCoefficient(abstractJunction.getCoefficient());
        abstractJunctionXML.setSummary(getResults(abstractJunction.getSummary()));

        abstractJunctionXML.setLogicalRuler(getJunctionRuler(abstractJunction.getRuler()));
        abstractJunctionXML.setRulerSettings(getRulerSettings(abstractJunction.getRulerSettings()));

        if(abstractJunction.getLogicalContour() != null) abstractJunctionXML.setLogicalContour(getLogicalContour(abstractJunction.getLogicalContour()));
        abstractJunctionXML.setContourSettings(getContoursSettings(abstractJunction.getContourSettings()));

        return abstractJunctionXML;
    }

    private SummaryXML getResults(Summary results) {
        SummaryXML xml = new SummaryXML();

        xml.setVolumeStandard(MyUtils.round(results.getVolumeStandard(), 6));
        xml.setGeometryVolume(MyUtils.round(results.getGeometryVolume(), 6));
        xml.setAverageDiameter(MyUtils.round(results.getAverageDiameter().getValue(), 6));
        xml.setAverageDiameterStandard(MyUtils.round(results.getAverageDiameterStandard().getValue(), 6));
        xml.setAverageDiameterGeometry(MyUtils.round(results.getAverageDiameterGeometry().getValue(), 6));

        return xml;
    }

    private MarkersJunctionXML getMarkersJunction(MarkersJunction markersJunction, Classifier classifier) {
        return new MarkersJunctionXML(getAbstractJunction(markersJunction, classifier), getMarkers(markersJunction.getMarkers()), getMarkersSettings(markersJunction.getMarkerSettings()));
    }

    private CircleMarkersJunctionXML getCircleMarkersJunction(CircleMarkersJunction circleMarkersJunction, CubageDatabase cubageDatabase, Classifier classifier) {
        CircleMarkersDomain domain = new CircleMarkersDomain(circleMarkersJunction, cubageDatabase);

        return new CircleMarkersJunctionXML(getAbstractJunction(circleMarkersJunction, classifier),
                getCircleMarkers(domain.getLcmCubageWrappers()), getCircleMarkerSettings(circleMarkersJunction.getCircleMarkerSettings()), getResultRows(domain.getResultRows()));
    }

    private List<MarkerXML> getMarkers(List<Marker> markers) {
        return markers.stream().collect(ArrayList::new, (markersXMLS, marker) -> {
            MarkerXML markersXML = new MarkerXML();

            markersXML.setX(marker.getX());
            markersXML.setY(marker.getY());

            markersXMLS.add(markersXML);
        }, ArrayList::addAll);
    }

    private List<LogicalCircleMarkerXML> getCircleMarkers(List<LCMCubageWrapper> lcmCubageWrappers) {
        return lcmCubageWrappers.stream().collect(ArrayList::new, (logicalCircleMarkersXMLS, wrapper) -> {
            LogicalCircleMarkerXML junctionCircleMarkerXML = new LogicalCircleMarkerXML();

            LogicalCircleMarker lcm = wrapper.getLogicalCircleMarker();

            junctionCircleMarkerXML.setX(lcm.getX());
            junctionCircleMarkerXML.setY(lcm.getY());
            junctionCircleMarkerXML.setRadius(lcm.getRadius());

            junctionCircleMarkerXML.setLogicalDiameter(lcm.getLogicalDiameter().getValue());

            junctionCircleMarkerXML.setLcmCubage(getLCMCubage(wrapper.getLcmCubage()));

            logicalCircleMarkersXMLS.add(junctionCircleMarkerXML);
        }, ArrayList::addAll);
    }

    private LogicalContourXML getLogicalContour(LogicalContour logicalContour) {
        LogicalContourXML logicalContourXML = new LogicalContourXML();

        logicalContourXML.setX(logicalContour.getX());
        logicalContourXML.setY(logicalContour.getY());
        logicalContourXML.setArea(logicalContour.getArea());
        logicalContourXML.setPoints(logicalContour.getPoints().stream().collect(ArrayList::new, (pointXMLS, point2D) -> {
            pointXMLS.add(getPointXML(point2D));
        }, ArrayList::addAll));
        logicalContourXML.setLogicalArea(logicalContour.getLogicalArea());

        return logicalContourXML;
    }

    private LogicalRulerXML getJunctionRuler(LogicalRuler logicalRuler) {
        LogicalRulerXML rulerXML = new LogicalRulerXML();

        rulerXML.setX(logicalRuler.getX());
        rulerXML.setY(logicalRuler.getY());
        rulerXML.setStart(getPointXML(logicalRuler.getStart()));
        rulerXML.setEnd(getPointXML(logicalRuler.getEnd()));
        rulerXML.setLogicalLength(logicalRuler.getLogicalLength().getValue());

        return rulerXML;
    }


    private MarkerSettingsXML getMarkersSettings(MarkerSettings markerSettings) {
        MarkerSettingsXML settingsXML = new MarkerSettingsXML();

        settingsXML.setFontSize(markerSettings.getFontSize());
        settingsXML.setTextColor(MyUtils.toRGBHexCode(markerSettings.getTextColor()));

        return settingsXML;
    }

    private CircleMarkerSettingsXML getCircleMarkerSettings(CircleMarkerSettings settings) {
        CircleMarkerSettingsXML xmlSettings = new CircleMarkerSettingsXML();

        xmlSettings.setTextColor(MyUtils.toRGBHexCode(settings.getTextColor()));
        xmlSettings.setColor(MyUtils.toRGBHexCode(settings.getColor()));
        xmlSettings.setFontSize(settings.getFontSize());

        return xmlSettings;
    }

    private ContourSettingsXML getContoursSettings(ContourSettings contourSettings) {
        ContourSettingsXML contourSettingsXML = new ContourSettingsXML();

        contourSettingsXML.setTextColor(MyUtils.toRGBHexCode(contourSettings.getTextColor()));
        contourSettingsXML.setFontSize(contourSettings.getFontSize());
        contourSettingsXML.setColor(MyUtils.toRGBHexCode(contourSettings.getColor()));
        contourSettingsXML.setHighlightInnerArea(contourSettings.isHighlightInnerArea());

        return contourSettingsXML;
    }

    private RulerSettingsXML getRulerSettings(RulerSettings settings) {
        RulerSettingsXML settingsXML = new RulerSettingsXML();

        settingsXML.setColor(MyUtils.toRGBHexCode(settings.getColor()));
        settingsXML.setFontSize(settings.getFontSize());
        settingsXML.setTextColor(MyUtils.toRGBHexCode(settings.getTextColor()));

        return settingsXML;
    }


    private CubageTypeXML getCubageType(CubageType cubageType) {
        if(cubageType != null) {
            CubageTypeXML xml = new CubageTypeXML();

            xml.setId(cubageType.getId());
            xml.setText(cubageType.getText());

            return xml;
        }
        else return null;
    }

    private LengthXML getLength(Length length) {
        if(length != null) {
            LengthXML xml = new LengthXML();

            xml.setId((int) length.getLength().getValueInCm());
            xml.setText(length.getText());

            return xml;
        }
        else return null;
    }

    private BreedXML getBreed(Breed breed) {
        if(breed != null) {
            BreedXML xml = new BreedXML();

            xml.setId(breed.getId());
            xml.setText(breed.getText());

            return xml;
        }
        else return null;
    }


    private LCMCubageXML getLCMCubage(LCMCubage lcmCubage) {
        LCMCubageXML xml = new LCMCubageXML();

        xml.setDiameterStandard(lcmCubage.getDiameterStandard().getValue());
        xml.setCubage(lcmCubage.getCubage());
        xml.setVolumeStandard(lcmCubage.getVolumeStandard());

        return xml;
    }

    private List<ResultRowXML> getResultRows(List<ResultRow> resultRows) {
        return resultRows.stream().collect(ArrayList::new, (resultRowXMLS, resultRow) -> {
            ResultRowXML xml = new ResultRowXML();

            xml.setDiameterStandard(resultRow.getDiameterStandard().getValue());
            xml.setCubage(resultRow.getCubage());
            xml.setCount(resultRow.getCount());
            xml.setSumVolumeStandard(MyUtils.round(resultRow.getSumVolumeStandard(), 6));
            xml.setPercentage(MyUtils.round(resultRow.getPercentage(), 6));

            resultRowXMLS.add(xml);
        }, ArrayList::addAll);
    }


    private FileXML getFile(File file){
        FileXML outFile = new FileXML();

        outFile.setAbsolutePath(file.getPath());

        if(file.getPath().matches("(?i)" + Pattern.quote(this.file.getParentFile().getPath()) + ".*")) outFile.setRelativePath(getRelativePath(file));

        return outFile;
    }

    private String getRelativePath(File file){
        return file.getPath().replaceAll("(?i)" + Pattern.quote(this.file.getParentFile().getPath()), "");
    }

    private PointXML getPointXML(Point2D point2D) {
        PointXML pointXML = new PointXML();

        pointXML.setX(point2D.getX());
        pointXML.setY(point2D.getY());

        return pointXML;
    }
}
