package main.repository.xml;

import main.repository.xml.project.structure.v2.common.ProjectRootXML;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class RootReader {
    private File file;

    public RootReader(File file){
        this.file = file;
    }

    public Object read(Class c) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(c);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return unmarshaller.unmarshal(file);
    }
}
