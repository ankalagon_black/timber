package main.repository;

import javafx.application.Application;

import java.io.File;

public class CommandLineArgsParser {
    public static final String OPEN_PROJECT_FLAG = "open", WORKING_DIRECTORY_FLAG = "dir", CLASSIFIER = "classifier";

    private Application.Parameters parameters;

    public CommandLineArgsParser(Application.Parameters parameters){
        this.parameters = parameters;
    }

    public File getProjectFile() {
        String filePath = parameters.getNamed().get(OPEN_PROJECT_FLAG);
        if(filePath != null){
            File projectFile = new File(filePath);
            if(projectFile.exists()) return projectFile;
        }
        return null;
    }

    public File getWorkingDirectory() {
        String filePath = parameters.getNamed().get(WORKING_DIRECTORY_FLAG);
        if(filePath != null){
            File workingDirectory = new File(filePath);
            if(workingDirectory.exists()) return workingDirectory;
        }
        return null;
    }

    public File getClassifier() {
        String name = parameters.getNamed().get(CLASSIFIER);
        if(name != null) {
            File classifier = new File(".", name);
            if(classifier.exists()) return classifier;
        }
        return null;
    }
}
