package main.repository.loaders;

import java.io.File;

public class CascadeLoader {
    public static final String CASCADE_NAME = "cascade.xml";

    private File cascade;

    public CascadeLoader() {
        cascade = new File(".", CASCADE_NAME);
    }

    public File getCascade() {
        return cascade;
    }
}
