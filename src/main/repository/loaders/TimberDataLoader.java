package main.repository.loaders;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.reactivex.*;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import io.reactivex.schedulers.Schedulers;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Classifier;
import main.core.data_types.common.CubageType;
import main.core.data_types.common.Length;
import main.core.data_types.settings.LogParameters;
import main.repository.database.CubageDatabase;
import main.repository.network.GetTimberData;
import main.repository.network.data_types.CubageRow;
import main.repository.network.data_types.CubageTableRequest;
import main.core.utils.strings.DoubleCommaAdapter;
import main.truck.data_types.proxys.CubageTypeProxy;
import main.truck.data_types.proxys.LengthProxy;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

import java.text.ParseException;
import java.util.List;

public class TimberDataLoader {
    public static final double STEP = 0.001;

    private Gson gson = new GsonBuilder().registerTypeAdapter(Double.class, new DoubleCommaAdapter()).create();

    private GetTimberData getTimberData;
    private CubageDatabase cubageDatabase;

    private List<CubageTypeProxy> cubageTypes;
    private List<LengthProxy> lengths;

    private Classifier classifier;
    private LogParameters logParameters;

    private LogicalValue currentValue, difference;

    public TimberDataLoader(GetTimberData getTimberData, CubageDatabase cubageDatabase,
                            Classifier classifier,
                            LogParameters logParameters,
                            List<CubageTypeProxy> cubageTypes, List<LengthProxy> lengths){
        this.getTimberData = getTimberData;
        this.cubageDatabase = cubageDatabase;

        this.classifier = classifier;
        this.logParameters = logParameters;

        this.cubageTypes = cubageTypes;
        this.lengths = lengths;

        currentValue = new LogicalValue(logParameters.getMinDiameter());
        difference = new LogicalValue(logParameters.getMaxDiameter()).subtract(currentValue);
    }
    
    public Disposable load(Consumer<Double> onNext, Consumer<Throwable> onError, Action onComplete, Updatable updatable){
        return Observable.create((ObservableOnSubscribe<Double>) observableEmitter -> {
            for (CubageTypeProxy cubageType: cubageTypes) {
                CubageTableRequest request = new CubageTableRequest(cubageType.getCubageType());
                updatable.updateCubageType(classifier.getCubageTypeById(cubageType.getCubageType()));
                for (LengthProxy length: lengths) {
                    observableEmitter.onNext(0.0);
                    currentValue = new LogicalValue(logParameters.getMinDiameter());

                    updatable.updateLength(classifier.getLengthByLogicalValue(length.getLength()));

                    for (; logParameters.getMaxDiameter().getValue() - currentValue.getValue() > -STEP; currentValue = currentValue.add(STEP)){
                        if(observableEmitter.isDisposed()) break;

                        Response<JsonObject> response = getTimberData.get(RequestBody.create(MediaType.parse("text/plain"),
                                request.getRequest(currentValue, length.getLength()))).execute();

                        JsonObject object = response.body();
                        CubageRow cubageRow = null;
                        if (object != null) {
                            JsonElement element = object.get("Результат");
                            if (element != null) cubageRow = gson.fromJson(element, CubageRow.class);
                        }

                        if(cubageRow != null && cubageRow.isCorrect()) {
                            cubageDatabase.addCubageRecord(cubageRow);
                        }

                        observableEmitter.onNext((currentValue.getValue() - logParameters.getMinDiameter().getValue()) / difference.getValue());
                    }
                }
            }

            observableEmitter.onComplete();

        }).subscribeOn(Schedulers.computation()).observeOn(JavaFxScheduler.platform()).subscribe(onNext, onError, onComplete);
    }

    public interface Updatable{
        void updateCubageType(CubageType cubageType);
        void updateLength(Length length);
    }
}
