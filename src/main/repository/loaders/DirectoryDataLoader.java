package main.repository.loaders;

import com.google.common.io.Files;
import main.core.data_types.common.Truck;
import main.truck.data_types.NavigationFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DirectoryDataLoader {
    public static final String MODIFIED_IMAGES_TEMP = "ModifiedImagesTemp";

    public List<NavigationFile> loadNavigationFiles(File directory, Truck truck) {
        File[] files = directory.listFiles(pathname ->
                pathname.isDirectory() || pathname.getName().matches("(?i).*((\\.jpg)|(\\.png)|(\\.jpeg))$"));

        List<NavigationFile> navigationFiles = new ArrayList<>();

        if (files != null) {
            for (File i : files) navigationFiles.add(NavigationFile.toNavigationFile(i, truck.contains(i)));
        }

        return navigationFiles;
    }

    public File loadProjectFile(File directory){
        File[] files = directory.listFiles((dir, name) -> name.matches(".*\\.tpf$"));
        return (files == null || files.length == 0) ? null : files[0];
    }

    public File getModifiedImagesDirectory(File projectFile) {
        if(projectFile != null)
            return new File(projectFile.getParentFile(), projectFile.getName().replace(".tpf", ""));
        else {
            File dir = new File(System.getProperty("java.io.tmpdir"), MODIFIED_IMAGES_TEMP);
            if(!dir.exists()) dir.mkdir();
            return dir;
        }
    }

    public File getModifiedImageFile(File projectFile, File originalFile) {
        Pattern pattern = Pattern.compile("(?i)((\\.jpg)|(\\.png)|(\\.jpeg))$");
        Matcher matcher = pattern.matcher(originalFile.getName());

        String extension = matcher.find() ? matcher.group(1) : ".jpg";

        return new File(getModifiedImagesDirectory(projectFile),
                originalFile.getName().replaceAll("(?i)(?::|\\\\|\\.png|\\.jpg|\\.jpeg)", "") +
                String.valueOf(originalFile.getName().hashCode()) + extension);
    }

}
