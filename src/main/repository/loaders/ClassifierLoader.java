package main.repository.loaders;

import main.core.data_types.common.Classifier;
import main.core.data_types.settings.Settings;
import main.repository.database.ClassifierDatabase;
import main.repository.database.DatabaseManager;
import main.repository.xml.classifier.ClassifierOpener;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.sql.SQLException;

public class ClassifierLoader {
    public static final String FILE_NAME = "classifier.xml";

    private File file;

    public ClassifierLoader() {
        file = new File(".", FILE_NAME);
    }

    public ClassifierLoader(File file) {
        this.file = file;
    }


    public void load(Classifier classifier, Settings settings) {
        ClassifierOpener opener = new ClassifierOpener(file);

        try {
            opener.open(classifier, settings);
        }
        catch (JAXBException e) {
            e.printStackTrace();
            return;
        }

        DatabaseManager databaseManager = new DatabaseManager();
        ClassifierDatabase classifierDatabase = null;
        try {
            classifierDatabase = databaseManager.getClassifierDatabase();
            classifierDatabase.updateTables(classifier.getCubageTypes(), classifier.getLengths());
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(classifierDatabase != null) classifierDatabase.close();
        }
    }
}
