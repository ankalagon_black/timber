package main.repository.network.data_types;

import main.core.data_types.base.LogicalValue;
import main.core.utils.strings.DoublePrecisionStringConverter;

public class CubageTableRequest {
    private int cubageType;

    private DoublePrecisionStringConverter precisionOne = new DoublePrecisionStringConverter(1),
            precisionThree = new DoublePrecisionStringConverter(3);

    public CubageTableRequest(int cubageType){
        this.cubageType = cubageType;
    }

    public String getRequest(LogicalValue diameter, LogicalValue length){
        return "N1=XXXX\n" +
                "N2=ОбъемПоКубатурнику\n" +
                "N3=" + cubageType + "\n" +
                "N4=1\n" +
                "N5=" + precisionOne.toString(length.getValueInCm()) + "\n" +
                "N6=" + precisionThree.toString(diameter.getValueInCm()) + "\n";
    }
}
