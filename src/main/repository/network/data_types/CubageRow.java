package main.repository.network.data_types;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import main.core.data_types.base.LogicalValue;
import main.core.utils.MyUtils;
import main.repository.database.data_types.CubageRecord;

public class CubageRow {

    public static CubageRecord toCubageRecord(CubageRow cubageRow){
        return new CubageRecord(cubageRow.getCubageType(),
                new LogicalValue(cubageRow.getLengthCm(), LogicalValue.CENTIMETERS).getValue(),
                new LogicalValue(cubageRow.getDiameterCm(), LogicalValue.CENTIMETERS).getRounded(3, LogicalValue.METERS),
                new LogicalValue(cubageRow.getDiameterStandardCm(), LogicalValue.CENTIMETERS).getValue(),
                cubageRow.getCubage());
    }

    @SerializedName("ВидКубатурника")
    @Expose
    private Integer cubageType;

    public Integer getCubageType() {
        return cubageType;
    }

    public void setCubageType(Integer cubageType) {
        this.cubageType = cubageType;
    }


    @SerializedName("Длина")
    @Expose
    private Double lengthCm;

    public Double getLengthCm() {
        return lengthCm;
    }

    public void setLengthCm(Double lengthCm) {
        this.lengthCm = lengthCm;
    }


    @SerializedName("Диаметр")
    @Expose
    private Double diameterCm;

    public Double getDiameterCm() {
        return diameterCm;
    }

    public void setDiameterCm(Double diameterCm) {
        this.diameterCm = diameterCm;
    }


    @SerializedName("ДиаметрГОСТ")
    @Expose
    private Double diameterStandardCm;

    public Double getDiameterStandardCm() {
        return diameterStandardCm;
    }

    public void setDiameterStandardCm(Double diameterStandardCm) {
        this.diameterStandardCm = diameterStandardCm;
    }


    @SerializedName("Кубатурник")
    @Expose
    private Double cubage;

    public Double getCubage() {
        return cubage;
    }

    public void setCubage(Double cubage) {
        this.cubage = cubage;
    }


    public boolean isCorrect() {
        return cubageType != null && lengthCm != null && diameterCm != null && diameterStandardCm != null && cubage != null;
    }
}
