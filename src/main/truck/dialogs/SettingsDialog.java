package main.truck.dialogs;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Dialog;
import main.core.data_types.common.Project;
import main.truck.controllers.dialogs.SettingsContainerController;

import java.io.IOException;

public class SettingsDialog extends Dialog<Void> {

    public SettingsDialog(Project project) throws IOException {
        setTitle("Настройки программы");

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation( getClass().getResource("/main/truck/resources/fxml/dialogs/settings_container.fxml"));

        setDialogPane(loader.load());

        SettingsContainerController settingsContainerController = loader.getController();
        settingsContainerController.setController(project);
    }
}
