package main.truck.dialogs;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Dialog;
import main.repository.loaders.TimberDataLoader;
import main.truck.controllers.dialogs.LoadingProgressController;

import java.io.IOException;

public class LoadingProgressDialog extends Dialog {

    public LoadingProgressDialog(TimberDataLoader timberDataLoader) throws IOException {
        setTitle("Идет загрузка данных");

        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/main/truck/resources/fxml/dialogs/loading_progress.fxml"));

        setDialogPane(fxmlLoader.load());

        LoadingProgressController.DialogInterface dialogInterface = this::setTitle;

        LoadingProgressController controller = fxmlLoader.getController();
        controller.setController(timberDataLoader, dialogInterface);

        setResizable(true);
    }
}
