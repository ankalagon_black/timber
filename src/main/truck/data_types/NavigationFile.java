package main.truck.data_types;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.input.DataFormat;

import java.io.File;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NavigationFile implements Serializable {
    public static final int JUNCTION = 0, CABIN = 1, UNDEFINED = -1;
    public static final DataFormat NAVIGATION_FILE_FORMAT = new DataFormat("NavigationFileFormat");

    public static NavigationFile toNavigationFile(File file, boolean isUsed) {
        int type;
        String initialNumber = null;

        if(file.getName().matches("^[ks]0[ ,].*")){
            type = NavigationFile.CABIN;
        }
        else if(file.getName().matches("^k(([1-9][0-9]?)|0[1-9])[ ,].*")){
            type = NavigationFile.JUNCTION;
            Matcher matcher = Pattern.compile("^k(([1-9][0-9]?)|0[1-9])").matcher(file.getName());
            if(matcher.find()) initialNumber = matcher.group(1);
        }
        else type = NavigationFile.UNDEFINED;

        return new NavigationFile(file, type, initialNumber, isUsed);
    }


    public NavigationFile(File file, int type, String initialNumber, boolean isUsed) {
        this.file = file;
        this.type = type;
        this.initialNumber = initialNumber;
        this.isUsed.set(isUsed);
    }

    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }


    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    private String initialNumber;

    public String getInitialNumber() {
        return initialNumber;
    }

    public void setInitialNumber(String initialNumber) {
        this.initialNumber = initialNumber;
    }


    private transient SimpleBooleanProperty isUsed = new SimpleBooleanProperty();

    public SimpleBooleanProperty isUsedProperty() {
        return isUsed;
    }

    public boolean isUsed(){
        return isUsed.get();
    }

    public void setIsUsed(boolean isUsed) {
        this.isUsed.set(isUsed);
    }
}
