package main.truck.data_types.proxys;

import main.core.data_types.common.CubageType;

public class CubageTypeProxy {

    public CubageTypeProxy(int cubageType, boolean selected) {
        this.cubageType = cubageType;
        this.selected = selected;
    }

    private int cubageType;

    public int getCubageType() {
        return cubageType;
    }

    public void setCubageType(int cubageType) {
        this.cubageType = cubageType;
    }


    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
