package main.truck.data_types.proxys;

import main.core.data_types.base.LogicalValue;

public class LengthProxy {

    public LengthProxy(LogicalValue length, boolean selected) {
        this.length = length;
        this.selected = selected;
    }

    private LogicalValue length;

    public LogicalValue getLength() {
        return length;
    }

    public void setLength(LogicalValue length) {
        this.length = length;
    }



    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
