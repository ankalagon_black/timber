package main.truck.data_types.proxys;

import main.core.data_types.base.LogicalValue;
import main.repository.database.data_types.CubageRecord;

public class CubageProxy {

    public CubageProxy(int cubageType, LogicalValue length, LogicalValue diameter, LogicalValue diameterStandard, double cubage) {
        this.cubageType = cubageType;
        this.length = length;
        this.diameter = diameter;
        this.diameterStandard = diameterStandard;
        this.cubage = cubage;
    }

    public CubageProxy(CubageRecord cubageRecord) {
        this(cubageRecord.getCubageType(),
                new LogicalValue(cubageRecord.getLength()),
                new LogicalValue(cubageRecord.getDiameter()),
                new LogicalValue(cubageRecord.getDiameterStandard()),
                cubageRecord.getCubage());
    }

    public CubageProxy(int cubageType, LogicalValue length, LogicalValue diameter) {
        this(cubageType, length, diameter, new LogicalValue(), 0);
    }


    private int cubageType;

    public int getCubageType() {
        return cubageType;
    }

    public void setCubageType(int cubageType) {
        this.cubageType = cubageType;
    }


    private LogicalValue length;

    public LogicalValue getLength() {
        return length;
    }

    public void setLength(LogicalValue length) {
        this.length = length;
    }


    private LogicalValue diameter;

    public LogicalValue getDiameter() {
        return diameter;
    }

    public void setDiameter(LogicalValue diameter) {
        this.diameter = diameter;
    }


    private LogicalValue diameterStandard;

    public LogicalValue getDiameterStandard() {
        return diameterStandard;
    }

    public void setDiameterStandard(LogicalValue diameterStandard) {
        this.diameterStandard = diameterStandard;
    }


    private double cubage;

    public double getCubage() {
        return cubage;
    }

    public void setCubage(double cubage) {
        this.cubage = cubage;
    }
}
