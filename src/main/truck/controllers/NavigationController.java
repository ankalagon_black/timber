package main.truck.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import main.core.data_types.common.Project;
import main.repository.loaders.DirectoryDataLoader;
import main.repository.xml.project.ProjectOpener;
import main.truck.data_types.NavigationFile;
import main.truck.interfaces.Disposable;
import main.truck.interfaces.MovableToDirectory;
import main.truck.views.cells.NavigationFileCell;
import main.core.utils.MyUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class NavigationController implements Initializable, Disposable, MovableToDirectory {
    public VBox root;
    public TextField directoryPath;
    public ListView<NavigationFile> navigationFilesListView;

    private ObservableList<NavigationFile> navigationFiles;
    private Project project;


    private ChangeListener<File> workingDirectoryChangeListener = (observable, oldValue, newValue) -> {
        showWorkingDirectory(newValue);
    };

    private WeakChangeListener<File> workingDirectoryWeakChangeListener = new WeakChangeListener<>(workingDirectoryChangeListener);


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        navigationFilesListView.setCellFactory(param -> {
            try{
                return new NavigationFileCell(NavigationController.this);
            }
            catch (IOException e){
                e.printStackTrace();
                return null;
            }
        });
    }

    public void setController(Project project, ObservableList<NavigationFile> navigationFiles) {
        this.project = project;
        this.navigationFiles = navigationFiles;
        navigationFilesListView.setItems(navigationFiles);

        if(project.hasWorkingDirectory()) showWorkingDirectory(project.getWorkingDirectory());
        project.workingDirectoryProperty().addListener(workingDirectoryWeakChangeListener);
    }

    @Override
    public void dispose() {
        project.workingDirectoryProperty().removeListener(workingDirectoryWeakChangeListener);
    }


    public ListView<NavigationFile> getNavigationFilesListView() {
        return navigationFilesListView;
    }


    public void toParent(ActionEvent actionEvent) {
        if(project.hasWorkingDirectory()){
            File parent = project.getWorkingDirectory().getParentFile();
            if(parent != null && parent.exists()) showOnChangeDirectory(parent);
        }
    }

    public void onPathTyped(KeyEvent keyEvent) {
        if(keyEvent.getCharacter().equals("\r")){
            File file = new File(directoryPath.getText());
            if(file.exists() && file.isDirectory()) showOnChangeDirectory(file);
            else{
                Alert alert = new Alert(Alert.AlertType.ERROR, "Не удалось перейти в указанную директорию.", new ButtonType("Ок", ButtonBar.ButtonData.OK_DONE));
                alert.setTitle("Ошибка доступа к файлу");
                alert.setHeaderText(null);
                alert.show();

                if(project.hasWorkingDirectory()) directoryPath.setText(project.getWorkingDirectory().getPath());
                else directoryPath.setText("");
            }
        }
    }

    public void chooseDirectory(ActionEvent actionEvent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Выберите директорию");
        if(project.hasWorkingDirectory()) directoryChooser.setInitialDirectory(project.getWorkingDirectory());

        File directory = directoryChooser.showDialog(root.getScene().getWindow());
        if(directory != null){
            showOnChangeDirectory(directory);
        }
    }

    public void initializeTruck(ActionEvent actionEvent) {
        if(project.hasWorkingDirectory()) {
            File projectFile = new DirectoryDataLoader().loadProjectFile(project.getWorkingDirectory());
            if (projectFile != null) {
                ButtonType open = new ButtonType("Открыть файл проекта", ButtonBar.ButtonData.OK_DONE),
                        fillFromDirectory = new ButtonType("Заполнить из раб. директории", ButtonBar.ButtonData.OK_DONE),
                        cancel = new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE);

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                        "Рабочая директория содержит в себе файл проекта (" + projectFile.getName() + "). Вы можете открыть его или выполнить стандартное заполнение из рабочей директории.",
                        open, fillFromDirectory, cancel);

                alert.setTitle("Автозаполнение");
                alert.setHeaderText(null);

                alert.showAndWait().ifPresent(buttonType -> {
                    if (buttonType.equals(open)) openProject(projectFile);
                    else if(buttonType.equals(fillFromDirectory)) initializeFromDirectory();
                });
            }
            else initializeFromDirectory();
        }
    }

    public void openInExplorer(ActionEvent actionEvent) {
        try {
            if(project.hasWorkingDirectory() && project.getWorkingDirectory().exists())
                new ProcessBuilder("explorer.exe", project.getWorkingDirectory().getPath()).start();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void showWorkingDirectory(File workingDirectory){
        navigationFiles.clear();
        directoryPath.setText(workingDirectory.getPath());
        navigationFiles.addAll(new DirectoryDataLoader().loadNavigationFiles(workingDirectory, project.getTruck()));
    }

    private void showOnChangeDirectory(File dir){
        if(project.getTruck().hasAnyData()) {
            ButtonType contin = new ButtonType("Продолжить", ButtonBar.ButtonData.OK_DONE);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Рабочая директория будет изменена и текущие данные будут удалены.", contin, MyUtils.CANCEL);
            alert.setTitle("Подтверждение действия");
            alert.setHeaderText(null);
            alert.showAndWait().ifPresent(buttonType -> {
                if (buttonType.equals(contin)) {
                    project.getTruck().clear();
                    project.setWorkingDirectory(dir);
                }
            });
        }
        else project.setWorkingDirectory(dir);
    }

    private void initializeFromDirectory(){
        boolean hasAny = navigationFiles.stream().anyMatch(navigationFile ->
                navigationFile.getType() == NavigationFile.CABIN ||
                        navigationFile.getType() == NavigationFile.JUNCTION);

        if(hasAny){
            if(project.getTruck().hasAnyData()) {
                ButtonType contin = new ButtonType("Продолжить", ButtonBar.ButtonData.OK_DONE),
                        cancel = new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE);

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Перед автозаполением текущие данные будут удалены.", contin, cancel);
                alert.setTitle("Подтверждение действия");
                alert.setHeaderText(null);

                alert.showAndWait().ifPresent(buttonType -> {
                    if (buttonType.equals(contin)) {
                        project.getTruck().clear();
                        project.getTruck().initialize(project.getWorkingDirectory(), navigationFiles, project.getSetting());
                    }
                });
            }
            else project.getTruck().initialize(project.getWorkingDirectory(), navigationFiles, project.getSetting());
        }
    }

    private void openProject(File projectFile) {
        if(project.getTruck().hasAnyData()) {
            ButtonType contin = new ButtonType("Продолжить", ButtonBar.ButtonData.OK_DONE),
                    cancel = new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE);

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Перед открытием файла проекта текущие данные будут удалены.", contin, cancel);
            alert.setTitle("Подтверждение действия");
            alert.setHeaderText(null);

            alert.showAndWait().ifPresent(buttonType -> {
                if (buttonType.equals(contin)) {
                    try {
                        new ProjectOpener(projectFile).open(project);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        else {
            try {
                new ProjectOpener(projectFile).open(project);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void move(File directory) {
        if(directory.exists()) {
            showOnChangeDirectory(directory);
        }
        else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Указанный файл больше не достижим. Возможно, он находился на съемном носителе, который был извлечен.",
                    new ButtonType("Ок", ButtonBar.ButtonData.OK_DONE));
            alert.setTitle("Ошибка доступа к файлу");
            alert.setHeaderText(null);
            alert.show();
        }
    }
}
