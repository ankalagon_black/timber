package main.truck.controllers;

import com.sun.javafx.binding.BidirectionalBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import main.common.dialogs.ShowImageDialog;
import main.core.data_types.common.Truck;
import main.repository.image.ImageCache;
import main.truck.data_types.NavigationFile;
import main.truck.interfaces.Disposable;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class CabinController implements Disposable {
    public ImageView imageView;
    public Button removeCabin;
    public Label cabinFileName;
    public TextField truckNumberTextField;

    private Truck truck;
    private ObservableList<NavigationFile> navigationFiles;

    private ChangeListener<File> cabinChangeListener = (observable, oldValue, newValue) -> {
        showCabin(newValue);

        if(oldValue != null) {
            navigationFiles.stream().
                    filter(navigationFile -> navigationFile.getFile().equals(oldValue)).
                    findFirst().ifPresent(navigationFile -> navigationFile.setIsUsed(false));
        }

        if(newValue != null) {
            navigationFiles.stream().
                    filter(navigationFile -> navigationFile.getFile().equals(newValue)).
                    findFirst().ifPresent(navigationFile -> navigationFile.setIsUsed(true));
        }
    };

    private WeakChangeListener<File> weakCabinChangeListener = new WeakChangeListener<>(cabinChangeListener);

    private BidirectionalBinding binding;


    public void setController(Truck truck, ObservableList<NavigationFile> navigationFiles){
        this.truck = truck;
        this.navigationFiles = navigationFiles;

        showCabin(truck.getCabin());
        truckNumberTextField.setText(truck.getTruckNumber());

        binding = BidirectionalBinding.bind(truck.truckNumberProperty(), truckNumberTextField.textProperty());
        truck.cabinProperty().addListener(weakCabinChangeListener);
    }

    @Override
    public void dispose() {
        truck.cabinProperty().removeListener(weakCabinChangeListener);
    }


    public void onRemoveCabin(ActionEvent actionEvent) {
        truck.setCabin(null);
    }

    public void onRemoveAllMarkers(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.WARNING, "Вы действительно хотите удалить маркеры во всех стыках?",
                new ButtonType("Да", ButtonBar.ButtonData.OK_DONE), new ButtonType("Нет", ButtonBar.ButtonData.CANCEL_CLOSE));
        alert.setTitle("Подтверждение удаления");
        alert.setHeaderText(null);
        alert.showAndWait().ifPresent(buttonType -> {
            if(buttonType.getButtonData().equals(ButtonBar.ButtonData.OK_DONE)) truck.removeAllMarkers();
        });
    }

    public void onDragDropped(DragEvent dragEvent) {
        if(!dragEvent.isConsumed()) {
            Dragboard dragboard = dragEvent.getDragboard();
            if(dragboard.hasContent(NavigationFile.NAVIGATION_FILE_FORMAT)){
                NavigationFile navigationFile = (NavigationFile) dragboard.getContent(NavigationFile.NAVIGATION_FILE_FORMAT);

                truck.setCabin(navigationFile.getFile());

                dragEvent.setDropCompleted(true);
                dragEvent.consume();
            }
        }
    }

    public void onDragOver(DragEvent dragEvent) {
        if(!dragEvent.isConsumed()) {
            if (dragEvent.getDragboard().hasContent(NavigationFile.NAVIGATION_FILE_FORMAT)){
                dragEvent.acceptTransferModes(TransferMode.MOVE);
                dragEvent.consume();
            }
        }
    }

    public void onMouseClicked(MouseEvent mouseEvent) {
        if(truck.hasCabin() && !mouseEvent.isConsumed() &&
                mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2){
            try {
                new ShowImageDialog(truck.getCabin()).show();
            }
            catch (IOException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }


    private void showCabin(File cabin){
        if(cabin == null){
            imageView.setImage(null);
            cabinFileName.setText(null);
            removeCabin.setVisible(false);
        }
        else{
            try {
                imageView.setImage(ImageCache.get().get(cabin.getPath()));
            }
            catch (ExecutionException e) {
                e.printStackTrace();
            };
            cabinFileName.setText(cabin.getName());
            removeCabin.setVisible(true);
        }
    }
}
