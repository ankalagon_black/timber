package main.truck.controllers;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import main.common.dialogs.ShowImageDialog;
import main.repository.image.ImageCache;
import main.truck.data_types.NavigationFile;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class PreviewController {
    public VBox root;

    public Label preview;
    public ImageView imageView;
    public Label previewFileName;

    private File previewFile;

    public void setController(ListView<NavigationFile> navigationFilesListView) {
        navigationFilesListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null){
                File file = newValue.getFile();
                if(!file.isDirectory()) showPreview(file);
                else showPreview(null);

                this.previewFile = file;
            }
        });

        Platform.runLater(() -> {
            imageView.fitWidthProperty().bind(root.widthProperty());
            imageView.fitHeightProperty().bind(root.heightProperty().subtract(preview.heightProperty()).subtract(previewFileName.heightProperty()));
        });
    }


    public void onMouseClicked(MouseEvent mouseEvent) {
        if(previewFile != null && mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2){
            try{
                new ShowImageDialog(previewFile).show();
            }
            catch (IOException | ExecutionException e){
                e.printStackTrace();
            }
        }
    }


    private void showPreview(File previewFile){
        if(previewFile == null){
            imageView.setImage(null);
            previewFileName.setText(null);
        }
        else{
            try {
                imageView.setImage(ImageCache.get().get(previewFile.getPath()));
            }
            catch (ExecutionException e) {
                e.printStackTrace();
            }
            previewFileName.setText(previewFile.getName());
        }
    }

    //        Platform.runLater(() -> {
//            imageView.fitWidthProperty().bind(upperPane.widthProperty());
//            imageView.fitHeightProperty().bind(splitPane.heightProperty().subtract(upperPane.heightProperty()).
//                    subtract(preview.heightProperty()).subtract(previewFileName.heightProperty()).subtract(MARGIN));
//        });
}
