package main.truck.controllers.dialogs;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.WeakEventHandler;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import main.core.data_types.common.CubageType;
import main.core.data_types.common.Length;
import main.repository.loaders.TimberDataLoader;
import main.core.utils.MyUtils;

public class LoadingProgressController implements TimberDataLoader.Updatable {
    public DialogPane root;

    public Label cubageType;
    public Label length;
    public ProgressBar progressBar;

    public HBox errorRoot;
    public Label errorDescription;

    private TimberDataLoader timberDataLoader;
    private DialogInterface dialogInterface;


    private Disposable disposable = null;


    private EventHandler<ActionEvent> onClose = event -> {
        if(disposable != null) disposable.dispose();
    };

    private Consumer<Double> onNext = aDouble -> {
        progressBar.setProgress(aDouble);
    };

    private Consumer<Throwable> onError = throwable -> {
        disposable = null;
        errorRoot.setVisible(true);
        errorDescription.setText(String.format("Ошибка: %s", throwable.getMessage()));

        changeButtonToClose();
    };

    private Action onComplete = () -> {
        dialogInterface.updateDialogTitle("Загрузка завершена");

        changeButtonToClose();
    };


    public void setController(TimberDataLoader timberDataLoader, DialogInterface dialogInterface){
        this.timberDataLoader = timberDataLoader;
        this.dialogInterface = dialogInterface;

        root.lookupButton(MyUtils.CANCEL).addEventFilter(ActionEvent.ACTION, new WeakEventHandler<>(onClose));

        disposable = timberDataLoader.load(onNext, onError, onComplete, this);
    }

    private void changeButtonToClose(){
        root.getButtonTypes().clear();

        root.getButtonTypes().add(MyUtils.CLOSE);
        root.lookupButton(MyUtils.CLOSE).addEventFilter(ActionEvent.ACTION, new WeakEventHandler<>(onClose));
    }

    private void changeButtonToCancel(){
        root.getButtonTypes().clear();

        root.getButtonTypes().add(MyUtils.CANCEL);
        root.lookupButton(MyUtils.CANCEL).addEventFilter(ActionEvent.ACTION, new WeakEventHandler<>(onClose));
    }


    public void onRepeat(ActionEvent actionEvent) {
        errorRoot.setVisible(false);
        disposable = timberDataLoader.load(onNext, onError, onComplete, this);

        changeButtonToCancel();
    }

    @Override
    public void updateCubageType(CubageType cubageType) {
        Platform.runLater(() -> this.cubageType.setText(cubageType.getText()));
    }

    @Override
    public void updateLength(Length length) {
        Platform.runLater(() -> this.length.setText(length.getText()));
    }

    public interface DialogInterface{
        void updateDialogTitle(String str);
    }
}
