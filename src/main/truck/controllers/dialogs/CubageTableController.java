package main.truck.controllers.dialogs;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Classifier;
import main.core.data_types.settings.HttpsSettings;
import main.core.data_types.settings.LogParameters;
import main.core.data_types.settings.Settings;
import main.core.utils.MyUtils;
import main.repository.loaders.ClassifierLoader;
import main.repository.loaders.TimberDataLoader;
import main.repository.database.ClassifierDatabase;
import main.repository.database.CubageDatabase;
import main.repository.database.DatabaseManager;
import main.repository.network.NetworkManager;
import main.truck.data_types.proxys.CubageProxy;
import main.truck.data_types.proxys.CubageTypeProxy;
import main.truck.data_types.proxys.LengthProxy;
import main.truck.dialogs.LoadingProgressDialog;
import main.truck.views.cells.T2IntegerCell;
import main.truck.views.cells.T2LogicalValueCmCell;
import main.truck.views.cells.T2CubageCell;
import main.truck.views.cells.CubageTypeProxyCell;
import main.truck.views.cells.LengthProxyCell;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Comparator;

public class CubageTableController {

    public ListView<CubageTypeProxy> cubageTypeList;
    public ListView<LengthProxy> lengthsList;
    public TableView<CubageProxy> table;

    public TableColumn<CubageProxy, Number> cubageTypeColumn;
    public TableColumn<CubageProxy, LogicalValue> lengthColumn;
    public TableColumn<CubageProxy, LogicalValue> diameterColumn;
    public TableColumn<CubageProxy, LogicalValue> diameterStandardColumn;
    public TableColumn<CubageProxy, Number> cubageColumn;

    private Settings settings;
    private Classifier classifier;
    private DatabaseManager databaseManager;

    public void setController(Settings settings, Classifier classifier, DatabaseManager databaseManager) {
        this.settings = settings;
        this.classifier = classifier;
        this.databaseManager = databaseManager;

        try {
            CubageDatabase cubageDatabase = databaseManager.getCubageDatabase();
            ClassifierDatabase classifierDatabase = databaseManager.getClassifierDatabase();

            cubageTypeList.setCellFactory(param -> {
                try {
                    return new CubageTypeProxyCell(classifier);
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            });
            cubageTypeList.setItems(FXCollections.observableArrayList(classifierDatabase.getCubageTypes()));

            lengthsList.setCellFactory(param -> {
                try {
                    return new LengthProxyCell(classifier);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            });
            lengthsList.setItems(FXCollections.observableArrayList(classifierDatabase.getLengths()));

            cubageTypeColumn.setCellValueFactory(param -> new SimpleIntegerProperty(param.getValue().getCubageType()));
            cubageTypeColumn.setCellFactory(param -> new T2IntegerCell<>());

            lengthColumn.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getLength()));
            lengthColumn.setCellFactory(param -> new T2LogicalValueCmCell<>());
            lengthColumn.setComparator(Comparator.comparingDouble(LogicalValue::getValue));

            diameterColumn.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getDiameter()));
            diameterColumn.setCellFactory(param -> new T2LogicalValueCmCell<>());
            diameterColumn.setComparator(Comparator.comparingDouble(LogicalValue::getValue));

            diameterStandardColumn.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getDiameterStandard()));
            diameterStandardColumn.setCellFactory(param -> new T2LogicalValueCmCell<>());
            diameterStandardColumn.setComparator(Comparator.comparingDouble(LogicalValue::getValue));

            cubageColumn.setCellValueFactory(param -> new SimpleDoubleProperty(param.getValue().getCubage()));
            cubageColumn.setCellFactory(param -> new T2CubageCell<>());

            table.setItems(FXCollections.observableArrayList(cubageDatabase.getCubageTable()));

            cubageDatabase.close();
            classifierDatabase.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void onLoadCubageTable(ActionEvent actionEvent) {
        LogParameters logParameters = settings.getLogParameters();
        if(logParameters.areLogBoundsCorrect()) {
            try {
                HttpsSettings httpsSettings = settings.getHttpsSettings();

                CubageDatabase cubageDatabase = databaseManager.getCubageDatabase();
                cubageDatabase.clear();

                NetworkManager networkManager = new NetworkManager();

                TimberDataLoader timberDataLoader = new TimberDataLoader(networkManager.getTimberDataInterface(httpsSettings),
                        cubageDatabase,
                        classifier,
                        logParameters,
                        cubageTypeList.getItems().filtered(CubageTypeProxy::isSelected),
                        lengthsList.getItems().filtered(LengthProxy::isSelected));

                LoadingProgressDialog loadingProgressDialog = new LoadingProgressDialog(timberDataLoader);
                loadingProgressDialog.showAndWait();

                table.setItems(FXCollections.observableArrayList(cubageDatabase.getCubageTable()));

                cubageDatabase.close();
            }
            catch (SQLException | NoSuchAlgorithmException | IOException | KeyManagementException e) {
                e.printStackTrace();
            }
        }
        else {
            showMinMaxWarning();
        }
    }

    public void showMinMaxWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING, "Минимальный диаметр бревна не может быть больше максимального диаметра бревна.");
        alert.setHeaderText(null);
        alert.setTitle("Внимание");
        alert.showAndWait();
    }

    public void saveListsChanges() {
        try {
            ClassifierDatabase classifierDatabase = databaseManager.getClassifierDatabase();

            classifierDatabase.setCubageTypes(cubageTypeList.getItems());
            classifierDatabase.setLengths(lengthsList.getItems());

            classifierDatabase.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void onDeleteCubageTable(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Вы действительно хотите удалить кубатурник?", MyUtils.YES, MyUtils.NO);
        alert.setTitle("Подтверждение удаления");
        alert.setHeaderText(null);
        alert.showAndWait().ifPresent(buttonType -> {
            if (buttonType.equals(MyUtils.YES)) {
                CubageDatabase cubageDatabase = null;
                try {
                    cubageDatabase = databaseManager.getCubageDatabase();
                    cubageDatabase.clear();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
                finally {
                    if(cubageDatabase != null) cubageDatabase.close();
                }
                table.getItems().clear();
            }
        });
    }

    public void loadClassifier(ActionEvent actionEvent) {
        ClassifierLoader classifierLoader = new ClassifierLoader();
        classifierLoader.load(classifier, settings);

        ClassifierDatabase classifierDatabase = null;
        try {
            classifierDatabase = databaseManager.getClassifierDatabase();

            cubageTypeList.setItems(FXCollections.observableArrayList(classifierDatabase.getCubageTypes()));
            lengthsList.setItems(FXCollections.observableArrayList(classifierDatabase.getLengths()));
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(classifierDatabase != null) classifierDatabase.close();
        }
    }
}
