package main.truck.controllers.dialogs;

import javafx.collections.FXCollections;
import javafx.scene.control.*;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Classifier;
import main.core.data_types.common.CubageType;
import main.core.data_types.common.Length;
import main.core.data_types.settings.HttpsSettings;
import main.core.data_types.settings.LogParameters;
import main.core.data_types.settings.Settings;
import main.core.utils.strings.DoubleFilter;
import main.core.utils.strings.DoublePrecisionStringConverter;
import main.truck.views.cells.CubageTypeCell;
import main.truck.views.cells.LengthCell;

public class SettingsController {
    public ToggleGroup group;
    public RadioButton markersType;
    public RadioButton circleMarkersType;

    public TextField ipAddress;
    public TextField gateway;

    public TextField minLogDiameter;
    public TextField maxLogDiameter;

    public ComboBox<CubageType> cubageType;
    public ComboBox<Length> logLength;

    private TextFormatter<Double> minDiameterFormatter;
    private TextFormatter<Double> maxDiameterFormatter;

    public void setController(Settings settings, Classifier classifier) {

        switch (settings.getJunctionMode()) {
            case Settings.MARKERS:
                markersType.setSelected(true);
                break;
            case Settings.CIRCLE_MARKERS:
                circleMarkersType.setSelected(true);
                break;
        }
        group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.equals(markersType)) settings.setJunctionMode(Settings.MARKERS);
            else if(newValue.equals(circleMarkersType)) settings.setJunctionMode(Settings.CIRCLE_MARKERS);
        });

        LogParameters logParameters = settings.getLogParameters();

        minDiameterFormatter = new TextFormatter<>(new DoublePrecisionStringConverter(1), logParameters.getMinDiameter().getValueInCm(), new DoubleFilter());
        minDiameterFormatter.valueProperty().addListener((observable, oldValue, newValue) -> {
            logParameters.setMinDiameter(new LogicalValue(newValue, LogicalValue.CENTIMETERS));
        });

        maxDiameterFormatter = new TextFormatter<>(new DoublePrecisionStringConverter(1), logParameters.getMaxDiameter().getValueInCm(), new DoubleFilter());
        maxDiameterFormatter.valueProperty().addListener((observable, oldValue, newValue) -> {
            logParameters.setMaxDiameter(new LogicalValue(newValue, LogicalValue.CENTIMETERS));
        });

        minLogDiameter.setTextFormatter(minDiameterFormatter);
        maxLogDiameter.setTextFormatter(maxDiameterFormatter);

        cubageType.setCellFactory(param -> new CubageTypeCell());
        cubageType.setItems(classifier.getCubageTypes());

        cubageType.setButtonCell(new CubageTypeCell());
        if(logParameters.hasCubageType()) cubageType.getSelectionModel().select(classifier.getCubageTypeById(logParameters.getCubageType()));

        cubageType.valueProperty().addListener((observable, oldValue, newValue) -> {
            logParameters.setCubageType(newValue != null ? newValue.getId() : null);
        });


        logLength.setCellFactory(param -> new LengthCell());
        logLength.setItems(classifier.getLengths());

        logLength.setButtonCell(new LengthCell());
        if(logParameters.hasLength()) logLength.getSelectionModel().select(classifier.getLengthByLogicalValue(logParameters.getLength()));

        logLength.valueProperty().addListener((observable, oldValue, newValue) -> {
            logParameters.setLength(newValue != null ? newValue.getLength() : null);
        });

        HttpsSettings httpsSettings = settings.getHttpsSettings();

        ipAddress.setText(httpsSettings.getIp());
        ipAddress.textProperty().addListener((observable, oldValue, newValue) -> {
            httpsSettings.setIp(newValue);
        });

        gateway.setText(httpsSettings.getGateway());
        gateway.textProperty().addListener((observable, oldValue, newValue) -> {
            httpsSettings.setGateway(newValue);
        });
    }
}
