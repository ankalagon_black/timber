package main.truck.controllers.dialogs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.DialogPane;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.VBox;
import main.core.data_types.common.Project;
import main.core.data_types.common.Truck;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.junctions.MarkersJunction;
import main.core.data_types.settings.Settings;
import main.core.utils.MyUtils;
import main.repository.database.DatabaseManager;

import java.util.ArrayList;
import java.util.List;

public class SettingsContainerController {
    public DialogPane root;

    public VBox settingsPart;
    public SettingsController settingsPartController;

    public SplitPane cubage;
    public CubageTableController cubageController;

    private Project project;
    private Settings copy;

    private EventHandler<ActionEvent> onSave = event -> {
        if(copy.getLogParameters().areLogBoundsCorrect()) {
            int oldValue = project.getSetting().getJunctionMode(),
                    newValue = copy.getJunctionMode();

            if (oldValue != newValue) {
                Truck truck = project.getTruck();
                switch (oldValue) {
                    case Settings.MARKERS:
                        List<MarkersJunction> markersJunctions = truck.getJunctions().stream().
                                collect(ArrayList::new, (junctions, junction) -> junctions.add((MarkersJunction) junction), ArrayList::addAll);
                        switch (newValue) {
                            case Settings.CIRCLE_MARKERS:
                                truck.setJunctions(markersJunctions.stream().collect(ArrayList::new, (junctions, junction) ->
                                        junctions.add(new CircleMarkersJunction(junction)), ArrayList::addAll));
                                break;
                        }
                        break;
                    case Settings.CIRCLE_MARKERS:
                        List<CircleMarkersJunction> circleMarkersJunctions = truck.getJunctions().stream().
                                collect(ArrayList::new, (junctions, junction) -> junctions.add((CircleMarkersJunction) junction), ArrayList::addAll);
                        switch (newValue) {
                            case Settings.MARKERS:
                                truck.setJunctions(circleMarkersJunctions.stream().collect(ArrayList::new, (junctions, junction) ->
                                        junctions.add(new MarkersJunction(junction)), ArrayList::addAll));
                                break;
                        }
                        break;
                }
            }
            cubageController.saveListsChanges();
            project.setSetting(copy);
        }
        else {
            cubageController.showMinMaxWarning();
            event.consume();
        }
    };

    public void setController(Project project) {
        this.project = project;
        copy = new Settings(project.getSetting());

        settingsPartController.setController(copy, project.getClassifier());
        cubageController.setController(copy, project.getClassifier(), new DatabaseManager());

        root.lookupButton(MyUtils.SAVE).addEventFilter(ActionEvent.ACTION, onSave);
    }
}
