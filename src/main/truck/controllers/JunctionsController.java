package main.truck.controllers;

import impl.org.controlsfx.skin.GridViewSkin;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.WeakListChangeListener;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.settings.Settings;
import main.truck.data_types.NavigationFile;
import main.truck.interfaces.Disposable;
import main.truck.interfaces.JunctionOpenable;
import main.truck.interfaces.OutputJunctionData;
import main.truck.views.cells.JunctionCell;
import org.controlsfx.control.GridView;

import java.io.IOException;
import java.util.function.Consumer;


public class JunctionsController implements Disposable {
    public GridView<AbstractJunction> junctionsGridView;

    private ObservableList<NavigationFile> navigationFiles;
    private Settings settings;

    private ListChangeListener<AbstractJunction> junctionListChangeListener = c -> {
        boolean isStructural = false;
        while (c.next()){
            if(c.wasAdded()){
                c.getAddedSubList().forEach(junction -> {
                    navigationFiles.stream().filter(navigationFile -> navigationFile.getFile().equals(junction.getOriginalFile())).
                            findFirst().ifPresent(navigationFile -> navigationFile.setIsUsed(true));
                });
                isStructural = true;
            }
            else if(c.wasRemoved()){
                c.getRemoved().forEach((Consumer<AbstractJunction>) junction -> {
                    navigationFiles.stream().filter(navigationFile -> navigationFile.getFile().equals(junction.getOriginalFile())).
                            findFirst().ifPresent(navigationFile -> navigationFile.setIsUsed(false));
                });
                isStructural = true;
            }
        }

        if(isStructural) {
            GridViewSkin gridViewSkin = (GridViewSkin) junctionsGridView.getSkin();
            if(gridViewSkin != null) (gridViewSkin).updateGridViewItems();
        }
    };
    private WeakListChangeListener<AbstractJunction> junctionWeakListChangeListener = new WeakListChangeListener<>(junctionListChangeListener);

    public void setController(ObservableList<AbstractJunction> junctions, ObservableList<NavigationFile> navigationFiles,
                              JunctionOpenable junctionOpenable, OutputJunctionData outputJunctionData, Settings settings) {
        junctionsGridView.setItems(junctions);
        this.navigationFiles = navigationFiles;
        this.settings = settings;

        junctionsGridView.setCellFactory(param -> {
            try {
                return new JunctionCell(junctionOpenable, outputJunctionData);
            }
            catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        });

        junctionsGridView.getItems().addListener(junctionWeakListChangeListener);
    }

    @Override
    public void dispose() {
        junctionsGridView.getItems().removeListener(junctionWeakListChangeListener);
    }


    public void onDragDropped(DragEvent dragEvent) {
        if(!dragEvent.isConsumed()) {
            Dragboard dragboard = dragEvent.getDragboard();
            if(dragboard.hasContent(NavigationFile.NAVIGATION_FILE_FORMAT)) {
                NavigationFile navigationFile = (NavigationFile) dragboard.getContent(NavigationFile.NAVIGATION_FILE_FORMAT);

                junctionsGridView.getItems().add(AbstractJunction.toJunction(navigationFile, settings));

                dragEvent.setDropCompleted(true);
                dragEvent.consume();
            }
        }
    }

    public void onDragOver(DragEvent dragEvent) {
        if(!dragEvent.isConsumed()) {
            if (dragEvent.getDragboard().hasContent(NavigationFile.NAVIGATION_FILE_FORMAT)){
                dragEvent.acceptTransferModes(TransferMode.MOVE);
                dragEvent.consume();
            }
        }
    }
}
