package main.truck.controllers.views;

import javafx.beans.value.ChangeListener;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import main.common.dialogs.ShowImageDialog;
import main.truck.data_types.NavigationFile;
import main.truck.interfaces.MovableToDirectory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

public class NavigationFileController {

    private MovableToDirectory movableToDirectory;

    public void setController(MovableToDirectory movableToDirectory) {
        this.movableToDirectory = movableToDirectory;
    }

    public HBox root;
    public ImageView imageView;
    public Label name;

    private NavigationFile navigationFile = null;

    private ChangeListener<Boolean> changeListener = (observable, oldValue, newValue) -> {
        setIsUsed(newValue);
    };


    public void setNavigationFile(NavigationFile navigationFile) {
        this.navigationFile = navigationFile;

        setIsUsed(navigationFile.isUsed());

        File file = navigationFile.getFile();
        try {
            String path = file.isDirectory() ? "/main/truck/resources/icons/ic_folder_black_24dp.png" : "/main/truck/resources/icons/ic_image_black_24dp.png";
            InputStream inputStream = getClass().getResourceAsStream(path);
            imageView.setImage(new Image(inputStream));
            inputStream.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        name.setText(file.getName());

        navigationFile.isUsedProperty().addListener(changeListener);
    }

    public void reset() {
        if(navigationFile != null) {
            navigationFile.isUsedProperty().removeListener(changeListener);

            setIsUsed(false);
            imageView.setImage(null);
            name.setText(null);
        }
    }


    private void setIsUsed(boolean isUsed){
        if(isUsed) root.setStyle("-fx-background-color: derive(seagreen, 50%);");
        else root.setStyle(null);
    }

    public void onDragDetected(MouseEvent mouseEvent) {
        if (navigationFile != null && !navigationFile.getFile().isDirectory() && !navigationFile.isUsed()) {
            Dragboard dragboard = root.startDragAndDrop(TransferMode.MOVE);

            ClipboardContent content = new ClipboardContent();
            content.put(NavigationFile.NAVIGATION_FILE_FORMAT, navigationFile);
            dragboard.setContent(content);

            mouseEvent.consume();
        }
    }

    public void onClick(MouseEvent mouseEvent) {
        if (navigationFile != null && mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2){
            File file = navigationFile.getFile();

            if(file.isDirectory()) movableToDirectory.move(file);
            else {
                try {
                    new ShowImageDialog(file).showAndWait();
                }
                catch (IOException | ExecutionException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
