package main.truck.controllers.views;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.WeakListChangeListener;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.markers.Marker;
import main.repository.image.ImageCache;
import main.truck.interfaces.OutputJunctionData;
import main.truck.interfaces.JunctionOpenable;
import main.truck.interfaces.Removable;

import java.util.concurrent.ExecutionException;

public class JunctionController {

    private Removable<AbstractJunction> removable;
    private JunctionOpenable openable;
    private OutputJunctionData outputJunctionData;

    public void setController(Removable<AbstractJunction> removable,
                              JunctionOpenable openable,
                              OutputJunctionData outputJunctionData) {
        this.removable = removable;
        this.openable = openable;
        this.outputJunctionData = outputJunctionData;
    }


    public ImageView imageView;
    public Label originalFileName;
    public TextField junctionNumber;
    public Label markersCount;


    private AbstractJunction<Marker> junction;


    private ListChangeListener<Marker> listChangeListener = c -> {
        boolean structural = false;
        while (c.next()){
            if (c.wasRemoved() || c.wasAdded()){
                structural = true;
                break;
            }
        }

        if(structural) showMarkersCount();
    };
    private WeakListChangeListener<Marker> weakListChangeListener = new WeakListChangeListener<>(listChangeListener);

    private ChangeListener<String> numberChangeListener = (observable, oldValue, newValue) -> {
        junction.setNumber(newValue);
    };
    private WeakChangeListener<String> numberWeakChangeListener = new WeakChangeListener<>(numberChangeListener);

    public void setJunction(AbstractJunction<Marker> junction) {
        this.junction = junction;

        try {
            imageView.setImage(ImageCache.get().get(junction.getCurrentSource().getPath()));
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }

        originalFileName.setText(junction.getOriginalFile().getName());
        junctionNumber.setText(junction.getNumber());

        showMarkersCount();

        junction.getMarkers().addListener(weakListChangeListener);
        junctionNumber.textProperty().addListener(numberWeakChangeListener);
    }

    public void reset() {
        if(junction != null) {
            junction.getMarkers().removeListener(weakListChangeListener);
            junctionNumber.textProperty().removeListener(numberWeakChangeListener);

            imageView.setImage(null);
            originalFileName.setText(null);
            junctionNumber.setText(null);
            markersCount.setText(null);
        }
    }


    private void showMarkersCount(){
        markersCount.setText(String.format("Кол-во маркеров: %d", junction.getMarkers().size()));
    }


    public void onClick(MouseEvent mouseEvent) {
        if (mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2) openable.openJunction(junction);
    }

    public void onDelete(ActionEvent mouseEvent) {
        removable.remove(junction);
    }

    public void onOutputJunctionData(ActionEvent actionEvent) {
        outputJunctionData.output(junction);
    }

    public void onRemoveMarkers(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.WARNING, "Вы действительно хотите удалить все маркеры данного стыка?", new ButtonType("Да", ButtonBar.ButtonData.OK_DONE),
                new ButtonType("Нет", ButtonBar.ButtonData.CANCEL_CLOSE));
        alert.setTitle("Удаление маркеров");
        alert.setHeaderText(null);
        alert.showAndWait().ifPresent(buttonType -> {
            if(buttonType.getButtonData().equals(ButtonBar.ButtonData.OK_DONE)) junction.removeMarkers();
        });
    }


}
