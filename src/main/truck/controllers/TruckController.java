package main.truck.controllers;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import main.common.dialogs.OutputJunctionDialog;
import main.core.data_types.common.Project;
import main.core.data_types.junctions.AbstractJunction;
import main.repository.CommandLineArgsParser;
import main.repository.loaders.ClassifierLoader;
import main.repository.xml.project.ProjectOpener;
import main.repository.xml.project.ProjectSaver;
import main.truck.data_types.NavigationFile;
import main.truck.dialogs.SettingsDialog;
import main.truck.interfaces.Disposable;
import main.truck.interfaces.JunctionOpenable;
import main.truck.interfaces.OutputJunctionData;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Created by Константин on 04.09.2017.
 */
public class TruckController implements Disposable, OutputJunctionData{
    public BorderPane root;

    public JunctionsController junctionsController;
    public VBox junctions;

    public CabinController cabinController;
    public VBox cabin;

    public NavigationController navigationController;
    public VBox navigation;

    public PreviewController previewController;
    public VBox preview;


    private Project project;
    private ObservableList<NavigationFile> navigationFiles = FXCollections.observableArrayList();


    public void setController(Project project, JunctionOpenable junctionOpenable){
        initialize(project, junctionOpenable);

        ClassifierLoader loader = new ClassifierLoader();
        loader.load(project.getClassifier(), project.getSetting());
    }

    public void setController(Project project, JunctionOpenable junctionOpenable, Application.Parameters parameters){
        initialize(project, junctionOpenable);

        CommandLineArgsParser commandLineArgsParser = new CommandLineArgsParser(parameters);

        File projectFile = commandLineArgsParser.getProjectFile();
        if(projectFile != null){
            try {
                new ProjectOpener(projectFile).open(project);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        File wd = commandLineArgsParser.getWorkingDirectory();
        if(wd != null) project.setWorkingDirectory(wd);


        File classifier = commandLineArgsParser.getClassifier();
        if(classifier != null) {
            ClassifierLoader loader = new ClassifierLoader(classifier);
            loader.load(project.getClassifier(), project.getSetting());
        }
        else {
            ClassifierLoader loader = new ClassifierLoader();
            loader.load(project.getClassifier(), project.getSetting());
        }
    }

    private void initialize(Project project, JunctionOpenable junctionOpenable) {
        this.project = project;

        junctionsController.setController(project.getTruck().getJunctions(), navigationFiles, junctionOpenable,this, project.getSetting());
        cabinController.setController(project.getTruck(), navigationFiles);
        navigationController.setController(project, navigationFiles);
        previewController.setController(navigationController.getNavigationFilesListView());
    }

    @Override
    public void dispose() {
        cabinController.dispose();
        navigationController.dispose();
        junctionsController.dispose();
    }


    public void onOpenProject(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Открыть проект");

        if(project.hasWorkingDirectory()) fileChooser.setInitialDirectory(project.getWorkingDirectory());

        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("TPF файлы", "*.tpf"));

        File file = fileChooser.showOpenDialog(root.getScene().getWindow());
        if(file != null) {
            try {
              new ProjectOpener(file).open(project);
            }
            catch (Exception e){
                e.printStackTrace();

                Alert alert = new Alert(Alert.AlertType.ERROR, "Во время открытия файла произошла ошибка: " + e.getMessage(),
                        new ButtonType("Ок", ButtonBar.ButtonData.OK_DONE));
                alert.setTitle("Ошбика открытия");
                alert.setHeaderText(null);
                alert.show();
            }
        }
    }

    public void onSaveProject(ActionEvent actionEvent) {
        if (!project.getTruck().canBeSaved()){
            Alert alert = new Alert(Alert.AlertType.WARNING, "Для сохранения воз должен содержать: кабину, номер и хотя бы один стык",
                    new ButtonType("Ок", ButtonBar.ButtonData.OK_DONE));
            alert.setTitle("Ошибка сохранения.");
            alert.setHeaderText(null);
            alert.show();
            return;
        }

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Сохранить проект");

        if(project.hasWorkingDirectory()) fileChooser.setInitialDirectory(project.getWorkingDirectory());

        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("TPF файлы", "*.tpf"));

        fileChooser.setInitialFileName("truck");

        File file = fileChooser.showSaveDialog(root.getScene().getWindow());
        if (file != null) {
            try {
                new ProjectSaver(file).save(project);
            }
            catch (Exception e) {
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR, "Во время сохранения файла произошла ошибка: " + e.getMessage(),
                        new ButtonType("Ок", ButtonBar.ButtonData.OK_DONE));
                alert.setTitle("Ошибка сохранения");
                alert.setHeaderText(null);
                alert.show();
            }
        }
    }

    public void onSettings(ActionEvent actionEvent) {
        try {
            SettingsDialog settingsDialog = new SettingsDialog(project);
            settingsDialog.setResizable(true);
            settingsDialog.showAndWait();
        }
        catch (IOException e) {
           e.printStackTrace();
        }
    }

    public void onClearProject(ActionEvent actionEvent) {
        if(project.getTruck().hasAnyData()) {
            ButtonType yes = new ButtonType("Да", ButtonBar.ButtonData.OK_DONE), no = new ButtonType("Нет", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(Alert.AlertType.WARNING, "Вы действительно хотите удалить текущие данные?", yes, no);
            alert.setTitle("Подтверждение удаления");
            alert.setHeaderText(null);
            alert.showAndWait().ifPresent(buttonType -> {
                if (buttonType.equals(yes)) project.getTruck().clear();
            });
        }
    }

    public void onExit(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void onAboutProgram(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("О программе");
        alert.setHeaderText(null);
        alert.setContentText("Версия программы: 2.2.9 от 26.05.2018");

        alert.showAndWait();
    }

    @Override
    public void output(AbstractJunction junction) {
        if(junction.hasNumber() && project.getTruck().hasTruckNumber()) {
            try {
                OutputJunctionDialog outputJunctionDialog = new OutputJunctionDialog(junction, project);
                outputJunctionDialog.showAndWait();
            }
            catch (ExecutionException | IOException e) {
                e.printStackTrace();
            }
        }
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Номер стыка и номер воза должены быть заполнены перед выводом данных.");
            alert.show();
        }
    }
}