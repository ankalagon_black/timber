package main.truck.interfaces;

public interface Disposable {
    void dispose();
}
