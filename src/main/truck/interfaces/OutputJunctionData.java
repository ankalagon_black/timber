package main.truck.interfaces;

import main.core.data_types.junctions.AbstractJunction;

public interface OutputJunctionData {
    void output(AbstractJunction junction);
}
