package main.truck.interfaces;

import main.core.data_types.junctions.AbstractJunction;

public interface JunctionOpenable {
    void openJunction(AbstractJunction junction);
}
