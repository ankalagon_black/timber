package main.truck.interfaces;

import main.core.data_types.junctions.AbstractJunction;

public interface JunctionBackable {
    void backFromJunction(AbstractJunction junction);
}
