package main.truck.interfaces;

public interface Removable<T> {
    void remove(T object);
}
