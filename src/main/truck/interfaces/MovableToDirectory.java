package main.truck.interfaces;

import java.io.File;

public interface MovableToDirectory {
    void move(File directory);
}
