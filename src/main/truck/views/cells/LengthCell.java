package main.truck.views.cells;

import javafx.scene.control.ListCell;
import main.core.data_types.common.Length;

public class LengthCell extends ListCell<Length> {

    @Override
    protected void updateItem(Length item, boolean empty) {
        super.updateItem(item, empty);

        if(empty) setText(null);
        else setText(item.getText());
    }
}
