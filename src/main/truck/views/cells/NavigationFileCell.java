package main.truck.views.cells;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import main.truck.controllers.views.NavigationFileController;
import main.truck.data_types.NavigationFile;
import main.truck.interfaces.MovableToDirectory;
import java.io.IOException;

public class NavigationFileCell extends ListCell<NavigationFile> {
    private Node node;
    private NavigationFileController controller;

    public NavigationFileCell(MovableToDirectory movableToDirectory) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/main/truck/resources/fxml/views/navigation_file.fxml"));

        node = loader.load();
        controller = loader.getController();
        controller.setController(movableToDirectory);
    }

    @Override
    protected void updateItem(NavigationFile item, boolean empty) {
        super.updateItem(item, empty);

        if(empty || item == null){
            controller.reset();
            setGraphic(null);
        }
        else {
            controller.setNavigationFile(item);
            setGraphic(node);
        }
    }
}
