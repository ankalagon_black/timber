package main.truck.views.cells;

import javafx.scene.control.TableCell;
import main.core.data_types.base.LogicalValue;
import main.truck.data_types.proxys.CubageProxy;

import java.util.Locale;

public class T2LogicalValueCmCell<T> extends TableCell<T, LogicalValue> {
    @Override
    protected void updateItem(LogicalValue item, boolean empty) {
        super.updateItem(item, empty);
        if(empty) setText(null);
        else setText(String.format(Locale.US, "%.1f", item.getValueInCm()));
    }
}
