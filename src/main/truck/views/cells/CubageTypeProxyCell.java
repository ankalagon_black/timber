package main.truck.views.cells;

import javafx.beans.value.ChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListCell;
import main.core.data_types.common.Classifier;
import main.core.data_types.settings.LogParameters;
import main.truck.data_types.proxys.CubageTypeProxy;

import java.io.IOException;

public class CubageTypeProxyCell extends ListCell<CubageTypeProxy> {

    private Classifier classifier;

    private CheckBox checkBox;

    private ChangeListener<Boolean> changeListener = null;

    public CubageTypeProxyCell(Classifier classifier) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/main/truck/resources/fxml/dialogs/check_box.fxml"));

        this.classifier = classifier;

        checkBox = fxmlLoader.load();
    }


    @Override
    protected void updateItem(CubageTypeProxy item, boolean empty) {
        super.updateItem(item, empty);

        if(empty) {
            if(changeListener != null) checkBox.selectedProperty().removeListener(changeListener);
            setGraphic(null);
        }
        else{
            checkBox.setText(classifier.getCubageTypeById(item.getCubageType()).getText());
            checkBox.setSelected(item.isSelected());

            changeListener = (observable, oldValue, newValue) -> {
                item.setSelected(newValue);
            };
            checkBox.selectedProperty().addListener(changeListener);

            setGraphic(checkBox);
        }
    }
}
