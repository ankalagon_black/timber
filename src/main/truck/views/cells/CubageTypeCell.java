package main.truck.views.cells;

import javafx.scene.control.ListCell;
import main.core.data_types.common.CubageType;
import main.core.data_types.settings.LogParameters;

public class CubageTypeCell extends ListCell<CubageType> {

    @Override
    protected void updateItem(CubageType item, boolean empty) {
        super.updateItem(item, empty);

        if(empty) setText(null);
        else setText(item.getText());
    }
}
