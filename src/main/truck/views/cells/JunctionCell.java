package main.truck.views.cells;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import main.core.data_types.junctions.AbstractJunction;
import main.truck.interfaces.JunctionOpenable;
import main.truck.interfaces.OutputJunctionData;
import main.truck.interfaces.Removable;
import main.truck.controllers.views.JunctionController;
import org.controlsfx.control.GridCell;

import java.io.IOException;

public class JunctionCell extends GridCell<AbstractJunction> implements Removable<AbstractJunction>{
    private Node node;
    private JunctionController controller;

    public JunctionCell(JunctionOpenable openable, OutputJunctionData outputJunctionData) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/main/truck/resources/fxml/views/junction.fxml"));

        node = fxmlLoader.load();
        controller = fxmlLoader.getController();
        controller.setController(this, openable, outputJunctionData);
    }

    @Override
    protected void updateItem(AbstractJunction item, boolean empty) {
        super.updateItem(item, empty);

        if(empty || item == null){
            controller.reset();
            setGraphic(null);
        }
        else {
            controller.setJunction(item);
            setGraphic(node);
        }
    }

    @Override
    public void remove(AbstractJunction junction) {
        getGridView().getItems().remove(junction);
    }
}
