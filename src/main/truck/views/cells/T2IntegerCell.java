package main.truck.views.cells;


import javafx.scene.control.TableCell;


public class T2IntegerCell<T> extends TableCell<T, Number> {

    @Override
    protected void updateItem(Number item, boolean empty) {
        super.updateItem(item, empty);
        if(empty) setText(null);
        else setText(String.valueOf(item.intValue()));
    }
}
