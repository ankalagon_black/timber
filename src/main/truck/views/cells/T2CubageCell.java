package main.truck.views.cells;

import javafx.scene.control.TableCell;
import main.truck.data_types.proxys.CubageProxy;

import java.util.Locale;

public class T2CubageCell<T> extends TableCell<T, Number> {
    @Override
    protected void updateItem(Number item, boolean empty) {
        super.updateItem(item, empty);
        if(empty) setText(null);
        else setText(String.format(Locale.US, "%.6f", item.doubleValue()));
    }
}
