package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import main.core.data_types.common.Project;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.junctions.MarkersJunction;
import main.core.data_types.settings.Settings;
import main.junctions.controllers.main.CircleMarkersJunctionController;
import main.junctions.controllers.main.MarkersJunctionController;
import main.repository.image.ImageCache;
import main.repository.loaders.ClassifierLoader;
import main.truck.controllers.TruckController;
import main.truck.interfaces.JunctionBackable;
import main.truck.interfaces.Disposable;
import main.truck.interfaces.JunctionOpenable;

import java.io.IOException;

public class Main extends Application implements JunctionOpenable, JunctionBackable {
    private Stage primaryStage;

    private Project project = new Project();
    private Disposable disposable = null;

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Timber 2");
        this.primaryStage.setMinWidth(640);
        this.primaryStage.setMinHeight(480);

        //LogicalRuler logicalRuler = new LogicalRuler(200, 1500, new Point2D(436, 1820), new Point2D(1500, 1841),
        //        new LogicalValue(3));

        //CircleMarkerSettingsWrapper circleMarkerSettings = new CircleMarkerSettingsWrapper(Color.RED, 14, Color.RED, true, true);

        //LogParameters logParameters = new LogParameters();

        //Image image = new Image(MyUtils.getLocalURL("C:\\Users\\Константин\\Documents\\Работа\\Проект (о предприятии)\\error\\forTests\\4.jpeg"));

        //WoodLogsDetectionDialog dialog = new WoodLogsDetectionDialog(image, logicalRuler, logParameters, circleMarkerSettings);
        //dialog.setWidth(1800);
        //dialog.setResizable(true);
        //dialog.showAndWait();
        //primaryStage.setScene(new Scene(testLoader.getRoot()));

        setTruckScene(true);


        //project.getSetting().setUseDiameterMarkers(true);
        //setJunctionScene(new MarkersJunction(new FileXML("C:/Users/Константин/Downloads/408782f2d538dd544d42fa85c1a54749.jpg"), "some_number"));

        this.primaryStage.show();
        //--open="C:\Users\Константин\Documents\Работа\Проект (о предприятии)\Фото\10 4, 27-07-2017 00-20\truck.tpf"
        //--classifier="classifier_2.xml"
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void setTruckScene(boolean initial) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/main/truck/resources/fxml/truck.fxml"));

        Parent root = loader.load();
        TruckController controller = loader.getController();

        if(disposable != null) disposable.dispose();
        disposable = controller;

        if(initial) {
            controller.setController(project, this, getParameters());
            primaryStage.setScene(new Scene(root));
        }
        else {
            controller.setController(project, this);
            primaryStage.setScene(new Scene(root, primaryStage.getScene().getWidth(), primaryStage.getScene().getHeight()));
            primaryStage.setOnCloseRequest(null);
        }
    }

    private void setJunctionScene(AbstractJunction abstractJunction) throws IOException {
        FXMLLoader loader = new FXMLLoader();

        if (disposable != null) disposable.dispose();

        Parent root = null;
        switch (project.getSetting().getJunctionMode()) {
            case Settings.MARKERS:
                loader.setLocation(getClass().getResource("/main/junctions/resources/fxml/main/markers_junction.fxml"));

                root = loader.load();
                MarkersJunctionController markersJunctionController = loader.getController();

                markersJunctionController.setController(((MarkersJunction) abstractJunction), project, this);

                disposable = markersJunctionController;
                break;
            case Settings.CIRCLE_MARKERS:
                loader.setLocation(getClass().getResource("/main/junctions/resources/fxml/main/circle_markers_junction.fxml"));

                root = loader.load();
                CircleMarkersJunctionController circleMarkersJunctionController = loader.getController();

                circleMarkersJunctionController.setController(((CircleMarkersJunction) abstractJunction), project, this);

                disposable = circleMarkersJunctionController;
                break;
        }

        primaryStage.setScene(new Scene(root, primaryStage.getScene().getWidth(), primaryStage.getScene().getHeight()));
        primaryStage.setOnCloseRequest(event -> {
            ButtonType yes = new ButtonType("Да", ButtonBar.ButtonData.OK_DONE), no = new ButtonType("Нет", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(Alert.AlertType.WARNING, "Вы действительно хотите выйти? Результаты работы не будут сохранены.", yes, no);
            alert.setTitle("Подтверждение выхода");
            alert.setHeaderText(null);
            alert.showAndWait().ifPresent(buttonType -> {
                if(buttonType.equals(no)) event.consume();
            });
        });
    }

    @Override
    public void openJunction(AbstractJunction junction) {
        if(project.getTruck().hasTruckNumber() && junction.hasNumber()){
            try {
                setJunctionScene(junction);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Для открытия стыка необходимо указать его номер и номер воза.", new ButtonType("Ок", ButtonBar.ButtonData.OK_DONE));
            alert.setTitle("Ошибка открытия стыка.");
            alert.setHeaderText(null);
            alert.show();
        }
    }

    @Override
    public void backFromJunction(AbstractJunction junction) {
        try {
            ImageCache.get().refresh(junction.getCurrentSource().getPath());
            setTruckScene(false);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
