package main.common.controllers;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import main.core.views.ImageScrollPane;

public class ShowImageDialogController {
    public ImageScrollPane imageScrollPane;
    public ImageView imageView;

    public Label name;

    public void setController(String name, Image image) {
        imageView.setImage(image);
        this.name.setText(name);
    }
}
