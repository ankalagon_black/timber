package main.common.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.WeakEventHandler;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import main.common.data_types.TextMarkerWrapper;
import main.common.data_types.TextMarkersListHandler;
import main.core.data_types.common.Project;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.junctions.MarkersJunction;
import main.domain.marker_wrappers.junction.JunctionCircleMarkerWrapper;
import main.domain.marker_wrappers.junction.JunctionMarkerWrapper;
import main.domain.marker_wrappers.base.LogicalCircleMarkerWrapper;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.settings.Settings;
import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
import main.domain.marker_settings_wrappers.MarkerSettingsWrapper;
import main.core.data_types.settings.markers_settings.CircleMarkerSettings;
import main.core.data_types.settings.markers_settings.MarkerSettings;
import main.core.utils.MyUtils;
import main.core.utils.images.ImageViewScale;
import main.core.views.MarkerImageView;
import main.junctions.data_types.handlers.JunctionCircleMarkersListHandler;
import main.junctions.data_types.handlers.JunctionMarkersListHandler;
import main.repository.image.ImageSaver;

import java.io.File;
import java.util.ArrayList;
import java.util.function.BiConsumer;

public class OutputJunctionDialogController {
    public DialogPane root;

    public MarkerImageView markersImageView;
    public ImageView imageView;

    public Slider titleFontSize;

    private AbstractJunction junction;
    private Project project;
    private Object settingsWrapper;
    private TextMarkerWrapper textMarkerWrapper;

    private EventHandler<Event> onSave = event -> {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Сохранить изображение");

        if(project.hasWorkingDirectory()) fileChooser.setInitialDirectory(project.getWorkingDirectory());

        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JPG файл", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG файл", "*.png"));
        fileChooser.setInitialFileName(textMarkerWrapper.getText());

        File file = fileChooser.showSaveDialog(root.getScene().getWindow());
        if (file != null) {
            ImageViewScale imageViewScale = new ImageViewScale();
            imageViewScale.setImageView(imageView);

            double ratio = imageViewScale.getActualToScaledRatio();

            if(project.getSetting().getJunctionMode() == Settings.CIRCLE_MARKERS) {
                int fontSize = ((CircleMarkerSettingsWrapper) settingsWrapper).getCircleMarkerSettings().getFontSize();
                ((CircleMarkerSettingsWrapper) settingsWrapper).getCircleMarkerSettings().setFontSize((int) (ratio * fontSize));
            }
            else {
                int fontSize = ((MarkerSettingsWrapper) settingsWrapper).getMarkerSettings().getFontSize();
                ((MarkerSettingsWrapper) settingsWrapper).getMarkerSettings().setFontSize((int) (ratio * fontSize));
            }

            imageView.setFitWidth(imageView.getImage().getWidth());
            imageView.setFitHeight(imageView.getImage().getHeight());

            new ImageSaver(file).save(SwingFXUtils.fromFXImage(markersImageView.snapshot(null, null), null));
        }
    };

    public void setController(AbstractJunction junction, Project project, Image image) {
        this.junction = junction;
        this.project = project;

        if(project.getSetting().getJunctionMode() == Settings.CIRCLE_MARKERS) {
            CircleMarkersJunction circleMarkersJunction = ((CircleMarkersJunction) junction);

            CircleMarkerSettings circleMarkerSettings = new CircleMarkerSettings(circleMarkersJunction.getCircleMarkerSettings());
            CircleMarkerSettingsWrapper settingsWrapper = new CircleMarkerSettingsWrapper(circleMarkerSettings, false, true);

            ObservableList<JunctionCircleMarkerWrapper> markerWrappers = FXCollections.observableArrayList();
            markerWrappers.addAll(circleMarkersJunction.getMarkers().stream().collect(ArrayList::new, (BiConsumer<ArrayList<JunctionCircleMarkerWrapper>, LogicalCircleMarker>)
                    (list, logicalCircleMarker) -> list.add(new JunctionCircleMarkerWrapper(
                            new LogicalCircleMarkerWrapper(logicalCircleMarker, circleMarkersJunction.getRuler(), project.getSetting().getLogParameters()),
                    list.size(), settingsWrapper)), ArrayList::addAll));

            markersImageView.add(JunctionCircleMarkersListHandler.KEY, new JunctionCircleMarkersListHandler(markerWrappers, markersImageView, circleMarkersJunction, project.getSetting().getLogParameters()));

            this.settingsWrapper = settingsWrapper;
        }
        else {
            MarkersJunction markersJunction = ((MarkersJunction) junction);

            MarkerSettings markerSettings = new MarkerSettings(markersJunction.getMarkerSettings());
            MarkerSettingsWrapper settingsWrapper = new MarkerSettingsWrapper(markerSettings, false, true);

            ObservableList<JunctionMarkerWrapper> markerWrappers = FXCollections.observableArrayList();
            markerWrappers.addAll(markersJunction.getMarkers().stream().collect(ArrayList::new, (BiConsumer<ArrayList<JunctionMarkerWrapper>, Marker>)
                    (list, marker) -> list.add(new JunctionMarkerWrapper(marker, list.size(), settingsWrapper)), ArrayList::addAll));

            markersImageView.add(JunctionMarkersListHandler.KEY, new JunctionMarkersListHandler(markerWrappers, markersImageView, markersJunction));

            this.settingsWrapper = settingsWrapper;
        }

        MarkerSettings textSettings = new MarkerSettings(Color.RED, 42);
        MarkerSettingsWrapper textSettingsWrapper = new MarkerSettingsWrapper(textSettings, false, true);

        textMarkerWrapper = new TextMarkerWrapper(new Marker(180, 50), junction.getOutputName(project.getTruck().getTruckNumber()), textSettingsWrapper);

        markersImageView.add(TextMarkersListHandler.KEY, new TextMarkersListHandler(FXCollections.observableArrayList(textMarkerWrapper), markersImageView));

        imageView.setImage(image);

        titleFontSize.setValue(textSettings.getFontSize());
        titleFontSize.valueProperty().addListener((observable, oldValue, newValue) -> {
            textSettings.setFontSize(newValue.intValue());
        });

        root.lookupButton(MyUtils.SAVE).addEventFilter(ActionEvent.ACTION, new WeakEventHandler<>(onSave));
    }
}
