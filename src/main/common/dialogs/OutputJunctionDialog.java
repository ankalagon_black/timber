package main.common.dialogs;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.image.Image;
import main.common.controllers.OutputJunctionDialogController;
import main.core.data_types.common.Project;
import main.core.data_types.junctions.AbstractJunction;
import main.repository.image.ImageCache;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class OutputJunctionDialog extends Dialog {

    public OutputJunctionDialog(AbstractJunction abstractJunction, Project project, Image image) throws IOException {
        setTitle("Вывод данных стыка");

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation( getClass().getResource("/main/common/resources/output_junction_data.fxml"));

        DialogPane dialogPane =  loader.load();
        OutputJunctionDialogController controller = loader.getController();
        controller.setController(abstractJunction, project, image);

        setDialogPane(dialogPane);
    }

    public OutputJunctionDialog(AbstractJunction junction, Project project) throws ExecutionException, IOException {
        this(junction, project, ImageCache.get().get(junction.getCurrentSource().getPath()));
    }
}
