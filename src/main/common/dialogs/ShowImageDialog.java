package main.common.dialogs;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import main.common.controllers.ShowImageDialogController;
import main.repository.image.ImageCache;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class ShowImageDialog extends Dialog{

    public ShowImageDialog(File file) throws IOException, ExecutionException {
        setTitle("Просмотр изображения");

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/main/common/resources/show_image.fxml"));
        DialogPane dialogPane =  loader.load();

        ShowImageDialogController controller = loader.getController();
        controller.setController(file.getPath(), ImageCache.get().get(file.getPath()));

        setDialogPane(dialogPane);
    }

}
