package main.common.views;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import main.domain.marker_settings_wrappers.MarkerSettingsWrapper;
import main.core.views.skins.marker_views.MarkerViewSkin;

public class TextMarkerViewSkin extends MarkerViewSkin<TextMarkerView, TextMarkerViewBehavior> {
    private final String TEXT = "TEXT";
    private final String FONT_SIZE  = "FONT_SIZE";
    private final String TEXT_COLOR = "TEXT_COLOR";
    private final String ENABLED = "ENABLED";

    public TextMarkerViewSkin(TextMarkerView control) {
        super(control, new TextMarkerViewBehavior(control));

        MarkerSettingsWrapper wrapper = getSkinnable().getContent().getSettingsWrapper();

        setText(getSkinnable().getContent().getText());

        setTextColor(wrapper.getMarkerSettings().getTextColor());
        setEnabled(wrapper.isEnabled());

        registerChangeListener(getSkinnable().getContent().textProperty(), TEXT);

        registerChangeListener(wrapper.getMarkerSettings().fontSizeProperty(), FONT_SIZE);
        registerChangeListener(wrapper.getMarkerSettings().textColorProperty(), TEXT_COLOR);
        registerChangeListener(wrapper.enabledProperty(), ENABLED);
    }


    @Override
    protected void updatePosition() {
        super.updatePosition();
        setFontSize(getSkinnable().getContent().getSettingsWrapper().getMarkerSettings().getFontSize());
    }

    @Override
    protected void handleControlPropertyChanged(String propertyReference) {
        super.handleControlPropertyChanged(propertyReference);
        switch (propertyReference) {
            case TEXT:
                setText(getSkinnable().getContent().getText());
                break;
            case FONT_SIZE:
                setFontSize(getSkinnable().getContent().getSettingsWrapper().getMarkerSettings().getFontSize());
                break;
            case TEXT_COLOR:
                setTextColor(getSkinnable().getContent().getSettingsWrapper().getMarkerSettings().getTextColor());
                break;
            case ENABLED:
                setEnabled(getSkinnable().getContent().getSettingsWrapper().isEnabled());
                break;
        }
    }


    private void setText(String text) {
        getLabel().setText(text);
    }

    private void setFontSize(double fontSize) {
        fontSize *= getImageViewScale().getScaledToActualRatio();
        getLabel().setFont(new Font("System Bold", fontSize));
    }

    private void setTextColor(Color color) {
        getLabel().setTextFill(color);
    }

    private void setEnabled(boolean enabled) {
        getSkinnable().setDisable(!enabled);
        getLabel().setStyle("-fx-opacity: 1.0");
    }
}
