package main.common.views;

import main.core.views.behaviors.marker_views.MarkerViewBehavior;

import java.util.ArrayList;

public class TextMarkerViewBehavior extends MarkerViewBehavior<TextMarkerView> {

    public TextMarkerViewBehavior(TextMarkerView control) {
        super(control, new ArrayList<>());
    }
}
