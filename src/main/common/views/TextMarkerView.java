package main.common.views;

import javafx.scene.control.Skin;
import main.common.data_types.TextMarkerWrapper;
import main.core.data_types.markers.Marker;
import main.core.views.MarkerImageView;
import main.core.views.marker_views.AbstractMarkerView;

public class TextMarkerView extends AbstractMarkerView<TextMarkerWrapper> {

    public TextMarkerView(TextMarkerWrapper wrapper, MarkerImageView markerImageView) {
        super(wrapper, markerImageView);
    }

    @Override
    public Marker getMarker() {
        return getContent().getMarker();
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new TextMarkerViewSkin(this);
    }
}
