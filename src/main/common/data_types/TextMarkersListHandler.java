package main.common.data_types;

import javafx.collections.ObservableList;
import main.common.views.TextMarkerView;
import main.core.views.MarkerImageView;
import main.core.views.data_types.ListHandler;

public class TextMarkersListHandler extends ListHandler<TextMarkerWrapper, TextMarkerView> {
    public static final String KEY = "TEXT_MARKER";

    public TextMarkersListHandler(ObservableList<TextMarkerWrapper> list, MarkerImageView markerImageView) {
        super(list);
        setCallback(param -> new TextMarkerView(param, markerImageView));
    }
}
