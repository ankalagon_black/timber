package main.common.data_types;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import main.core.data_types.markers.Marker;
import main.domain.marker_settings_wrappers.MarkerSettingsWrapper;

public class TextMarkerWrapper {

    public TextMarkerWrapper(Marker marker, String text, MarkerSettingsWrapper markerSettingsWrapper) {
        this.marker = marker;
        setText(text);
        this.settingsWrapper = markerSettingsWrapper;
    }

    private Marker marker;

    public Marker getMarker() {
        return marker;
    }


    private StringProperty text = new SimpleStringProperty();

    public String getText() {
        return text.get();
    }

    public StringProperty textProperty() {
        return text;
    }

    public void setText(String text) {
        this.text.set(text);
    }


    private MarkerSettingsWrapper settingsWrapper;

    public MarkerSettingsWrapper getSettingsWrapper() {
        return settingsWrapper;
    }
}
