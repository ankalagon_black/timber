package main.core.utils;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import main.core.data_types.markers.contour_branch.LogicalContour;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;

import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ViewUtils {

    public static Point2D eventToPoint(MouseEvent mouseEvent){
        return new Point2D(mouseEvent.getX(), mouseEvent.getY());
    }

    public static Mat imageToMat(Image image) throws UnsatisfiedLinkError, IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", byteArrayOutputStream);
        byteArrayOutputStream.flush();
        return Imgcodecs.imdecode(new MatOfByte(byteArrayOutputStream.toByteArray()), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
    }

    public static Image matToImage(Mat mat) {
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".png", mat, matOfByte);
        return new Image(new ByteArrayInputStream(matOfByte.toArray()));
    }

    public static MatOfPoint getPoints(LogicalContour logicalContour) {
        MatOfPoint matOfPoint = new MatOfPoint();

        double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE;

        for (Point2D point2D: logicalContour.getPoints()) {
            double x = point2D.getX(), y = point2D.getY();

            if(x < minX) minX = x;
            if(y < minY) minY = y;
        }

        double finalMinX = minX;
        double finalMinY = minY;
        matOfPoint.fromList(logicalContour.getPoints().stream().collect(ArrayList::new, (points, point2D) -> points.add(new Point(point2D.getX() - finalMinX, point2D.getY() - finalMinY)), ArrayList::addAll));

        return matOfPoint;
    }

    public static MatOfPoint getPointsAbs(LogicalContour logicalContour) {
        MatOfPoint matOfPoint = new MatOfPoint();

        double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE;

        for (Point2D point2D: logicalContour.getPoints()) {
            double x = point2D.getX(), y = point2D.getY();

            if(x < minX) minX = x;
            if(y < minY) minY = y;
        }

        double finalMinX = minX;
        double finalMinY = minY;
        matOfPoint.fromList(logicalContour.getPoints().stream().collect(ArrayList::new, (points, point2D) -> points.add(new Point(point2D.getX() - finalMinX + logicalContour.getX(),
                point2D.getY() - finalMinY + logicalContour.getY())), ArrayList::addAll));

        return matOfPoint;
    }

    public static Point getConvexHullCenter(List<Point> convexHull) {
        Point innerPoint = new Point();
        for (Point i: convexHull) {
            innerPoint.x += i.x;
            innerPoint.y += i.y;
        }

        int size = convexHull.size();

        innerPoint.x /= (double) size;
        innerPoint.y /= (double) size;

        Point center = new Point();
        double totalS = 0;

        for (int i = 0; i < size; i++) {
            Point a = convexHull.get(i),
                    b = convexHull.get((i + 1) % size);
            double square = Math.abs((b.y - innerPoint.y) * (a.x - innerPoint.x) - (a.y - innerPoint.y) * (b.x - innerPoint.x)) / 2;

            center.x += square * (innerPoint.x + a.x + b.x) / 3;
            center.y += square * (innerPoint.y + a.y + b.y) / 3;

            totalS += square;
        }

        center.x /= totalS;
        center.y /= totalS;

        return center;
    }

    public static double getConvexHullInscribedCircleRadius(Point center, List<Point> convexHull) {
        int size = convexHull.size();
        double min = Double.MAX_VALUE;
        for (int i = 0; i < size; i++) {
            Point a = convexHull.get(i),
                    b = convexHull.get((i + 1) % size);
            Point norm = new Point(b.y - a.y, -b.x + a.x);
            double length = Math.sqrt(norm.dot(norm));
            norm.x /= length;
            norm.y /= length;

            Point vector = new Point(a.x - center.x, a.y - center.y);
            double radius = vector.dot(norm);
            if(radius < min) min = radius;
        }

        return min;
    }

    public static Rect truncate(Rect rect, Rect bounds) {
        int lowerX = Math.max(rect.x, bounds.x), lowerY = Math.max(rect.y, bounds.y);
        int upperX = Math.min(rect.x + rect.width, bounds.x + bounds.width), upperY = Math.min(rect.y + rect.height, bounds.y + bounds.height);

        return new Rect(lowerX, lowerY, upperX - lowerX, upperY - lowerY);
    }
}
