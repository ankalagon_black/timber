package main.core.utils.images;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ImageViewLoader {
    private LoadedInterface loadedInterface;

    public void setLoadedInterface(LoadedInterface loadedInterface) {
        this.loadedInterface = loadedInterface;

        imageViewProperty().removeListener(imageViewChangeListener);

        if(getImageView() != null) {
            setImage(getImageView().getImage());
            getImageView().imageProperty().addListener(imageWeakChangeListener);
        }
        imageViewProperty().addListener(imageViewChangeListener);
    }


    private ObjectProperty<ImageView> imageView = new SimpleObjectProperty<>();

    public ImageView getImageView() {
        return imageView.get();
    }

    public ObjectProperty<ImageView> imageViewProperty() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView.set(imageView);
    }


    private void setImage(Image image) {
        if(image != null) {
            if(image.getProgress() == 1) {
                loadedInterface.onLoaded(image);
            }
            else {
                isImageLoaded = image.progressProperty().isEqualTo(1);
                isImageLoaded.addListener(booleanWeakChangeListener);
            }
        }
    }

    public boolean isImageLoaded() {
        return imageView.get() != null && imageView.get().getImage() != null && imageView.get().getImage().getProgress() == 1;
    }


    private BooleanBinding isImageLoaded = null;


    private ChangeListener<Boolean> booleanChangeListener = (observable, oldValue, newValue) -> {
        if(newValue) loadedInterface.onLoaded(imageView.get().getImage());
    };
    private WeakChangeListener<Boolean> booleanWeakChangeListener = new WeakChangeListener<>(booleanChangeListener);


    private ChangeListener<Image> imageChangeListener = (observable, oldValue, newValue) -> {
        if(oldValue != null && isImageLoaded != null) isImageLoaded.removeListener(booleanWeakChangeListener);

        if(newValue != null) {
            setImage(newValue);
        }
    };
    private WeakChangeListener<Image> imageWeakChangeListener = new WeakChangeListener<>(imageChangeListener);


    private ChangeListener<ImageView> imageViewChangeListener = (observable, oldValue, newValue) -> {
        if(oldValue != null) {
            if(isImageLoaded != null) isImageLoaded.removeListener(booleanWeakChangeListener);
            oldValue.imageProperty().removeListener(imageWeakChangeListener);
        }

        if(newValue != null) {
            setImage(newValue.getImage());
            newValue.imageProperty().addListener(imageWeakChangeListener);
        }
    };


    public interface LoadedInterface {
        void onLoaded(Image image);
    }
}
