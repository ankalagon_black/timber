package main.core.utils.images;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.image.ImageView;

public class ImageViewScale {

    private ObjectProperty<ImageView> imageView = new SimpleObjectProperty<>();

    public ImageView getImageView() {
        return imageView.get();
    }

    public ObjectProperty<ImageView> imageViewProperty() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView.set(imageView);
    }


    public double getScaledToActualRatio() {
        ImageView iv = getImageView();
        if(iv != null && iv.getImage() != null && iv.getImage().getProgress() == 1) return iv.getBoundsInLocal().getWidth() / iv.getImage().getWidth();
        else return 0;
    }

    public double getActualToScaledRatio() {
        ImageView iv = getImageView();
        if(iv != null && iv.getImage() != null && iv.getImage().getProgress() == 1) return  iv.getImage().getWidth() / iv.getBoundsInLocal().getWidth();
        else return 0;
    }

    public Bounds getBounds() {
        ImageView iv = getImageView();
        if(iv != null && iv.getImage() != null && iv.getImage().getProgress() == 1) return iv.getBoundsInLocal();
        else return new BoundingBox(0,0,0,0);
    }
}
