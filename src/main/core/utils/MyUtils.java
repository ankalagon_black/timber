package main.core.utils;

import javafx.geometry.Point2D;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.paint.Color;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;

public class MyUtils {
    public static final ButtonType SAVE = new ButtonType("Сохранить", ButtonBar.ButtonData.OK_DONE),
            CLOSE = new ButtonType("Закрыть", ButtonBar.ButtonData.CANCEL_CLOSE),
            APPLY = new ButtonType("Применить", ButtonBar.ButtonData.OK_DONE),
            CROP = new ButtonType("Обрезать", ButtonBar.ButtonData.OK_DONE),
            CANCEL = new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE),
            YES = new ButtonType("Да", ButtonBar.ButtonData.YES),
            NO = new ButtonType("Нет", ButtonBar.ButtonData.NO),
            CONTINUE = new ButtonType("Продолжить", ButtonBar.ButtonData.APPLY),
            FINISH = new ButtonType("Завершить", ButtonBar.ButtonData.FINISH);


    public static String getLocalURL(String path){
        return "file:///" + path;
    }

    public static String toRGBHexCode(Color color ) {
        return String.format( "#%02X%02X%02X", (int)(color.getRed() * 255), (int)(color.getGreen() * 255), (int)(color.getBlue() * 255) );
    }

    public static String toRGBA(Color color, double a) {
        return String.format(Locale.US, "rgba(%d, %d, %d, %.1f)", (int)(color.getRed() * 255), (int)(color.getGreen() * 255), (int)(color.getBlue() * 255), a);
    }


    public static void deleteContent(File dir){
        if(!dir.isDirectory()) return;

        File[] files = dir.listFiles();
        if(files != null){
            for (File i: files){
                delete(i);
            }
        }
    }

    public static void delete(File file){
        if(file.isDirectory()){
            File[] files = file.listFiles();
            if(files != null){
                for (File i: files) delete(i);
            }
        }

        file.delete();
    }


    public static double clampMax(double value, double max){
        return value > max ? max : value;
    }

    public static double round(double value, int places){
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bigDecimal = new BigDecimal(value);
        bigDecimal = bigDecimal.setScale(places, RoundingMode.HALF_UP);
        return bigDecimal.doubleValue();
    }

    public static double crossProduct(Point2D p1, Point2D p2, Point2D p3) {
        Point2D v1 = p2.subtract(p1), v2 = p3.subtract(p1);
        return v1.getX() * v2.getY() - v1.getY() * v2.getX();
    }

    public static double crossProduct(Point2D p1, Point2D p2) {
        return p1.getX() * p2.getY() - p2.getX() * p1.getY();
    }
}
