package main.core.utils.strings;

import javafx.scene.control.TextFormatter;

import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

public class DoubleFilter implements UnaryOperator<TextFormatter.Change> {

    private Pattern pattern = Pattern.compile("\\d*\\.?\\d?|\\d*");

    @Override
    public TextFormatter.Change apply(TextFormatter.Change change) {
        return pattern.matcher(change.getControlNewText()).matches() ? change : null;
    }
}
