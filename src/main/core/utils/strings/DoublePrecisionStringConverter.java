package main.core.utils.strings;

import javafx.util.converter.DoubleStringConverter;

import java.util.Locale;

public class DoublePrecisionStringConverter extends DoubleStringConverter {
    private String places;

    public DoublePrecisionStringConverter(int places){
        if (places < 0) throw new IllegalArgumentException();
        this.places = String.valueOf(places);
    }

    @Override
    public String toString(Double value) {
        if (value == null) return "";
        else return String.format(Locale.US, "%." + places + "f", value);
    }
}
