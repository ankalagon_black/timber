package main.core.utils.strings;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class DoubleCommaAdapter extends TypeAdapter<Double> {

    @Override
    public Double read(JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            return null;
        } else {
            String stringValue = jsonReader.nextString();
            try {
                NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
                return format.parse(stringValue).doubleValue();
            } catch (NumberFormatException | ParseException e) {
                e.printStackTrace();
                return null;
            }
        }
    }


    @Override
    public void write(JsonWriter jsonWriter, Double aDouble) throws IOException {
        if(aDouble == null) {
            jsonWriter.nullValue();
        }
        else jsonWriter.value(aDouble);
    }
}
