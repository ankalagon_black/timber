package main.core.data_types.common;

import main.core.data_types.base.LogicalValue;

public class Length {

    public Length(LogicalValue length, String text) {
        setLength(length);
        setText(text);
    }

    private LogicalValue length;

    public LogicalValue getLength() {
        return length;
    }

    private void setLength(LogicalValue length) {
        this.length = length;
    }


    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass().equals(Length.class) &&
                ((Length) obj).getLength().equals(getLength()) && ((Length) obj).getText().equals(getText());
    }
}
