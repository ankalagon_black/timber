package main.core.data_types.common;

import javafx.beans.property.SimpleObjectProperty;
import main.core.data_types.settings.Settings;

import java.io.File;

public class Project {
    private SimpleObjectProperty<File> projectFile = new SimpleObjectProperty<>();

    public File getProjectFile() {
        return projectFile.get();
    }

    public void setProjectFile(File projectFile) {
        this.projectFile.set(projectFile);
    }

    public boolean hasProjectFile() {
        return projectFile.get() != null;
    }

    public SimpleObjectProperty<File> projectFileProperty() {
        return projectFile;
    }


    private SimpleObjectProperty<File> workingDirectory = new SimpleObjectProperty<>();

    public File getWorkingDirectory() {
        return workingDirectory.get();
    }

    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory.set(workingDirectory);
    }

    public boolean hasWorkingDirectory(){
        return workingDirectory.get() != null;
    }

    public SimpleObjectProperty<File> workingDirectoryProperty() {
        return workingDirectory;
    }


    private SimpleObjectProperty<Truck> truck = new SimpleObjectProperty<>(new Truck());

    public Truck getTruck() {
        return truck.get();
    }

    public void setTruck(Truck truck) {
        this.truck.set(truck);
    }

    public SimpleObjectProperty<Truck> truckProperty() {
        return truck;
    }


    private SimpleObjectProperty<Settings> setting = new SimpleObjectProperty<>(new Settings());

    public Settings getSetting() {
        return setting.get();
    }

    public void setSetting(Settings setting) {
        this.setting.set(setting);
    }

    public SimpleObjectProperty<Settings> settingProperty() {
        return setting;
    }


    private Classifier classifier = new Classifier();

    public void setClassifier(Classifier classifier) {
        this.classifier = classifier;
    }

    public Classifier getClassifier() {
        return classifier;
    }
}
