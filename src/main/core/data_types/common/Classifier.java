package main.core.data_types.common;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.core.data_types.base.LogicalValue;

import java.util.List;
import java.util.function.Predicate;

public class Classifier {

    public Classifier() {}

    public Classifier(List<CubageType> cubageTypes, List<Length> lengths, List<Breed> breeds) {
        this.cubageTypes.addAll(cubageTypes);
        this.lengths.addAll(lengths);
        this.breeds.addAll(breeds);
    }


    private ObservableList<CubageType> cubageTypes = FXCollections.observableArrayList();

    public void setCubageTypes(List<CubageType> cubageTypes) {
        this.cubageTypes.removeIf(cubageType -> !cubageTypes.contains(cubageType));

        for (CubageType i: cubageTypes) if(!this.cubageTypes.contains(i)) this.cubageTypes.add(i);
    }

    public ObservableList<CubageType> getCubageTypes() {
        return cubageTypes;
    }


    private ObservableList<Length> lengths = FXCollections.observableArrayList();

    public void setLengths(List<Length> lengths) {
        this.lengths.removeIf(length -> !lengths.contains(length));

        for (Length i: lengths) if(!this.lengths.contains(i)) this.lengths.add(i);
    }

    public ObservableList<Length> getLengths() {
        return lengths;
    }


    private ObservableList<Breed> breeds = FXCollections.observableArrayList();

    public void setBreeds(List<Breed> breeds) {
        this.breeds.clear();
        this.breeds.addAll(breeds);
    }

    public ObservableList<Breed> getBreeds() {
        return breeds;
    }


    public CubageType getCubageTypeById(int id) {
        for (CubageType i: cubageTypes) {
            if(i.getId() == id) return i;
        }
        return null;
    }

    public Length getLengthByLogicalValue(LogicalValue value) {
        for (Length i: lengths) {
            if(i.getLength().equals(value)) return i;
        }
        return null;
    }

    public Breed getBreedById(int id) {
        for (Breed i: breeds) {
            if(i.getId() == id) return i;
        }
        return null;
    }
}

//    public static final int CUBAGE_TYPE_1 = 1, CUBAGE_TYPE_2 = 2, CUBAGE_TYPE_3 = 3, CUBAGE_TYPE_4 = 4;
//
//    public static final String CUBAGE_TYPE_1_TEXT = "Кубатурник русский (только четные диаметры)",
//            CUBAGE_TYPE_2_TEXT = "Кубатурник русский (только четные и нечетные диаметры) ",
//            CUBAGE_TYPE_3_TEXT = "Кубатурник китайский (только четные диаметры)",
//            CUBAGE_TYPE_4_TEXT = "Кубатурник китайский (только четные и нечетные диаметры) ";
//
//    public static final LogicalValue LENGTH_380 = new LogicalValue(3.8),
//            LENGTH_390 = new LogicalValue(3.9),
//            LENGTH_400 = new LogicalValue(4),
//            LENGTH_410 = new LogicalValue(4.1),
//            LENGTH_600 = new LogicalValue(6);
//
//    public static String getCubageTextByType(int cubageType) {
//        switch (cubageType) {
//            case CUBAGE_TYPE_1:
//                return CUBAGE_TYPE_1_TEXT;
//            case CUBAGE_TYPE_2:
//                return CUBAGE_TYPE_2_TEXT;
//            case CUBAGE_TYPE_3:
//                return CUBAGE_TYPE_3_TEXT;
//            case CUBAGE_TYPE_4:
//                return CUBAGE_TYPE_4_TEXT;
//            default:
//                return null;
//        }
//    }
//
//    public static List<Integer> getCubageTypes() {
//        return Arrays.asList(CUBAGE_TYPE_1, CUBAGE_TYPE_2, CUBAGE_TYPE_3, CUBAGE_TYPE_4);
//    }
//
//    public static List<LogicalValue> getLengths() {
//        return Arrays.asList(LENGTH_380, LENGTH_390, LENGTH_400, LENGTH_410, LENGTH_600);
//    }
