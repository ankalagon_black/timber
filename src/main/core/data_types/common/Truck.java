package main.core.data_types.common;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.junctions.MarkersJunction;
import main.core.data_types.settings.Settings;
import main.truck.data_types.NavigationFile;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Truck {
    private ObservableList<AbstractJunction> junctions = FXCollections.observableArrayList();

    public void setJunctions(List<AbstractJunction> junctions) {
        this.junctions.clear();
        this.junctions.addAll(junctions);
    }

    public ObservableList<AbstractJunction> getJunctions() {
        return junctions;
    }


    private SimpleObjectProperty<File> cabin = new SimpleObjectProperty<>();

    public SimpleObjectProperty<File> cabinProperty() {
        return cabin;
    }

    public void setCabin(File cabin) {
        this.cabin.set(cabin);
    }

    public File getCabin() {
        return cabin.get();
    }

    public boolean hasCabin(){
        return cabin.get() != null;
    }


    private SimpleObjectProperty<String> truckNumber = new SimpleObjectProperty<>();

    public SimpleObjectProperty<String> truckNumberProperty() {
        return truckNumber;
    }

    public String getTruckNumber() {
        return truckNumber.get();
    }

    public void setTruckNumber(String truckNumber) {
        this.truckNumber.set(truckNumber);
    }

    public boolean hasTruckNumber(){
        return truckNumber.get() != null && !truckNumber.get().isEmpty();
    }


    public void initialize(File workingDir, List<NavigationFile> navigationFiles, Settings settings) {
        if(workingDir != null) {
            String name =  workingDir.getName();
            if(name.matches("^\\d*[,. ].*")) {
                Matcher matcher = Pattern.compile("^\\d*").matcher(name);
                if(matcher.find()) truckNumber.set(matcher.group());
            }
        }

        for (NavigationFile i: navigationFiles) {
            switch (i.getType()) {
                case NavigationFile.CABIN:
                    setCabin(i.getFile());
                    break;
                case NavigationFile.JUNCTION:
                    junctions.add(AbstractJunction.toJunction(i, settings));
                    break;
            }
        }
    }


    public boolean canBeSaved(){
        return junctions.size() != 0 && cabin.get() != null && truckNumber.get() != null && !truckNumber.get().isEmpty();
    }

    public boolean hasAnyData(){
        return cabin.get() != null || truckNumber.get() != null || junctions.size() != 0;
    }

    public boolean contains(File file) {
        return hasCabin() && file.equals(getCabin()) || junctions.stream().anyMatch(junction -> junction.getOriginalFile().equals(file));
    }

    public void clear(){
        setCabin(null);
        setTruckNumber(null);
        getJunctions().clear();
    }

    public void removeAllMarkers(){
        junctions.forEach(AbstractJunction::removeMarkers);
    }
}
