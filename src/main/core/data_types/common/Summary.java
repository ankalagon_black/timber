package main.core.data_types.common;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import main.core.data_types.base.LogicalValue;

public class Summary {

    public Summary() {
        this(0, 0, new LogicalValue(), new LogicalValue(), new LogicalValue());
    }

    public Summary(double volumeStandard, double geometryVolume, LogicalValue averageDiameter, LogicalValue averageDiameterStandard, LogicalValue averageDiameterGeometry) {
        setVolumeStandard(volumeStandard);
        setGeometryVolume(geometryVolume);
        setAverageDiameter(averageDiameter);
        setAverageDiameterStandard(averageDiameterStandard);
        setAverageDiameterGeometry(averageDiameterGeometry);
    }


    private DoubleProperty volumeStandard = new SimpleDoubleProperty();

    public double getVolumeStandard() {
        return volumeStandard.get();
    }

    public DoubleProperty volumeStandardProperty() {
        return volumeStandard;
    }

    public void setVolumeStandard(double volumeStandard) {
        this.volumeStandard.set(volumeStandard);
    }


    private DoubleProperty geometryVolume = new SimpleDoubleProperty();

    public double getGeometryVolume() {
        return geometryVolume.get();
    }

    public DoubleProperty geometryVolumeProperty() {
        return geometryVolume;
    }

    public void setGeometryVolume(double geometryVolume) {
        this.geometryVolume.set(geometryVolume);
    }


    private ObjectProperty<LogicalValue> averageDiameter = new SimpleObjectProperty<>();

    public LogicalValue getAverageDiameter() {
        return averageDiameter.get();
    }

    public ObjectProperty<LogicalValue> averageDiameterProperty() {
        return averageDiameter;
    }

    public void setAverageDiameter(LogicalValue averageDiameter) {
        this.averageDiameter.set(averageDiameter);
    }


    private ObjectProperty<LogicalValue> averageDiameterStandard = new SimpleObjectProperty<>();

    public LogicalValue getAverageDiameterStandard() {
        return averageDiameterStandard.get();
    }

    public ObjectProperty<LogicalValue> averageDiameterStandardProperty() {
        return averageDiameterStandard;
    }

    public void setAverageDiameterStandard(LogicalValue averageDiameterStandard) {
        this.averageDiameterStandard.set(averageDiameterStandard);
    }


    private ObjectProperty<LogicalValue> averageDiameterGeometry = new SimpleObjectProperty<>();

    public LogicalValue getAverageDiameterGeometry() {
        return averageDiameterGeometry.get();
    }

    public ObjectProperty<LogicalValue> averageDiameterGeometryProperty() {
        return averageDiameterGeometry;
    }

    public void setAverageDiameterGeometry(LogicalValue averageDiameterGeometry) {
        this.averageDiameterGeometry.set(averageDiameterGeometry);
    }


    public void resetMarkersResults() {
        averageDiameter.set(new LogicalValue());
        averageDiameterStandard.set(new LogicalValue());
        volumeStandard.set(0);
    }
}
