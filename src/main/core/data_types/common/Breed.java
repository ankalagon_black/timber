package main.core.data_types.common;

public class Breed {

    public Breed(int id, String text) {
        setId(id);
        setText(text);
    }

    private int id;

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }


    private String text;

    public String getText() {
        return text;
    }

    private void setText(String text) {
        this.text = text;
    }
}
