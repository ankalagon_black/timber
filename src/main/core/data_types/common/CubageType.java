package main.core.data_types.common;

public class CubageType {

    public CubageType(int id, String text) {
        setId(id);
        setText(text);
    }

    private int id;

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }


    private String text;

    public String getText() {
        return text;
    }

    private void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass().equals(CubageType.class) &&
                ((CubageType) obj).getId() == getId() && ((CubageType) obj).getText().equals(getText());
    }
}
