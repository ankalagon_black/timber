package main.core.data_types.base;

import main.core.utils.MyUtils;

public class LogicalValue {
    public static final int METERS = 0, CENTIMETERS = 1;


    public LogicalValue() {
       this(0, METERS);
    }

    public LogicalValue(double value, int type) {
        switch (type) {
            case METERS:
                this.value = value;
                break;
            case CENTIMETERS:
                this.value = fromCentimetersToMeters(value);
                break;
        }
    }

    public LogicalValue(double value) {
        this(value, METERS);
    }

    public LogicalValue(LogicalValue logicalValue) {
        this(logicalValue.getValue());
    }


    private double value;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }


    public LogicalValue add(LogicalValue logicalValue) {
        return new LogicalValue(value + logicalValue.getValue());
    }

    public LogicalValue add(double value) {
        return add(value, METERS);
    }

    public LogicalValue add(double value, int type) {
        switch (type) {
            case METERS:
                return new LogicalValue(this.value + value);
            case CENTIMETERS:
                return new LogicalValue(this.value + fromCentimetersToMeters(value));
            default:
                return null;
        }
    }


    public LogicalValue subtract(LogicalValue logicalValue) {
        return new LogicalValue(value - logicalValue.getValue());
    }

    public LogicalValue subtract(double value) {
        return add(-value, METERS);
    }


    public LogicalValue multiply(double value) {
        return new LogicalValue(this.value * value);
    }


    public double getValueInCm(){
        return fromMetersToCentimeters(value);
    }

    public void setValueInCm(double valueInCm) {
        value = fromCentimetersToMeters(valueInCm);
    }


    public double getRounded(int precision, int type) {
        switch (type) {
            case METERS:
                return MyUtils.round(value, precision);
            case CENTIMETERS:
                return MyUtils.round(fromMetersToCentimeters(value), precision);
            default:
                return 0;
        }
    }


    private double fromMetersToCentimeters(double meters) {
        return meters * 100;
    }

    private double fromCentimetersToMeters(double centimeters) {
        return centimeters / 100;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass().equals(LogicalValue.class) && ((LogicalValue) obj).getValue() == getValue();
    }
}
