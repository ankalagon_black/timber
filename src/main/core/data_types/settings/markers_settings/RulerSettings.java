package main.core.data_types.settings.markers_settings;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;

public class RulerSettings extends MarkerSettings {
    public static String DEFAULT_COLOR = "#ff0000";

    public RulerSettings() {
        this(Color.web(DEFAULT_TEXT_COLOR), DEFAULT_FONT_SIZE, Color.web(DEFAULT_COLOR));
    }

    public RulerSettings(Color textColor, int fontSize, Color color) {
        super(textColor, fontSize);
        setColor(color);
    }


    private ObjectProperty<Color> color = new SimpleObjectProperty<>();

    public Color getColor() {
        return color.get();
    }

    public ObjectProperty<Color> colorProperty() {
        return color;
    }

    public void setColor(Color color) {
        this.color.set(color);
    }
}
