package main.core.data_types.settings.markers_settings;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;

public class ContourSettings extends MarkerSettings {

    public ContourSettings() {
        super();
        initialize(Color.web(DEFAULT_TEXT_COLOR), true);
    }

    public ContourSettings(MarkerSettings markerSettings, Color color, boolean highlightInnerArea) {
        super(markerSettings);
        initialize(color, highlightInnerArea);
    }

    public ContourSettings(MarkerSettings markerSettings) {
        super(markerSettings);
        initialize(Color.web(DEFAULT_TEXT_COLOR), true);
    }

    public ContourSettings(CircleMarkerSettings circleMarkerSettings) {
        super(circleMarkerSettings);
        initialize(circleMarkerSettings.getColor(), true);
    }

    public ContourSettings(Color textColor, int fontSize, Color color, boolean highlightInnerArea) {
        super(textColor, fontSize);
        initialize(color, highlightInnerArea);
    }

    private void initialize(Color color, boolean highlightInnerArea) {
        setColor(color);
        setHighlightInnerArea(highlightInnerArea);
    }


    private ObjectProperty<Color> color = new SimpleObjectProperty<>();

    public Color getColor() {
        return color.get();
    }

    public ObjectProperty<Color> colorProperty() {
        return color;
    }

    public void setColor(Color color) {
        this.color.set(color);
    }


    private BooleanProperty highlightInnerArea = new SimpleBooleanProperty();

    public boolean isHighlightInnerArea() {
        return highlightInnerArea.get();
    }

    public BooleanProperty highlightInnerAreaProperty() {
        return highlightInnerArea;
    }

    public void setHighlightInnerArea(boolean highlightInnerArea) {
        this.highlightInnerArea.set(highlightInnerArea);
    }
}
