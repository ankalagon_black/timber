package main.core.data_types.settings.markers_settings;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;

public class MarkerSettings {
    public static String DEFAULT_TEXT_COLOR = "#001d80";
    public static int DEFAULT_FONT_SIZE = 14;

    public MarkerSettings() {
        this(Color.web(DEFAULT_TEXT_COLOR), DEFAULT_FONT_SIZE);
    }

    public MarkerSettings(Color textColor, int fontSize) {
        setTextColor(textColor);
        setFontSize(fontSize);
    }

    public MarkerSettings(MarkerSettings markerSettings) {
        this(markerSettings.getTextColor(), markerSettings.getFontSize());
    }


    private ObjectProperty<Color> textColor = new SimpleObjectProperty<>();

    public Color getTextColor() {
        return textColor.get();
    }

    public void setTextColor(Color textColor) {
        this.textColor.set(textColor);
    }

    public ObjectProperty<Color> textColorProperty() {
        return textColor;
    }


    private IntegerProperty fontSize = new SimpleIntegerProperty();

    public int getFontSize() {
        return fontSize.get();
    }

    public void setFontSize(int fontSize) {
        this.fontSize.set(fontSize);
    }

    public IntegerProperty fontSizeProperty() {
        return fontSize;
    }
}
