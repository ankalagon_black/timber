package main.core.data_types.settings.markers_settings;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;

public class CircleMarkerSettings extends MarkerSettings {

    public CircleMarkerSettings() {
        this(Color.web(DEFAULT_TEXT_COLOR), DEFAULT_FONT_SIZE, Color.web(DEFAULT_TEXT_COLOR));
    }

    public CircleMarkerSettings(Color textColor, int fontSize, Color color) {
        super(textColor, fontSize);
        setColor(color);
    }

    public CircleMarkerSettings(CircleMarkerSettings circleMarkerSettings) {
        this(circleMarkerSettings.getTextColor(), circleMarkerSettings.getFontSize(), circleMarkerSettings.getColor());
    }

    public CircleMarkerSettings(MarkerSettings markerSettings) {
        super(markerSettings);
        setColor(Color.web(DEFAULT_TEXT_COLOR));
    }

    public CircleMarkerSettings(ContourSettings contourSettings) {
        super(contourSettings);
        setColor(contourSettings.getColor());
    }


    private ObjectProperty<Color> color = new SimpleObjectProperty<>();

    public Color getColor() {
        return color.get();
    }

    public ObjectProperty<Color> colorProperty() {
        return color;
    }

    public void setColor(Color color) {
        this.color.set(color);
    }
}
