package main.core.data_types.settings;

import main.core.data_types.base.LogicalValue;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import org.controlsfx.tools.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LogParameters {
    public static final LogicalValue DEFAULT_MIN_DIAMETER = new LogicalValue(0.04),
            DEFAULT_MAX_DIAMETER = new LogicalValue(0.70);

    public LogParameters() {
        this(DEFAULT_MIN_DIAMETER, DEFAULT_MAX_DIAMETER, null, null);
    }

    public LogParameters(LogicalValue minDiameter, LogicalValue maxDiameter, Integer cubageType, LogicalValue length) {
        setMinDiameter(minDiameter);
        setMaxDiameter(maxDiameter);
        setCubageType(cubageType);
        setLength(length);
    }

    private LogicalValue minDiameter;

    public LogicalValue getMinDiameter() {
        return minDiameter;
    }

    public void setMinDiameter(LogicalValue minDiameter) {
        this.minDiameter = minDiameter;
    }


    private LogicalValue maxDiameter;

    public LogicalValue getMaxDiameter() {
        return maxDiameter;
    }

    public void setMaxDiameter(LogicalValue maxDiameter) {
        this.maxDiameter = maxDiameter;
    }


    private Integer cubageType;

    public boolean hasCubageType() {
        return cubageType != null;
    }

    public Integer getCubageType() {
        return cubageType;
    }

    public void setCubageType(Integer cubageType) {
        this.cubageType = cubageType;
    }


    private LogicalValue length;

    public boolean hasLength() {
        return length != null;
    }

    public LogicalValue getLength() {
        return length;
    }

    public void setLength(LogicalValue length) {
        this.length = length;
    }


    public LogicalValue truncate(LogicalValue logicalValue) {
        double newValue = Utils.clamp(minDiameter.getValue(), logicalValue.getValue(), maxDiameter.getValue());
        if(newValue == logicalValue.getValue()) return logicalValue;
        else return new LogicalValue(newValue);
    }

    public double getMinDiameterInPixels(LogicalRuler logicalRuler) {
        return logicalRuler.getPixelPerLogical().getValue() * minDiameter.getValue();
    }

    public double getMaxDiameterInPixels(LogicalRuler logicalRuler) {
        return logicalRuler.getPixelPerLogical().multiply(maxDiameter.getValue()).getValue();
    }

    public boolean areLogBoundsCorrect(){
        return minDiameter.getValue() < maxDiameter.getValue();
    }
}
