package main.core.data_types.settings;

public class HttpsSettings {
    public static final String DEFAULT_IP = "87.103.173.228:1941"; //77.243.115.106:1940
    public static final String DEFAULT_GATEWAY = "skdemo/hs/gate/mobile";

    public HttpsSettings(){}

    public HttpsSettings(String ip, String gateway){
        this.ip = ip;
        this.gateway = gateway;
    }

    private String ip = DEFAULT_IP;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }


    private String gateway = DEFAULT_GATEWAY;

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }


    public String getUrl(){
        return "https://" + ip + "/" + gateway + "/";
    }
}
