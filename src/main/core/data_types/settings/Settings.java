package main.core.data_types.settings;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Settings {
    public static final int MARKERS = 0, CIRCLE_MARKERS = 1;

    public Settings() {}

    public Settings(int junctionMode, LogParameters logParameters, HttpsSettings httpsSettings) {
        setJunctionMode(junctionMode);
        setLogParameters(logParameters);
        setHttpsSettings(httpsSettings);
    }

    public Settings(Settings settings) {
        this(settings.getJunctionMode(), settings.getLogParameters(), settings.getHttpsSettings());
    }


    private IntegerProperty junctionMode = new SimpleIntegerProperty(MARKERS);

    public int getJunctionMode() {
        return junctionMode.get();
    }

    public IntegerProperty junctionModeProperty() {
        return junctionMode;
    }

    public void setJunctionMode(int junctionMode) {
        this.junctionMode.set(junctionMode);
    }


    private LogParameters logParameters = new LogParameters();

    public LogParameters getLogParameters() {
        return logParameters;
    }

    public void setLogParameters(LogParameters logParameters) {
        this.logParameters = logParameters;
    }


    private HttpsSettings httpsSettings = new HttpsSettings();

    public HttpsSettings getHttpsSettings() {
        return httpsSettings;
    }

    public void setHttpsSettings(HttpsSettings httpsSettings) {
        this.httpsSettings = httpsSettings;
    }
}
