package main.core.data_types.junctions;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Summary;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.markers_settings.CircleMarkerSettings;
import main.core.data_types.settings.markers_settings.ContourSettings;
import main.core.data_types.settings.markers_settings.RulerSettings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CircleMarkersJunction extends AbstractJunction<LogicalCircleMarker> {

    public CircleMarkersJunction(File originalFile, String number, Integer cubageType, LogicalValue length) {
        super(originalFile, number, cubageType, length);
        initialize(new ArrayList<>(), new CircleMarkerSettings());
    }

    public CircleMarkersJunction(AbstractJunction abstractJunction, List<LogicalCircleMarker> logicalCircleMarkers, CircleMarkerSettings circleMarkerSettings) {
        super(abstractJunction);
        initialize(logicalCircleMarkers, circleMarkerSettings);
    }

    public CircleMarkersJunction(File originalFile, File modifiedFile,
                                 String number, Integer cubageType, LogicalValue length, Integer breed, String commentary, double coefficient,
                                 Summary results,
                                 LogicalContour logicalContours, ContourSettings contourSettings,
                                 LogicalRuler logicalRuler, RulerSettings rulerSettings,
                                 List<LogicalCircleMarker> logicalCircleMarkers, CircleMarkerSettings circleMarkerSettings) {
        super(originalFile, modifiedFile,
                number, cubageType, length, breed, commentary, coefficient,
                results,
                logicalRuler, rulerSettings,
                logicalContours, contourSettings);
        initialize(logicalCircleMarkers, circleMarkerSettings);
    }

    public CircleMarkersJunction(MarkersJunction markersJunction) {
        super(markersJunction);

        List<LogicalCircleMarker> markers = markersJunction.getMarkers().stream().collect(ArrayList::new, (logicalCircleMarkers, marker) ->
                logicalCircleMarkers.add(new LogicalCircleMarker(marker, 0, new LogicalValue())), ArrayList::addAll);

        initialize(markers, new CircleMarkerSettings(markersJunction.getMarkerSettings()));
    }


    private void initialize(List<LogicalCircleMarker> logicalCircleMarkers, CircleMarkerSettings circleMarkerSettings) {
        markers.addAll(logicalCircleMarkers);
        setCircleMarkerSettings(circleMarkerSettings);
    }


    private ObservableList<LogicalCircleMarker> markers = FXCollections.observableArrayList();

    public ObservableList<LogicalCircleMarker> getMarkers() {
        return markers;
    }

    @Override
    public void removeMarkers() {
        super.removeMarkers();
        markers.clear();
    }

    @Override
    public int getMarkersCount() {
        return markers.size();
    }


    private CircleMarkerSettings circleMarkerSettings;

    public CircleMarkerSettings getCircleMarkerSettings() {
        return circleMarkerSettings;
    }

    private void setCircleMarkerSettings(CircleMarkerSettings settings) {
        this.circleMarkerSettings = settings;
    }
}
