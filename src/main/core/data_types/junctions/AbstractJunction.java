package main.core.data_types.junctions;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Summary;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.LogParameters;
import main.core.data_types.settings.Settings;
import main.core.data_types.settings.markers_settings.ContourSettings;
import main.core.data_types.settings.markers_settings.RulerSettings;
import main.truck.data_types.NavigationFile;

import java.io.File;

public abstract class AbstractJunction<T extends Marker> {

    public static AbstractJunction toJunction(NavigationFile sourceFile, Settings settings) {
        LogParameters logParameters = settings.getLogParameters();
        switch (settings.getJunctionMode()) {
            case Settings.CIRCLE_MARKERS:
                return new CircleMarkersJunction(sourceFile.getFile(), sourceFile.getInitialNumber(), logParameters.getCubageType(), logParameters.getLength());
            case Settings.MARKERS:
                return new MarkersJunction(sourceFile.getFile(), sourceFile.getInitialNumber(), logParameters.getCubageType(), logParameters.getLength());
            default:
                return new MarkersJunction(sourceFile.getFile(), sourceFile.getInitialNumber(), logParameters.getCubageType(), logParameters.getLength());
        }
    }


    public AbstractJunction(File originalFile, String number, Integer cubageType, LogicalValue length){
       this(originalFile, null,
               number, cubageType, length, null, null, 100,
               new Summary(),
               new LogicalRuler(), new RulerSettings(),
               null, new ContourSettings());
    }

    public AbstractJunction(AbstractJunction abstractJunction) {
        this(abstractJunction.getOriginalFile(), abstractJunction.getModifiedFile(),
                abstractJunction.getNumber(), abstractJunction.getCubageType(), abstractJunction.getLength(), abstractJunction.getBreed(), abstractJunction.getCommentary(), abstractJunction.getCoefficient(),
                abstractJunction.getSummary(),
                abstractJunction.getRuler(), abstractJunction.getRulerSettings(),
                abstractJunction.getLogicalContour(), abstractJunction.getContourSettings());
    }

    public AbstractJunction(File originalFile, File modifiedFile,
                            String number, Integer cubageType, LogicalValue length, Integer breed, String commentary, double coefficient,
                            Summary results,
                            LogicalRuler logicalRuler, RulerSettings rulerSettings,
                            LogicalContour logicalContour, ContourSettings contourSettings) {
        this.originalFile = originalFile;
        this.modifiedFile = modifiedFile;
        setNumber(number);
        setCubageType(cubageType);
        setLength(length);
        setBreed(breed);
        setCommentary(commentary);
        setCoefficient(coefficient);

        this.summary = results;

        setRuler(logicalRuler);
        setRulerSettings(rulerSettings);

        setLogicalContour(logicalContour);
        setContourSettings(contourSettings);
    }

    private StringProperty number = new SimpleStringProperty();

    public StringProperty numberProperty() {
        return number;
    }

    public String getNumber() {
        return number.get();
    }

    public void setNumber(String number) {
        this.number.set(number);
    }

    public boolean hasNumber(){
        return number.get() != null && !number.get().isEmpty();
    }


    private File originalFile, modifiedFile;

    public File getCurrentSource(){
        return modifiedFile == null ? originalFile : modifiedFile;
    }


    public File getOriginalFile() {
        return originalFile;
    }


    public void setModifiedFile(File modifiedFile) {
        this.modifiedFile = modifiedFile;
    }

    public File getModifiedFile() {
        return modifiedFile;
    }

    public boolean hasModifiedFile(){
        return modifiedFile != null;
    }


    private ObjectProperty<Integer> cubageType = new SimpleObjectProperty<>();

    public boolean hasCubageType() {
        return cubageType.get() != null;
    }

    public Integer getCubageType() {
        return cubageType.get();
    }

    public ObjectProperty<Integer> cubageTypeProperty() {
        return cubageType;
    }

    public void setCubageType(Integer cubageType) {
        this.cubageType.set(cubageType);
    }


    private ObjectProperty<LogicalValue> length = new SimpleObjectProperty<>();

    public boolean hasLength() {
        return length.get() != null;
    }

    public LogicalValue getLength() {
        return length.get();
    }

    public ObjectProperty<LogicalValue> lengthProperty() {
        return length;
    }

    public void setLength(LogicalValue length) {
        this.length.set(length);
    }


    private ObjectProperty<Integer> breed = new SimpleObjectProperty<>();

    public boolean hasBreed() {
        return breed.get() != null;
    }

    public Integer getBreed() {
        return breed.get();
    }

    public ObjectProperty<Integer> breedProperty() {
        return breed;
    }

    public void setBreed(Integer breed) {
        this.breed.set(breed);
    }


    private StringProperty commentary = new SimpleStringProperty();

    public String getCommentary() {
        return commentary.get();
    }

    public StringProperty commentaryProperty() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary.set(commentary);
    }


    private DoubleProperty coefficient = new SimpleDoubleProperty();

    public double getCoefficient() {
        return coefficient.get();
    }

    public DoubleProperty coefficientProperty() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient.set(coefficient);
    }


    private Summary summary;

    public Summary getSummary() {
        return summary;
    }


    private ObjectProperty<LogicalRuler> ruler = new SimpleObjectProperty<>();

    private void setRuler(LogicalRuler ruler) {
        this.ruler.set(ruler);
    }

    public LogicalRuler getRuler() {
        return ruler.get();
    }


    private RulerSettings rulerSettings;

    public RulerSettings getRulerSettings() {
        return rulerSettings;
    }

    private void setRulerSettings(RulerSettings rulerSettings) {
        this.rulerSettings = rulerSettings;
    }


    private ObjectProperty<LogicalContour> logicalContour = new SimpleObjectProperty<>();

    public boolean hasLogicalContour() {
        return logicalContour.get() != null;
    }

    public LogicalContour getLogicalContour() {
        return logicalContour.get();
    }

    public ObjectProperty<LogicalContour> logicalContourProperty() {
        return logicalContour;
    }

    public void setLogicalContour(LogicalContour logicalContours) {
        this.logicalContour.set(logicalContours);
    }


    private ContourSettings contourSettings;

    public ContourSettings getContourSettings() {
        return contourSettings;
    }

    private void setContourSettings(ContourSettings contourSettings) {
        this.contourSettings = contourSettings;
    }


    public abstract ObservableList<T> getMarkers();

    public abstract int getMarkersCount();

    public void removeMarkers() {
    }


    public String getOutputName(String truckNumber){
        return "S_" + number.get() + "_" + String.valueOf(getMarkersCount()) + "_" + truckNumber;
    }
}
