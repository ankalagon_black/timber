package main.core.data_types.junctions;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Summary;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.markers_settings.ContourSettings;
import main.core.data_types.settings.markers_settings.MarkerSettings;
import main.core.data_types.settings.markers_settings.RulerSettings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MarkersJunction extends AbstractJunction<Marker> {

    public MarkersJunction(File originalFile, String number, Integer cubageType, LogicalValue length) {
        super(originalFile, number, cubageType, length);
        setMarkerSettings(new MarkerSettings());
    }

    public MarkersJunction(AbstractJunction abstractJunction, List<Marker> junctionMarkers, MarkerSettings markerSettings) {
        super(abstractJunction);
        initialize(junctionMarkers, markerSettings);
    }

    public MarkersJunction(File originalFile, File modifiedFile,
                           String number, Integer cubageType, LogicalValue length, Integer breed, String commentary, double coefficient,
                           Summary results,
                           LogicalRuler logicalRuler, RulerSettings rulerSettings,
                           LogicalContour logicalContours, ContourSettings contourSettings,
                           List<Marker> markers, MarkerSettings markerSettings) {
        super(originalFile, modifiedFile,
                number, cubageType, length, breed, commentary, coefficient,
                results,
                logicalRuler, rulerSettings,
                logicalContours, contourSettings);
        initialize(markers, markerSettings);
    }

    public MarkersJunction(CircleMarkersJunction circleMarkersJunction) {
        super(circleMarkersJunction);
        List<Marker> markers = circleMarkersJunction.getMarkers().stream().collect(ArrayList::new, (markerList, marker) ->
                markerList.add(new Marker(marker)), ArrayList::addAll);
        initialize(markers, new MarkerSettings(circleMarkersJunction.getCircleMarkerSettings()));
    }


    private void initialize(List<Marker> junctionMarkers, MarkerSettings markerSettings) {
        markers.addAll(junctionMarkers);
        setMarkerSettings(markerSettings);
    }


    private ObservableList<Marker> markers = FXCollections.observableArrayList();

    public ObservableList<Marker> getMarkers() {
        return markers;
    }

    public void removeMarkers(){
        super.removeMarkers();
        markers.clear();
    }

    @Override
    public int getMarkersCount() {
        return markers.size();
    }


    private MarkerSettings markerSettings;

    public MarkerSettings getMarkerSettings() {
        return markerSettings;
    }

    private void setMarkerSettings(MarkerSettings markerSettings) {
        this.markerSettings = markerSettings;
    }
}
