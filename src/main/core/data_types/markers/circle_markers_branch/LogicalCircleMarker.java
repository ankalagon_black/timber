package main.core.data_types.markers.circle_markers_branch;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.markers.Marker;

public class LogicalCircleMarker extends CircleMarker {

    public LogicalCircleMarker(double x, double y, double radius, LogicalValue logicalDiameter) {
        super(x, y, radius);
        setLogicalDiameter(logicalDiameter);
    }

    public LogicalCircleMarker(Marker marker, double radius, LogicalValue logicalDiameter) {
        this(marker.getX(), marker.getY(), radius, logicalDiameter);
    }


    private ObjectProperty<LogicalValue> logicalDiameter = new SimpleObjectProperty<>();

    public LogicalValue getLogicalDiameter() {
        return logicalDiameter.get();
    }

    public ObjectProperty<LogicalValue> logicalDiameterProperty() {
        return logicalDiameter;
    }

    public void setLogicalDiameter(LogicalValue logicalDiameter) {
        this.logicalDiameter.set(logicalDiameter);
    }
}
