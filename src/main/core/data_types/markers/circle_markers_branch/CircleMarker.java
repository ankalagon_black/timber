package main.core.data_types.markers.circle_markers_branch;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import main.core.data_types.markers.Marker;

public class CircleMarker extends Marker {

    public CircleMarker(double x, double y, double radius) {
        super(x, y);
        this.radius.set(radius);
    }

    public CircleMarker(CircleMarker circleMarker) {
        this(circleMarker.getX(), circleMarker.getY(), circleMarker.getRadius());
    }


    private DoubleProperty radius = new SimpleDoubleProperty();

    public double getRadius() {
        return radius.get();
    }

    public DoubleProperty radiusProperty() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius.set(radius);
    }
}
