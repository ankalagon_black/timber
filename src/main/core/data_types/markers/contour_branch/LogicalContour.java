package main.core.data_types.markers.contour_branch;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Point2D;
import main.core.data_types.markers.Marker;

import java.util.List;

public class LogicalContour extends Contour {

    public LogicalContour(double x, double y) {
        super(x, y);
        setLogicalArea(0);
    }

    public LogicalContour(double x, double y, double area, List<Point2D> points, double logicalArea) {
        super(x, y, area, points);
        setLogicalArea(logicalArea);
    }

    public LogicalContour(List<Point2D> points) {
        super(points);
        setLogicalArea(0);
    }


    private DoubleProperty logicalArea = new SimpleDoubleProperty();

    public double getLogicalArea() {
        return logicalArea.get();
    }

    public DoubleProperty logicalAreaProperty() {
        return logicalArea;
    }

    public void setLogicalArea(double logicalArea) {
        this.logicalArea.set(logicalArea);
    }
}
