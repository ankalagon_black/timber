package main.core.data_types.markers.contour_branch;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import main.core.data_types.markers.Marker;
import org.opencv.core.Point;

import java.util.*;

public class Contour extends Marker {

    public Contour(double x, double y) {
        super(x, y);

        points.add(new Point2D(0, 0));

        baseline = new Point2D(0,0);
    }

    public Contour(double x, double y, double area, List<Point2D> points) {
        super(x, y);

        this.points.addAll(points);

        recomputeBaseline();

        setArea(area);
    }

    public Contour(List<Point2D> points) {
        this.points.addAll(points);

        Point2D baseline = recomputeBaseline();

        setX(baseline.getX());
        setY(baseline.getY());

        calculateArea();
    }


    private Point2D baseline;

    public void setBaseline(Point2D baseline) {
        this.baseline = baseline;
    }

    public Point2D recomputeBaseline() {
        double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE;
        for (Point2D i: points) {
            if(i.getX() < minX) minX = i.getX();
            if(i.getY() < minY) minY = i.getY();
        }
        baseline = new Point2D(minX, minY);

        return baseline;
    }

    public Point2D getBaseline() {
        return baseline;
    }


    private ObservableList<Point2D> points = FXCollections.observableArrayList();

    public ObservableList<Point2D> getPoints() {
        return points;
    }


    private DoubleProperty area = new SimpleDoubleProperty();

    public double getArea() {
        return area.get();
    }

    public DoubleProperty areaProperty() {
        return area;
    }

    public void setArea(double area) {
        this.area.set(area);
    }


    public void calculateArea() {
        if(points.size() < 3) {
            setArea(0);
            return;
        }

        double area = 0;
        int size = points.size();
        for (int i = 0; i < size; i++) {
            Point2D v = points.get(i),
                    next = points.get((i + 1) % size);
            area += v.getX() * next.getY() - next.getX() * v.getY();
        }
        area = Math.abs(area) * 0.5;

        setArea(area);
    }


}