package main.core.data_types.markers.ruler_branch;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import main.core.data_types.markers.Marker;

public class Ruler extends Marker {

    public Ruler() {
        super();
        setStart(new Point2D(0,0));
        setEnd(new Point2D(0,0));
        setInitialized(false);
    }

    public Ruler(double x, double y, Point2D start, Point2D end) {
        super(x, y);
        setStart(start);
        setEnd(end);
        setInitialized(x != 0 || y != 0 || start.magnitude() != 0 || end.magnitude() != 0);
    }

    public Ruler(Ruler ruler) {
        super(ruler);
        setStart(ruler.getStart());
        setEnd(ruler.getEnd());
        setInitialized(getX() != 0 || getY() != 0 || getStart().magnitude() != 0 || getEnd().magnitude() != 0);
    }


    public void initialize(double width) {
        setX(0);
        setY(10);
        setStart(new Point2D(0, 10));
        setEnd(new Point2D(width, 10));
        setInitialized(true);
    }

    private BooleanProperty initialized = new SimpleBooleanProperty();

    public boolean isInitialized() {
        return initialized.get();
    }

    public BooleanProperty initializedProperty() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized.set(initialized);
    }


    private ObjectProperty<Point2D> start = new SimpleObjectProperty<>();

    public Point2D getStart() {
        return start.get();
    }

    public ObjectProperty<Point2D> startProperty() {
        return start;
    }

    public void setStart(Point2D start) {
        this.start.set(start);
    }


    private ObjectProperty<Point2D> end = new SimpleObjectProperty<>();

    public Point2D getEnd() {
        return end.get();
    }

    public ObjectProperty<Point2D> endProperty() {
        return end;
    }

    public void setEnd(Point2D end) {
        this.end.set(end);
    }
}
