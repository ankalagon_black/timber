package main.core.data_types.markers.ruler_branch;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import main.core.data_types.base.LogicalValue;

public class LogicalRuler extends Ruler {
    public static final LogicalValue DEFAULT_LOGICAL_LENGTH = new LogicalValue(3);

    public LogicalRuler() {
        super();
        setLogicalLength(DEFAULT_LOGICAL_LENGTH);
    }

    public LogicalRuler(double x, double y, Point2D start, Point2D end, LogicalValue logicalLength) {
        super(x, y, start, end);
        setLogicalLength(logicalLength);
    }

    public LogicalRuler(LogicalRuler logicalRuler) {
        super(logicalRuler);
        setLogicalLength(logicalRuler.getLogicalLength());
    }


    private ObjectProperty<LogicalValue> logicalLength = new SimpleObjectProperty<>();

    public LogicalValue getLogicalLength() {
        return logicalLength.get();
    }

    public ObjectProperty<LogicalValue> logicalLengthProperty() {
        return logicalLength;
    }

    public void setLogicalLength(LogicalValue logicalLength) {
        this.logicalLength.set(logicalLength);
    }


    public LogicalValue getLogicalPerPixel() {
        Point2D dist = getEnd().subtract(getStart());
        double w = dist.getX(), h = dist.getY();
        if(w != 0 || h != 0) return new LogicalValue(getLogicalLength().getValue() / Math.sqrt(w * w + h * h));
        else return new LogicalValue(0);
    }

    public LogicalValue getPixelPerLogical() {
        Point2D dist = getEnd().subtract(getStart());
        double w = dist.getX(), h = dist.getY();
        if(getLogicalLength().getValue() != 0) return new LogicalValue(Math.sqrt(w * w + h * h) / getLogicalLength().getValue());
        else return new LogicalValue(0);
    }
}
