package main.core.data_types.markers;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class Marker {

    public Marker() {
        setX(0);
        setY(0);
    }

    public Marker(double x, double y){
        setX(x);
        setY(y);
    }

    public Marker(Marker marker) {
        setX(marker.getX());
        setY(marker.getY());
    }


    private DoubleProperty x = new SimpleDoubleProperty();

    public void setX(double x) {
        this.x.set(x);
    }

    public double getX() {
        return x.get();
    }

    public DoubleProperty xProperty() {
        return x;
    }


    private DoubleProperty y = new SimpleDoubleProperty();

    public void setY(double y) {
        this.y.set(y);
    }

    public double getY() {
        return y.get();
    }

    public DoubleProperty yProperty() {
        return y;
    }
}
