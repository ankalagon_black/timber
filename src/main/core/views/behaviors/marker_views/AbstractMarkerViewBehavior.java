package main.core.views.behaviors.marker_views;

import com.sun.javafx.scene.control.behavior.BehaviorBase;
import com.sun.javafx.scene.control.behavior.KeyBinding;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import main.core.views.marker_views.AbstractMarkerView;
import main.core.views.skins.marker_views.AbstractMarkerViewSkin;

import java.util.List;

import static main.core.utils.ViewUtils.eventToPoint;

public abstract class AbstractMarkerViewBehavior<C extends AbstractMarkerView> extends BehaviorBase<C>{

    private boolean wasDragged = false;
    private boolean inMove = false, inResize = false;
    private Point2D lastPoint = null;

    private final EventHandler<ScrollEvent> scrollEventEventHandler = this::onScrolled;

    public AbstractMarkerViewBehavior(C control, List<KeyBinding> keyBindings) {
        super(control, keyBindings);

        control.addEventHandler(ScrollEvent.SCROLL, scrollEventEventHandler);
    }

    @Override
    public void dispose() {
        super.dispose();
        getControl().removeEventHandler(ScrollEvent.SCROLL, scrollEventEventHandler);
    }


    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton().equals(MouseButton.PRIMARY) && isMovementAndResizeAllowed()) {
            if(isInResizeRange(e)) {
                inResize = true;
                onAfterResizeStarted(e);

                lastPoint = getPointInParent(e);
                getControl().requestFocus();
                e.consume();
            }
            else if(isInMovementRange(e)) {
                inMove = true;
                onAfterMovementStarted(e);

                lastPoint = getPointInParent(e);
                getControl().requestFocus();
                e.consume();
            }
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if(e.getButton().equals(MouseButton.PRIMARY) && (inResize || inMove) && isMovementAndResizeAllowed()) {
            if(!wasDragged) wasDragged = true;

            Point2D pos = getPointInParent(e);
            Point2D difference = pos.subtract(lastPoint);

            if(inMove) ((AbstractMarkerViewSkin) getControl().getSkin()).onMoved(e, difference);
            else ((AbstractMarkerViewSkin) getControl().getSkin()).onResized(e, difference);

            lastPoint = pos;

            e.consume();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getButton().equals(MouseButton.PRIMARY)) {
            if((inResize || inMove) && isMovementAndResizeAllowed()) {
                if (inMove) {
                    inMove = false;
                    onAfterDragFinished(e);
                }
                else {
                    inResize = false;
                    onAfterResizeFinished(e);
                }

                if(wasDragged) {
                    e.consume();
                    wasDragged = false;
                }
            }
        }
    }


    protected void onScrolled(ScrollEvent e) {
        if(inMove || inResize) e.consume();
    }

    protected boolean isMovementAndResizeAllowed() {
        return true;
    };

    public boolean isInMove() {
        return inMove;
    }

    public boolean isInResize() {
        return inResize;
    }

    protected abstract boolean isInResizeRange(MouseEvent event);

    protected abstract boolean isInMovementRange(MouseEvent event);

    protected abstract void onAfterMovementStarted(MouseEvent event);
    protected abstract void onAfterResizeStarted(MouseEvent event);

    protected abstract void onAfterDragFinished(MouseEvent event);
    protected abstract void onAfterResizeFinished(MouseEvent event);

    private Point2D getPointInParent(MouseEvent mouseEvent){
        return getControl().localToParent(eventToPoint(mouseEvent));
    }
}
