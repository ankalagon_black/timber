package main.core.views.behaviors.marker_views;

import com.sun.javafx.scene.control.behavior.KeyBinding;
import javafx.scene.input.MouseEvent;
import main.core.views.marker_views.AbstractMarkerView;

import java.util.List;

public class MarkerViewBehavior<C extends AbstractMarkerView> extends AbstractMarkerViewBehavior<C> {

    public MarkerViewBehavior(C control, List<KeyBinding> keyBindings) {
        super(control, keyBindings);
    }

    @Override
    protected boolean isInResizeRange(MouseEvent event) {
        return false;
    }

    @Override
    protected boolean isInMovementRange(MouseEvent event) {
        return true;
    }

    @Override
    protected void onAfterMovementStarted(MouseEvent event) {

    }

    @Override
    protected void onAfterResizeStarted(MouseEvent event) {

    }

    @Override
    protected void onAfterDragFinished(MouseEvent event) {

    }

    @Override
    protected void onAfterResizeFinished(MouseEvent event) {

    }
}
