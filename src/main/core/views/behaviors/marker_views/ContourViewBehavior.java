package main.core.views.behaviors.marker_views;

import com.sun.javafx.scene.control.behavior.KeyBinding;
import javafx.event.EventHandler;
import javafx.event.WeakEventHandler;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import main.core.views.MarkerImageView;
import main.core.views.marker_views.AbstractContourView;
import main.core.views.skins.MarkerImageViewSkin;
import main.core.views.skins.marker_views.ContourViewSkin;
import main.junctions.views.custom.marker_image_views.JunctionImageView;

import java.util.List;

public class ContourViewBehavior<C extends AbstractContourView> extends AbstractMarkerViewBehavior<C> {
    public static final int NO = -1;

    private int pointerIndex = NO;

    public ContourViewBehavior(C control, List<KeyBinding> keyBindings) {
        super(control, keyBindings);
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    public int getPointerIndex() {
        return pointerIndex;
    }


    @Override
    protected boolean isInResizeRange(MouseEvent event) {
        return ((ContourViewSkin) getControl().getSkin()).isInResizeRange(event);
    }

    @Override
    protected boolean isInMovementRange(MouseEvent event) {
        return ((ContourViewSkin) getControl().getSkin()).isInMovementRange(event);
    }

    @Override
    protected void onAfterMovementStarted(MouseEvent event) {

    }

    @Override
    protected void onAfterResizeStarted(MouseEvent event) {
        pointerIndex = ((ContourViewSkin) getControl().getSkin()).getPointerIndex(event);
    }

    @Override
    protected void onAfterDragFinished(MouseEvent event) {

    }

    @Override
    protected void onAfterResizeFinished(MouseEvent event) {
        pointerIndex = NO;
    }
}

//            if (getControl().getContour().getPoints().size() >= 3) {
//                //endResize(e);
//                //getControl().createdProperty().set(true);
//                e.consume();
//            }
//if(getControl().isCreated()) else pointerIndex = getControl().getContour().getPoints().size() - 1;
//        pane.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);
//        pane.removeEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);
//        if(e.getButton().equals(MouseButton.PRIMARY)) {
//            //endResize(e);
//
//            Point2D point2D = getControl().getContour().getPoints().get(getControl().getContour().getPoints().size() - 1).get();
//            getControl().getContour().getPoints().add(new SimpleObjectProperty<>(point2D));
//
//            //startResize(e);
//            e.consume();
//        }
//        if(!getControl().isCreated()) {
//    private EventHandler<MouseEvent> mouseMovedEvent = e -> {
//        if(!getControl().isCreated()) {
//
//            e.consume();
//            e = e.copyFor(getControl(), getControl());
//
//            if(!isInResize()) startResize(e);
//            else onResize(e);
//
//            e.consume();
//        }
//    };
//Point2D point2D = getControl().getContour().getPoints().get(getControl().getContour().getPoints().size() - 1).get();
//        MarkerImageView imageView = getControl().getMarkerImageView();
//        Pane pane = ((MarkerImageViewSkin) imageView.getSkin()).getMarkersRoot();
//
//        pane.removeEventHandler(MouseEvent.MOUSE_CLICKED, mouseClicked);
//        MarkerImageView imageView = getControl().getMarkerImageView();
//        Pane pane = ((MarkerImageViewSkin) imageView.getSkin()).getMarkersRoot();

//control.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseClicked);
//        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, new WeakEventHandler<>(mouseClicked));
//    private EventHandler<MouseEvent> mouseClicked = e -> {
//        if(((JunctionImageView) getControl().getMarkerImageView()).getContourSettingsWrapper().isEnabled()) {
//            if(e.getButton().equals(MouseButton.SECONDARY)) {

//            }
//        }
//    };