package main.core.views.behaviors.marker_views;

import com.sun.javafx.scene.control.behavior.KeyBinding;
import javafx.scene.input.MouseEvent;
import main.core.views.marker_views.AbstractCircleMarkerView;
import main.core.views.skins.marker_views.CircleMarkerViewSkin;

import java.util.List;

public class CircleMarkerViewBehavior<C extends AbstractCircleMarkerView> extends AbstractMarkerViewBehavior<C> {

    public CircleMarkerViewBehavior(C control, List<KeyBinding> keyBindings) {
        super(control, keyBindings);
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    protected boolean isInResizeRange(MouseEvent event) {
        return ((CircleMarkerViewSkin) getControl().getSkin()).isInResizeRange(event);
    }

    @Override
    protected boolean isInMovementRange(MouseEvent event) {
        return ((CircleMarkerViewSkin) getControl().getSkin()).isInMovementRange(event);
    }

    @Override
    protected void onAfterMovementStarted(MouseEvent event) {
    }

    @Override
    protected void onAfterResizeStarted(MouseEvent event) {
    }

    @Override
    protected void onAfterDragFinished(MouseEvent event) {
    }

    @Override
    protected void onAfterResizeFinished(MouseEvent event) {
    }
}
