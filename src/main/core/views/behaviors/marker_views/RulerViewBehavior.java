package main.core.views.behaviors.marker_views;

import com.sun.javafx.scene.control.behavior.KeyBinding;
import javafx.scene.input.MouseEvent;
import main.core.views.marker_views.AbstractRulerView;
import main.core.views.skins.marker_views.RulerViewSkin;

import java.util.List;

public class RulerViewBehavior<C extends AbstractRulerView> extends AbstractMarkerViewBehavior<C> {
    public static final int START_POINTER = 0, END_POINTER = 1, NO = - 1;

    private int pointerType = NO;

    public RulerViewBehavior(C control, List<KeyBinding> keyBindings) {
        super(control, keyBindings);
    }


    public int getPointerType() {
        return pointerType;
    }


    @Override
    protected boolean isInResizeRange(MouseEvent event) {
        return ((RulerViewSkin) getControl().getSkin()).isInResizeRange(event);
    }

    @Override
    protected boolean isInMovementRange(MouseEvent event) {
        return ((RulerViewSkin) getControl().getSkin()).isInMovementRange(event);
    }

    @Override
    protected void onAfterMovementStarted(MouseEvent event) {

    }

    @Override
    protected void onAfterResizeStarted(MouseEvent event) {
        pointerType = ((RulerViewSkin) getControl().getSkin()).getPointerType(event);
    }

    @Override
    protected void onAfterDragFinished(MouseEvent event) {

    }

    @Override
    protected void onAfterResizeFinished(MouseEvent event) {
        pointerType = NO;
    }
}
