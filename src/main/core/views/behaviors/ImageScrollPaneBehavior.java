package main.core.views.behaviors;

import com.sun.javafx.scene.control.behavior.BehaviorBase;
import com.sun.javafx.scene.control.behavior.KeyBinding;
import javafx.geometry.NodeOrientation;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import main.core.views.ImageScrollPane;
import main.core.views.skins.ImageScrollPaneSkin;

import java.util.ArrayList;
import java.util.List;

import static javafx.scene.input.KeyCode.*;

public class ImageScrollPaneBehavior extends BehaviorBase<ImageScrollPane> {
    /**
     * Create a new BehaviorBase for the given control. The Control must not
     * be null.
     *
     * @param control     The control. Must not be null.
     */
    public ImageScrollPaneBehavior(ImageScrollPane control) {
        super(control, SCROLL_PANE_BINDINGS);
    }


    public void horizontalUnitIncrement() {
        ((ImageScrollPaneSkin)getControl().getSkin()).hsbIncrement();
    }
    public void horizontalUnitDecrement() {
        ((ImageScrollPaneSkin)getControl().getSkin()).hsbDecrement();
    }
    public void verticalUnitIncrement() {
        ((ImageScrollPaneSkin)getControl().getSkin()).vsbIncrement();
    }
    void verticalUnitDecrement() {
        ((ImageScrollPaneSkin)getControl().getSkin()).vsbDecrement();
    }
    void horizontalPageIncrement() {
        ((ImageScrollPaneSkin)getControl().getSkin()).hsbPageIncrement();
    }
    void horizontalPageDecrement() {
        ((ImageScrollPaneSkin)getControl().getSkin()).hsbPageDecrement();
    }
    void verticalPageIncrement() {
        ((ImageScrollPaneSkin)getControl().getSkin()).vsbPageIncrement();
    }
    void verticalPageDecrement() {
        ((ImageScrollPaneSkin)getControl().getSkin()).vsbPageDecrement();
    }
    void verticalHome() {
        ImageScrollPane sp = getControl();
        sp.setHvalue(sp.getHmin());
        sp.setVvalue(sp.getVmin());
    }
    void verticalEnd() {
        ImageScrollPane sp = getControl();
        sp.setHvalue(sp.getHmax());
        sp.setVvalue(sp.getVmax());
    }

    private void zoomInToCenter(){
        ((ImageScrollPaneSkin) getControl().getSkin()).resizeImageToCenter(5);
    }

    private void zoomOutFromCenter(){
        ((ImageScrollPaneSkin) getControl().getSkin()).resizeImageToCenter(-5);
    }

    /***************************************************************************
     *                                                                         *
     * Key event handling                                                      *
     *                                                                         *
     **************************************************************************/

    private static final String TRAVERSE_DEBUG = "TraverseDebug";
    private static final String HORIZONTAL_UNITDECREMENT = "HorizontalUnitDecrement";
    private static final String HORIZONTAL_UNITINCREMENT = "HorizontalUnitIncrement";
    private static final String VERTICAL_UNITDECREMENT = "VerticalUnitDecrement";
    private static final String VERTICAL_UNITINCREMENT = "VerticalUnitIncrement";
    private static final String VERTICAL_PAGEDECREMENT = "VerticalPageDecrement";
    private static final String VERTICAL_PAGEINCREMENT = "VerticalPageIncrement";
    private static final String VERTICAL_HOME = "VerticalHome";
    private static final String VERTICAL_END = "VerticalEnd";
    private static final String ZOOM_IN = "ZoomIn";
    private static final String ZOOM_OUT = "ZoomOut";

    /**
     * We manually handle focus traversal keys due to the ScrollPane binding
     * the left/right/up/down keys specially.
     */
    protected static final List<KeyBinding> SCROLL_PANE_BINDINGS = new ArrayList<>();
    static {
        // TODO XXX DEBUGGING ONLY
        SCROLL_PANE_BINDINGS.add(new KeyBinding(F4, TRAVERSE_DEBUG).alt().ctrl().shift());

        SCROLL_PANE_BINDINGS.add(new KeyBinding(LEFT, HORIZONTAL_UNITDECREMENT));
        SCROLL_PANE_BINDINGS.add(new KeyBinding(RIGHT, HORIZONTAL_UNITINCREMENT));

        SCROLL_PANE_BINDINGS.add(new KeyBinding(UP, VERTICAL_UNITDECREMENT));
        SCROLL_PANE_BINDINGS.add(new KeyBinding(DOWN, VERTICAL_UNITINCREMENT));

        SCROLL_PANE_BINDINGS.add(new KeyBinding(PAGE_UP, VERTICAL_PAGEDECREMENT));
        SCROLL_PANE_BINDINGS.add(new KeyBinding(PAGE_DOWN, VERTICAL_PAGEINCREMENT));
        SCROLL_PANE_BINDINGS.add(new KeyBinding(SPACE, VERTICAL_PAGEINCREMENT));

        SCROLL_PANE_BINDINGS.add(new KeyBinding(HOME, VERTICAL_HOME));
        SCROLL_PANE_BINDINGS.add(new KeyBinding(END, VERTICAL_END));

        SCROLL_PANE_BINDINGS.add(new KeyBinding(PLUS, ZOOM_IN).ctrl());
        SCROLL_PANE_BINDINGS.add(new KeyBinding(EQUALS, ZOOM_IN).ctrl());
        SCROLL_PANE_BINDINGS.add(new KeyBinding(ADD, ZOOM_IN).ctrl());

        SCROLL_PANE_BINDINGS.add(new KeyBinding(MINUS, ZOOM_OUT).ctrl());
        SCROLL_PANE_BINDINGS.add(new KeyBinding(SUBTRACT, ZOOM_OUT).ctrl());
    }

    protected /*final*/ String matchActionForEvent(KeyEvent e) {
        //TODO - untested code doesn't seem to get triggered (key eaten?)
        String action = super.matchActionForEvent(e);
        if (action != null) {
            if (e.getCode() == LEFT) {
                if (getControl().getEffectiveNodeOrientation() == NodeOrientation.RIGHT_TO_LEFT) {
                    action = "HorizontalUnitIncrement";
                }
            } else if (e.getCode() == RIGHT) {
                if (getControl().getEffectiveNodeOrientation() == NodeOrientation.RIGHT_TO_LEFT) {
                    action = "HorizontalUnitDecrement";
                }
            }
        }
        return action;
    }

    @Override protected void callAction(String name) {
        switch (name) {
            case HORIZONTAL_UNITDECREMENT:
                horizontalUnitDecrement();
                break;
            case HORIZONTAL_UNITINCREMENT:
                horizontalUnitIncrement();
                break;
            case VERTICAL_UNITDECREMENT:
                verticalUnitDecrement();
                break;
            case VERTICAL_UNITINCREMENT:
                verticalUnitIncrement();
                break;
            case VERTICAL_PAGEDECREMENT:
                verticalPageDecrement();
                break;
            case VERTICAL_PAGEINCREMENT:
                verticalPageIncrement();
                break;
            case VERTICAL_HOME:
                verticalHome();
                break;
            case VERTICAL_END:
                verticalEnd();
                break;
            case ZOOM_IN:
                zoomInToCenter();
                break;
            case ZOOM_OUT:
                zoomOutFromCenter();
                break;
            default :
                super.callAction(name);
                break;
        }
    }

    public void contentDragged(double deltaX, double deltaY) {
        // negative when dragged to the right/bottom
        ImageScrollPane scroll = getControl();
        if (!scroll.isPannable()) return;
        if (deltaX < 0 && scroll.getHvalue() != 0 || deltaX > 0 && scroll.getHvalue() != scroll.getHmax()) {
            scroll.setHvalue(scroll.getHvalue() + deltaX);
        }
        if (deltaY < 0 && scroll.getVvalue() != 0 || deltaY > 0 && scroll.getVvalue() != scroll.getVmax()) {
            scroll.setVvalue(scroll.getVvalue() + deltaY);
        }
    }

    /***************************************************************************
     *                                                                         *
     * Mouse event handling                                                    *
     *                                                                         *
     **************************************************************************/

    public void mouseClicked() {
        getControl().requestFocus();
    }

    @Override public void mousePressed(MouseEvent e) {
        super.mousePressed(e);
        getControl().requestFocus();
    }
}
