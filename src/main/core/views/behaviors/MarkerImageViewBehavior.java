package main.core.views.behaviors;

import com.sun.javafx.scene.control.behavior.BehaviorBase;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import main.core.views.MarkerImageView;
import main.core.views.skins.MarkerImageViewSkin;

import java.util.ArrayList;

public class MarkerImageViewBehavior<C extends MarkerImageView> extends BehaviorBase<C> {

    private ChangeListener<EventHandler<MouseEvent>> changeListener = (observable, oldValue, newValue) -> {
        if(newValue != null) ((MarkerImageViewSkin) getControl().getSkin()).getMarkersRoot().setOnMouseReleased(newValue);
    };

    public MarkerImageViewBehavior(C control) {
        super(control, new ArrayList<>());

        Platform.runLater(() -> {
            if(getControl().getMouseEventHandler() != null)
                ((MarkerImageViewSkin) getControl().getSkin()).getMarkersRoot().setOnMouseReleased(getControl().getMouseEventHandler());
        });

        getControl().mouseEventHandlerProperty().addListener(changeListener);
    }

    @Override
    public void dispose() {
        super.dispose();
        getControl().mouseEventHandlerProperty().removeListener(changeListener);
    }
}
