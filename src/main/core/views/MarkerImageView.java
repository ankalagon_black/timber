package main.core.views;

import javafx.beans.DefaultProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.Skin;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import main.core.data_types.markers.Marker;
import main.core.views.behaviors.MarkerImageViewBehavior;
import main.core.views.data_types.ListHandler;
import main.core.views.marker_views.AbstractMarkerView;
import main.core.views.skins.MarkerImageViewSkin;

@DefaultProperty("imageView")
public class MarkerImageView extends Control {

    public MarkerImageView() {}


    private ObjectProperty<ImageView> imageView = new SimpleObjectProperty<>();

    public void setImageView(ImageView imageView) {
        this.imageView.set(imageView);
    }

    public ImageView getImageView() {
        return imageView.get();
    }

    public ObjectProperty<ImageView> imageViewProperty() {
        return imageView;
    }


    private ObservableMap<String, ListHandler<?, ? extends AbstractMarkerView>> listMap = FXCollections.observableHashMap();

    public ObservableMap<String, ListHandler<?, ? extends AbstractMarkerView>> getListMap() {
        return listMap;
    }


    public void add(String type, ListHandler<?, ? extends AbstractMarkerView> listHandler) {
        listMap.put(type, listHandler);
    }

    public ObservableList<?> getList(String type) {
        return listMap.containsKey(type) ? listMap.get(type).getList() : null;
    }

    public void remove(String type) {
        listMap.remove(type);
    }



    private ObjectProperty<SelectionModel> selectionModel = new SimpleObjectProperty<>();

    public SelectionModel getSelectionModel() {
        return selectionModel.get();
    }

    public ObjectProperty<SelectionModel> selectionModelProperty() {
        return selectionModel;
    }

    public void setSelectionModel(SelectionModel selectionModel) {
        this.selectionModel.set(selectionModel);
    }


    private ObjectProperty<EventHandler<MouseEvent>> mouseEventHandler = new SimpleObjectProperty<>();

    public EventHandler<MouseEvent> getMouseEventHandler() {
        return mouseEventHandler.get();
    }

    public ObjectProperty<EventHandler<MouseEvent>> mouseEventHandlerProperty() {
        return mouseEventHandler;
    }

    public void setMouseEventHandler(EventHandler<MouseEvent> mouseEventHandler) {
        this.mouseEventHandler.set(mouseEventHandler);
    }


    @Override
    protected Skin<?> createDefaultSkin() {
        return new MarkerImageViewSkin(this, new MarkerImageViewBehavior(this));
    }
}
