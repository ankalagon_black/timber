package main.core.views.skins.marker_views;

import com.sun.javafx.scene.control.skin.BehaviorSkinBase;
import javafx.geometry.Point2D;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import main.core.utils.images.ImageViewLoader;
import main.core.utils.images.ImageViewScale;
import main.core.views.marker_views.AbstractMarkerView;
import main.core.views.behaviors.marker_views.AbstractMarkerViewBehavior;
import main.core.views.skins.MarkerImageViewSkin;

public abstract class AbstractMarkerViewSkin<C extends AbstractMarkerView, BB extends AbstractMarkerViewBehavior<C>> extends BehaviorSkinBase<C, BB> {
    private static final String IMAGE_VIEW = "IMAGE_VIEW";

    private ImageViewLoader imageViewLoader = new ImageViewLoader();
    private ImageViewScale imageViewScale = new ImageViewScale();

    public AbstractMarkerViewSkin(C control, BB behaviorBase) {
        super(control, behaviorBase);

        ImageView imageView = getSkinnable().getMarkerImageView().getImageView();
        imageViewLoader.setImageView(imageView);
        imageViewScale.setImageView(imageView);

        Pane pane = ((MarkerImageViewSkin) getSkinnable().getMarkerImageView().getSkin()).getMarkersRoot();
        pane.layoutBoundsProperty().addListener((observable, oldValue, newValue) -> {
            if(imageViewLoader.isImageLoaded()) updatePosition();
        });

        consumeMouseEvents(false);

        registerChangeListener(getSkinnable().getMarkerImageView().imageViewProperty(), IMAGE_VIEW);
    }


    protected abstract void updatePosition();

    public abstract void onMoved(MouseEvent mouseEvent, Point2D difference);

    public abstract void onResized(MouseEvent mouseEvent, Point2D difference);


    public ImageViewLoader getImageViewLoader() {
        return imageViewLoader;
    }

    public ImageViewScale getImageViewScale() {
        return imageViewScale;
    }

    @Override
    protected void handleControlPropertyChanged(String propertyReference) {
        super.handleControlPropertyChanged(propertyReference);
        switch (propertyReference) {
            case IMAGE_VIEW:
                imageViewLoader.setImageView(getSkinnable().getMarkerImageView().getImageView());
                imageViewScale.setImageView(getSkinnable().getMarkerImageView().getImageView());

                if(imageViewLoader.isImageLoaded()) updatePosition();
                break;
        }
    }
}
