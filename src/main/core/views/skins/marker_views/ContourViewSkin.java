package main.core.views.skins.marker_views;

import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.WeakListChangeListener;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.*;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.contour_branch.Contour;
import main.core.views.behaviors.marker_views.ContourViewBehavior;
import main.core.views.marker_views.AbstractContourView;

import java.util.ArrayList;
import java.util.List;

import static main.core.utils.ViewUtils.eventToPoint;

public class ContourViewSkin<C extends AbstractContourView, BB extends ContourViewBehavior<C>> extends AbstractMarkerViewSkin<C, BB> {

    private StackPane stackPane;
    private Label label;
    private Group group;
    private Polygon polygon;
    private List<Circle> circles = new ArrayList<>();

    private double xCurrent = 0, yCurrent = 0;


    private ChangeListener<Bounds> boundsChangeListener = (observable, oldValue, newValue) -> {
        stackPane.layout();

        Point2D offset = getContourOffsetFromStackPane();

        getNode().setLayoutX(xCurrent - offset.getX());
        getNode().setLayoutY(yCurrent - offset.getY());
    };

    private ListChangeListener<Point2D> point2DListener = c -> {
        while (c.next()) {
            if(c.wasReplaced()) return;
            else if(c.wasAdded()) {
                List<Point2D> added = (List<Point2D>) c.getAddedSubList();

                for (int i = 0; i < added.size(); i++) {
                    Circle circle = new Circle();
                    setCircle(circle);
                    circles.add(circle);
                    group.getChildren().add(circle);
                }
                updatePosition();
            }
            else if(c.wasRemoved()) {
                List<Point2D> removed = (List<Point2D>) c.getRemoved();

                for (int i = 0; i < removed.size(); i++) {
                    int index = c.getFrom() + i;
                    group.getChildren().remove(circles.get(index));
                    circles.remove(index);
                }
                updatePosition();
            }
        }
    };

    public ContourViewSkin(C control, BB behaviorBase) {
        super(control, behaviorBase);

        stackPane = new StackPane();
        stackPane.layoutBoundsProperty().addListener(boundsChangeListener);

        label = new Label();

        group = new Group();
        group.layoutBoundsProperty().addListener(boundsChangeListener);

        polygon = new Polygon();

        int size = getSkinnable().getContour().getPoints().size();
        for (int i = 0; i < size; i++) circles.add(new Circle());

        getSkinnable().getContour().getPoints().addListener(new WeakListChangeListener<>(point2DListener));

        group.getChildren().add(polygon);
        group.getChildren().addAll(circles);

        stackPane.getChildren().add(group);
        stackPane.getChildren().add(label);

        getChildren().add(stackPane);

        updatePosition();
    }


    public List<Circle> getCircles() {
        return circles;
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public Label getLabel() {
        return label;
    }

    public StackPane getStackPane() {
        return stackPane;
    }


    protected void setCircle(Circle circle) {

    }

    @Override
    protected void updatePosition() {
        polygon.getPoints().clear();

        Contour contour = getSkinnable().getContour();

        double relation = getImageViewScale().getScaledToActualRatio();

        List<Point2D> points = contour.getPoints().stream().collect(ArrayList::new, (point2DS, point2D) -> point2DS.add(point2D.multiply(relation)), ArrayList::addAll);

        int size = points.size();
        for (int i = 0; i < size; i++) {
            Point2D a = points.get(i);

            Circle circle = circles.get(i);
            circle.setCenterX(a.getX());
            circle.setCenterY(a.getY());

            polygon.getPoints().add(a.getX());
            polygon.getPoints().add(a.getY());
        }

        xCurrent = contour.getX() * relation;
        yCurrent = contour.getY() * relation;

        Point2D offset = getContourOffsetFromStackPane();

        getNode().setLayoutX(xCurrent - offset.getX());
        getNode().setLayoutY(yCurrent - offset.getY());
    }

    @Override
    public void onMoved(MouseEvent mouseEvent, Point2D difference) {
        xCurrent += difference.getX();
        yCurrent += difference.getY();

        Bounds bounds = getImageViewScale().getBounds();

        Point2D size = new Point2D(polygon.getBoundsInLocal().getWidth(), polygon.getBoundsInLocal().getHeight());

        double lowerXBorder = bounds.getMinX(),
                upperXBorder = bounds.getMaxX() - size.getX();

        double lowerYBorder = bounds.getMinY(),
                upperYBorder = bounds.getMaxY() - size.getY();

        if(xCurrent < lowerXBorder) xCurrent = lowerXBorder;
        else if(xCurrent > upperXBorder) xCurrent = upperXBorder;

        if(yCurrent < lowerYBorder) yCurrent = lowerYBorder;
        else if(yCurrent > upperYBorder) yCurrent = upperYBorder;

        Point2D offset = getContourOffsetFromStackPane();

        getNode().setLayoutX(xCurrent - offset.getX());
        getNode().setLayoutY(yCurrent - offset.getY());

        double relation = getImageViewScale().getActualToScaledRatio();

        Marker marker = getSkinnable().getMarker();
        marker.setX(xCurrent * relation);
        marker.setY(yCurrent * relation);
    }

    @Override
    public void onResized(MouseEvent mouseEvent, Point2D difference) {
        double newXValue, newYValue;

        int pointerIndex = getBehavior().getPointerIndex();

        Circle circle = getCircles().get(pointerIndex);

        Point2D centerInGroup = circle.localToParent(circle.getCenterX(), circle.getCenterY());

        newXValue = centerInGroup.getX() + difference.getX();
        newYValue = centerInGroup.getY() + difference.getY();

        double baselineX = Double.MAX_VALUE, baselineY = Double.MAX_VALUE,
                newBaselineX = Double.MAX_VALUE, newBaselineY = Double.MAX_VALUE;

        ObservableList<Double> coordinates = polygon.getPoints();
        int size = coordinates.size();
        for (int i = 0; i < size; i+=2) {
            if(coordinates.get(i) < baselineX) baselineX = coordinates.get(i);
            if(coordinates.get(i + 1) < baselineY) baselineY = coordinates.get(i + 1);
        }

        for (int i = 0; i < size; i+=2) {
            if(i == pointerIndex * 2) {
                if(newXValue < newBaselineX) newBaselineX = newXValue;
                if(newYValue < newBaselineY) newBaselineY = newYValue;
            }
            else {
                if(coordinates.get(i) < newBaselineX) newBaselineX = coordinates.get(i);
                if(coordinates.get(i + 1) < newBaselineY) newBaselineY = coordinates.get(i + 1);
            }
        }

        Bounds bounds = getImageViewScale().getBounds();

        if(baselineX != newBaselineX) {
            double shift = newBaselineX - baselineX;

            double newX = xCurrent + shift;

            double lowerXBorder = bounds.getMinX();
            if(newX < lowerXBorder) {
                newXValue += lowerXBorder - newX;
                newX = lowerXBorder;
            }

            xCurrent = newX;
        }
        else {
            double upperXBorder = bounds.getMaxX() - xCurrent + baselineX;
            if (newXValue > upperXBorder) newXValue = upperXBorder;
        }

        if(baselineY != newBaselineY) {
            double shift = newBaselineY - baselineY;

            double newY = yCurrent + shift;

            double lowerYBorder = bounds.getMinY();
            if(newY < lowerYBorder) {
                newYValue += lowerYBorder - newY;
                newY = lowerYBorder;
            }

            yCurrent = newY;
        }
        else {
            double upperYBorder = bounds.getMaxY() - yCurrent + baselineY;
            if (newYValue > upperYBorder) newYValue = upperYBorder;
        }

        Point2D inCircle = circle.parentToLocal(newXValue, newYValue);

        circle.setCenterX(inCircle.getX());
        circle.setCenterY(inCircle.getY());

        Point2D inPolygon = polygon.parentToLocal(newXValue, newYValue);

        polygon.getPoints().set(pointerIndex * 2, inPolygon.getX());
        polygon.getPoints().set(pointerIndex * 2 + 1, inPolygon.getY());

        double invRelation = getImageViewScale().getActualToScaledRatio();

        Contour contour = getSkinnable().getContour();

        contour.setX(xCurrent * invRelation);
        contour.setY(yCurrent * invRelation);

        contour.getPoints().set(pointerIndex, inPolygon.multiply(invRelation));
    }


    public int getPointerIndex(MouseEvent event) {
        Point2D point = group.parentToLocal(eventToPoint(event));
        int size = circles.size();
        for (int i = 0; i < size; i++) {
            Circle circle = circles.get(i);
            if(circle.contains(circle.parentToLocal(point))) return i;
        }
        return ContourViewBehavior.NO;
    }

    public boolean isInMovementRange(MouseEvent event) {
        Point2D inStackPane = stackPane.parentToLocal(eventToPoint(event));

        if(label.contains(label.parentToLocal(inStackPane))) return true;

        Point2D inGroup = group.parentToLocal(inStackPane);

        return polygon.contains(polygon.parentToLocal(inGroup));
    }

    public boolean isInResizeRange(MouseEvent event) {
        Point2D inGroup = group.parentToLocal(stackPane.parentToLocal(eventToPoint(event)));

        for (Circle circle: getCircles()) {
            if(circle.contains(circle.parentToLocal(inGroup))) return true;
        }

        return false;
    }

    public Point2D fromPaneToPolygon(MouseEvent e) {
        return polygon.sceneToLocal(e.getSceneX(), e.getSceneY());
    }


    private Point2D getContourOffsetFromStackPane() {
        Point2D point2D = getSkinnable().getContour().getBaseline().multiply(getImageViewScale().getScaledToActualRatio());
        return group.localToParent(polygon.localToParent(point2D)).subtract(stackPane.getBoundsInLocal().getMinX(), stackPane.getBoundsInLocal().getMinY());
    }
}
