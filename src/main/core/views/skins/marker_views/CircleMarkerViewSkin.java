package main.core.views.skins.marker_views;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WeakChangeListener;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.circle_markers_branch.CircleMarker;
import main.core.views.marker_views.AbstractCircleMarkerView;
import main.core.views.behaviors.marker_views.CircleMarkerViewBehavior;

import static main.core.utils.ViewUtils.eventToPoint;

public class CircleMarkerViewSkin<C extends AbstractCircleMarkerView, BB extends CircleMarkerViewBehavior<C>> extends AbstractMarkerViewSkin<C, BB> {

    private StackPane stackPane;
    private Circle circle;
    private TextFlow textFlow;

    protected double xCurrent = 0, yCurrent = 0;

    private ChangeListener<Number> radiusChangeListener = (observable, oldValue, newValue) -> {
        circle.setRadius(newValue.doubleValue() * getImageViewScale().getScaledToActualRatio());
    };

    public CircleMarkerViewSkin(C control, BB behavior) {
        super(control, behavior);

        stackPane = new StackPane();

        stackPane.layoutBoundsProperty().addListener((observable, oldValue, newValue) -> {
            stackPane.layout();

            Point2D offset = getCircleCenterOffsetFromStackPane();

            getNode().setLayoutX(xCurrent - offset.getX());
            getNode().setLayoutY(yCurrent - offset.getY());
        });

        circle = new Circle();

        textFlow = new TextFlow();

        Group group = new Group();
        group.getChildren().addAll(textFlow);

        stackPane.getChildren().add(group);
        stackPane.getChildren().add(circle);

        getChildren().add(stackPane);


        getSkinnable().getCircleMarker().radiusProperty().addListener(new WeakChangeListener<>(radiusChangeListener));

        updatePosition();
    }


    public TextFlow getTextFlow() {
        return textFlow;
    }

    public StackPane getStackPane() {
        return stackPane;
    }

    public Circle getCircle() {
        return circle;
    }


    @Override
    protected void updatePosition() {
        CircleMarker circleMarker = getSkinnable().getCircleMarker();

        double relation = getImageViewScale().getScaledToActualRatio();

        xCurrent = circleMarker.getX() * relation;
        yCurrent = circleMarker.getY() * relation;

        Point2D offset = getCircleCenterOffsetFromStackPane();

        getNode().setLayoutX(xCurrent - offset.getX());
        getNode().setLayoutY(yCurrent - offset.getY());

        circle.setRadius(circleMarker.getRadius() * relation);
    }

    @Override
    public void onMoved(MouseEvent mouseEvent, Point2D difference) {
        xCurrent += difference.getX();
        yCurrent += difference.getY();

        Bounds bounds = getImageViewScale().getBounds();

        double lowerXBorder = bounds.getMinX() + circle.getRadius(),
                upperXBorder = bounds.getMaxX() - circle.getRadius();

        double lowerYBorder = bounds.getMinY() + circle.getRadius(),
                upperYBorder = bounds.getMaxY() - circle.getRadius();

        if(xCurrent < lowerXBorder) xCurrent = lowerXBorder;
        else if(xCurrent > upperXBorder) xCurrent = upperXBorder;

        if(yCurrent < lowerYBorder) yCurrent = lowerYBorder;
        else if(yCurrent > upperYBorder) yCurrent = upperYBorder;

        Point2D offset = getCircleCenterOffsetFromStackPane();

        getNode().setLayoutX(xCurrent - offset.getX());
        getNode().setLayoutY(yCurrent - offset.getY());

        Marker marker = getSkinnable().getMarker();

        marker.setX(xCurrent * getImageViewScale().getActualToScaledRatio());
        marker.setY(yCurrent * getImageViewScale().getActualToScaledRatio());
    }

    @Override
    public void onResized(MouseEvent mouseEvent, Point2D difference) {
        double magnitude = eventToPoint(mouseEvent).
                subtract(circle.localToParent(circle.getCenterX(), circle.getCenterY())).
                normalize().dotProduct(difference);
        resize(magnitude);
    }

    public void resize(double magnitude) {
        CircleMarker circleMarker = getSkinnable().getCircleMarker();
        circleMarker.setRadius(circleMarker.getRadius() + magnitude * getImageViewScale().getActualToScaledRatio());

        double radius = circleMarker.getRadius() * getImageViewScale().getScaledToActualRatio();

        Bounds bounds = getImageViewScale().getBounds();
        double lowerXBorder = bounds.getMinX() + radius,
                upperXBorder = bounds.getMaxX() - radius;

        double lowerYBorder = bounds.getMinY() + radius,
                upperYBorder = bounds.getMaxY() - radius;

        if(xCurrent < lowerXBorder) xCurrent = lowerXBorder;
        else if(xCurrent > upperXBorder) xCurrent = upperXBorder;

        if(yCurrent < lowerYBorder) yCurrent = lowerYBorder;
        else if(yCurrent > upperYBorder) yCurrent = upperYBorder;

        circleMarker.setX(xCurrent * getImageViewScale().getActualToScaledRatio());
        circleMarker.setY(yCurrent * getImageViewScale().getActualToScaledRatio());
    }

    public boolean isInMovementRange(MouseEvent event) {
        Point2D inStackPane = stackPane.parentToLocal(eventToPoint(event));
        return circle.contains(circle.parentToLocal(inStackPane));
    }

    public boolean isInResizeRange(MouseEvent event) {
        Point2D inStackPane = stackPane.parentToLocal(eventToPoint(event));
        Point2D point = circle.parentToLocal(inStackPane);
        double x = point.getX(),
                y = point.getY(),
                r = circle.getRadius() - circle.getStrokeWidth();
        return x * x + y * y > r * r;
    }

    protected Point2D getCircleCenterOffsetFromStackPane() {
       return circle.localToParent(circle.getCenterX(), circle.getCenterY()).subtract(stackPane.getBoundsInLocal().getMinX(), stackPane.getBoundsInLocal().getMinY());
    }
}
