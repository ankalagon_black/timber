package main.core.views.skins.marker_views;

import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.ruler_branch.Ruler;
import main.core.views.behaviors.marker_views.RulerViewBehavior;
import main.core.views.marker_views.AbstractRulerView;

import static main.core.utils.ViewUtils.eventToPoint;

public class RulerViewSkin<C extends AbstractRulerView, BB extends RulerViewBehavior<C>> extends AbstractMarkerViewSkin<C, BB> {
    private final String INITIALIZED = "INITIALIZED";

    private StackPane stackPane;
    private Label label;
    private Group group;
    private Line line;
    private Circle startPointer, endPointer;

    private double xCurrent = 0, yCurrent = 0;

    public RulerViewSkin(C control, BB behavior) {
        super(control, behavior);

        stackPane = new StackPane();

        stackPane.layoutBoundsProperty().addListener((observable, oldValue, newValue) -> {
            stackPane.layout();

            Point2D offset = getLineOffsetFromStackPane();

            getNode().setLayoutX(xCurrent - offset.getX());
            getNode().setLayoutY(yCurrent - offset.getY());
        });

        label = new Label();

        group = new Group();

        group.layoutBoundsProperty().addListener((observable, oldValue, newValue) -> {
            stackPane.layout();

            Point2D offset = getLineOffsetFromStackPane();

            getNode().setLayoutX(xCurrent - offset.getX());
            getNode().setLayoutY(yCurrent - offset.getY());
        });

        line = new Line();

        startPointer = new Circle();
        endPointer = new Circle();

        group.getChildren().add(line);
        group.getChildren().add(startPointer);
        group.getChildren().add(endPointer);

        stackPane.getChildren().add(group);
        stackPane.getChildren().add(label);

        getChildren().add(stackPane);

        getImageViewLoader().setLoadedInterface(image -> {
            Ruler ruler = getSkinnable().getRuler();
            if(!ruler.isInitialized()) ruler.initialize(image.getWidth());

            updatePosition();
        });

        updatePosition();

        registerChangeListener(getSkinnable().getRuler().initializedProperty(), INITIALIZED);
    }


    public StackPane getStackPane() {
        return stackPane;
    }

    public Label getLabel() {
        return label;
    }

    public Group getGroup() {
        return group;
    }

    public Line getLine() {
        return line;
    }

    public Circle getStartPointer() {
        return startPointer;
    }

    public Circle getEndPointer() {
        return endPointer;
    }


    @Override
    protected void updatePosition() {
        Ruler ruler = getSkinnable().getRuler();

        double relation = getImageViewScale().getScaledToActualRatio();

        Point2D start = ruler.getStart().multiply(relation),
                end = ruler.getEnd().multiply(relation);

        line.setStartX(start.getX());
        line.setStartY(start.getY());
        startPointer.setCenterX(start.getX());
        startPointer.setCenterY(start.getY());

        line.setEndX(end.getX());
        line.setEndY(end.getY());
        endPointer.setCenterX(end.getX());
        endPointer.setCenterY(end.getY());

        xCurrent = ruler.getX() * relation;
        yCurrent = ruler.getY() * relation;

        Point2D offset = getLineOffsetFromStackPane();

        getNode().setLayoutX(xCurrent - offset.getX());
        getNode().setLayoutY(yCurrent - offset.getY());
    }

    @Override
    public void onMoved(MouseEvent mouseEvent, Point2D difference) {
        xCurrent += difference.getX();
        yCurrent += difference.getY();

        double sizeX = Math.abs(line.getStartX() - line.getEndX()), sizeY = Math.abs(line.getStartY() - line.getEndY());

        Bounds bounds = getImageViewScale().getBounds();

        double lowerXBorder = bounds.getMinX(),
                upperXBorder = bounds.getMaxX() - sizeX;

        double lowerYBorder = bounds.getMinY(),
                upperYBorder = bounds.getMaxY() - sizeY;

        if(xCurrent < lowerXBorder) xCurrent = lowerXBorder;
        else if(xCurrent > upperXBorder) xCurrent = upperXBorder;

        if(yCurrent < lowerYBorder) yCurrent = lowerYBorder;
        else if(yCurrent > upperYBorder) yCurrent = upperYBorder;

        Point2D offset = getLineOffsetFromStackPane();

        getNode().setLayoutX(xCurrent - offset.getX());
        getNode().setLayoutY(yCurrent - offset.getY());

        double relation = getImageViewScale().getActualToScaledRatio();

        Marker marker = getSkinnable().getMarker();
        marker.setX(xCurrent * relation);
        marker.setY(yCurrent * relation);
    }

    @Override
    public void onResized(MouseEvent mouseEvent, Point2D difference) {

        double newXValue, newYValue;

        int pointerType = getBehavior().getPointerType();

        Point2D baseline = new Point2D(Math.min(line.getStartX(), line.getEndX()), Math.min(line.getStartY(), line.getEndY())),
                newBaseline;

        if(pointerType == RulerViewBehavior.START_POINTER) {
            newXValue = line.getStartX() + difference.getX();
            newYValue = line.getStartY() + difference.getY();

            newBaseline = new Point2D(Math.min(line.getEndX(), newXValue), Math.min(line.getEndY(), newYValue));
        }
        else {
            newXValue = line.getEndX() + difference.getX();
            newYValue = line.getEndY() + difference.getY();

            newBaseline = new Point2D(Math.min(line.getStartX(), newXValue), Math.min(line.getStartY(), newYValue));
        }

        Bounds bounds = getImageViewScale().getBounds();

        if(baseline.getX() != newBaseline.getX()) {
            double shift = newBaseline.getX() - baseline.getX();
            double newX = xCurrent + shift;

            double lowerXBorder = bounds.getMinX();
            if(newX < lowerXBorder) {
                newXValue += lowerXBorder - newX;
                newX = lowerXBorder;
            }

            xCurrent = newX;
        }
        else {
            double upperXBorder = bounds.getMaxX() - xCurrent + baseline.getX();
            if (newXValue > upperXBorder) newXValue = upperXBorder;
        }

        if(baseline.getY() != newBaseline.getY()) {
            double shift = newBaseline.getY() - baseline.getY();
            double newY = yCurrent + shift;

            double lowerYBorder = bounds.getMinY();
            if(newY < lowerYBorder) {
                newYValue += lowerYBorder - newY;
                newY = lowerYBorder;
            }

            yCurrent = newY;
        }
        else {
            double upperYBorder = bounds.getMaxY() - yCurrent + baseline.getY();
            if (newYValue > upperYBorder) newYValue = upperYBorder;
        }

        Ruler ruler = getSkinnable().getRuler();

        double invRelation = getImageViewScale().getActualToScaledRatio();

        if(pointerType == RulerViewBehavior.START_POINTER) {
            line.setStartX(newXValue);
            line.setStartY(newYValue);

            startPointer.setCenterX(newXValue);
            startPointer.setCenterY(newYValue);

            ruler.setStart(new Point2D(newXValue * invRelation, newYValue * invRelation));
        }
        else {
            line.setEndX(newXValue);
            line.setEndY(newYValue);

            endPointer.setCenterX(newXValue);
            endPointer.setCenterY(newYValue);

            ruler.setEnd(new Point2D(newXValue * invRelation, newYValue * invRelation));
        }

        ruler.setX(xCurrent * invRelation);
        ruler.setY(yCurrent * invRelation);
    }


    public int getPointerType(MouseEvent event) {
        Point2D point = group.parentToLocal(eventToPoint(event));
        if(startPointer.contains(startPointer.parentToLocal(point))) return RulerViewBehavior.START_POINTER;
        else if (endPointer.contains(endPointer.parentToLocal(point)))return RulerViewBehavior.END_POINTER;
        else return RulerViewBehavior.NO;
    }

    public boolean isInMovementRange(MouseEvent event) {
        Point2D inStackPane = stackPane.parentToLocal(eventToPoint(event));
        return line.contains(line.parentToLocal(group.parentToLocal(inStackPane))) ||
                label.contains(label.parentToLocal(inStackPane));
    }

    public boolean isInResizeRange(MouseEvent event) {
        Point2D inStackPane = stackPane.parentToLocal(eventToPoint(event));
        Point2D inGroup = group.parentToLocal(inStackPane);
        return startPointer.contains(startPointer.parentToLocal(inGroup))||
                endPointer.contains(endPointer.parentToLocal(inGroup));
    }


    private Point2D getLineOffsetFromStackPane() {
        return group.localToParent(line.getBoundsInParent().getMinX(), line.getBoundsInParent().getMinY()).
                subtract(stackPane.getBoundsInLocal().getMinX(), stackPane.getBoundsInLocal().getMinY());
    }
}
