package main.core.views.skins.marker_views;

import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import main.core.data_types.markers.Marker;
import main.core.views.behaviors.marker_views.MarkerViewBehavior;
import main.core.views.marker_views.AbstractMarkerView;

public class MarkerViewSkin<C extends AbstractMarkerView, BB extends MarkerViewBehavior<C>> extends AbstractMarkerViewSkin<C, BB> {

    private Label label;

    private double xCurrent = 0, yCurrent = 0;

    public MarkerViewSkin(C control, BB behavior) {
        super(control, behavior);

        label = new Label();

        label.layoutBoundsProperty().addListener((observable, oldValue, newValue) -> {
            Point2D offset = new Point2D(newValue.getMinX() + newValue.getWidth() / 2,
                    newValue.getMinY() + newValue.getHeight() / 2);

            getNode().setLayoutX(xCurrent - offset.getX());
            getNode().setLayoutY(yCurrent - offset.getY());
        });

        getChildren().add(label);

        updatePosition();
    }


    public Label getLabel() {
        return label;
    }


    @Override
    protected void updatePosition() {
        Marker marker = getSkinnable().getMarker();

        xCurrent = marker.getX() * getImageViewScale().getScaledToActualRatio();
        yCurrent = marker.getY() * getImageViewScale().getScaledToActualRatio();

        Point2D offset = getOffsetFromCenter();

        getNode().setLayoutX(xCurrent - offset.getX());
        getNode().setLayoutY(yCurrent - offset.getY());
    }

    @Override
    public void onMoved(MouseEvent e, Point2D difference) {
        xCurrent += difference.getX();
        yCurrent += difference.getY();

        Bounds bounds = getImageViewScale().getBounds();

        double lowerXBorder = bounds.getMinX(),
                upperXBorder = bounds.getMaxX();

        double lowerYBorder = bounds.getMinY(),
                upperYBorder = bounds.getMaxY();

        if(xCurrent < lowerXBorder) xCurrent = lowerXBorder;
        else if(xCurrent > upperXBorder) xCurrent = upperXBorder;

        if(yCurrent < lowerYBorder) yCurrent = lowerYBorder;
        else if(yCurrent > upperYBorder) yCurrent = upperYBorder;

        Point2D offset = getOffsetFromCenter();

        getNode().setLayoutX(xCurrent - offset.getX());
        getNode().setLayoutY(yCurrent - offset.getY());

        Marker marker = getSkinnable().getMarker();

        marker.setX(xCurrent * getImageViewScale().getActualToScaledRatio());
        marker.setY(yCurrent * getImageViewScale().getActualToScaledRatio());
    }

    @Override
    public void onResized(MouseEvent e, Point2D difference) {}


    private Point2D getOffsetFromCenter() {
        return new Point2D(label.getBoundsInLocal().getMinX() + label.getBoundsInLocal().getWidth() / 2,
                label.getBoundsInLocal().getMinY() + label.getBoundsInLocal().getHeight() / 2);
    }
}
