package main.core.views.skins;

import com.sun.javafx.scene.control.skin.BehaviorSkinBase;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.WeakListChangeListener;
import javafx.geometry.Bounds;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;
import main.core.data_types.markers.Marker;
import main.core.views.data_types.ListHandler;
import main.core.views.marker_views.AbstractMarkerView;
import main.core.views.MarkerImageView;
import main.core.views.behaviors.MarkerImageViewBehavior;

import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

public class MarkerImageViewSkin<C extends MarkerImageView, BB extends MarkerImageViewBehavior<C>> extends BehaviorSkinBase<C, BB> {
    private static final String IMAGE_VIEW = "IMAGE_VIEW";

    private StackPane stackPane;
    private ImageView imageView = null;
    private Pane pane;

    public MarkerImageViewSkin(C control, BB behavior) {
        super(control, behavior);

        getChildren().clear();

        stackPane = new StackPane();
        pane = new Pane();

        setImageView(getSkinnable().getImageView());
        getSkinnable().getListMap().keySet().forEach(s -> {
            addListHandlerListener(s, (ListHandler<Object, AbstractMarkerView>) getSkinnable().getListMap().get(s));
        });

        stackPane.getChildren().add(pane);
        getChildren().add(stackPane);

        consumeMouseEvents(false);

        registerChangeListener(getSkinnable().imageViewProperty(), IMAGE_VIEW);
        getSkinnable().getListMap().addListener(mapChangeListener);
    }


    private HashMap<String, ListenerWrapper> listenerWrapperHashMap = new HashMap<>();

    private MapChangeListener<String, ListHandler<?, ? extends AbstractMarkerView>> mapChangeListener = change -> {
        if(change.wasAdded()) {
            addListHandlerListener(change.getKey(), (ListHandler<Object, AbstractMarkerView>) change.getValueAdded());
        }
        else if(change.wasRemoved()) {
            ListHandler<Object, AbstractMarkerView> listHandler = ((ListHandler<Object, AbstractMarkerView>) change.getValueRemoved());
            for (Object i: listHandler.getList()) removeContent(i);

            ListenerWrapper listenerWrapper = listenerWrapperHashMap.get(change.getKey());
            listenerWrapperHashMap.remove(change.getKey());
            listHandler.getList().removeListener(listenerWrapper.weakListChangeListener);
        }
    };


    private ChangeListener<Bounds> boundsChangeListener = (observable, oldValue, newValue) -> {
        pane.setMaxWidth(newValue.getWidth());
        pane.setMaxHeight(newValue.getHeight());
    };
    private WeakChangeListener<Bounds> boundsWeakChangeListener = new WeakChangeListener<>(boundsChangeListener);

    private void setImageView(ImageView imageView) {
        if(this.imageView != null) {
            this.imageView.layoutBoundsProperty().removeListener(boundsWeakChangeListener);
            stackPane.getChildren().remove(this.imageView);
        }

        this.imageView = imageView;
        if(this.imageView != null) {
            this.imageView.layoutBoundsProperty().addListener(boundsWeakChangeListener);
            stackPane.getChildren().add(0, this.imageView);
        }
    }


    private void addListHandlerListener(String key, ListHandler<Object, AbstractMarkerView> listHandler) {
        for (Object i: listHandler.getList()) addContent(i, listHandler.getCallback());

        ListChangeListener<Object> markersChangeListener = c -> {
            while (c.next()){
                if(c.wasAdded()){
                    List<Object> added = (List<Object>) c.getAddedSubList();
                    for (Object i: added) addContent(i, listHandler.getCallback());
                }
                else if(c.wasRemoved()){
                    List<Object> removed = (List<Object>) c.getRemoved();
                    for (Object i: removed) removeContent(i);
                }
            }
        };

        ListenerWrapper listenerWrapper = new ListenerWrapper(markersChangeListener, new WeakListChangeListener<>(markersChangeListener));
        listenerWrapperHashMap.put(key, listenerWrapper);
        listHandler.getList().addListener(listenerWrapper.weakListChangeListener);
    }

    @Override
    protected void handleControlPropertyChanged(String p) {
        switch (p) {
            case IMAGE_VIEW:
                setImageView(getSkinnable().getImageView());
                break;
            default:
                super.handleControlPropertyChanged(p);
                break;
        }
    }


    public Pane getMarkersRoot(){
        return pane;
    }


    private void addContent(Object content, Callback<Object, AbstractMarkerView> callback) {
        AbstractMarkerView markerView = callback.call(content);
        pane.getChildren().add(markerView);
    }

    private void removeContent(Object content) {
        pane.getChildren().stream().filter(node -> ((AbstractMarkerView) node).getContent().equals(content)).
                findFirst().
                ifPresent(node -> {
                    pane.getChildren().remove(node);
                });
    }

    public class ListenerWrapper{
        public ListChangeListener<Object> listChangeListener;
        public WeakListChangeListener<Object> weakListChangeListener;

        public ListenerWrapper(ListChangeListener<Object> listChangeListener, WeakListChangeListener<Object> weakListChangeListener) {
            this.listChangeListener = listChangeListener;
            this.weakListChangeListener = weakListChangeListener;
        }
    }
}
