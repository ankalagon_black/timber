package main.core.views.skins;

import com.sun.javafx.scene.control.skin.BehaviorSkinBase;
import com.sun.javafx.scene.traversal.ParentTraversalEngine;
import com.sun.javafx.scene.traversal.TraverseListener;
import com.sun.javafx.util.Utils;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.DoublePropertyBase;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventDispatcher;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.AccessibleAttribute;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.ScrollBar;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import main.core.utils.images.ImageViewLoader;
import main.core.views.ImageScrollPane;
import main.core.views.behaviors.ImageScrollPaneBehavior;

import static com.sun.javafx.scene.control.skin.Utils.boundedSize;

public class ImageScrollPaneSkin extends BehaviorSkinBase<ImageScrollPane, ImageScrollPaneBehavior> implements TraverseListener {

    /***************************************************************************
     *                                                                         *
     * UI Subcomponents                                                        *
     *                                                                         *
     **************************************************************************/

    private static final double DEFAULT_PREF_SIZE = 100.0;

    private static final double DEFAULT_MIN_SIZE = 36.0;

    private static final double DEFAULT_SB_BREADTH = 12.0;
    private static final double DEFAULT_EMBEDDED_SB_BREADTH = 8.0;

    private static final double PAN_THRESHOLD = 0.5;

    private static final double ZOOM_FACTOR = 1.15;

    // state from the control

    private StackPane stackPane;

    private ImageView imageView = null;

    private ImageViewLoader loader;

    private double nodeWidth;
    private double nodeHeight;
    private boolean nodeSizeInvalid = true;

    private double posX;
    private double posY;

    // working state

    private boolean hsbvis;
    private boolean vsbvis;
    private double hsbHeight;
    private double vsbWidth;

    // substructure

    private StackPane viewRect;
    private StackPane viewContent;
    private double contentWidth;
    private double contentHeight;
    private StackPane corner;
    protected ScrollBar hsb;
    protected ScrollBar vsb;

    double pressX;
    double pressY;
    double ohvalue;
    double ovvalue;
    private Cursor saveCursor =  null;
    private boolean dragDetected = false;
    private boolean touchDetected = false;
    private boolean mouseDown = false;

    Rectangle clipRect;

    public ImageScrollPaneSkin(ImageScrollPane scrollPane) {
        super(scrollPane, new ImageScrollPaneBehavior(scrollPane));
        initalize();

        loader = new ImageViewLoader();
        imageView = getSkinnable().getImageView();

        loader.setLoadedInterface(image -> {
            fitWidthToScrollPane();
            fitHeightToScrollPane();
        });
        loader.setImageView(imageView);

        registerChangeListener(scrollPane.hbarPolicyProperty(), "HBAR_POLICY");
        registerChangeListener(scrollPane.vbarPolicyProperty(), "VBAR_POLICY");
        registerChangeListener(scrollPane.hvalueProperty(), "HVALUE");
        registerChangeListener(scrollPane.hmaxProperty(), "HMAX");
        registerChangeListener(scrollPane.hminProperty(), "HMIN");
        registerChangeListener(scrollPane.vvalueProperty(), "VVALUE");
        registerChangeListener(scrollPane.vmaxProperty(), "VMAX");
        registerChangeListener(scrollPane.vminProperty(), "VMIN");
        registerChangeListener(scrollPane.prefViewportWidthProperty(), "VIEWPORT_SIZE_HINT");
        registerChangeListener(scrollPane.prefViewportHeightProperty(), "VIEWPORT_SIZE_HINT");
        registerChangeListener(scrollPane.minViewportWidthProperty(), "VIEWPORT_SIZE_HINT");
        registerChangeListener(scrollPane.minViewportHeightProperty(), "VIEWPORT_SIZE_HINT");
        registerChangeListener(scrollPane.contentProperty(), "CONTENT");
        registerChangeListener(scrollPane.imageViewProperty(), "IMAGE_VIEW");
    }


    private void initalize(){
        ImageScrollPane imageScrollPane = getSkinnable();

        stackPane = new StackPane();

        ChangeListener<Number> widthChangeListener = new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(loader.isImageLoaded()) fitWidthToScrollPane();

                imageScrollPane.widthProperty().removeListener(this);
            }
        };

        ChangeListener<Number> heightChangeListener = new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(loader.isImageLoaded()) fitHeightToScrollPane();

                imageScrollPane.heightProperty().removeListener(this);
            }
        };

        imageScrollPane.widthProperty().addListener(widthChangeListener);
        imageScrollPane.heightProperty().addListener(heightChangeListener);

        if(imageScrollPane.getContent() != null){
            stackPane.getChildren().add(imageScrollPane.getContent());
        }

        stackPane.minWidthProperty().bind(imageScrollPane.widthProperty());
        stackPane.minHeightProperty().bind(imageScrollPane.heightProperty());

        ParentTraversalEngine traversalEngine = new ParentTraversalEngine(imageScrollPane);
        traversalEngine.addTraverseListener(this);
        imageScrollPane.setImpl_traversalEngine(traversalEngine);

        if (stackPane != null) {
            stackPane.layoutBoundsProperty().addListener(nodeListener);
            stackPane.layoutBoundsProperty().addListener(boundsChangeListener);
        }

        viewRect = new StackPane() {

            @Override
            protected void layoutChildren() {
                viewContent.resize(getWidth(), getHeight());
            }

        };
        // prevent requestLayout requests from within scrollNode from percolating up
        viewRect.setManaged(false);
        viewRect.setCache(true);
        viewRect.getStyleClass().add("viewport");

        clipRect = new Rectangle();
        viewRect.setClip(clipRect);

        hsb = new ScrollBar();

        vsb = new ScrollBar();
        vsb.setOrientation(Orientation.VERTICAL);

        EventHandler<MouseEvent> barHandler = ev -> {
            imageScrollPane.requestFocus();
        };

        hsb.addEventFilter(MouseEvent.MOUSE_PRESSED, barHandler);
        vsb.addEventFilter(MouseEvent.MOUSE_PRESSED, barHandler);

        corner = new StackPane();
        corner.getStyleClass().setAll("corner");

        viewContent = new StackPane() {
            @Override public void requestLayout() {
                // if scrollNode requested layout, will want to recompute
                nodeSizeInvalid = true;

                super.requestLayout(); // add as layout root for next layout pass

                // Need to layout the ScrollPane as well in case scrollbars
                // appeared or disappeared.
                imageScrollPane.requestLayout();
            }
            @Override protected void layoutChildren() {
                if (nodeSizeInvalid) {
                    computeScrollNodeSize(getWidth(),getHeight());
                }
                if (stackPane != null && stackPane.isResizable()) {
                    stackPane.resize(snapSize(nodeWidth), snapSize(nodeHeight));
                    if (vsbvis != determineVerticalSBVisible() || hsbvis != determineHorizontalSBVisible()) {
                        imageScrollPane.requestLayout();
                    }
                }
                if (stackPane != null) {
                    stackPane.relocate(0,0);
                }
            }
        };
        viewRect.getChildren().add(viewContent);

        viewContent.getChildren().add(stackPane);
        viewRect.nodeOrientationProperty().bind(stackPane.nodeOrientationProperty());

        getChildren().clear();
        getChildren().addAll(viewRect, vsb, hsb, corner);


        InvalidationListener vsbListener = valueModel -> {
            if (!IS_TOUCH_SUPPORTED) {
                posY = Utils.clamp(imageScrollPane.getVmin(), vsb.getValue(), imageScrollPane.getVmax());
            }
            else {
                posY = vsb.getValue();
            }
            updatePosY();
        };
        vsb.valueProperty().addListener(vsbListener);

        InvalidationListener hsbListener = valueModel -> {
            if (!IS_TOUCH_SUPPORTED) {
                posX = Utils.clamp(imageScrollPane.getHmin(), hsb.getValue(), imageScrollPane.getHmax());
            }
            else {
                posX = hsb.getValue();
            }
            updatePosX();
        };
        hsb.valueProperty().addListener(hsbListener);

        viewRect.setOnMousePressed(e -> {
            if((e.getButton().equals(MouseButton.PRIMARY) && !imageScrollPane.getUseRightPanMode()) ||
                    (e.getButton().equals(MouseButton.SECONDARY) && imageScrollPane.getUseRightPanMode())) {
                mouseDown = true;
                if (IS_TOUCH_SUPPORTED) {
                    startSBReleasedAnimation();
                }
                pressX = e.getX();
                pressY = e.getY();
                ohvalue = hsb.getValue();
                ovvalue = vsb.getValue();

                if (saveCursor == null) {
                    saveCursor = imageScrollPane.getCursor();
                    if (saveCursor == null) {
                        saveCursor = Cursor.OPEN_HAND;
                    }
                    imageScrollPane.setCursor(Cursor.CLOSED_HAND);
                    imageScrollPane.requestLayout();
                }
            }
        });

        viewRect.setOnDragDetected(e -> {
            if((e.getButton().equals(MouseButton.PRIMARY) && !imageScrollPane.getUseRightPanMode()) ||
                    (e.getButton().equals(MouseButton.SECONDARY) && imageScrollPane.getUseRightPanMode())) {
                if (IS_TOUCH_SUPPORTED) {
                    startSBReleasedAnimation();
                }
                if (imageScrollPane.isPannable()) {
                    dragDetected = true;
                }
            }
        });

        viewRect.addEventFilter(MouseEvent.MOUSE_RELEASED, e -> {
            if((e.getButton().equals(MouseButton.PRIMARY) && !imageScrollPane.getUseRightPanMode()) ||
                    (e.getButton().equals(MouseButton.SECONDARY) && imageScrollPane.getUseRightPanMode())) {
                mouseDown = false;
                if (dragDetected) {
                    dragDetected = false;
                }

                if (saveCursor != null) {
                    imageScrollPane.setCursor(saveCursor);
                    saveCursor = null;
                    imageScrollPane.requestLayout();
                }

                /*
                 ** if the contents need repositioning, and there's is no
                 ** touch event in progress, then startArea the repositioning.
                 */
                if ((posY > imageScrollPane.getVmax() || posY < imageScrollPane.getVmin() ||
                        posX > imageScrollPane.getHmax() || posX < imageScrollPane.getHmin()) && !touchDetected) {
                    startContentsToViewport();
                }
            }
        });

        viewRect.setOnMouseDragged(e -> {
            if((e.getButton().equals(MouseButton.PRIMARY) && !imageScrollPane.getUseRightPanMode())
                    || (e.getButton().equals(MouseButton.SECONDARY) && imageScrollPane.getUseRightPanMode())) {
                if (IS_TOUCH_SUPPORTED) {
                    startSBReleasedAnimation();
                }
                /*
                 ** for mobile-touch we allow drag, even if not pannagle
                 */
                if (imageScrollPane.isPannable() || IS_TOUCH_SUPPORTED) {
                    double deltaX = pressX - e.getX();
                    double deltaY = pressY - e.getY();
                    /*
                     ** we only drag if not all of the content is visible.
                     */
                    if (hsb.getVisibleAmount() > 0.0 && hsb.getVisibleAmount() < hsb.getMax()) {
                        if (Math.abs(deltaX) > PAN_THRESHOLD) {
                            if (isReverseNodeOrientation()) {
                                deltaX = -deltaX;
                            }
                            double newHVal = (ohvalue + deltaX / (nodeWidth - viewRect.getWidth()) * (hsb.getMax() - hsb.getMin()));
                            if (!IS_TOUCH_SUPPORTED) {
                                if (newHVal > hsb.getMax()) {
                                    newHVal = hsb.getMax();
                                } else if (newHVal < hsb.getMin()) {
                                    newHVal = hsb.getMin();
                                }
                                hsb.setValue(newHVal);
                            } else {
                                hsb.setValue(newHVal);
                            }
                        }
                    }
                    /*
                     ** we only drag if not all of the content is visible.
                     */
                    if (vsb.getVisibleAmount() > 0.0 && vsb.getVisibleAmount() < vsb.getMax()) {
                        if (Math.abs(deltaY) > PAN_THRESHOLD) {
                            double newVVal = (ovvalue + deltaY / (nodeHeight - viewRect.getHeight()) * (vsb.getMax() - vsb.getMin()));
                            if (!IS_TOUCH_SUPPORTED) {
                                if (newVVal > vsb.getMax()) {
                                    newVVal = vsb.getMax();
                                } else if (newVVal < vsb.getMin()) {
                                    newVVal = vsb.getMin();
                                }
                                vsb.setValue(newVVal);
                            } else {
                                vsb.setValue(newVVal);
                            }
                        }
                    }
                }
            }
            /*
             ** we need to consume drag events, as we don't want
             ** the scrollpane itself to be dragged on every mouse click
             */
            //e.consume();
        });

        /*
         ** don't allow the ScrollBar to handle the ScrollEvent,
         ** In a ScrollPane a vertical scroll should scroll on the vertical only,
         ** whereas in a horizontal ScrollBar it can scroll horizontally.
         */
        // block the event from being passed down to children
        final EventDispatcher blockEventDispatcher = (event, tail) -> event;
        // block ScrollEvent from being passed down to scrollbar's skin
        final EventDispatcher oldHsbEventDispatcher = hsb.getEventDispatcher();
        hsb.setEventDispatcher((event, tail) -> {
            if (event.getEventType() == ScrollEvent.SCROLL &&
                    !((ScrollEvent)event).isDirect()) {
                tail = tail.prepend(blockEventDispatcher);
                tail = tail.prepend(oldHsbEventDispatcher);
                return tail.dispatchEvent(event);
            }
            return oldHsbEventDispatcher.dispatchEvent(event, tail);
        });
        // block ScrollEvent from being passed down to scrollbar's skin
        final EventDispatcher oldVsbEventDispatcher = vsb.getEventDispatcher();
        vsb.setEventDispatcher((event, tail) -> {
            if (event.getEventType() == ScrollEvent.SCROLL &&
                    !((ScrollEvent)event).isDirect()) {
                tail = tail.prepend(blockEventDispatcher);
                tail = tail.prepend(oldVsbEventDispatcher);
                return tail.dispatchEvent(event);
            }
            return oldVsbEventDispatcher.dispatchEvent(event, tail);
        });

        /*
         * listen for ScrollEvents over the whole of the ScrollPane
         * area, the above dispatcher having removed the ScrollBars
         * scroll event handling.
         *
         * Note that we use viewRect here, rather than setting the eventHandler
         * on the ScrollPane itself. This is for RT-31582, and effectively
         * allows for us to prioritise handling (and consuming) the event
         * internally, before it is made available to users listening to events
         * on the control. This is consistent with the VirtualFlow-based controls.
         */

        stackPane.setOnScroll(event -> {
            if(loader.isImageLoaded()) {
                resizeImage(new Point2D(event.getX(), event.getY()), event.getDeltaY());
                event.consume();
            }
        });

        /*
         ** there are certain animations that need to know if the touch is
         ** happening.....
         */
        imageScrollPane.addEventHandler(TouchEvent.TOUCH_PRESSED, e -> {
            touchDetected = true;
            startSBReleasedAnimation();
            e.consume();
        });

        imageScrollPane.addEventHandler(TouchEvent.TOUCH_RELEASED, e -> {
            touchDetected = false;
            e.consume();
        });

        // ScrollPanes do not block all MouseEvents by default, unlike most other UI Controls.
        consumeMouseEvents(false);

        // update skin initial state to match control (see RT-35554)
        hsb.setValue(imageScrollPane.getHvalue());
        vsb.setValue(imageScrollPane.getVvalue());

        Platform.runLater(imageScrollPane::requestFocus);
    }

    public void resizeImageToCenter(double yDelta){
        if(getSkinnable().getImageView() != null) resizeImage(new Point2D(stackPane.getWidth() / 2, stackPane.getHeight() / 2), yDelta);
    }

    private void resizeImage(Point2D pos, double yDelta) {
        double coeff = yDelta > 0 ? ZOOM_FACTOR : 1 / ZOOM_FACTOR;

        double fitWidth = imageView.getFitWidth(), fitHeight = imageView.getFitHeight();

        double imageWidth = imageView.getImage().getWidth(),
                scrPaneW = getSkinnable().getWidth(),
                imageHeight = imageView.getImage().getHeight(),
                scrPaneH = getSkinnable().getHeight();

        double prevWidth = imageView.getBoundsInLocal().getWidth();
        double wMin =  Math.min(imageWidth, scrPaneW), hMin = Math.min(imageHeight, scrPaneH);

        double newFitWidth = Utils.clamp(wMin, fitWidth * coeff, imageWidth),
                newFitHeight = Utils.clamp(hMin, fitHeight * coeff, imageHeight);

        if(fitWidth != newFitWidth || fitHeight != newFitHeight) {
            Point2D origin = new Point2D(getSkinnable().getHvalue() * (imageView.getBoundsInLocal().getWidth() - scrPaneW),
                    getSkinnable().getVvalue() * (imageView.getBoundsInLocal().getHeight() - scrPaneH));

            imageView.setFitWidth(newFitWidth);
            imageView.setFitHeight(newFitHeight);

            double newWidth = imageView.getBoundsInLocal().getWidth();
            double ratio = newWidth / prevWidth;
            if(Math.abs(ratio - coeff) > 0.0001) coeff = ratio;

            Point2D shift = new Point2D(pos.getX(), pos.getY()).multiply(coeff - 1);
            Point2D newPos = origin.add(shift);

            getSkinnable().layout();

            double divisorW = stackPane.getWidth() - getSkinnable().getWidth();
            if(divisorW > 0) {
                double hv = newPos.getX() / divisorW;
                getSkinnable().setHvalue(Utils.clamp(0, hv, 1));
            }
            else getSkinnable().setHvalue(0);

            double divisorH = stackPane.getHeight() - getSkinnable().getHeight();
            if(divisorH > 0){
                double vv = newPos.getY() / divisorH;
                getSkinnable().setVvalue(Utils.clamp(0, vv,1));
            }
            else getSkinnable().setVvalue(0);
        }
    }

    private void fitWidthToScrollPane(){
        double width = getSkinnable().getWidth();
        if(width != 0) imageView.setFitWidth(Math.min(imageView.getImage().getWidth(), width));
    }

    private void fitHeightToScrollPane(){
        double height = getSkinnable().getHeight();
        if(height != 0) imageView.setFitHeight(Math.min(imageView.getImage().getHeight(), height));
    }

    private final InvalidationListener nodeListener = new InvalidationListener() {
        @Override public void invalidated(Observable valueModel) {
            if (!nodeSizeInvalid) {
                final Bounds scrollNodeBounds = stackPane.getLayoutBounds();
                final double scrollNodeWidth = scrollNodeBounds.getWidth();
                final double scrollNodeHeight = scrollNodeBounds.getHeight();

                /*
                 ** if the new size causes scrollbar visibility to change, then need to relayout
                 ** we also need to correct the thumb size when the scrollnode's size changes
                 */
                if (vsbvis != determineVerticalSBVisible() || hsbvis != determineHorizontalSBVisible() ||
                        (scrollNodeWidth != 0.0  && nodeWidth != scrollNodeWidth) ||
                        (scrollNodeHeight != 0.0 && nodeHeight != scrollNodeHeight)) {
                    getSkinnable().requestLayout();
                } else {
                    /**
                     * we just need to update scrollbars based on new scrollNode size,
                     * but we don't do this while dragging, there's no need,
                     * and it jumps, as dragging updates the scrollbar too.
                     */
                    if (!dragDetected) {
                        updateVerticalSB();
                        updateHorizontalSB();
                    }
                }
            }
        }
    };


    /*
     ** The content of the ScrollPane has just changed bounds, check scrollBar positions.
     */
    private final ChangeListener<Bounds> boundsChangeListener = new ChangeListener<Bounds>() {
        @Override public void changed(ObservableValue<? extends Bounds> observable, Bounds oldBounds, Bounds newBounds) {

            /*
             ** For a height change then we want to reduce
             ** viewport vertical jumping as much as possible.
             ** We set a new vsb value to try to keep the same
             ** content position at the top of the viewport
             */
            double oldHeight = oldBounds.getHeight();
            double newHeight = newBounds.getHeight();
            if (oldHeight > 0 && oldHeight != newHeight) {
                double oldPositionY = (snapPosition(snappedTopInset() - posY / (vsb.getMax() - vsb.getMin()) * (oldHeight - contentHeight)));
                double newPositionY = (snapPosition(snappedTopInset() - posY / (vsb.getMax() - vsb.getMin()) * (newHeight - contentHeight)));

                double newValueY = (oldPositionY/newPositionY)*vsb.getValue();
                if (newValueY < 0.0) {
                    vsb.setValue(0.0);
                }
                else if (newValueY < 1.0) {
                    vsb.setValue(newValueY);
                }
                else if (newValueY > 1.0) {
                    vsb.setValue(1.0);
                }
            }

            /*
             ** For a width change then we want to reduce
             ** viewport horizontal jumping as much as possible.
             ** We set a new hsb value to try to keep the same
             ** content position to the left of the viewport
             */
            double oldWidth = oldBounds.getWidth();
            double newWidth = newBounds.getWidth();
            if (oldWidth > 0 && oldWidth != newWidth) {
                double oldPositionX = (snapPosition(snappedLeftInset() - posX / (hsb.getMax() - hsb.getMin()) * (oldWidth - contentWidth)));
                double newPositionX = (snapPosition(snappedLeftInset() - posX / (hsb.getMax() - hsb.getMin()) * (newWidth - contentWidth)));

                double newValueX = (oldPositionX/newPositionX)*hsb.getValue();
                if (newValueX < 0.0) {
                    hsb.setValue(0.0);
                }
                else if (newValueX < 1.0) {
                    hsb.setValue(newValueX);
                }
                else if (newValueX > 1.0) {
                    hsb.setValue(1.0);
                }
            }
        }
    };



    @Override protected void handleControlPropertyChanged(String p) {
        super.handleControlPropertyChanged(p);
        if ("HBAR_POLICY".equals(p) || "VBAR_POLICY".equals(p)) {
            // change might affect pref size, so requestLayout on control
            getSkinnable().requestLayout();
        } else if ("HVALUE".equals(p)) {
            hsb.setValue(getSkinnable().getHvalue());
        } else if ("HMAX".equals(p)) {
            hsb.setMax(getSkinnable().getHmax());
        } else if ("HMIN".equals(p)) {
            hsb.setMin(getSkinnable().getHmin());
        } else if ("VVALUE".equals(p)) {
            vsb.setValue(getSkinnable().getVvalue());
        } else if ("VMAX".equals(p)) {
            vsb.setMax(getSkinnable().getVmax());
        } else if ("VMIN".equals(p)) {
            vsb.setMin(getSkinnable().getVmin());
        } else if ("VIEWPORT_SIZE_HINT".equals(p)) {
            // change affects pref size, so requestLayout on control
            getSkinnable().requestLayout();
        }
        else if(p.equals("CONTENT")){
            stackPane.getChildren().clear();

            Node content = getSkinnable().getContent();
            if(content != null){
                stackPane.getChildren().add(content);
            }
        }
        else if(p.equals("IMAGE_VIEW")){
            imageView = getSkinnable().getImageView();
            loader.setImageView(imageView);
        }
    }

    private void scrollBoundsIntoView(Bounds b) {
        double dx = 0.0;
        double dy = 0.0;
        if (b.getMaxX() > contentWidth) {
            dx = b.getMinX() - snappedLeftInset();
        }
        if (b.getMinX() < snappedLeftInset()) {
            dx = b.getMaxX() - contentWidth - snappedLeftInset();
        }
        if (b.getMaxY() > snappedTopInset() + contentHeight) {
            dy = b.getMinY() - snappedTopInset();
        }
        if (b.getMinY() < snappedTopInset()) {
            dy = b.getMaxY() - contentHeight - snappedTopInset();
        }
        // We want to move contentPanel's layoutX,Y by (dx,dy).
        // But to do this we have to set the scrollbars' values appropriately.

        if (dx != 0) {
            double sdx = dx * (hsb.getMax() - hsb.getMin()) / (nodeWidth - contentWidth);
            // Adjust back for some amount so that the Node border is not too close to views border
            sdx += -1 * Math.signum(sdx) * hsb.getUnitIncrement() / 5; // This accounts to 2% of views width
            hsb.setValue(hsb.getValue() + sdx);
            getSkinnable().requestLayout();
        }
        if (dy != 0) {
            double sdy = dy * (vsb.getMax() - vsb.getMin()) / (nodeHeight - contentHeight);
            // Adjust back for some amount so that the Node border is not too close to views border
            sdy += -1 * Math.signum(sdy) * vsb.getUnitIncrement() / 5; // This accounts to 2% of views height
            vsb.setValue(vsb.getValue() + sdy);
            getSkinnable().requestLayout();
        }

    }

    /*
     ** auto-scroll so node is within (0,0),(contentWidth,contentHeight)
     */
    @Override public void onTraverse(Node n, Bounds b) {
        scrollBoundsIntoView(b);
    }

    public void hsbIncrement() {
        if (hsb != null) hsb.increment();
    }
    public void hsbDecrement() {
        if (hsb != null) hsb.decrement();
    }

    // TODO: add page increment and decrement
    public void hsbPageIncrement() {
        if (hsb != null) hsb.increment();
    }
    // TODO: add page increment and decrement
    public void hsbPageDecrement() {
        if (hsb != null) hsb.decrement();
    }

    public void vsbIncrement() {
        if (vsb != null) vsb.increment();
    }
    public void vsbDecrement() {
        if (vsb != null) vsb.decrement();
    }

    // TODO: add page increment and decrement
    public void vsbPageIncrement() {
        if (vsb != null) vsb.increment();
    }
    // TODO: add page increment and decrement
    public void vsbPageDecrement() {
        if (vsb != null) vsb.decrement();
    }

    /***************************************************************************
     *                                                                         *
     * Layout                                                                  *
     *                                                                         *
     **************************************************************************/

    @Override protected double computePrefWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset) {
        final ImageScrollPane sp = getSkinnable();

        double vsbWidth = computeVsbSizeHint(sp);
        double minWidth = vsbWidth + snappedLeftInset() + snappedRightInset();

        if (sp.getPrefViewportWidth() > 0) {
            return (sp.getPrefViewportWidth() + minWidth);
        }
        else {
            return (stackPane.prefWidth(height) + minWidth);
        }
    }

    @Override protected double computePrefHeight(double width, double topInset, double rightInset, double bottomInset, double leftInset) {
        final ImageScrollPane sp = getSkinnable();

        double hsbHeight = computeHsbSizeHint(sp);
        double minHeight = hsbHeight + snappedTopInset() + snappedBottomInset();

        if (sp.getPrefViewportHeight() > 0) {
            return (sp.getPrefViewportHeight() + minHeight);
        }
        else {
            return (stackPane.prefHeight(width) + minHeight);
        }
    }

    @Override protected double computeMinWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset) {
        final ImageScrollPane sp = getSkinnable();

        double vsbWidth = computeVsbSizeHint(sp);
        double minWidth = vsbWidth + snappedLeftInset() + snappedRightInset();

        if (sp.getMinViewportWidth() > 0) {
            return (sp.getMinViewportWidth() + minWidth);
        } else {
            double w = corner.minWidth(-1);
            return (w > 0) ? (3 * w) : (DEFAULT_MIN_SIZE);
        }

    }

    @Override protected double computeMinHeight(double width, double topInset, double rightInset, double bottomInset, double leftInset) {
        final ImageScrollPane sp = getSkinnable();

        double hsbHeight = computeHsbSizeHint(sp);
        double minHeight = hsbHeight + snappedTopInset() + snappedBottomInset();

        if (sp.getMinViewportHeight() > 0) {
            return (sp.getMinViewportHeight() + minHeight);
        } else {
            double h = corner.minHeight(-1);
            return (h > 0) ? (3 * h) : (DEFAULT_MIN_SIZE);
        }
    }

    /**
     * Computes the size that should be reserved for horizontal scrollbar in size hints (min/pref height)
     */
    private double computeHsbSizeHint(ImageScrollPane sp) {
        return ((sp.getHbarPolicy() == ImageScrollPane.ScrollBarPolicy.ALWAYS) ||
                (sp.getHbarPolicy() == ImageScrollPane.ScrollBarPolicy.AS_NEEDED && (sp.getPrefViewportHeight() > 0 || sp.getMinViewportHeight() > 0)))
                ? hsb.prefHeight(ScrollBar.USE_COMPUTED_SIZE)
                : 0;
    }

    /**
     * Computes the size that should be reserved for vertical scrollbar in size hints (min/pref width)
     */
    private double computeVsbSizeHint(ImageScrollPane sp) {
        return ((sp.getVbarPolicy() == ImageScrollPane.ScrollBarPolicy.ALWAYS) ||
                (sp.getVbarPolicy() == ImageScrollPane.ScrollBarPolicy.AS_NEEDED && (sp.getPrefViewportWidth() > 0
                        || sp.getMinViewportWidth() > 0)))
                ? vsb.prefWidth(ScrollBar.USE_COMPUTED_SIZE)
                : 0;
    }

    @Override protected void layoutChildren(final double x, final double y,
                                            final double w, final double h) {
        final ImageScrollPane control = getSkinnable();
        final Insets padding = control.getPadding();
        final double rightPadding = snapSize(padding.getRight());
        final double leftPadding = snapSize(padding.getLeft());
        final double topPadding = snapSize(padding.getTop());
        final double bottomPadding = snapSize(padding.getBottom());

        vsb.setMin(control.getVmin());
        vsb.setMax(control.getVmax());

        //should only do this on css setup
        hsb.setMin(control.getHmin());
        hsb.setMax(control.getHmax());

        contentWidth = w;
        contentHeight = h;

        /*
         ** we want the scrollbars to go right to the border
         */
        double hsbWidth = 0;
        double vsbHeight = 0;

        computeScrollNodeSize(contentWidth, contentHeight);
        computeScrollBarSize();

        for (int i = 0; i < 2; ++i) {
            vsbvis = determineVerticalSBVisible();
            hsbvis = determineHorizontalSBVisible();

            if (vsbvis && !IS_TOUCH_SUPPORTED) {
                contentWidth = w - vsbWidth;
            }
            hsbWidth = w + leftPadding + rightPadding - (vsbvis ? vsbWidth : 0);
            if (hsbvis && !IS_TOUCH_SUPPORTED) {
                contentHeight = h - hsbHeight;
            }
            vsbHeight = h + topPadding + bottomPadding - (hsbvis ? hsbHeight : 0);
        }


        if (stackPane != null && stackPane.isResizable()) {
            // maybe adjust size now that scrollbars may take up space
            if (vsbvis && hsbvis) {
                // adjust just once to accommodate
                computeScrollNodeSize(contentWidth, contentHeight);

            } else if (hsbvis && !vsbvis) {
                computeScrollNodeSize(contentWidth, contentHeight);
                vsbvis = determineVerticalSBVisible();
                if (vsbvis) {
                    // now both are visible
                    contentWidth -= vsbWidth;
                    hsbWidth -= vsbWidth;
                    computeScrollNodeSize(contentWidth, contentHeight);
                }
            } else if (vsbvis && !hsbvis) {
                computeScrollNodeSize(contentWidth, contentHeight);
                hsbvis = determineHorizontalSBVisible();
                if (hsbvis) {
                    // now both are visible
                    contentHeight -= hsbHeight;
                    vsbHeight -= hsbHeight;
                    computeScrollNodeSize(contentWidth, contentHeight);
                }
            }
        }

        // figure out the content area that is to be filled
        double cx = snappedLeftInset() - leftPadding;
        double cy = snappedTopInset() - topPadding;

        vsb.setVisible(vsbvis);
        if (vsbvis) {
            /*
             ** round up position of ScrollBar, round down it's size.
             **
             ** Positioning the ScrollBar
             **  The Padding should go between the content and the edge,
             **  otherwise changes in padding move the ScrollBar, and could
             **  in extreme cases size the ScrollBar to become unusable.
             **  The -1, +1 plus one bit :
             **   If padding in => 1 then we allow one pixel to appear as the
             **   outside border of the Scrollbar, and the rest on the inside.
             **   If padding is < 1 then we just stick to the edge.
             */
            vsb.resizeRelocate(snappedLeftInset() + w - vsbWidth + (rightPadding < 1 ? 0 : rightPadding - 1) ,
                    cy, vsbWidth, vsbHeight);
        }
        updateVerticalSB();

        hsb.setVisible(hsbvis);
        if (hsbvis) {
            /*
             ** round up position of ScrollBar, round down it's size.
             **
             ** Positioning the ScrollBar
             **  The Padding should go between the content and the edge,
             **  otherwise changes in padding move the ScrollBar, and could
             **  in extreme cases size the ScrollBar to become unusable.
             **  The -1, +1 plus one bit :
             **   If padding in => 1 then we allow one pixel to appear as the
             **   outside border of the Scrollbar, and the rest on the inside.
             **   If padding is < 1 then we just stick to the edge.
             */
            hsb.resizeRelocate(cx, snappedTopInset() + h - hsbHeight + (bottomPadding < 1 ? 0 : bottomPadding - 1),
                    hsbWidth, hsbHeight);
        }
        updateHorizontalSB();

        viewRect.resizeRelocate(snappedLeftInset(), snappedTopInset(), snapSize(contentWidth), snapSize(contentHeight));
        resetClip();

        if (vsbvis && hsbvis) {
            corner.setVisible(true);
            double cornerWidth = vsbWidth;
            double cornerHeight = hsbHeight;
            corner.resizeRelocate(snapPosition(vsb.getLayoutX()), snapPosition(hsb.getLayoutY()), snapSize(cornerWidth), snapSize(cornerHeight));
        } else {
            corner.setVisible(false);
        }
        control.setViewportBounds(new BoundingBox(snapPosition(viewContent.getLayoutX()), snapPosition(viewContent.getLayoutY()), snapSize(contentWidth), snapSize(contentHeight)));
    }

    private void computeScrollNodeSize(double contentWidth, double contentHeight) {
        if (stackPane != null) {
            if (stackPane.isResizable()) {
                ImageScrollPane control = getSkinnable();
                Orientation bias = stackPane.getContentBias();
                if (bias == null) {
                    nodeWidth = snapSize(boundedSize(stackPane.prefWidth(-1),
                            stackPane.minWidth(-1), stackPane.maxWidth(-1)));
                    nodeHeight = snapSize(boundedSize(stackPane.prefHeight(-1),
                            stackPane.minHeight(-1), stackPane.maxHeight(-1)));

                } else if (bias == Orientation.HORIZONTAL) {
                    nodeWidth = snapSize(boundedSize(stackPane.prefWidth(-1),
                            stackPane.minWidth(-1), stackPane.maxWidth(-1)));
                    nodeHeight = snapSize(boundedSize(stackPane.prefHeight(nodeWidth),
                            stackPane.minHeight(nodeWidth),stackPane.maxHeight(nodeWidth)));

                } else { // bias == VERTICAL
                    nodeHeight = snapSize(boundedSize(stackPane.prefHeight(-1),
                            stackPane.minHeight(-1), stackPane.maxHeight(-1)));
                    nodeWidth = snapSize(boundedSize(stackPane.prefWidth(nodeHeight),
                            stackPane.minWidth(nodeHeight),stackPane.maxWidth(nodeHeight)));
                }

            } else {
                nodeWidth = snapSize(stackPane.getLayoutBounds().getWidth());
                nodeHeight = snapSize(stackPane.getLayoutBounds().getHeight());
            }
            nodeSizeInvalid = false;
        }
    }

    private boolean isReverseNodeOrientation() {
        return (stackPane != null &&
                getSkinnable().getEffectiveNodeOrientation() !=
                        stackPane.getEffectiveNodeOrientation());
    }

    private boolean determineHorizontalSBVisible() {
        final ImageScrollPane sp = getSkinnable();

        if (IS_TOUCH_SUPPORTED) {
            return (tempVisibility && (nodeWidth > contentWidth));
        }
        else {
            // RT-17395: ScrollBarPolicy might be null. If so, treat it as "AS_NEEDED", which is the default
            ImageScrollPane.ScrollBarPolicy hbarPolicy = sp.getHbarPolicy();
            return (ImageScrollPane.ScrollBarPolicy.NEVER != hbarPolicy) && ((ImageScrollPane.ScrollBarPolicy.ALWAYS == hbarPolicy) || (nodeWidth > contentWidth));
        }
    }

    private boolean determineVerticalSBVisible() {
        final ImageScrollPane sp = getSkinnable();

        if (IS_TOUCH_SUPPORTED) {
            return (tempVisibility && (nodeHeight > contentHeight));
        }
        else {
            // RT-17395: ScrollBarPolicy might be null. If so, treat it as "AS_NEEDED", which is the default
            ImageScrollPane.ScrollBarPolicy vbarPolicy = sp.getVbarPolicy();
            return (ImageScrollPane.ScrollBarPolicy.NEVER != vbarPolicy) && ((ImageScrollPane.ScrollBarPolicy.ALWAYS == vbarPolicy) || (nodeHeight > contentHeight));
        }
    }


    private void computeScrollBarSize() {
        vsbWidth = snapSize(vsb.prefWidth(-1));
        if (vsbWidth == 0) {
            //            println("*** WARNING ScrollPaneSkin: can't get scroll bar width, using {DEFAULT_SB_BREADTH}");
            if (IS_TOUCH_SUPPORTED) {
                vsbWidth = DEFAULT_EMBEDDED_SB_BREADTH;
            }
            else {
                vsbWidth = DEFAULT_SB_BREADTH;
            }
        }
        hsbHeight = snapSize(hsb.prefHeight(-1));
        if (hsbHeight == 0) {
            //            println("*** WARNING ScrollPaneSkin: can't get scroll bar height, using {DEFAULT_SB_BREADTH}");
            if (IS_TOUCH_SUPPORTED) {
                hsbHeight = DEFAULT_EMBEDDED_SB_BREADTH;
            }
            else {
                hsbHeight = DEFAULT_SB_BREADTH;
            }
        }
    }

    private void updateHorizontalSB() {
        double contentRatio = nodeWidth * (hsb.getMax() - hsb.getMin());
        if (contentRatio > 0.0) {
            hsb.setVisibleAmount(contentWidth / contentRatio);
            hsb.setBlockIncrement(0.9 * hsb.getVisibleAmount());
            hsb.setUnitIncrement(0.1 * hsb.getVisibleAmount());
        }
        else {
            hsb.setVisibleAmount(0.0);
            hsb.setBlockIncrement(0.0);
            hsb.setUnitIncrement(0.0);
        }

        if (hsb.isVisible()) {
            updatePosX();
        } else {
            if (nodeWidth > contentWidth) {
                updatePosX();
            } else {
                viewContent.setLayoutX(0);
            }
        }
    }

    private void updateVerticalSB() {
        double contentRatio = nodeHeight * (vsb.getMax() - vsb.getMin());
        if (contentRatio > 0.0) {
            vsb.setVisibleAmount(contentHeight / contentRatio);
            vsb.setBlockIncrement(0.9 * vsb.getVisibleAmount());
            vsb.setUnitIncrement(0.1 * vsb.getVisibleAmount());
        }
        else {
            vsb.setVisibleAmount(0.0);
            vsb.setBlockIncrement(0.0);
            vsb.setUnitIncrement(0.0);
        }

        if (vsb.isVisible()) {
            updatePosY();
        } else {
            if (nodeHeight > contentHeight) {
                updatePosY();
            } else {
                viewContent.setLayoutY(0);
            }
        }
    }

    private double updatePosX() {
        final ImageScrollPane sp = getSkinnable();
        double x = isReverseNodeOrientation() ? (hsb.getMax() - (posX - hsb.getMin())) : posX;
        double minX = Math.min((- x / (hsb.getMax() - hsb.getMin()) * (nodeWidth - contentWidth)), 0);
        viewContent.setLayoutX(snapPosition(minX));
        if (!sp.hvalueProperty().isBound()) sp.setHvalue(Utils.clamp(sp.getHmin(), posX, sp.getHmax()));
        return posX;
    }

    private double updatePosY() {
        final ImageScrollPane sp = getSkinnable();
        double minY = Math.min((- posY / (vsb.getMax() - vsb.getMin()) * (nodeHeight - contentHeight)), 0);
        viewContent.setLayoutY(snapPosition(minY));
        if (!sp.vvalueProperty().isBound()) sp.setVvalue(Utils.clamp(sp.getVmin(), posY, sp.getVmax()));
        return posY;
    }

    private void resetClip() {
        clipRect.setWidth(snapSize(contentWidth));
        clipRect.setHeight(snapSize(contentHeight));
    }

    Timeline sbTouchTimeline;
    KeyFrame sbTouchKF1;
    KeyFrame sbTouchKF2;
    Timeline contentsToViewTimeline;
    KeyFrame contentsToViewKF1;
    KeyFrame contentsToViewKF2;
    KeyFrame contentsToViewKF3;

    private boolean tempVisibility;


    protected void startSBReleasedAnimation() {
        if (sbTouchTimeline == null) {
            /*
             ** timeline to leave the scrollbars visible for a short
             ** while after a scroll/drag
             */
            sbTouchTimeline = new Timeline();
            sbTouchKF1 = new KeyFrame(Duration.millis(0), event -> {
                tempVisibility = true;
                if (touchDetected == true || mouseDown == true) {
                    sbTouchTimeline.playFromStart();
                }
            });

            sbTouchKF2 = new KeyFrame(Duration.millis(1000), event -> {
                tempVisibility = false;
                getSkinnable().requestLayout();
            });
            sbTouchTimeline.getKeyFrames().addAll(sbTouchKF1, sbTouchKF2);
        }
        sbTouchTimeline.playFromStart();
    }

    protected void startContentsToViewport() {
        double newPosX = posX;
        double newPosY = posY;

        setContentPosX(posX);
        setContentPosY(posY);

        if (posY > getSkinnable().getVmax()) {
            newPosY = getSkinnable().getVmax();
        }
        else if (posY < getSkinnable().getVmin()) {
            newPosY = getSkinnable().getVmin();
        }


        if (posX > getSkinnable().getHmax()) {
            newPosX = getSkinnable().getHmax();
        }
        else if (posX < getSkinnable().getHmin()) {
            newPosX = getSkinnable().getHmin();
        }

        if (!IS_TOUCH_SUPPORTED) {
            startSBReleasedAnimation();
        }

        /*
         ** timeline to return the contents of the scrollpane to the viewport
         */
        if (contentsToViewTimeline != null) {
            contentsToViewTimeline.stop();
        }
        contentsToViewTimeline = new Timeline();
        /*
         ** short pause before animation starts
         */
        contentsToViewKF1 = new KeyFrame(Duration.millis(50));
        /*
         ** reposition
         */
        contentsToViewKF2 = new KeyFrame(Duration.millis(150), event -> {
            getSkinnable().requestLayout();
        },
                new KeyValue(contentPosX, newPosX),
                new KeyValue(contentPosY, newPosY)
        );
        /*
         ** block out 'aftershocks', but real events will
         ** still reactivate
         */
        contentsToViewKF3 = new KeyFrame(Duration.millis(1500));
        contentsToViewTimeline.getKeyFrames().addAll(contentsToViewKF1, contentsToViewKF2, contentsToViewKF3);
        contentsToViewTimeline.playFromStart();
    }


    private DoubleProperty contentPosX;

    private void setContentPosX(double value) { contentPosXProperty().set(value); }

    private double getContentPosX() { return contentPosX == null ? 0.0 : contentPosX.get(); }

    private DoubleProperty contentPosXProperty() {
        if (contentPosX == null) {
            contentPosX = new DoublePropertyBase() {
                @Override protected void invalidated() {
                    hsb.setValue(getContentPosX());
                    getSkinnable().requestLayout();
                }

                @Override
                public Object getBean() {
                    return ImageScrollPaneSkin.this;
                }

                @Override
                public String getName() {
                    return "contentPosX";
                }
            };
        }
        return contentPosX;
    }


    private DoubleProperty contentPosY;

    private void setContentPosY(double value) { contentPosYProperty().set(value); }

    private double getContentPosY() { return contentPosY == null ? 0.0 : contentPosY.get(); }

    private DoubleProperty contentPosYProperty() {
        if (contentPosY == null) {
            contentPosY = new DoublePropertyBase() {
                @Override protected void invalidated() {
                    vsb.setValue(getContentPosY());
                    getSkinnable().requestLayout();
                }

                @Override
                public Object getBean() {
                    return ImageScrollPaneSkin.this;
                }

                @Override
                public String getName() {
                    return "contentPosY";
                }
            };
        }
        return contentPosY;
    }

    @Override
    protected Object queryAccessibleAttribute(AccessibleAttribute attribute, Object... parameters) {
        switch (attribute) {
            case VERTICAL_SCROLLBAR: return vsb;
            case HORIZONTAL_SCROLLBAR: return hsb;
            default: return super.queryAccessibleAttribute(attribute, parameters);
        }
    }
}
