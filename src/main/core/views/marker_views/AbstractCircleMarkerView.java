package main.core.views.marker_views;

import main.core.data_types.markers.circle_markers_branch.CircleMarker;
import main.core.views.MarkerImageView;

public abstract class AbstractCircleMarkerView<T> extends AbstractMarkerView<T> {

    public AbstractCircleMarkerView(T marker, MarkerImageView markerImageView) {
        super(marker, markerImageView);
    }

    public abstract CircleMarker getCircleMarker();
}
