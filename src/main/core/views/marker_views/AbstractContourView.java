package main.core.views.marker_views;

import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import main.core.data_types.markers.contour_branch.Contour;
import main.core.views.MarkerImageView;
import main.core.views.skins.marker_views.ContourViewSkin;

public abstract class AbstractContourView<T> extends AbstractMarkerView<T> {

    public AbstractContourView(T content, MarkerImageView markerImageView) {
        super(content, markerImageView);
    }

    public abstract Contour getContour();

    public void addPoint(MouseEvent e) {
        if(getSkin() != null) {
            double invRatio = ((ContourViewSkin) getSkin()).getImageViewScale().getActualToScaledRatio();

            Point2D point2D = ((ContourViewSkin) getSkin()).fromPaneToPolygon(e);

            getContour().getPoints().add(point2D.multiply(invRatio));

            e.consume();
        }
    }
}
