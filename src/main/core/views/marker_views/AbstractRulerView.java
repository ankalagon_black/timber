package main.core.views.marker_views;

import main.core.data_types.markers.ruler_branch.Ruler;
import main.core.views.MarkerImageView;

public abstract class AbstractRulerView<T> extends AbstractMarkerView<T> {

    public AbstractRulerView(T marker, MarkerImageView markerImageView) {
        super(marker, markerImageView);
    }

    public abstract Ruler getRuler();
}
