package main.core.views.marker_views;

import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import main.core.data_types.markers.Marker;
import main.core.views.MarkerImageView;

public abstract class AbstractMarkerView<T> extends Control {

    public AbstractMarkerView(T content, MarkerImageView markerImageView) {
        this.content = content;
        this.markerImageView = markerImageView;
    }

    private T content;

    public T getContent() {
        return content;
    }


    public abstract Marker getMarker();


    private MarkerImageView markerImageView;

    public MarkerImageView getMarkerImageView() {
        return markerImageView;
    }


    protected abstract Skin<?> createDefaultSkin();
}
