package main.core.views.data_types;

import javafx.collections.ObservableList;
import javafx.util.Callback;
import main.core.views.marker_views.AbstractMarkerView;

public abstract class ListHandler<T, V extends AbstractMarkerView> {

    public ListHandler(ObservableList<T> list) {
        this.list = list;
    }


    private ObservableList<T> list;

    public ObservableList<T> getList() {
        return list;
    }


    private Callback<T, V> callback;

    protected void setCallback(Callback<T, V> callback) {
        this.callback = callback;
    }

    public Callback<T, V> getCallback() {
        return callback;
    }
}
