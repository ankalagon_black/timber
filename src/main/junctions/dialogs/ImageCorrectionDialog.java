package main.junctions.dialogs;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.image.Image;
import main.junctions.controllers.dialogs.ImageCorrectionController;

import java.io.IOException;
import java.io.InputStream;

public class ImageCorrectionDialog extends Dialog<Image> {

    public ImageCorrectionDialog(Image originalImage) throws IOException {
        setTitle("Коррекция изображения");

        FXMLLoader loader = new FXMLLoader();
        InputStream inputStream = getClass().getResourceAsStream("/main/junctions/resources/fxml/dialogs/image_correction.fxml");
        DialogPane dialogPane =  loader.load(inputStream);
        inputStream.close();

        ImageCorrectionController correctionController = loader.getController();
        correctionController.setController(originalImage);

        setDialogPane(dialogPane);
        setResultConverter(param -> {
            if(param.getButtonData().equals(ButtonBar.ButtonData.OK_DONE)){
                return correctionController.getImage();
            }
            else return null;
        });
    }
}
