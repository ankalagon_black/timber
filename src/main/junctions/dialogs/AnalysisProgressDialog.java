package main.junctions.dialogs;

import io.reactivex.Completable;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import main.junctions.controllers.dialogs.AnalysisProgressController;

import java.io.IOException;

public class AnalysisProgressDialog extends Dialog<Void> {

    public AnalysisProgressDialog(Completable completable, String title) throws IOException {
        setTitle(title);
        setResizable(true);

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/main/junctions/resources/fxml/dialogs/analysis_progress.fxml"));

        setDialogPane(loader.load());

        AnalysisProgressController controller = loader.getController();
        controller.setController(completable);
    }
}
