package main.junctions.dialogs;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.*;
import main.repository.image.ImageCropper;
import main.junctions.controllers.dialogs.CropImageController;

import java.io.IOException;

/**
 * Created by Константин on 01.08.2017.
 */
public class CropImageDialog extends Dialog<Image>{

    public CropImageDialog(Image image) throws IOException {
        setTitle("Обрезка изображения");

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/main/junctions/resources/fxml/dialogs/crop_image.fxml"));

        DialogPane dialogPane =  loader.load();
        CropImageController controller = loader.getController();
        controller.setController(image);

        setDialogPane(dialogPane);
        setResultConverter(param -> {
            if(param.getButtonData().equals(ButtonBar.ButtonData.OK_DONE)){
                return new ImageCropper(image).crop(controller.getSelectionInImageCoordinates());
            }
            else return null;
        });
    }
}
