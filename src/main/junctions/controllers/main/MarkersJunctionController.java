package main.junctions.controllers.main;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.*;
import javafx.scene.control.Accordion;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import main.core.data_types.common.Project;
import main.core.data_types.junctions.MarkersJunction;
import main.domain.marker_wrappers.junction.JunctionContourWrapper;
import main.domain.marker_wrappers.junction.JunctionMarkerWrapper;
import main.domain.marker_wrappers.junction.JunctionRulerWrapper;
import main.domain.marker_wrappers.base.LogicalContourWrapper;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.domain.marker_settings_wrappers.ContourSettingsWrapper;
import main.domain.marker_settings_wrappers.MarkerSettingsWrapper;
import main.domain.marker_settings_wrappers.RulerSettingsWrapper;
import main.core.utils.images.ImageViewScale;
import main.junctions.controllers.rigth_part.GeometryController;
import main.junctions.controllers.rigth_part.markers_lists.MarkersListController;
import main.junctions.controllers.rigth_part.settings.ContourSettingsController;
import main.junctions.controllers.rigth_part.settings.RulerSettingsController;
import main.junctions.views.custom.marker_image_views.MarkersJunctionImageView;
import main.junctions.controllers.bar.JunctionBarController;
import main.repository.database.CubageDatabase;
import main.repository.database.DatabaseManager;
import main.repository.image.ImageCache;
import main.truck.interfaces.Disposable;
import main.truck.interfaces.JunctionBackable;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;

public class MarkersJunctionController implements Disposable {
    public VBox junctionBar;
    public MarkersJunctionImageView markersJunctionImageView;
    public ImageView imageView;

    public Accordion accordion;
    public TitledPane rulerTitledPane;
    public TitledPane contourTitledPane;
    public TitledPane markerListTitledPane;
    public TitledPane geometryTitledPane;

    public VBox ruler;
    public VBox contour;
    public ScrollPane markersList;
    public ScrollPane geometry;

    public JunctionBarController junctionBarController;

    public RulerSettingsController rulerController;
    public ContourSettingsController contourController;
    public MarkersListController markersListController;
    public GeometryController geometryController;

    private MarkersJunction junction;

    private CubageDatabase cubageDatabase;

    private JunctionRulerWrapper junctionRulerWrapper;
    private ObservableList<JunctionContourWrapper> contourWrappers = FXCollections.observableArrayList();
    private ObservableList<JunctionMarkerWrapper> junctionMarkers = FXCollections.observableArrayList();

    private RulerSettingsWrapper rulerSettingsWrapper;
    private ContourSettingsWrapper contourSettingsWrapper;
    private MarkerSettingsWrapper markerSettingsWrapper;

    private ListChangeListener<JunctionMarkerWrapper> markerWrapperListChangeListener = c -> {
        int minIndex = Integer.MAX_VALUE;
        while (c.next())
            if(c.getFrom() < minIndex) minIndex = c.getFrom();

        if(minIndex != -1) {
            for (int i = minIndex; i < junctionMarkers.size(); i++)
                junctionMarkers.get(i).setIndex(i);
        }
    };

    private ChangeListener<LogicalContour> logicalContourChangeListener = (observable, oldValue, newValue) -> {
        if(newValue == null) contourWrappers.removeIf(junctionContourWrapper -> junctionContourWrapper.get().getLogicalContour().equals(oldValue));
        else {
            LogicalContourWrapper contourWrapper = new LogicalContourWrapper(newValue, junction.getRuler());
            JunctionContourWrapper wrapper = new JunctionContourWrapper(contourWrapper, contourSettingsWrapper, junction, cubageDatabase);

            contourWrappers.add(wrapper);
        }
    };
    private WeakChangeListener<LogicalContour> weakContourWrapperListChangeListener = new WeakChangeListener<>(logicalContourChangeListener);

    private ListChangeListener<Marker> markerListChangeListener = c -> {
        while (c.next()) {
            if(c.wasRemoved()) {
                List<Marker> markerList = (List<Marker>) c.getRemoved();

                for (Marker i: markerList) junctionMarkers.removeIf(wrapper -> wrapper.getMarker().equals(i));
            }
            else if(c.wasAdded()) {
                List<Marker> markerList = (List<Marker>) c.getAddedSubList();

                for (Marker i: markerList) junctionMarkers.add(new JunctionMarkerWrapper(i, junctionMarkers.size(), markerSettingsWrapper));
            }
        }
    };
    private WeakListChangeListener<Marker> markerWeakListChangeListener = new WeakListChangeListener<>(markerListChangeListener);


    public void setController(MarkersJunction junction, Project project, JunctionBackable junctionBackable) {
        this.junction = junction;

        junction.getSummary().resetMarkersResults();

        try {
            cubageDatabase = new DatabaseManager().getCubageDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            imageView.setImage(ImageCache.get().get(junction.getCurrentSource().getPath()));
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }

        rulerSettingsWrapper = new RulerSettingsWrapper(junction.getRulerSettings(), false, false);
        contourSettingsWrapper = new ContourSettingsWrapper(junction.getContourSettings(), false, true);
        markerSettingsWrapper = new MarkerSettingsWrapper(junction.getMarkerSettings(), true, true);

        junctionBarController.setController(junctionBackable, imageView, junction, project);

        junctionRulerWrapper = new JunctionRulerWrapper(junction.getRuler(), rulerSettingsWrapper);

        junctionMarkers.addAll(junction.getMarkers().stream().collect(ArrayList::new, (BiConsumer<ArrayList<JunctionMarkerWrapper>, Marker>) (junctionMarkers, marker) -> {
            junctionMarkers.add(new JunctionMarkerWrapper(marker, junctionMarkers.size(), markerSettingsWrapper));
        }, ArrayList::addAll));
        junctionMarkers.addListener(markerWrapperListChangeListener);

        if(junction.hasLogicalContour()) contourWrappers.add(new JunctionContourWrapper(new LogicalContourWrapper(junction.getLogicalContour(), junction.getRuler()), contourSettingsWrapper, junction, cubageDatabase));
        junction.logicalContourProperty().addListener(weakContourWrapperListChangeListener);


        rulerController.setController(junctionRulerWrapper);
        contourController.setController(contourSettingsWrapper);
        markersListController.setController(junction, markerSettingsWrapper);
        geometryController.setController(junction, project.getClassifier());

        markersJunctionImageView.initialize(junctionRulerWrapper, rulerSettingsWrapper, contourWrappers, contourSettingsWrapper, junction);
        markersJunctionImageView.setMarkerSettingsWrapper(markerSettingsWrapper);
        markersJunctionImageView.add(junctionMarkers, junction);

        markersJunctionImageView.setEnabled(MarkersJunctionImageView.MARKERS);
        markersJunctionImageView.setMouseEventHandler(event -> {
            if(markerSettingsWrapper.isEnabled()) {
                if(event.getButton().equals(MouseButton.SECONDARY)) {
                    ImageViewScale imageViewScale = new ImageViewScale();
                    imageViewScale.setImageView(imageView);

                    double invRatio = imageViewScale.getActualToScaledRatio();

                    Marker marker = new Marker(event.getX() * invRatio, event.getY() * invRatio);

                    junction.getMarkers().add(marker);
                }
            }
            else if(contourSettingsWrapper.isEnabled()) {
                if(event.getButton().equals(MouseButton.SECONDARY) && contourWrappers.size() == 0) {
                    ImageViewScale imageViewScale = new ImageViewScale();
                    imageViewScale.setImageView(imageView);

                    double invRatio = imageViewScale.getActualToScaledRatio();
                    LogicalContour contour = new LogicalContour(event.getX() * invRatio, event.getY() * invRatio);

                    junction.setLogicalContour(contour);
                }
            }
        });

        accordion.expandedPaneProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                if(newValue.getId().equals(rulerTitledPane.getId())) markersJunctionImageView.setEnabled(MarkersJunctionImageView.RULER);
                else if(newValue.getId().equals(contourTitledPane.getId())) markersJunctionImageView.setEnabled(MarkersJunctionImageView.CONTOURS);
                else if(newValue.getId().equals(markerListTitledPane.getId())) markersJunctionImageView.setEnabled(MarkersJunctionImageView.MARKERS);
                else if(newValue.getId().equals(geometryTitledPane.getId())) {
                    markersJunctionImageView.setEnabled(MarkersJunctionImageView.GEOMETRY);
                    geometryController.updateLogCount(junction.getMarkersCount());
                }
            }
            else markersJunctionImageView.setEnabled(MarkersJunctionImageView.MARKERS);
        });

        junction.getMarkers().addListener(markerWeakListChangeListener);

        if(!junction.hasModifiedFile() && junction.getMarkers().size() == 0 && !junction.hasLogicalContour()) Platform.runLater(() -> junctionBarController.onCrop(null));
    }

    @Override
    public void dispose() {
        cubageDatabase.close();
        junction.getMarkers().removeListener(markerWeakListChangeListener);
        junction.logicalContourProperty().removeListener(weakContourWrapperListChangeListener);
    }
}
