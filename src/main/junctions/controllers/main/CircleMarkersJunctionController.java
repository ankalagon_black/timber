package main.junctions.controllers.main;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.WeakListChangeListener;
import javafx.scene.control.Accordion;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import main.core.data_types.common.Project;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.domain.CircleMarkersDomain;
import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
import main.domain.marker_settings_wrappers.ContourSettingsWrapper;
import main.domain.marker_settings_wrappers.RulerSettingsWrapper;
import main.core.utils.images.ImageViewScale;
import main.domain.marker_wrappers.base.LogicalCircleMarkerWrapper;
import main.domain.marker_wrappers.base.LogicalContourWrapper;
import main.domain.marker_wrappers.junction.JunctionCircleMarkerWrapper;
import main.domain.marker_wrappers.junction.JunctionContourWrapper;
import main.domain.marker_wrappers.junction.JunctionRulerWrapper;
import main.junctions.controllers.bar.CircleMarkersJunctionBarController;
import main.junctions.controllers.rigth_part.ResultsController;
import main.junctions.controllers.rigth_part.GeometryController;
import main.junctions.controllers.rigth_part.settings.ContourSettingsController;
import main.junctions.views.custom.marker_image_views.CircleMarkersJunctionImageView;
import main.junctions.controllers.rigth_part.markers_lists.CircleMarkersListController;
import main.junctions.controllers.rigth_part.settings.RulerSettingsController;
import main.junctions.controllers.bar.JunctionBarController;
import main.repository.database.CubageDatabase;
import main.repository.database.DatabaseManager;
import main.repository.image.ImageCache;
import main.truck.interfaces.Disposable;
import main.truck.interfaces.JunctionBackable;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;

public class CircleMarkersJunctionController implements Disposable {
    public VBox junctionBar;
    public CircleMarkersJunctionImageView circleMarkersJunctionImageView;
    public ImageView imageView;

    public Accordion accordion;
    public TitledPane rulerTitledPane;
    public TitledPane contourTitledPane;
    public TitledPane circleMarkerListTitledPane;
    public TitledPane geometryTitledPane;
    public TitledPane resultsTitledPane;

    public VBox ruler;
    public VBox contour;
    public ScrollPane circleMarkersList;
    public ScrollPane geometry;
    public VBox results;

    public JunctionBarController junctionBarController;

    public RulerSettingsController rulerController;
    public ContourSettingsController contourController;
    public CircleMarkersListController circleMarkersListController;
    public GeometryController geometryController;
    public ResultsController resultsController;

    private CircleMarkersJunction junction;
    private Project project;
    private CircleMarkersDomain domain;

    private CubageDatabase cubageDatabase;

    private JunctionRulerWrapper junctionRulerWrapper;
    private ObservableList<JunctionContourWrapper> contourWrappers = FXCollections.observableArrayList();
    private ObservableList<JunctionCircleMarkerWrapper> junctionMarkers = FXCollections.observableArrayList();

    private RulerSettingsWrapper rulerSettingsWrapper;
    private CircleMarkerSettingsWrapper circleMarkerSettingsWrapper;
    private ContourSettingsWrapper contourSettingsWrapper;

    private ListChangeListener<JunctionCircleMarkerWrapper> listChangeListener = c -> {
        int minIndex = Integer.MAX_VALUE;
        while (c.next()) {
            if (c.wasRemoved() && c.getFrom() < minIndex) minIndex = c.getFrom();
        }

        if(minIndex != -1) {
            for (int i = minIndex; i < junction.getMarkersCount(); i++)
                junctionMarkers.get(i).setIndex(i);
        }
    };

    private ChangeListener<LogicalContour> logicalContourChangeListener = (observable, oldValue, newValue) -> {
        if(newValue == null) contourWrappers.removeIf(junctionContourWrapper -> junctionContourWrapper.get().getLogicalContour().equals(oldValue));
        else {
            LogicalContourWrapper contourWrapper = new LogicalContourWrapper(newValue, junction.getRuler());
            JunctionContourWrapper wrapper = new JunctionContourWrapper(contourWrapper, contourSettingsWrapper, junction, cubageDatabase);

            contourWrappers.add(wrapper);
        }
    };
    private WeakChangeListener<LogicalContour> weakContourWrapperListChangeListener = new WeakChangeListener<>(logicalContourChangeListener);

    private ListChangeListener<LogicalCircleMarker> markerListChangeListener = c -> {
        while (c.next()) {
            if(c.wasRemoved()) {
                List<LogicalCircleMarker> markerList = (List<LogicalCircleMarker>) c.getRemoved();

                for (LogicalCircleMarker i: markerList) junctionMarkers.removeIf(wrapper -> wrapper.getLogicalCircleMarkerWrapper().getLogicalCircleMarker().equals(i));
            }
            else if(c.wasAdded()) {
                List<LogicalCircleMarker> markerList = (List<LogicalCircleMarker>) c.getAddedSubList();

                for (LogicalCircleMarker i: markerList) junctionMarkers.add(new JunctionCircleMarkerWrapper(new LogicalCircleMarkerWrapper(i, junction.getRuler(),
                        project.getSetting().getLogParameters()),
                        junctionMarkers.size(), circleMarkerSettingsWrapper));
            }
        }
    };
    private WeakListChangeListener<LogicalCircleMarker> markerWeakListChangeListener = new WeakListChangeListener<>(markerListChangeListener);

    public void setController(CircleMarkersJunction junction, Project project, JunctionBackable junctionBackable) {
        this.junction = junction;
        this.project = project;

        try {
            cubageDatabase = new DatabaseManager().getCubageDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            imageView.setImage(ImageCache.get().get(junction.getCurrentSource().getPath()));
            domain = new CircleMarkersDomain(junction, cubageDatabase);
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }

        rulerSettingsWrapper = new RulerSettingsWrapper(junction.getRulerSettings(), true, true);
        contourSettingsWrapper = new ContourSettingsWrapper(junction.getContourSettings(), false, true);
        circleMarkerSettingsWrapper = new CircleMarkerSettingsWrapper(junction.getCircleMarkerSettings(), true, true);

        junctionBarController.setController(junctionBackable, imageView, junction, project);

        junctionRulerWrapper = new JunctionRulerWrapper(junction.getRuler(), rulerSettingsWrapper);

        if(junction.hasLogicalContour()) contourWrappers.add(new JunctionContourWrapper(new LogicalContourWrapper(junction.getLogicalContour(), junction.getRuler()), contourSettingsWrapper, junction, cubageDatabase));
        junction.logicalContourProperty().addListener(weakContourWrapperListChangeListener);

        junctionMarkers.addAll(junction.getMarkers().stream().collect(ArrayList::new,
                (BiConsumer<ArrayList<JunctionCircleMarkerWrapper>, LogicalCircleMarker>) (arrayList, circleMarker) -> {
                    arrayList.add(new JunctionCircleMarkerWrapper(new LogicalCircleMarkerWrapper(circleMarker, junction.getRuler(), project.getSetting().getLogParameters()), arrayList.size(), circleMarkerSettingsWrapper));
                }, ArrayList::addAll));
        junctionMarkers.addListener(listChangeListener);


        rulerController.setController(junctionRulerWrapper);
        contourController.setController(contourSettingsWrapper);
        circleMarkersListController.setController(junction, circleMarkerSettingsWrapper, domain);
        geometryController.setController(junction, project.getClassifier());
        resultsController.setController(domain);

        circleMarkersJunctionImageView.initialize(junctionRulerWrapper, rulerSettingsWrapper, contourWrappers, contourSettingsWrapper, junction);
        circleMarkersJunctionImageView.setCircleMarkerSettings(circleMarkerSettingsWrapper);
        circleMarkersJunctionImageView.add(junctionMarkers, junction, project.getSetting().getLogParameters());

        circleMarkersJunctionImageView.setEnabled(CircleMarkersJunctionImageView.MARKERS);
        circleMarkersJunctionImageView.setMouseEventHandler(event -> {
            if(circleMarkerSettingsWrapper.isEnabled()) {
                if(event.getButton().equals(MouseButton.SECONDARY)) {
                    ImageViewScale imageViewScale = new ImageViewScale();
                    imageViewScale.setImageView(imageView);

                    double invRatio = imageViewScale.getActualToScaledRatio();

                    LogicalCircleMarker logicalCircleMarker = new LogicalCircleMarker(event.getX() * invRatio, event.getY() * invRatio,
                            project.getSetting().getLogParameters().getMinDiameterInPixels(junction.getRuler()) / 2,
                            project.getSetting().getLogParameters().getMinDiameter());

                    junction.getMarkers().add(logicalCircleMarker);
                }
            }
            else if(contourSettingsWrapper.isEnabled()) {
                if(event.getButton().equals(MouseButton.SECONDARY) && contourWrappers.size() == 0) {
                    ImageViewScale imageViewScale = new ImageViewScale();
                    imageViewScale.setImageView(imageView);

                    double invRatio = imageViewScale.getActualToScaledRatio();
                    LogicalContour contour = new LogicalContour(event.getX() * invRatio, event.getY() * invRatio);

                    junction.setLogicalContour(contour);
                }
            }
        });

        accordion.expandedPaneProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                if(newValue.getId().equals(rulerTitledPane.getId())) circleMarkersJunctionImageView.setEnabled(CircleMarkersJunctionImageView.RULER);
                else if(newValue.getId().equals(contourTitledPane.getId())) circleMarkersJunctionImageView.setEnabled(CircleMarkersJunctionImageView.CONTOURS);
                else if(newValue.getId().equals(circleMarkerListTitledPane.getId())) circleMarkersJunctionImageView.setEnabled(CircleMarkersJunctionImageView.MARKERS);
                else if(newValue.getId().equals(geometryTitledPane.getId())) {
                    circleMarkersJunctionImageView.setEnabled(CircleMarkersJunctionImageView.GEOMETRY);
                    geometryController.updateLogCount(junction.getMarkersCount());
                }
                else if(newValue.getId().equals(resultsTitledPane.getId())) {
                    circleMarkersJunctionImageView.setEnabled(CircleMarkersJunctionImageView.RESULTS);
                    resultsController.update();
                }
            }
            else circleMarkersJunctionImageView.setEnabled(CircleMarkersJunctionImageView.MARKERS);
        });

        junction.getMarkers().addListener(markerWeakListChangeListener);

        if(!junction.hasModifiedFile() && junction.getMarkers().size() == 0 && !junction.hasLogicalContour()) Platform.runLater(() -> junctionBarController.onCrop(null));
    }

    @Override
    public void dispose() {
        cubageDatabase.close();
        junction.getMarkers().removeListener(markerWeakListChangeListener);
    }
}
