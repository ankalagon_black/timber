package main.junctions.controllers.rigth_part.settings;

import javafx.event.ActionEvent;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import main.domain.marker_settings_wrappers.MarkerSettingsWrapper;

public class MarkersListSettingsController {
    public ColorPicker markersTextColorPicker;
    public Slider fontSizeSlider;

    private MarkerSettingsWrapper settings;

    public void setController(MarkerSettingsWrapper settings) {
        this.settings = settings;

        markersTextColorPicker.setValue(settings.getMarkerSettings().getTextColor());
        fontSizeSlider.setValue(settings.getMarkerSettings().getFontSize());

        fontSizeSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            settings.getMarkerSettings().setFontSize(newValue.intValue());
        });
    }

    public void onMarkerTextColorChanged(ActionEvent actionEvent) {
        settings.getMarkerSettings().setTextColor(markersTextColorPicker.getValue());
    }
}
