package main.junctions.controllers.rigth_part.settings;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import main.core.data_types.base.LogicalValue;
import main.domain.marker_wrappers.junction.JunctionRulerWrapper;
import main.core.utils.strings.DoubleFilter;
import main.core.utils.strings.DoublePrecisionStringConverter;

public class RulerSettingsController {

    public CheckBox showRuler;
    public TextField lengthInCm;
    public ColorPicker rulerColor;
    public ColorPicker textColor;
    public Slider textFontSize;

    private TextFormatter<Double> lengthInCmFormatter;

    private JunctionRulerWrapper junctionRuler;

    public void setController(JunctionRulerWrapper junctionRuler) {
        this.junctionRuler = junctionRuler;

        textFontSize.setValue(junctionRuler.getRulerSettingsWrapper().getRulerSettings().getFontSize());
        textColor.setValue(junctionRuler.getRulerSettingsWrapper().getRulerSettings().getTextColor());
        rulerColor.setValue(junctionRuler.getRulerSettingsWrapper().getRulerSettings().getColor());
        showRuler.setSelected(junctionRuler.getRulerSettingsWrapper().isVisible());

        lengthInCmFormatter = new TextFormatter<>(new DoublePrecisionStringConverter(1), junctionRuler.getLogicalRuler().getLogicalLength().getValueInCm(), new DoubleFilter());
        lengthInCm.setTextFormatter(lengthInCmFormatter);

        showRuler.selectedProperty().addListener((observable, oldValue, newValue) -> {
            junctionRuler.getRulerSettingsWrapper().setVisible(newValue);
        });

        textFontSize.valueProperty().addListener((observable, oldValue, newValue) -> junctionRuler.getRulerSettingsWrapper().getRulerSettings().setFontSize(newValue.intValue()));
    }


    public void onKeyTyped(KeyEvent keyEvent) {
        if(keyEvent.getCharacter().equals("\r")) {
            LogicalValue logicalValue = new LogicalValue();
            logicalValue.setValueInCm(lengthInCmFormatter.getValue());

            junctionRuler.getLogicalRuler().setLogicalLength(logicalValue);
            lengthInCmFormatter.setValue(junctionRuler.getLogicalRuler().getLogicalLength().getValueInCm());
        }
    }

    public void onRulerColorChanged(ActionEvent actionEvent) {
        junctionRuler.getRulerSettingsWrapper().getRulerSettings().setColor(rulerColor.getValue());
    }

    public void onTextColorChanged(ActionEvent actionEvent) {
        junctionRuler.getRulerSettingsWrapper().getRulerSettings().setTextColor(textColor.getValue());
    }
}
