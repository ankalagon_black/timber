package main.junctions.controllers.rigth_part.settings;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.event.ActionEvent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import main.domain.marker_settings_wrappers.ContourSettingsWrapper;

public class ContourSettingsController {

    public ColorPicker contoursColorPicker;
    public CheckBox highlightInnerArea;
    public ColorPicker contoursTextColorPicker;
    public Slider fontSizeSlider;

    private ContourSettingsWrapper wrapper;

    private ChangeListener<Boolean> enabledListener = (observable, oldValue, newValue) -> {
        if(newValue) wrapper.setHighlightInnerAreaProxy(wrapper.get().isHighlightInnerArea());
        else wrapper.setHighlightInnerAreaProxy(false);
    };

    private ChangeListener<Boolean> highlightInnerAreaProxyListener = (observable, oldValue, newValue) -> {
        wrapper.setHighlightInnerAreaProxy(newValue);
    };

    public void setController(ContourSettingsWrapper wrapper) {
        this.wrapper = wrapper;

        contoursColorPicker.setValue(wrapper.get().getColor());
        highlightInnerArea.setSelected(wrapper.get().isHighlightInnerArea());
        contoursTextColorPicker.setValue(wrapper.get().getTextColor());
        fontSizeSlider.setValue(wrapper.get().getFontSize());

        wrapper.get().highlightInnerAreaProperty().addListener(new WeakChangeListener<>(highlightInnerAreaProxyListener));

        wrapper.enabledProperty().addListener(enabledListener);

        fontSizeSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            wrapper.get().setFontSize(newValue.intValue());
        });
        highlightInnerArea.selectedProperty().addListener((observable, oldValue, newValue) -> {
            wrapper.get().setHighlightInnerArea(newValue);
        });
    }


    public void onContoursColorChanged(ActionEvent actionEvent) {
        wrapper.get().setColor(contoursColorPicker.getValue());
    }

    public void onContoursTextColorChanged(ActionEvent actionEvent) {
        wrapper.get().setTextColor(contoursTextColorPicker.getValue());
    }
}
