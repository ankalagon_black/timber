package main.junctions.controllers.rigth_part.settings;

import javafx.event.ActionEvent;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;

public class CircleMarkersListSettingsController {
    public ColorPicker markersColorPicker;
    public ColorPicker markersTextColorPicker;
    public Slider fontSizeSlider;

    private CircleMarkerSettingsWrapper settings;

    public void setController(CircleMarkerSettingsWrapper settings) {
        this.settings = settings;

        markersColorPicker.setValue(settings.getCircleMarkerSettings().getColor());
        markersTextColorPicker.setValue(settings.getCircleMarkerSettings().getTextColor());
        fontSizeSlider.setValue(settings.getCircleMarkerSettings().getFontSize());

        fontSizeSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            settings.getCircleMarkerSettings().setFontSize(newValue.intValue());
        });
    }


    public void onMarkersColorChanged(ActionEvent actionEvent) {
        settings.getCircleMarkerSettings().setColor(markersColorPicker.getValue());
    }

    public void onMarkersTextColorChanged(ActionEvent actionEvent) {
        settings.getCircleMarkerSettings().setTextColor(markersTextColorPicker.getValue());
    }
}
