package main.junctions.controllers.rigth_part;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.scene.control.*;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.*;
import main.core.data_types.junctions.AbstractJunction;
import main.core.utils.strings.DoubleFilter;
import main.core.utils.strings.DoublePrecisionStringConverter;
import main.junctions.views.cells.BreedCell;
import main.truck.views.cells.CubageTypeCell;
import main.truck.views.cells.LengthCell;

import java.util.Locale;

public class GeometryController {
    public ComboBox<CubageType> cubageType;
    public ComboBox<Length> logLength;
    public ComboBox<Breed> breed;
    public TextArea commentary;
    public TextField coefficient;

    public Label averageDiameterLabel;
    public Label averageDiameterStandardLabel;
    public Label volumeStandardLabel;

    public Label logCountLabel;
    public Label averageDiameterGeometryLabel;
    public Label volumeGeometryLabel;

    private ChangeListener<LogicalValue> averageDiameterListener = (observable, oldValue, newValue) -> updateAverageDiameter(newValue);
    private ChangeListener<LogicalValue> averageDiameterStandardListener = (observable, oldValue, newValue) -> updateAverageDiameterStandard(newValue);
    private ChangeListener<Number> volumeStandardListener = (observable, oldValue, newValue) -> updateVolumeStandard(newValue.doubleValue());

    private ChangeListener<LogicalValue> averageDiameterGeometryListener = (observable, oldValue, newValue) -> updateAverageDiameterGeometry(newValue);
    private ChangeListener<Number> volumeGeometryListener = (observable, oldValue, newValue) -> updateVolumeGeometry(newValue.doubleValue());


    public void setController(AbstractJunction junction, Classifier classifier) {
        cubageType.setCellFactory(param -> new CubageTypeCell());
        cubageType.setItems(classifier.getCubageTypes());

        cubageType.setButtonCell(new CubageTypeCell());
        if(junction.hasCubageType()) cubageType.getSelectionModel().select(classifier.getCubageTypeById(junction.getCubageType()));

        cubageType.valueProperty().addListener((observable, oldValue, newValue) -> {
            junction.setCubageType(newValue.getId());
        });


        logLength.setCellFactory(param -> new LengthCell());
        logLength.setItems(classifier.getLengths());

        logLength.setButtonCell(new LengthCell());
        if(junction.hasLength()) logLength.getSelectionModel().select(classifier.getLengthByLogicalValue(junction.getLength()));

        logLength.valueProperty().addListener((observable, oldValue, newValue) -> {
            junction.setLength(newValue.getLength());
        });


        breed.setCellFactory(param -> new BreedCell());
        breed.setItems(classifier.getBreeds());

        breed.setButtonCell(new BreedCell());
        if(junction.hasBreed()) breed.getSelectionModel().select(classifier.getBreedById(junction.getBreed()));

        breed.valueProperty().addListener((observable, oldValue, newValue) -> {
            junction.setBreed(newValue.getId());
        });


        commentary.setText(junction.getCommentary());
        commentary.textProperty().addListener((observable, oldValue, newValue) -> {
            junction.setCommentary(newValue);
        });

        TextFormatter<Double> coefficientFormatter = new TextFormatter<>(new DoublePrecisionStringConverter(1), junction.getCoefficient(), new DoubleFilter());
        coefficientFormatter.valueProperty().addListener((observable, oldValue, newValue) -> {
            junction.setCoefficient(newValue);
        });
        coefficient.setTextFormatter(coefficientFormatter);

        Summary results = junction.getSummary();

        updateAverageDiameter(results.getAverageDiameter());
        updateAverageDiameterStandard(results.getAverageDiameterStandard());
        updateVolumeStandard(results.getVolumeStandard());
        updateAverageDiameterGeometry(results.getAverageDiameterGeometry());
        updateVolumeGeometry(results.getGeometryVolume());

        results.averageDiameterProperty().addListener(new WeakChangeListener<>(averageDiameterListener));
        results.averageDiameterStandardProperty().addListener(new WeakChangeListener<>(averageDiameterStandardListener));
        results.volumeStandardProperty().addListener(new WeakChangeListener<>(volumeStandardListener));
        results.averageDiameterGeometryProperty().addListener(new WeakChangeListener<>(averageDiameterGeometryListener));
        results.geometryVolumeProperty().addListener(new WeakChangeListener<>(volumeGeometryListener));
    }


    private void updateAverageDiameter(LogicalValue averageDiameter) {
        averageDiameterLabel.setText(String.format(Locale.US, "Средний диаметр: %.1f см", averageDiameter.getValueInCm()));
    }

    private void updateAverageDiameterStandard(LogicalValue averageDiameterStandard) {
        averageDiameterStandardLabel.setText(String.format(Locale.US, "Средний диаметр по ГОСТу: %.1f см", averageDiameterStandard.getValueInCm()));
    }

    private void updateVolumeStandard(double volumeStandard) {
        volumeStandardLabel.setText(String.format(Locale.US, "Всего объем по ГОСТу: %.3f м3", volumeStandard));
    }

    public void updateLogCount(int logCount) {
        logCountLabel.setText(String.format("Количество бревен: %d", logCount));
    }

    private void updateAverageDiameterGeometry(LogicalValue averageDiameterGeometry) {
        averageDiameterGeometryLabel.setText(String.format(Locale.US, "Средний диаметр по геометрии: %.1f см", averageDiameterGeometry.getValueInCm()));
    }

    private void updateVolumeGeometry(double volumeGeometry) {
        volumeGeometryLabel.setText(String.format(Locale.US, "Всего объем по геометрии: %.3f м3", volumeGeometry));
    }
}
