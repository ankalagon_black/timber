package main.junctions.controllers.rigth_part;

import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Summary;
import main.domain.CircleMarkersDomain;
import main.junctions.data_types.ResultRow;
import main.truck.views.cells.T2IntegerCell;
import main.truck.views.cells.T2LogicalValueCmCell;
import main.truck.views.cells.T2CubageCell;

import java.util.*;

public class ResultsController {
    public TableView<ResultRow> resultsTable;
    public TableColumn<ResultRow, LogicalValue> diameterStandard;
    public TableColumn<ResultRow, Number> cubage;
    public TableColumn<ResultRow, Number> count;
    public TableColumn<ResultRow, Number> sumVolumeStandard;
    public TableColumn<ResultRow, Number> volumePercentage;

    public Label logCount;
    public Label averageStandardDiameterLabel;
    public Label totalStandardVolumeLabel;

    private CircleMarkersDomain domain;

    public void setController(CircleMarkersDomain domain) {
        this.domain = domain;

        diameterStandard.setCellValueFactory(param -> param.getValue().diameterStandardProperty());
        diameterStandard.setCellFactory(param -> new T2LogicalValueCmCell<>());
        diameterStandard.setComparator(Comparator.comparingDouble(LogicalValue::getValue));

        cubage.setCellValueFactory(param -> param.getValue().cubageProperty());
        cubage.setCellFactory(param -> new T2CubageCell<>());

        count.setCellValueFactory(param -> param.getValue().countProperty());
        count.setCellFactory(param -> new T2IntegerCell<>());

        sumVolumeStandard.setCellValueFactory(param -> param.getValue().sumVolumeStandardProperty());
        sumVolumeStandard.setCellFactory(new Callback<TableColumn<ResultRow, Number>, TableCell<ResultRow, Number>>() {
            @Override
            public TableCell<ResultRow, Number> call(TableColumn<ResultRow, Number> param) {
                return new TableCell<ResultRow, Number>() {
                    @Override
                    protected void updateItem(Number item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty) setText(null);
                        else setText(String.format(Locale.US, "%.3f", item.doubleValue()));
                    }
                };
            }
        });

        volumePercentage.setCellValueFactory(param -> param.getValue().percentageProperty());
        volumePercentage.setCellFactory(new Callback<TableColumn<ResultRow, Number>, TableCell<ResultRow, Number>>() {
            @Override
            public TableCell<ResultRow, Number> call(TableColumn<ResultRow, Number> param) {
                return new TableCell<ResultRow, Number>() {
                    @Override
                    protected void updateItem(Number item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty) setText(null);
                        else setText(String.format(Locale.US, "%.1f", item.doubleValue() * 100));
                    }
                };
            }
        });
    }

    public void update() {
        resultsTable.setItems(domain.getResultRows());

        diameterStandard.setSortType(TableColumn.SortType.ASCENDING);
        if(resultsTable.getSortOrder().isEmpty()) resultsTable.getSortOrder().add(diameterStandard);

        Summary results = domain.getJunction().getSummary();

        this.logCount.setText(String.format("Количество бревен: %d", domain.getJunction().getMarkersCount()));
        this.averageStandardDiameterLabel.setText(String.format(Locale.US, "Средний диаметр по ГОСТу: %.1f", results.getAverageDiameterStandard().getValueInCm()));
        this.totalStandardVolumeLabel.setText(String.format(Locale.US, "Всего объем по ГОСТу: %.3f", results.getVolumeStandard()));
    }
}
