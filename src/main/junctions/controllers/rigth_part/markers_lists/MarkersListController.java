package main.junctions.controllers.rigth_part.markers_lists;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import main.core.data_types.junctions.MarkersJunction;
import main.core.data_types.markers.Marker;
import main.domain.marker_settings_wrappers.MarkerSettingsWrapper;
import main.junctions.controllers.rigth_part.settings.MarkersListSettingsController;
import main.junctions.views.cells.MarkerCell;

import java.io.IOException;

public class MarkersListController {
    public VBox markersListHeader;
    public MarkersListHeaderController markersListHeaderController;

    public ListView<Marker> listView;

    public void setController(MarkersJunction markersJunction, MarkerSettingsWrapper settings) {
        markersListHeaderController.setController(markersJunction);
        markersListHeaderController.settingsRoot.getChildren().add(loadSettings(settings));

        listView.setCellFactory(param -> {
            try {
                return new MarkerCell(markersJunction);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        });
        listView.setItems(markersJunction.getMarkers());
    }

    private Node loadSettings(MarkerSettingsWrapper settings) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/main/junctions/resources/fxml/right_part/markers_lists/settings/markers_list_settings.fxml"));

        try {
            Node node = fxmlLoader.load();

            MarkersListSettingsController controller = fxmlLoader.getController();
            controller.setController(settings);

            return node;
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
