package main.junctions.controllers.rigth_part.markers_lists;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.common.Summary;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
import main.core.utils.MyUtils;
import main.domain.CircleMarkersDomain;
import main.junctions.controllers.rigth_part.settings.CircleMarkersListSettingsController;
import main.junctions.data_types.LCMCubage;
import main.junctions.data_types.LCMCubageWrapper;
import main.junctions.views.cells.LCMCubage2Cubage;
import main.junctions.views.cells.LCMCubage2LogicalValueCmCell;
import main.junctions.views.cells.LCMCubage2VolumeStandardCell;
import main.truck.views.cells.T2IntegerCell;
import main.truck.views.cells.T2LogicalValueCmCell;

import java.io.IOException;
import java.util.*;

public class CircleMarkersListController {
    public VBox markersListHeader;
    public MarkersListHeaderController markersListHeaderController;

    public TableView<LCMCubageWrapper> circleMarkersTable;
    public TableColumn<LCMCubageWrapper, Number> index;
    public TableColumn<LCMCubageWrapper, LogicalValue> diameterColumn;
    public TableColumn<LCMCubageWrapper, LCMCubage> diameterStandardColumn;
    public TableColumn<LCMCubageWrapper, LCMCubage> cubageColumn;
    public TableColumn<LCMCubageWrapper, LCMCubage> volumeStandardColumn;

    public Label averageDiameterLabel;
    public Label averageStandardDiameterLabel;
    public Label totalStandardVolumeLabel;

    private ChangeListener<LogicalValue> adListener = (observable, oldValue, newValue) -> updateAverageDiameter(newValue);
    private ChangeListener<LogicalValue> adsListener = (observable, oldValue, newValue) -> updateAverageStandardDiameter(newValue);
    private ChangeListener<Number> tsvListener = (observable, oldValue, newValue) -> updateTotalStandardVolume(newValue.doubleValue());

    public void setController(CircleMarkersJunction circleMarkersJunction, CircleMarkerSettingsWrapper settings, CircleMarkersDomain domain){
        Summary results = circleMarkersJunction.getSummary();

        markersListHeaderController.setController(circleMarkersJunction);
        markersListHeaderController.settingsRoot.getChildren().add(loadSettings(settings));

        index.setCellValueFactory(param -> param.getValue().indexProperty().add(1));
        index.setCellFactory(param -> new T2IntegerCell<>());

        diameterColumn.setCellValueFactory(param -> param.getValue().getLogicalCircleMarker().logicalDiameterProperty());
        diameterColumn.setCellFactory(param -> new T2LogicalValueCmCell<>());
        diameterColumn.setComparator(Comparator.comparingDouble(LogicalValue::getValue));

        diameterStandardColumn.setCellValueFactory(param -> param.getValue().lcmCubageProperty());
        diameterStandardColumn.setCellFactory(param -> new LCMCubage2LogicalValueCmCell());
        diameterStandardColumn.setComparator(Comparator.comparingDouble(o -> o.getDiameterStandard().getValue()));

        cubageColumn.setCellValueFactory(param -> param.getValue().lcmCubageProperty());
        cubageColumn.setCellFactory(param -> new LCMCubage2Cubage());
        cubageColumn.setComparator(Comparator.comparingDouble(LCMCubage::getCubage));

        volumeStandardColumn.setCellValueFactory(param -> param.getValue().lcmCubageProperty());
        volumeStandardColumn.setCellFactory(param -> new LCMCubage2VolumeStandardCell());
        volumeStandardColumn.setComparator(Comparator.comparingDouble(LCMCubage::getVolumeStandard));

        circleMarkersTable.setItems(domain.getLcmCubageWrappers());

        updateAverageDiameter(results.getAverageDiameter());
        updateAverageStandardDiameter(results.getAverageDiameterStandard());
        updateTotalStandardVolume(results.getVolumeStandard());

        results.averageDiameterProperty().addListener(new WeakChangeListener<>(adListener));
        results.averageDiameterStandardProperty().addListener(new WeakChangeListener<>(adsListener));
        results.volumeStandardProperty().addListener(new WeakChangeListener<>(tsvListener));
    }

    private Node loadSettings(CircleMarkerSettingsWrapper settings) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/main/junctions/resources/fxml/right_part/markers_lists/settings/circle_markers_list_settings.fxml"));

        try {
            Node node = fxmlLoader.load();

            CircleMarkersListSettingsController controller = fxmlLoader.getController();
            controller.setController(settings);

            return node;
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


    public void updateAverageDiameter(LogicalValue averageDiameter) {
        averageDiameterLabel.setText(String.format(Locale.US, "Средний диаметр: %.1f см", averageDiameter.getRounded(1, LogicalValue.CENTIMETERS)));
    }

    private void updateAverageStandardDiameter(LogicalValue averageStandardDiameter) {
        averageStandardDiameterLabel.setText(String.format(Locale.US, "Средний диаметр по ГОСТу: %.1f см", averageStandardDiameter.getRounded(1, LogicalValue.CENTIMETERS)));
    }

    private void updateTotalStandardVolume(double volumeStandard) {
        totalStandardVolumeLabel.setText(String.format(Locale.US, "Всего объем по ГОСТу: %.3f м", MyUtils.round(volumeStandard,3)));
    }
}
