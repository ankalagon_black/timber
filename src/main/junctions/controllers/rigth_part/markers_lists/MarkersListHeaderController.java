package main.junctions.controllers.rigth_part.markers_lists;

import javafx.collections.ListChangeListener;
import javafx.collections.WeakListChangeListener;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.markers.Marker;

public class MarkersListHeaderController {
    public VBox root;
    public TextField junctionNumber;
    public Label markersCount;

    public VBox settingsRoot;

    private AbstractJunction abstractJunction;


    private ListChangeListener<Marker> markerListChangeListener = c -> {
        boolean structural = false;
        while (c.next()) {
            if (c.wasAdded()) {
                structural = true;
                break;
            } else if (c.wasRemoved()) {
                structural = true;
                break;
            }
        }

        if (structural) updateMarkersCount(abstractJunction.getMarkersCount());
    };
    private WeakListChangeListener<Marker> markerWeakListChangeListener = new WeakListChangeListener<>(markerListChangeListener);


    public void setController(AbstractJunction<? extends Marker> abstractJunction) {
        this.abstractJunction = abstractJunction;

        junctionNumber.setText(abstractJunction.getNumber());
        updateMarkersCount(abstractJunction.getMarkersCount());

        junctionNumber.textProperty().addListener((observable, oldValue, newValue) -> {
            abstractJunction.setNumber(newValue);
        });

        abstractJunction.getMarkers().addListener(markerWeakListChangeListener);
    }

    public void updateMarkersCount(int size) {
        markersCount.setText("Список маркеров (" + String.valueOf(size) + " штук)");
    }

    public void onClear(ActionEvent actionEvent) {
        abstractJunction.getMarkers().clear();
    }
}
