package main.junctions.controllers.rigth_part.markers_lists.cells;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import main.core.data_types.junctions.MarkersJunction;
import main.core.data_types.markers.Marker;

public class MarkerCellController {
    public Label name;

    private Marker marker;

    private MarkersJunction markersJunction;

    public void setController(MarkersJunction markersJunction) {
        this.markersJunction = markersJunction;
    }

    public void setMarker(Marker marker){
        this.marker = marker;
        name.setText(String.format("№%d", markersJunction.getMarkers().indexOf(marker) + 1));
    }

    public void reset() {
        if(marker != null) marker = null;
    }

    public void onDeleteAction(ActionEvent actionEvent) {
        markersJunction.getMarkers().remove(marker);
    }
}
