package main.junctions.controllers.dialogs;

import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import main.core.utils.MyUtils;
import main.core.views.ImageScrollPane;

import java.net.URL;
import java.util.ResourceBundle;

public class CropImageController implements Initializable {
    public static final double MARGIN = 0.525;

    public DialogPane dialogPane;

    public ImageScrollPane imageScrollPane;
    public Group group;
    public ImageView imageView;

    private Rectangle selection = new Rectangle();

    private boolean isInDrag = false;

    private double mouseAnchorX;
    private double mouseAnchorY;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Button cropButton = (Button) dialogPane.lookupButton(MyUtils.CROP);

        cropButton.setDisable(true);

        selection.widthProperty().addListener((observable, oldValue, newValue) -> {
            cropButton.setDisable(!(newValue.doubleValue() > 0.01));
        });
        selection.heightProperty().addListener((observable, oldValue, newValue) -> {
            cropButton.setDisable(!(newValue.doubleValue() > 0.01));
        });

        selection.setStroke(Color.BLUE);
        selection.setStrokeWidth(1);
        selection.setFill(Color.LIGHTBLUE.deriveColor(0, 1.2, 1, 0.6));
    }

    public void setController(Image image) {
        imageView.setImage(image);
    }

    public Rectangle getSelectionInImageCoordinates(){
        double xScale = imageView.getImage().getWidth() / imageView.getBoundsInLocal().getWidth(),
                yScale = imageView.getImage().getHeight() / imageView.getBoundsInLocal().getHeight();

        return new Rectangle(selection.getX() * xScale, selection.getY() * yScale,
                selection.getWidth() * xScale, selection.getHeight() * yScale);
    }


    public void onScroll(ScrollEvent scrollEvent) {
        group.getChildren().remove(selection);
        selection.setWidth(0);
        selection.setHeight(0);
        isInDrag = false;
    }

    public void onMousePressed(MouseEvent mouseEvent) {
        if(mouseEvent.isSecondaryButtonDown()) return;

        group.getChildren().remove(selection);

        //todo resolve problem with anchor

        isInDrag = true;
        mouseAnchorX = mouseEvent.getX();
        mouseAnchorY = mouseEvent.getY();

        selection.setX(mouseAnchorX);
        selection.setY(mouseAnchorY);
        selection.setWidth(0);
        selection.setHeight(0);

        group.getChildren().add(selection);
    }

    public void onMouseDragged(MouseEvent mouseEvent) {
        if (mouseEvent.isSecondaryButtonDown() || !isInDrag) return;

        if (mouseEvent.getX() > mouseAnchorX) {
            selection.setX(mouseAnchorX);
            selection.setWidth(Math.min(imageView.getBoundsInLocal().getMaxX() - MARGIN, mouseEvent.getX()) - mouseAnchorX);
        }
        else {
            double max = Math.max(mouseEvent.getX(), imageView.getBoundsInLocal().getMinX() + MARGIN);
            selection.setX(max);
            selection.setWidth(mouseAnchorX - max);
        }

        if (mouseEvent.getY() > mouseAnchorY) {
            selection.setY(mouseAnchorY);
            selection.setHeight(Math.min(imageView.getBoundsInLocal().getMaxY() - MARGIN, mouseEvent.getY()) - mouseAnchorY);
        }
        else {
            double max = Math.max(mouseEvent.getY(), imageView.getBoundsInLocal().getMinY() + MARGIN);
            selection.setY(max);
            selection.setHeight(mouseAnchorY - max);
        }
    }

    public void onMouseReleased(MouseEvent mouseEvent) {
        if(mouseEvent.isSecondaryButtonDown()) return;
        isInDrag = false;
    }
}
