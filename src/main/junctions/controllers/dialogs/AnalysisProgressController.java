package main.junctions.controllers.dialogs;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.WeakEventHandler;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import main.core.utils.MyUtils;

public class AnalysisProgressController {

    public DialogPane root;
    public ProgressBar progressBar;
    public HBox errorRoot;
    public Label errorDescription;
    public Label label;

    private Disposable disposable = null;

    private EventHandler<ActionEvent> onClose = event -> {
        if(disposable != null) disposable.dispose();
    };

    public void setController(Completable completable) {
        disposable = completable.subscribe(this::changeButtonToClose, throwable -> {
            disposable = null;
            errorRoot.setVisible(true);
            errorDescription.setText(String.format("Ошибка: %s", throwable.getMessage()));

            changeButtonToClose();
        });

        root.lookupButton(MyUtils.CANCEL).addEventFilter(ActionEvent.ACTION, new WeakEventHandler<>(onClose));
    }

    private void changeButtonToClose() {
        label.setText("Готово");

        progressBar.setVisible(false);

        root.getButtonTypes().clear();

        root.getButtonTypes().add(MyUtils.CLOSE);
        root.lookupButton(MyUtils.CLOSE).addEventFilter(ActionEvent.ACTION, new WeakEventHandler<>(onClose));
    }
}
