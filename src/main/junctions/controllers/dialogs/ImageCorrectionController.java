package main.junctions.controllers.dialogs;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import main.core.views.ImageScrollPane;

import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.net.URL;
import java.util.ResourceBundle;

public class ImageCorrectionController implements Initializable {

    public ImageScrollPane imageScrollPane;
    public ImageView imageView;

    public Slider saturationSlider;
    public Slider hueSlider;
    public Slider brightnessSlider;
    public Slider contrastSlider;
    public Slider sharpnessSlider;

    private ColorAdjust colorAdjust = new ColorAdjust();

    private Image initialImage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        saturationSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            colorAdjust.setSaturation(newValue.doubleValue());
        });
        hueSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            colorAdjust.setHue(newValue.doubleValue());
        });
        brightnessSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            colorAdjust.setBrightness(newValue.doubleValue());
        });
        contrastSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            colorAdjust.setContrast(newValue.doubleValue());
        });
    }

    public void setController(Image image){
        initialImage = image;

        imageView.setImage(initialImage);
        imageView.setEffect(colorAdjust);
    }

    public Image getImage(){
        imageView.setFitWidth(imageView.getImage().getWidth());
        imageView.setFitHeight(imageView.getImage().getHeight());
        return imageView.snapshot(null, null);
    }

    public void onApplySharpness(ActionEvent actionEvent) {
        float sharpness = (float) sharpnessSlider.getValue();

        Kernel kernel = new Kernel(3, 3,
                new float[]
                        { 0, -1 * sharpness, 0,
                        -1 * sharpness, 9 * sharpness, -1 * sharpness,
                        0, -1 * sharpness, 0 });

        BufferedImage bufferedImage = SwingFXUtils.fromFXImage(initialImage, null);
        bufferedImage = new ConvolveOp(kernel).filter(bufferedImage, null);

        imageView.setImage(SwingFXUtils.toFXImage(bufferedImage, null));
    }

    public void onRestoreImage(ActionEvent actionEvent) {
        imageView.setImage(initialImage);

        saturationSlider.setValue(0);
        hueSlider.setValue(0);
        brightnessSlider.setValue(0);
        contrastSlider.setValue(0);
    }
}
