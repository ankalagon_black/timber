package main.junctions.controllers.bar;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import main.algorithms.WoodLogsDetector;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.LogParameters;
import main.junctions.dialogs.AnalysisProgressDialog;
import main.repository.loaders.CascadeLoader;
import org.opencv.core.Rect;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class MarkersJunctionBarController extends JunctionBarController {

    public void onDetectMarkers(ActionEvent actionEvent) {
        Completable completable = Completable.create(completableEmitter -> {
            System.load(new File("./libraries/opencv_java341.dll").getAbsolutePath());

            WoodLogsDetector detector = null;
            try {
                detector = new WoodLogsDetector(imageView.getImage());
            } catch (UnsatisfiedLinkError e) {
                e.printStackTrace();
            }
            CascadeLoader cascadeLoader = new CascadeLoader();

            LogicalRuler ruler = abstractJunction.getRuler();
            LogParameters logParameters = project.getSetting().getLogParameters();

            double min = logParameters.getMinDiameterInPixels(ruler),
                    max = logParameters.getMaxDiameterInPixels(ruler);

            if(abstractJunction.hasLogicalContour()) {
                List<Rect> rectList = detector.performInitialClassification(cascadeLoader.getCascade(), min, max, abstractJunction.getLogicalContour());

                if(!completableEmitter.isDisposed()) {
                    List<Marker> markers = rectList.stream().collect(ArrayList::new, (res, rect) -> {
                        double x = rect.x + rect.width / 2, y = rect.y + rect.height / 2;
                        res.add(new Marker(x, y));
                    }, ArrayList::addAll);

                    Platform.runLater(() -> {
                        abstractJunction.getMarkers().clear();
                        abstractJunction.getMarkers().addAll(markers);
                    });
                }
            }
            else {
                WoodLogsDetector.ICResults results = detector.performInitialClassification(cascadeLoader.getCascade(), min, max);

                if(!completableEmitter.isDisposed()) {
                    List<Marker> markers = results.rois.stream().collect(ArrayList::new, (res, rect) -> {
                        double x = rect.x + rect.width / 2, y = rect.y + rect.height / 2;
                        res.add(new Marker(x, y));
                    }, ArrayList::addAll);

                    Platform.runLater(() -> {
                        abstractJunction.setLogicalContour(results.logicalContour);
                        abstractJunction.getMarkers().clear();
                        abstractJunction.getMarkers().addAll(markers);
                    });
                }
            }

            completableEmitter.onComplete();
        }).subscribeOn(Schedulers.computation()).observeOn(JavaFxScheduler.platform());

        try {
            AnalysisProgressDialog dialog = new AnalysisProgressDialog(completable, "Расстановка маркеров");
            dialog.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
