package main.junctions.controllers.bar;

import io.reactivex.*;
import io.reactivex.functions.Action;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import main.algorithms.WoodLogsDetector;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.LogParameters;
import main.core.data_types.settings.Settings;
import main.junctions.dialogs.AnalysisProgressDialog;
import main.repository.loaders.CascadeLoader;
import org.opencv.core.Rect;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class CircleMarkersJunctionBarController extends JunctionBarController {

    public void onDetectCircleMarkers(ActionEvent actionEvent) {
        Completable completable = Completable.create(completableEmitter -> {
            System.load(new File("./libraries/opencv_java341.dll").getAbsolutePath());

            WoodLogsDetector detector = null;
            try {
                detector = new WoodLogsDetector(imageView.getImage());
            }
            catch (UnsatisfiedLinkError e) {
                e.printStackTrace();
                System.out.println(e.getMessage() + " path: " + new File("./libraries/opencv_java341.dll").getAbsolutePath());
            }
            CascadeLoader cascadeLoader = new CascadeLoader();

            LogicalRuler ruler = abstractJunction.getRuler();
            LogParameters logParameters = project.getSetting().getLogParameters();

            double min = logParameters.getMinDiameterInPixels(ruler),
                    max = logParameters.getMaxDiameterInPixels(ruler);

            if(abstractJunction.hasLogicalContour()) {
                List<Rect> rectList = detector.performInitialClassification(cascadeLoader.getCascade(), min, max, abstractJunction.getLogicalContour());

                if(!completableEmitter.isDisposed()) {
                    List<LogicalCircleMarker> markers = rectList.stream().collect(ArrayList::new, (logicalCircleMarkers, rect) -> {
                        double x = rect.x + rect.width / 2, y = rect.y + rect.height / 2;
                        double r = rect.width / 2;
                        LogicalValue ld = ruler.getLogicalPerPixel().multiply(rect.width);
                        logicalCircleMarkers.add(new LogicalCircleMarker(x, y, r, ld));
                    }, ArrayList::addAll);

                    Platform.runLater(() -> {
                        abstractJunction.getMarkers().clear();
                        abstractJunction.getMarkers().addAll(markers);
                    });
                }
            }
            else {
                WoodLogsDetector.ICResults results = detector.performInitialClassification(cascadeLoader.getCascade(), min, max);

                if(!completableEmitter.isDisposed()) {
                    List<LogicalCircleMarker> markers = results.rois.stream().collect(ArrayList::new, (logicalCircleMarkers, rect) -> {
                        double x = rect.x + rect.width / 2, y = rect.y + rect.height / 2;
                        double r = rect.width / 2;
                        LogicalValue ld = ruler.getLogicalPerPixel().multiply(rect.width);
                        logicalCircleMarkers.add(new LogicalCircleMarker(x, y, r, ld));
                    }, ArrayList::addAll);

                    Platform.runLater(() -> {
                        abstractJunction.setLogicalContour(results.logicalContour);
                        abstractJunction.getMarkers().clear();
                        abstractJunction.getMarkers().addAll(markers);
                    });
                }
            }

            completableEmitter.onComplete();
        }).subscribeOn(Schedulers.computation()).observeOn(JavaFxScheduler.platform());

        try {
            AnalysisProgressDialog dialog = new AnalysisProgressDialog(completable, "Расстановка маркеров");
            dialog.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onFindCircleMarkersRadius(ActionEvent actionEvent) {
        Completable completable = Completable.create(completableEmitter -> {
            System.load(new File("./libraries/opencv_java341.dll").getAbsolutePath());

            WoodLogsDetector detector = null;
            try {
                detector = new WoodLogsDetector(imageView.getImage());
            }
            catch (UnsatisfiedLinkError e) {
                e.printStackTrace();
            }
            CascadeLoader cascadeLoader = new CascadeLoader();

            LogicalRuler ruler = abstractJunction.getRuler();
            LogParameters logParameters = project.getSetting().getLogParameters();

            double min = logParameters.getMinDiameterInPixels(ruler),
                    max = logParameters.getMaxDiameterInPixels(ruler);

            AbstractJunction<LogicalCircleMarker> lcm = (AbstractJunction<LogicalCircleMarker>) abstractJunction;

            for (LogicalCircleMarker i: lcm.getMarkers()) {
                Rect rect = detector.detectInArea(cascadeLoader.getCascade(), i, min, max);
                if(rect != null) Platform.runLater(() -> i.setRadius(rect.width / 2));
            }

            completableEmitter.onComplete();
        }).subscribeOn(Schedulers.computation()).observeOn(JavaFxScheduler.platform());

        try {
            AnalysisProgressDialog dialog = new AnalysisProgressDialog(completable, "Определение радиусов маркеров");
            dialog.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onFindLesserCircleMarkersRadius(ActionEvent actionEvent) {
        Completable completable = Completable.create(completableEmitter -> {
            System.load(new File("./libraries/opencv_java341.dll").getAbsolutePath());

            WoodLogsDetector detector = null;
            try {
                detector = new WoodLogsDetector(imageView.getImage());
            }
            catch (UnsatisfiedLinkError e) {
                e.printStackTrace();
            }
            CascadeLoader cascadeLoader = new CascadeLoader();

            LogicalRuler ruler = abstractJunction.getRuler();
            LogParameters logParameters = project.getSetting().getLogParameters();

            double min = logParameters.getMinDiameterInPixels(ruler),
                    max = logParameters.getMaxDiameterInPixels(ruler);

            AbstractJunction<LogicalCircleMarker> lcm = (AbstractJunction<LogicalCircleMarker>) abstractJunction;

            for (LogicalCircleMarker i: lcm.getMarkers()) {
                if(i.getLogicalDiameter().getValue() < project.getSetting().getLogParameters().getMinDiameter().getValue() + 0.015) {
                    Rect rect = detector.detectInArea(cascadeLoader.getCascade(), i, min, max);
                    if (rect != null) Platform.runLater(() -> i.setRadius(rect.width / 2));
                }
            }

            completableEmitter.onComplete();
        }).subscribeOn(Schedulers.computation()).observeOn(JavaFxScheduler.platform());

        try {
            AnalysisProgressDialog dialog = new AnalysisProgressDialog(completable, "Определение радиусов маркеров");
            dialog.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
