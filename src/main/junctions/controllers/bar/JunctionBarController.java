package main.junctions.controllers.bar;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import main.common.dialogs.OutputJunctionDialog;
import main.common.dialogs.ShowImageDialog;
import main.core.data_types.common.Project;
import main.core.data_types.junctions.AbstractJunction;
import main.junctions.dialogs.CropImageDialog;
import main.junctions.dialogs.ImageCorrectionDialog;
import main.repository.image.ImageCache;
import main.repository.image.ModifiedImageManager;
import main.truck.interfaces.JunctionBackable;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class JunctionBarController {
    public MenuBar menuBar;

    public Menu fileMenu;
    public Menu data_output;

    public HBox iconsBar;
    public Menu auto_detection;

    private boolean isModified = false;
    private JunctionBackable junctionBackable;

    protected ImageView imageView;

    protected AbstractJunction abstractJunction;
    protected Project project;

    public void setController(JunctionBackable junctionBackable, ImageView imageView, AbstractJunction abstractJunction, Project project) {
        this.junctionBackable = junctionBackable;

        this.imageView = imageView;

        this.abstractJunction = abstractJunction;
        this.project = project;
    }

    public void onOutputJunctionData(ActionEvent actionEvent) {
        if(abstractJunction.hasNumber()){
            try {
                OutputJunctionDialog outputJunctionDialog = new OutputJunctionDialog(abstractJunction, project, imageView.getImage());
                outputJunctionDialog.showAndWait();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Номер стыка и номер воза должены быть заполнены перед выводом данных.");
            alert.show();
        }
    }

    public void onShowSourceImage(ActionEvent actionEvent) {
        try {
            ShowImageDialog showImageDialog = new ShowImageDialog(abstractJunction.getOriginalFile());
            showImageDialog.showAndWait();
        } catch (IOException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void onResetToSourceImage(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.WARNING, "Вы действительно хотите вернуться к исходной версии изображения? Данное действие также удалит все маркеры.",
                new ButtonType("Да", ButtonBar.ButtonData.OK_DONE), new ButtonType("Нет", ButtonBar.ButtonData.CANCEL_CLOSE));
        alert.setTitle("Подтверждение действия");
        alert.setHeaderText(null);
        alert.showAndWait().ifPresent(buttonType -> {
            if(buttonType.getButtonData().equals(ButtonBar.ButtonData.OK_DONE)){
                abstractJunction.removeMarkers();

                isModified = false;

                abstractJunction.setModifiedFile(null);

                try {
                    Image image = ImageCache.get().get(abstractJunction.getOriginalFile().getPath());
                    abstractJunction.getRuler().setInitialized(false);
                    imageView.setImage(image);
                }
                catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void onCrop(ActionEvent actionEvent) {
        try{
            CropImageDialog cropImageDialog = new CropImageDialog(imageView.getImage());
            cropImageDialog.showAndWait().ifPresent(image -> {
                abstractJunction.removeMarkers();

                abstractJunction.getRuler().setInitialized(false);
                imageView.setImage(image);

                isModified = true;
            });
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public void onCorrectImage(ActionEvent actionEvent) {
        try{
            ImageCorrectionDialog imageCorrectionDialog = new ImageCorrectionDialog(imageView.getImage());
            imageCorrectionDialog.showAndWait().ifPresent(image -> {
                imageView.setImage(image);

                isModified = true;
            });
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public void onBack(ActionEvent actionEvent) {
        if(abstractJunction.hasNumber()) {

            if (isModified) abstractJunction.setModifiedFile(new ModifiedImageManager().createModifiedFile(project, abstractJunction.getOriginalFile(), SwingFXUtils.fromFXImage(imageView.getImage(), null)));

            junctionBackable.backFromJunction(abstractJunction);
        }
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Номер стыка должен быть заполнен перед выходом.");
            alert.show();
        }
    }
}
