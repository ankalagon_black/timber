package main.junctions.views.cells;

import javafx.scene.control.ListCell;
import main.core.data_types.common.Breed;

public class BreedCell extends ListCell<Breed> {

    @Override
    protected void updateItem(Breed item, boolean empty) {
        super.updateItem(item, empty);

        if(empty) setText(null);
        else setText(item.getText());
    }
}
