package main.junctions.views.cells;

import javafx.scene.control.TableCell;
import main.junctions.data_types.LCMCubage;
import main.junctions.data_types.LCMCubageWrapper;

import java.util.Locale;

public class LCMCubage2LogicalValueCmCell extends TableCell<LCMCubageWrapper, LCMCubage> {
    @Override
    protected void updateItem(LCMCubage item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) setText(null);
        else setText(String.format(Locale.US, "%.1f", item.getDiameterStandard().getValueInCm()));
    }
}
