package main.junctions.views.cells;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import main.core.data_types.junctions.MarkersJunction;
import main.core.data_types.markers.Marker;
import main.junctions.controllers.rigth_part.markers_lists.cells.MarkerCellController;

import java.io.IOException;

public class MarkerCell extends ListCell<Marker> {
    private Node node;
    private MarkerCellController controller;

    public MarkerCell(MarkersJunction junction) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/main/junctions/resources/fxml/right_part/markers_lists/cells/marker_cell.fxml"));

        node = loader.load();
        controller = loader.getController();
        controller.setController(junction);
    }

    @Override
    protected void updateItem(Marker item, boolean empty) {
        super.updateItem(item, empty);
        if(empty || item == null){
            controller.reset();
            setGraphic(null);
        }
        else {
            controller.setMarker(item);
            setGraphic(node);
        }
    }
}
