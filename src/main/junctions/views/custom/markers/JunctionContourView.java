package main.junctions.views.custom.markers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.scene.control.Skin;
import main.core.data_types.junctions.AbstractJunction;
import main.domain.marker_wrappers.junction.JunctionContourWrapper;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.contour_branch.Contour;
import main.core.views.MarkerImageView;
import main.core.views.marker_views.AbstractContourView;
import main.junctions.views.custom.markers.skins.JunctionContourViewSkin;

public class JunctionContourView extends AbstractContourView<JunctionContourWrapper> {
    private AbstractJunction abstractJunction;

    private ChangeListener<Boolean> changeListener = (observable, oldValue, newValue) -> {
        setEnabled(newValue);
    };

    public JunctionContourView(JunctionContourWrapper wrapper, MarkerImageView markerImageView, AbstractJunction abstractJunction) {
        super(wrapper, markerImageView);
        this.abstractJunction = abstractJunction;

        setEnabled(wrapper.getWrapper().isEnabled());

        wrapper.getWrapper().enabledProperty().addListener(new WeakChangeListener<>(changeListener));
    }

    private void setEnabled(boolean enabled) {
        if(enabled) toFront();
        else toBack();
    }

    @Override
    public Marker getMarker() {
        return getContour();
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new JunctionContourViewSkin(this);
    }

    @Override
    public Contour getContour() {
        return getContent().get().getLogicalContour();
    }

    public AbstractJunction getAbstractJunction() {
        return abstractJunction;
    }
}
