package main.junctions.views.custom.markers.behaviors;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import main.core.views.behaviors.marker_views.ContourViewBehavior;
import main.junctions.views.custom.markers.JunctionContourView;
import main.junctions.views.custom.markers.skins.JunctionContourViewSkin;

import java.util.ArrayList;

public class JunctionContourViewBehavior extends ContourViewBehavior<JunctionContourView> {

    private final EventHandler<MouseEvent> mouseMovedHandler = event -> {
        if(!isInMove() && !isInResize() && getSkin().isInResizeRange(event)) {
            getSkin().onMouseMovedInResizeRange(event);
            return;
        }

        getSkin().onMouseMovedOutsideResizeRange(event);
    };

    private final EventHandler<MouseEvent> mouseExitedHandler = event -> {
        getSkin().onMouseMovedOutsideResizeRange(event);
    };

    private final EventHandler<MouseEvent> mouseClicked = event -> {
        getSkin().hideContextMenu();
        boolean isInResizeRange = getSkin().isInResizeRange(event);
        if(event.getButton().equals(MouseButton.SECONDARY) &&
                !isInResize() && !isInMove() &&
                (getSkin().isInMovementRange(event) || isInResizeRange)) {
            getSkin().onSecondaryButtonClicked(event, isInResizeRange);
            event.consume();
        }
    };

    public JunctionContourViewBehavior(JunctionContourView control) {
        super(control, new ArrayList<>());
        control.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedHandler);
        control.addEventHandler(MouseEvent.MOUSE_EXITED, mouseExitedHandler);
        control.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseClicked);
    }

    @Override
    public void dispose() {
        super.dispose();
        getControl().removeEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedHandler);
        getControl().removeEventHandler(MouseEvent.MOUSE_EXITED, mouseExitedHandler);
        getControl().removeEventHandler(MouseEvent.MOUSE_RELEASED, mouseClicked);
    }

    private JunctionContourViewSkin getSkin() {
        return ((JunctionContourViewSkin) getControl().getSkin());
    }
}
