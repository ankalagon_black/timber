package main.junctions.views.custom.markers.behaviors;

import com.sun.javafx.scene.control.behavior.KeyBinding;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import main.core.views.behaviors.marker_views.CircleMarkerViewBehavior;
import main.junctions.views.custom.markers.JunctionCircleMarkerView;
import main.junctions.views.custom.markers.skins.JunctionCircleMarkerViewSkin;

import java.util.ArrayList;
import java.util.List;

import static javafx.scene.input.KeyCode.*;

public class JunctionCircleMarkerViewBehavior extends CircleMarkerViewBehavior<JunctionCircleMarkerView> {

    protected static final List<KeyBinding> bindings = new ArrayList<>();

    private static final String INCREASE_RADIUS = "INCREASE_RADIUS";
    private static final String DECREASE_RADIUS = "DECREASE_RADIUS";
    private static final String FIND_RADIUS = "FIND_RADIUS";

    static {
        bindings.add(new KeyBinding(PLUS, INCREASE_RADIUS));
        bindings.add(new KeyBinding(EQUALS, INCREASE_RADIUS));
        bindings.add(new KeyBinding(ADD, INCREASE_RADIUS));

        bindings.add(new KeyBinding(MINUS, DECREASE_RADIUS));
        bindings.add(new KeyBinding(SUBTRACT, DECREASE_RADIUS));

        bindings.add(new KeyBinding(KeyCode.MULTIPLY, FIND_RADIUS));
    }

    private final EventHandler<MouseEvent> mouseMovedHandler = event -> {
        if(!isInMove() && !isInResize() && getSkin().isInMovementRange(event)) {
            if(getSkin().isInResizeRange(event)) {
                getSkin().onMouseMovedInResizeRange(event);
                return;
            }
        }

        getSkin().onMouseMovedOutsideMovementRange(event);
    };

    public JunctionCircleMarkerViewBehavior(JunctionCircleMarkerView control) {
        super(control, bindings);
        control.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedHandler);
    }

    @Override
    public void dispose() {
        super.dispose();
        getControl().removeEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedHandler);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);
        getSkin().hideContextMenu();

        if(!e.isConsumed()) {
            if (e.getButton().equals(MouseButton.PRIMARY)) {
                getSkin().onPrimaryButtonClicked(e);
                e.consume();
            } else if (e.getButton().equals(MouseButton.SECONDARY)) {
                getSkin().onSecondaryButtonClicked(e);
                e.consume();
            }
        }
    }

    @Override
    protected void callAction(String name) {
        super.callAction(name);
        if(getControl().isSelected()) {
            switch (name) {
                case INCREASE_RADIUS:
                    getSkin().resize(2);
                    break;
                case DECREASE_RADIUS:
                    getSkin().resize(-2);
                    break;
                case FIND_RADIUS:
                    getControl().findRadius();
                    break;
            }
        }
    }

    private JunctionCircleMarkerViewSkin getSkin() {
        return ((JunctionCircleMarkerViewSkin) getControl().getSkin());
    }
}
