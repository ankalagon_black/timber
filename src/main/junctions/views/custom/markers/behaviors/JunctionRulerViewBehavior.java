package main.junctions.views.custom.markers.behaviors;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import main.core.views.behaviors.marker_views.RulerViewBehavior;
import main.junctions.views.custom.markers.JunctionRulerView;
import main.junctions.views.custom.markers.skins.JunctionRulerViewSkin;

import java.util.ArrayList;

public class JunctionRulerViewBehavior extends RulerViewBehavior<JunctionRulerView> {

    private final EventHandler<MouseEvent> mouseMovedHandler = event -> {
        if(!isInMove() && !isInResize() && getSkin().isInResizeRange(event)) {
            getSkin().onMouseMovedInResizeRange(event);
            return;
        }

        getSkin().onMouseMovedOutsideResizeRange(event);
    };

    public JunctionRulerViewBehavior(JunctionRulerView control) {
        super(control, new ArrayList<>());
        control.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedHandler);
    }

    @Override
    public void dispose() {
        super.dispose();
        getControl().removeEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedHandler);
    }

    private JunctionRulerViewSkin getSkin() {
        return ((JunctionRulerViewSkin) getControl().getSkin());
    }
}
