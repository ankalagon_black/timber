package main.junctions.views.custom.markers.behaviors;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import main.core.views.behaviors.marker_views.MarkerViewBehavior;
import main.junctions.views.custom.markers.JunctionMarkerView;
import main.junctions.views.custom.markers.skins.JunctionMarkerViewSkin;

import java.util.ArrayList;

public class JunctionMarkerViewBehavior extends MarkerViewBehavior<JunctionMarkerView> {

    private EventHandler<MouseEvent> eventHandler = event -> {
        getSkin().hideContextMenu();

        if(event.getButton().equals(MouseButton.SECONDARY)) {
            getSkin().onSecondaryButtonClicked(event);
            event.consume();
        }
    };

    public JunctionMarkerViewBehavior(JunctionMarkerView control) {
        super(control, new ArrayList<>());
        control.addEventHandler(MouseEvent.MOUSE_RELEASED, eventHandler);
    }

    @Override
    public void dispose() {
        super.dispose();
        getControl().removeEventHandler(MouseEvent.MOUSE_RELEASED, eventHandler);
    }

    private JunctionMarkerViewSkin getSkin() {
        return ((JunctionMarkerViewSkin) getControl().getSkin());
    }
}
