package main.junctions.views.custom.markers.skins;

import javafx.scene.Cursor;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import main.domain.marker_settings_wrappers.MarkerSettingsWrapper;
import main.core.views.skins.marker_views.MarkerViewSkin;
import main.junctions.views.custom.markers.JunctionMarkerView;
import main.junctions.views.custom.markers.behaviors.JunctionMarkerViewBehavior;

import java.util.Locale;

public class JunctionMarkerViewSkin extends MarkerViewSkin<JunctionMarkerView, JunctionMarkerViewBehavior> {
    private final String INDEX  = "INDEX";
    private final String FONT_SIZE  = "FONT_SIZE";
    private final String TEXT_COLOR = "TEXT_COLOR";
    private final String ENABLED = "ENABLED";

    private ContextMenu contextMenu = null;

    public JunctionMarkerViewSkin(JunctionMarkerView control) {
        super(control, new JunctionMarkerViewBehavior(control));

        getLabel().setCursor(Cursor.OPEN_HAND);

        MarkerSettingsWrapper junctionMarkerSettings = getSkinnable().getContent().getMarkerSettingsWrapper();

        setText(getSkinnable().getContent().getIndex());

        setFontSize(junctionMarkerSettings.getMarkerSettings().getFontSize());
        setTextColor(junctionMarkerSettings.getMarkerSettings().getTextColor());
        setEnabled(junctionMarkerSettings.isEnabled());

        registerChangeListener(getSkinnable().getContent().indexProperty(), INDEX);

        registerChangeListener(junctionMarkerSettings.getMarkerSettings().fontSizeProperty(), FONT_SIZE);
        registerChangeListener(junctionMarkerSettings.getMarkerSettings().textColorProperty(), TEXT_COLOR);
        registerChangeListener(junctionMarkerSettings.enabledProperty(), ENABLED);
    }

    @Override
    protected void handleControlPropertyChanged(String propertyReference) {
        super.handleControlPropertyChanged(propertyReference);
        switch (propertyReference) {
            case INDEX:
                setText(getSkinnable().getContent().getIndex());
                break;
            case FONT_SIZE:
                setFontSize(getSkinnable().getContent().getMarkerSettingsWrapper().getMarkerSettings().getFontSize());
                break;
            case TEXT_COLOR:
                setTextColor(getSkinnable().getContent().getMarkerSettingsWrapper().getMarkerSettings().getTextColor());
                break;
            case ENABLED:
                setEnabled(getSkinnable().getContent().getMarkerSettingsWrapper().isEnabled());
                break;
        }
    }


    private void setText(int index) {
        getLabel().setText(String.format(Locale.US, "%d", index + 1));
    }

    private void setFontSize(int fontSize) {
        getLabel().setFont(new Font("System Bold", fontSize));
    }

    private void setTextColor(Color color) {
        getLabel().setTextFill(color);
    }

    private void setEnabled(boolean enabled) {
        getSkinnable().setDisable(!enabled);
        getLabel().setStyle("-fx-opacity: 1.0");
    }


    public void hideContextMenu() {
        if(this.contextMenu != null) contextMenu.hide();
    }

    public void onSecondaryButtonClicked(MouseEvent mouseEvent) {
        MenuItem remove = new MenuItem("Удалить");
        remove.setOnAction(event -> {
            getSkinnable().getJunction().getMarkers().remove(getSkinnable().getContent().getMarker());
        });

        ContextMenu contextMenu = new ContextMenu(remove);

        contextMenu.show(getSkinnable(), mouseEvent.getScreenX(), mouseEvent.getScreenY());
        this.contextMenu = contextMenu;
    }
}
