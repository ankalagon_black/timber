package main.junctions.views.custom.markers.skins;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import main.core.data_types.common.Summary;
import main.core.data_types.markers.contour_branch.LogicalContour;
import main.domain.marker_settings_wrappers.ContourSettingsWrapper;
import main.core.data_types.settings.markers_settings.ContourSettings;
import main.core.views.skins.marker_views.ContourViewSkin;
import main.junctions.views.custom.markers.behaviors.JunctionContourViewBehavior;
import main.junctions.views.custom.markers.JunctionContourView;

import java.util.Locale;

public class JunctionContourViewSkin extends ContourViewSkin<JunctionContourView, JunctionContourViewBehavior> {
    private final String LOGICAL_AREA = "LOGICAL_AREA";
    private final String VOLUME = "VOLUME";
    private final String FONT_SIZE  = "FONT_SIZE";
    private final String TEXT_COLOR = "TEXT_COLOR";
    private final String COLOR = "COLOR";
    private final String HIGHLIGHT_INNER_AREA = "HIGHLIGHT_INNER_AREA";
    private final String ENABLED = "ENABLED";

    private ContextMenu contextMenu = null;

    public JunctionContourViewSkin(JunctionContourView control) {
        super(control, new JunctionContourViewBehavior(control));

        for (Circle circle: getCircles()) setCircle(circle);

        LogicalContour logicalContour = getSkinnable().getContent().get().getLogicalContour();
        ContourSettingsWrapper wrapper = getSkinnable().getContent().getWrapper();
        ContourSettings settings = wrapper.get();

        Summary results = getSkinnable().getContent().getResults();

        setText(logicalContour.getLogicalArea(), results.getGeometryVolume());

        setFontSize(settings.getFontSize());
        setTextColor(settings.getTextColor());
        setColor(settings.getColor());
        setHighLightInnerArea(wrapper.isHighlightInnerAreaProxy());
        setEnabled(wrapper.isEnabled());

        registerChangeListener(logicalContour.logicalAreaProperty(), LOGICAL_AREA);
        registerChangeListener(results.geometryVolumeProperty(), VOLUME);

        registerChangeListener(wrapper.get().fontSizeProperty(), FONT_SIZE);
        registerChangeListener(wrapper.get().textColorProperty(), TEXT_COLOR);
        registerChangeListener(wrapper.get().colorProperty(), COLOR);
        registerChangeListener(wrapper.highlightInnerAreaProxyProperty(), HIGHLIGHT_INNER_AREA);
        registerChangeListener(wrapper.enabledProperty(), ENABLED);
    }


    @Override
    protected void handleControlPropertyChanged(String propertyReference) {
        super.handleControlPropertyChanged(propertyReference);
        switch (propertyReference) {
            case VOLUME:
            case LOGICAL_AREA:
                setText(getSkinnable().getContent().get().getLogicalContour().getLogicalArea(), getSkinnable().getContent().getResults().getGeometryVolume());
                break;
            case FONT_SIZE:
                setFontSize(getSkinnable().getContent().getWrapper().get().getFontSize());
                break;
            case TEXT_COLOR:
                setTextColor(getSkinnable().getContent().getWrapper().get().getTextColor());
                break;
            case COLOR:
                setColor(getSkinnable().getContent().getWrapper().get().getColor());
                setHighLightInnerArea(getSkinnable().getContent().getWrapper().isHighlightInnerAreaProxy());
                break;
            case HIGHLIGHT_INNER_AREA:
                setHighLightInnerArea(getSkinnable().getContent().getWrapper().isHighlightInnerAreaProxy());
                break;
            case ENABLED:
                setEnabled(getSkinnable().getContent().getWrapper().isEnabled());
                break;
        }
    }

    @Override
    protected void setCircle(Circle circle) {
        super.setCircle(circle);
        circle.setFill(Color.TRANSPARENT);
        circle.setRadius(7);
        circle.setStrokeWidth(2);
        circle.setStroke(getSkinnable().getContent().getWrapper().get().getColor());
    }

    private void setText(double logicalArea, double volume) {
        getLabel().setText(String.format(Locale.US, "%.3f м2/ %.3f м3", logicalArea, volume));
    }

    private void setFontSize(int fontSize) {
        getLabel().setFont(new Font("System Bold", fontSize));
    }

    private void setTextColor(Color textColor) {
        getLabel().setTextFill(textColor);
    }

    private void setColor(Color color) {
        for (Circle circle: getCircles()) {
            circle.setStroke(color);
        }

        getPolygon().setStroke(color);
    }

    private void setHighLightInnerArea(boolean highLightInnerArea) {
        if(highLightInnerArea) {
            Color color = getSkinnable().getContent().getWrapper().get().getColor();
            getPolygon().setFill(Color.rgb((int ) (color.getRed() * 255), (int ) (color.getGreen() * 255), (int ) (color.getBlue() * 255), 0.5f));
        }
        else {
            getPolygon().setFill(null);
        }
    }

    private void setEnabled(boolean enabled) {
        getSkinnable().setDisable(!enabled);
        getLabel().setStyle("-fx-opacity: 1.0");
    }


    public void hideContextMenu() {
        if(this.contextMenu != null) contextMenu.hide();
    }

    public void onSecondaryButtonClicked(MouseEvent mouseEvent, boolean isInResizeRange) {
        if(isInResizeRange) {
            MenuItem removePoint = new MenuItem("Удалить точку");
            removePoint.setOnAction(event -> {
                if(getSkinnable().getAbstractJunction().getLogicalContour().getPoints().size() == 1) {
                    getSkinnable().getAbstractJunction().setLogicalContour(null);
                }
                else {
                    int index = getPointerIndex(mouseEvent);
                    getSkinnable().getAbstractJunction().getLogicalContour().getPoints().remove(index);
                }
            });

            ContextMenu contextMenu = new ContextMenu(removePoint);

            contextMenu.show(getSkinnable(), mouseEvent.getScreenX(), mouseEvent.getScreenY());
            this.contextMenu = contextMenu;
        }
        else {
            MenuItem remove = new MenuItem("Удалить");
            remove.setOnAction(event -> {
                getSkinnable().getAbstractJunction().setLogicalContour(null);
            });

            ContextMenu contextMenu = new ContextMenu(remove);

            contextMenu.show(getSkinnable(), mouseEvent.getScreenX(), mouseEvent.getScreenY());
            this.contextMenu = contextMenu;
        }
    }


    public void onMouseMovedInResizeRange(MouseEvent mouseEvent) {
        int pointerIndex = getPointerIndex(mouseEvent);

        Circle circle = getCircles().get(pointerIndex);

        if(!circle.getFill().equals(Color.BLACK)) circle.setFill(Color.BLACK);
    }

    public void onMouseMovedOutsideResizeRange(MouseEvent mouseEvent) {
        for (Circle i: getCircles()) {
            if(!i.getFill().equals(Color.TRANSPARENT)) i.setFill(Color.TRANSPARENT);
        }
    }
}
