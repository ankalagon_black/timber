package main.junctions.views.custom.markers.skins;

import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
import main.core.views.skins.marker_views.CircleMarkerViewSkin;
import main.junctions.views.custom.markers.JunctionCircleMarkerView;
import main.junctions.views.custom.markers.behaviors.JunctionCircleMarkerViewBehavior;

import java.util.Locale;

import static main.core.utils.ViewUtils.eventToPoint;

public class JunctionCircleMarkerViewSkin extends CircleMarkerViewSkin<JunctionCircleMarkerView, JunctionCircleMarkerViewBehavior> {
    private final String INDEX  = "INDEX";
    private final String LOGICAL_DIAMETER  = "LOGICAL_DIAMETER";
    private final String FONT_SIZE  = "FONT_SIZE";
    private final String COLOR = "COLOR";
    private final String TEXT_COLOR = "TEXT_COLOR";
    private final String ENABLED = "ENABLED";

    private ContextMenu contextMenu = null;

    private Text index, diameter;

    public JunctionCircleMarkerViewSkin(JunctionCircleMarkerView control) {
        super(control, new JunctionCircleMarkerViewBehavior(control));

        getStackPane().setCursor(Cursor.OPEN_HAND);

        getCircle().setStrokeWidth(2);
        getCircle().setFill(Color.TRANSPARENT);

        index = new Text();
        diameter = new Text();

        getTextFlow().getChildren().addAll(index, diameter);

        LogicalCircleMarker logicalCircleMarker = getSkinnable().getContent().getLogicalCircleMarkerWrapper().getLogicalCircleMarker();
        CircleMarkerSettingsWrapper wrapper = getSkinnable().getContent().getMarkerSettingsWrapper();

        setText(getSkinnable().getContent().getIndex(), logicalCircleMarker.getLogicalDiameter());

        setFontSize(wrapper.getCircleMarkerSettings().getFontSize());
        setColor(wrapper.getCircleMarkerSettings().getColor());
        setTextColor(wrapper.getCircleMarkerSettings().getTextColor());
        setEnabled(wrapper.isEnabled());

        registerChangeListener(getSkinnable().getContent().indexProperty(), INDEX);
        registerChangeListener(logicalCircleMarker.logicalDiameterProperty(), LOGICAL_DIAMETER);

        registerChangeListener(wrapper.getCircleMarkerSettings().fontSizeProperty(), FONT_SIZE);
        registerChangeListener(wrapper.getCircleMarkerSettings().colorProperty(), COLOR);
        registerChangeListener(wrapper.getCircleMarkerSettings().textColorProperty(), TEXT_COLOR);
        registerChangeListener(wrapper.enabledProperty(), ENABLED);
    }

    @Override
    protected void handleControlPropertyChanged(String propertyReference) {
        super.handleControlPropertyChanged(propertyReference);
        switch (propertyReference) {
            case INDEX:
            case LOGICAL_DIAMETER:
                LogicalCircleMarker logicalCircleMarker = getSkinnable().getContent().getLogicalCircleMarkerWrapper().getLogicalCircleMarker();
                setText(getSkinnable().getContent().getIndex(), logicalCircleMarker.getLogicalDiameter());
                break;
            case FONT_SIZE:
                setFontSize(getSkinnable().getContent().getMarkerSettingsWrapper().getCircleMarkerSettings().getFontSize());
                break;
            case COLOR:
                setColor(getSkinnable().getContent().getMarkerSettingsWrapper().getCircleMarkerSettings().getColor());
                break;
            case TEXT_COLOR:
                setTextColor(getSkinnable().getContent().getMarkerSettingsWrapper().getCircleMarkerSettings().getTextColor());
                break;
            case ENABLED:
                setEnabled(getSkinnable().getContent().getMarkerSettingsWrapper().isEnabled());
                break;
        }
    }


    private void setText(int index, LogicalValue diameter) {
        if(index == -1) {
            this.index.setText(null);
            this.diameter.setText(String.format(Locale.US, "%.1f", diameter.getValueInCm()));
        }
        else {
            this.index.setText(String.valueOf(index + 1));
            this.diameter.setText(String.format(Locale.US, ";%.1f", diameter.getValueInCm()));
        }
    }

    private void setFontSize(int fontSize) {
        Font font = new Font("System Bold", fontSize);
        index.setFont(font);
        diameter.setFont(font);
    }

    private void setColor(Color color) {
        getCircle().setStroke(color);
        diameter.setFill(color);
    }

    private void setTextColor(Color color) {
        index.setFill(color);
    }

    public void setSelected(boolean isSelected) {
        getStackPane().setStyle(isSelected ? "-fx-border-color: indigo; -fx-border-width: 2;" : null);
    }

    private void setEnabled(boolean enabled) {
        getSkinnable().setDisable(!enabled);
        getTextFlow().setStyle("-fx-opacity: 1.0");
    }


    public void onMouseMovedInResizeRange(MouseEvent mouseEvent) {
        Point2D point2D = eventToPoint(mouseEvent).subtract(getSkinnable().getWidth() / 2, getSkinnable().getHeight() / 2);

        double degree = Math.toDegrees(Math.atan2(-point2D.getY(), point2D.getX()));

        if(22.5 >= degree && degree > -22.5) getStackPane().setCursor(Cursor.E_RESIZE);
        else if(67.5 >= degree && degree > 22.5) getStackPane().setCursor(Cursor.NE_RESIZE);
        else if(112.5 >= degree && degree > 67.5) getStackPane().setCursor(Cursor.N_RESIZE);
        else if(157.5 >= degree && degree > 112.5) getStackPane().setCursor(Cursor.NW_RESIZE);
        else if((180 >= degree && degree > 157.5) || (-180 <= degree && degree < -157.5)) getStackPane().setCursor(Cursor.W_RESIZE);
        else if(-157.5 <= degree && degree < -112.5) getStackPane().setCursor(Cursor.SW_RESIZE);
        else if(-112.5 <= degree && degree < -67.5) getStackPane().setCursor(Cursor.S_RESIZE);
        else if(-67.5 <= degree && degree <= -22.5) getStackPane().setCursor(Cursor.SE_RESIZE);
    }

    public void onMouseMovedOutsideMovementRange(MouseEvent mouseEvent) {
        if(!getStackPane().getCursor().equals(Cursor.OPEN_HAND)) getStackPane().setCursor(Cursor.OPEN_HAND);
    }

    public void hideContextMenu() {
        if(this.contextMenu != null) contextMenu.hide();
    }

    public void onPrimaryButtonClicked(MouseEvent mouseEvent) {
        if(getSkinnable().isSelected()) getSkinnable().clearSelection();
        else getSkinnable().select();
    }

    public void onSecondaryButtonClicked(MouseEvent mouseEvent) {
        MenuItem remove = new MenuItem("Удалить");
        remove.setOnAction(event -> {
            getSkinnable().getJunction().getMarkers().remove(getSkinnable().getContent().getLogicalCircleMarkerWrapper().getLogicalCircleMarker());
        });

        ContextMenu contextMenu = new ContextMenu(remove);

        contextMenu.show(getSkinnable(), mouseEvent.getScreenX(), mouseEvent.getScreenY());
        this.contextMenu = contextMenu;
    }
}
