package main.junctions.views.custom.markers.skins;

import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import main.core.data_types.base.LogicalValue;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.markers_settings.RulerSettings;
import main.core.views.skins.marker_views.RulerViewSkin;
import main.domain.marker_settings_wrappers.RulerSettingsWrapper;
import main.junctions.views.custom.markers.JunctionRulerView;
import main.junctions.views.custom.markers.behaviors.JunctionRulerViewBehavior;

public class JunctionRulerViewSkin extends RulerViewSkin<JunctionRulerView, JunctionRulerViewBehavior> {
    private final String LOGICAL_LENGTH = "LOGICAL_LENGTH";
    private final String FONT_SIZE  = "FONT_SIZE";
    private final String TEXT_COLOR = "TEXT_COLOR";
    private final String COLOR = "COLOR";
    private final String ENABLED = "ENABLED";
    private final String VISIBLE = "VISIBLE";

    public JunctionRulerViewSkin(JunctionRulerView control) {
        super(control, new JunctionRulerViewBehavior(control));

        LogicalRuler logicalRuler = getSkinnable().getContent().getLogicalRuler();
        RulerSettingsWrapper wrapper = getSkinnable().getContent().getRulerSettingsWrapper();
        RulerSettings settings = wrapper.getRulerSettings();

        getStartPointer().setFill(Color.TRANSPARENT);
        getStartPointer().setRadius(7);
        getStartPointer().setStrokeWidth(2);

        getEndPointer().setFill(Color.TRANSPARENT);
        getEndPointer().setRadius(7);
        getEndPointer().setStrokeWidth(2);

        setText(logicalRuler.getLogicalLength());

        setFontSize(settings.getFontSize());
        setTextColor(settings.getTextColor());
        setColor(settings.getColor());
        setEnabled(wrapper.isEnabled());
        setVisible(wrapper.isVisible());

        registerChangeListener(logicalRuler.logicalLengthProperty(), LOGICAL_LENGTH);

        registerChangeListener(settings.fontSizeProperty(), FONT_SIZE);
        registerChangeListener(settings.textColorProperty(), TEXT_COLOR);
        registerChangeListener(settings.colorProperty(), COLOR);
        registerChangeListener(wrapper.enabledProperty(), ENABLED);
        registerChangeListener(wrapper.visibleProperty(), VISIBLE);
    }


    @Override
    protected void handleControlPropertyChanged(String propertyReference) {
        super.handleControlPropertyChanged(propertyReference);
        switch (propertyReference) {
            case LOGICAL_LENGTH:
                setText(getSkinnable().getContent().getLogicalRuler().getLogicalLength());
                break;
            case FONT_SIZE:
                setFontSize(getSkinnable().getContent().getRulerSettingsWrapper().getRulerSettings().getFontSize());
                break;
            case TEXT_COLOR:
                setTextColor(getSkinnable().getContent().getRulerSettingsWrapper().getRulerSettings().getTextColor());
                break;
            case COLOR:
                setColor(getSkinnable().getContent().getRulerSettingsWrapper().getRulerSettings().getColor());
                break;
            case ENABLED:
                setEnabled(getSkinnable().getContent().getRulerSettingsWrapper().isEnabled());
                break;
            case VISIBLE:
                setVisible(getSkinnable().getContent().getRulerSettingsWrapper().isVisible());
                break;
        }
    }


    private void setText(LogicalValue logicalLength) {
        getLabel().setText(String.format("%.1f см", logicalLength.getValueInCm()));
    }

    private void setFontSize(int fontSize) {
        getLabel().setFont(new Font("System Bold", fontSize));
    }

    private void setTextColor(Color textColor) {
        getLabel().setTextFill(textColor);
    }

    private void setColor(Color color) {
        getLine().setStroke(color);
        getStartPointer().setStroke(color);
        getEndPointer().setStroke(color);
    }

    private void setEnabled(boolean enabled) {
        getSkinnable().setDisable(!enabled);
        getLabel().setStyle("-fx-opacity: 1.0");
    }

    private void setVisible(boolean visible) {
        getSkinnable().setVisible(visible);
    }


    public void onMouseMovedInResizeRange(MouseEvent mouseEvent) {
        int pointerType = getPointerType(mouseEvent);

        switch (pointerType) {
            case JunctionRulerViewBehavior.START_POINTER:
                if(!getStartPointer().getFill().equals(Color.BLACK)) getStartPointer().setFill(Color.BLACK);
                break;
            case JunctionRulerViewBehavior.END_POINTER:
                if(!getEndPointer().getFill().equals(Color.BLACK)) getEndPointer().setFill(Color.BLACK);
                break;
        }
    }

    public void onMouseMovedOutsideResizeRange(MouseEvent mouseEvent) {
        if (!getStartPointer().getFill().equals(Color.TRANSPARENT)) getStartPointer().setFill(Color.TRANSPARENT);
        if (!getEndPointer().getFill().equals(Color.TRANSPARENT)) getEndPointer().setFill(Color.TRANSPARENT);
    }
}
