package main.junctions.views.custom.markers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.scene.control.Skin;
import main.domain.marker_wrappers.junction.JunctionRulerWrapper;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.ruler_branch.Ruler;
import main.core.views.MarkerImageView;
import main.core.views.marker_views.AbstractRulerView;
import main.junctions.views.custom.markers.skins.JunctionRulerViewSkin;

public class JunctionRulerView extends AbstractRulerView<JunctionRulerWrapper> {

    private ChangeListener<Boolean> changeListener = (observable, oldValue, newValue) -> {
        if(newValue) toFront();
        else toBack();
    };

    public JunctionRulerView(JunctionRulerWrapper wrapper, MarkerImageView markerImageView) {
        super(wrapper, markerImageView);

        wrapper.getRulerSettingsWrapper().enabledProperty().addListener(new WeakChangeListener<>(changeListener));
    }

    @Override
    public Marker getMarker() {
        return getContent().getLogicalRuler();
    }

    @Override
    public Ruler getRuler() {
        return getContent().getLogicalRuler();
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new JunctionRulerViewSkin(this);
    }
}
