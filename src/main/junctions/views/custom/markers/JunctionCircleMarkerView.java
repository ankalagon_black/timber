package main.junctions.views.custom.markers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.Skin;
import main.algorithms.WoodLogsDetector;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.core.data_types.markers.ruler_branch.LogicalRuler;
import main.core.data_types.settings.LogParameters;
import main.core.views.skins.marker_views.CircleMarkerViewSkin;
import main.domain.marker_wrappers.junction.JunctionCircleMarkerWrapper;
import main.core.data_types.markers.Marker;
import main.core.data_types.markers.circle_markers_branch.CircleMarker;
import main.core.views.MarkerImageView;
import main.core.views.marker_views.AbstractCircleMarkerView;
import main.junctions.views.custom.markers.skins.JunctionCircleMarkerViewSkin;
import main.repository.loaders.CascadeLoader;
import org.opencv.core.Rect;

import java.io.File;
import java.io.IOException;

public class JunctionCircleMarkerView extends AbstractCircleMarkerView<JunctionCircleMarkerWrapper> {

    private ChangeListener<Object> junctionCircleMarkerChangeListener = (observable, oldValue, newValue) -> {
        if(oldValue != null && oldValue.equals(getContent())) ((JunctionCircleMarkerViewSkin) getSkin()).setSelected(false);
        if(newValue != null && newValue.equals(getContent())) ((JunctionCircleMarkerViewSkin) getSkin()).setSelected(true);
    };
    private WeakChangeListener<Object> junctionCircleMarkerWeakChangeListener = new WeakChangeListener<>(junctionCircleMarkerChangeListener);

    private ChangeListener<SelectionModel> selectionModelChangeListener = (observable, oldValue, newValue) -> {
        if(oldValue != null) {
            oldValue.selectedItemProperty().removeListener(junctionCircleMarkerWeakChangeListener);
        }

        if(newValue != null) {
            if(newValue.getSelectedItem().equals(getContent())) ((JunctionCircleMarkerViewSkin) getSkin()).setSelected(true);
            newValue.selectedItemProperty().addListener(junctionCircleMarkerWeakChangeListener);
        }
    };


    private CircleMarkersJunction junction;
    private LogParameters logParameters;

    private ChangeListener<Boolean> changeListener = (observable, oldValue, newValue) -> {
       setEnabled(newValue);
    };

    public JunctionCircleMarkerView(JunctionCircleMarkerWrapper wrapper, MarkerImageView markerImageView, CircleMarkersJunction junction, LogParameters logParameters) {
        super(wrapper, markerImageView);
        this.junction = junction;
        this.logParameters = logParameters;

        setEnabled(wrapper.getMarkerSettingsWrapper().isEnabled());

        SelectionModel selectionModel = markerImageView.getSelectionModel();
        if(selectionModel != null) {
            if(selectionModel.getSelectedItem() != null && selectionModel.getSelectedItem().equals(wrapper)) ((JunctionCircleMarkerViewSkin) getSkin()).setSelected(true);
            selectionModel.selectedItemProperty().addListener(junctionCircleMarkerWeakChangeListener);
        }

        markerImageView.selectionModelProperty().addListener(new WeakChangeListener<>(selectionModelChangeListener));
        wrapper.getMarkerSettingsWrapper().enabledProperty().addListener(new WeakChangeListener<>(changeListener));
    }

    public void setEnabled(boolean enabled) {
        if(enabled) toFront();
        else toBack();
    }

    public CircleMarkersJunction getJunction() {
        return junction;
    }

    @Override
    public Marker getMarker() {
        return getContent().getLogicalCircleMarkerWrapper().getLogicalCircleMarker();
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new JunctionCircleMarkerViewSkin(this);
    }

    @Override
    public CircleMarker getCircleMarker() {
        return getContent().getLogicalCircleMarkerWrapper().getLogicalCircleMarker();
    }


    public boolean isSelected() {
        SelectionModel selectionModel = getMarkerImageView().getSelectionModel();
        if(selectionModel != null) {
            Object selected = selectionModel.getSelectedItem();
            return selected != null && selected.equals(getContent());
        }
        else return false;
    }

    public void clearSelection() {
        SelectionModel selectionModel = getMarkerImageView().getSelectionModel();
        if(selectionModel != null) selectionModel.clearSelection();
    }

    public void select() {
        SelectionModel selectionModel = getMarkerImageView().getSelectionModel();
        if(selectionModel != null) selectionModel.select(getContent());
    }


    public void findRadius() {
        System.load(new File("./libraries/opencv_java341.dll").getAbsolutePath());

        WoodLogsDetector detector = null;
        try {
            detector = new WoodLogsDetector(getMarkerImageView().getImageView().getImage());
        } catch (UnsatisfiedLinkError | IOException e) {
            e.printStackTrace();
        }
        CascadeLoader cascadeLoader = new CascadeLoader();

        LogicalRuler ruler = junction.getRuler();

        double min = logParameters.getMinDiameterInPixels(ruler),
                max = logParameters.getMaxDiameterInPixels(ruler);

        LogicalCircleMarker marker = getContent().getLogicalCircleMarkerWrapper().getLogicalCircleMarker();

        Rect rect = detector.detectInArea(cascadeLoader.getCascade(), marker, min, max);

        marker.setRadius(rect.width / 2);
    }
}
