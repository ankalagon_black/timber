package main.junctions.views.custom.markers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.scene.control.Skin;
import main.core.data_types.junctions.MarkersJunction;
import main.domain.marker_wrappers.junction.JunctionMarkerWrapper;
import main.core.data_types.markers.Marker;
import main.core.views.MarkerImageView;
import main.core.views.marker_views.AbstractMarkerView;
import main.junctions.views.custom.markers.skins.JunctionMarkerViewSkin;

public class JunctionMarkerView extends AbstractMarkerView<JunctionMarkerWrapper> {

    private MarkersJunction junction;

    private ChangeListener<Boolean> changeListener = (observable, oldValue, newValue) -> {
        if(newValue) toFront();
        else toBack();
    };

    public JunctionMarkerView(JunctionMarkerWrapper wrapper, MarkerImageView markerImageView, MarkersJunction junction) {
        super(wrapper, markerImageView);
        this.junction = junction;

        wrapper.getMarkerSettingsWrapper().enabledProperty().addListener(new WeakChangeListener<>(changeListener));
    }

    @Override
    public Marker getMarker() {
        return getContent().getMarker();
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new JunctionMarkerViewSkin(this);
    }

    public MarkersJunction getJunction() {
        return junction;
    }
}
