package main.junctions.views.custom.marker_image_views.behaviors;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import main.core.views.behaviors.MarkerImageViewBehavior;
import main.core.views.marker_views.AbstractContourView;
import main.core.views.marker_views.AbstractMarkerView;
import main.core.views.skins.MarkerImageViewSkin;
import main.junctions.views.custom.marker_image_views.JunctionImageView;

import static main.core.utils.ViewUtils.eventToPoint;

public class JunctionImageViewBehavior extends MarkerImageViewBehavior<JunctionImageView> {

    private EventHandler<MouseEvent> mouseReleased = event -> {
        if(getControl().getSelectionModel() != null) getControl().getSelectionModel().clearSelection();

        if(getControl().getContourSettingsWrapper().isEnabled() && event.getButton().equals(MouseButton.SECONDARY)) {
            try {
                getControl().getContourList().stream().findFirst().ifPresent(wrapper ->
                        ((MarkerImageViewSkin) getControl().getSkin()).getMarkersRoot().getChildren().stream().
                                filter(node -> ((AbstractMarkerView) node).getContent().equals(wrapper)).
                                findFirst().ifPresent(node -> ((AbstractContourView) node).addPoint(event)));
                event.consume();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public JunctionImageViewBehavior(JunctionImageView control) {
        super(control);
        getControl().addEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleased);
    }

    @Override
    public void dispose() {
        super.dispose();
        getControl().removeEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleased);
    }
}
