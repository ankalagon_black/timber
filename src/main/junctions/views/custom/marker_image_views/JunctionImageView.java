package main.junctions.views.custom.marker_image_views;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Skin;
import main.core.data_types.junctions.AbstractJunction;
import main.domain.marker_wrappers.junction.JunctionContourWrapper;
import main.domain.marker_wrappers.junction.JunctionRulerWrapper;
import main.domain.marker_settings_wrappers.ContourSettingsWrapper;
import main.domain.marker_settings_wrappers.RulerSettingsWrapper;
import main.core.views.MarkerImageView;
import main.junctions.data_types.handlers.JunctionContoursListHandler;
import main.junctions.data_types.handlers.JunctionRulerListHandler;
import main.junctions.views.custom.marker_image_views.skins.JunctionImageViewSkin;

public class JunctionImageView extends MarkerImageView {
    public static final int RULER = 0, CONTOURS = 1, MARKERS = 2, GEOMETRY = 3;

    private RulerSettingsWrapper rulerSettingsWrapper;
    private ContourSettingsWrapper contourSettingsWrapper;


    public void initialize(JunctionRulerWrapper junctionRulerWrapper, RulerSettingsWrapper rulerSettingsWrapper,
                           ObservableList<JunctionContourWrapper> junctionContourWrapper, ContourSettingsWrapper contourSettingsWrapper,
                           AbstractJunction abstractJunction) {
        add(JunctionRulerListHandler.KEY, new JunctionRulerListHandler(FXCollections.observableArrayList(junctionRulerWrapper), this));
        this.rulerSettingsWrapper = rulerSettingsWrapper;

        add(JunctionContoursListHandler.KEY, new JunctionContoursListHandler(junctionContourWrapper, this, abstractJunction));
        this.contourSettingsWrapper = contourSettingsWrapper;
    }


    public ObservableList<JunctionRulerWrapper> getRulerList() {
        return (ObservableList<JunctionRulerWrapper>) getList(JunctionRulerListHandler.KEY);
    }

    public ObservableList<JunctionContourWrapper> getContourList() {
        return (ObservableList<JunctionContourWrapper>) getList(JunctionContoursListHandler.KEY);
    }


    @Override
    protected Skin<?> createDefaultSkin() {
        return new JunctionImageViewSkin(this);
    }


    public RulerSettingsWrapper getRulerSettingsWrapper() {
        return rulerSettingsWrapper;
    }

    public ContourSettingsWrapper getContourSettingsWrapper() {
        return contourSettingsWrapper;
    }
}
