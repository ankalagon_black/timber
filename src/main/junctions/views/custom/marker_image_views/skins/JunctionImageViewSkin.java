package main.junctions.views.custom.marker_image_views.skins;

import main.core.views.skins.MarkerImageViewSkin;
import main.junctions.views.custom.marker_image_views.JunctionImageView;
import main.junctions.views.custom.marker_image_views.behaviors.JunctionImageViewBehavior;

public class JunctionImageViewSkin extends MarkerImageViewSkin<JunctionImageView, JunctionImageViewBehavior> {

    public JunctionImageViewSkin(JunctionImageView imageView) {
        super(imageView, new JunctionImageViewBehavior(imageView));
    }
}
