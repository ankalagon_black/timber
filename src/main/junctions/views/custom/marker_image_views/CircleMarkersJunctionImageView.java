package main.junctions.views.custom.marker_image_views;

import javafx.collections.ObservableList;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SingleSelectionModel;
import main.core.data_types.junctions.AbstractJunction;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.settings.LogParameters;
import main.domain.marker_settings_wrappers.ContourSettingsWrapper;
import main.domain.marker_settings_wrappers.RulerSettingsWrapper;
import main.domain.marker_wrappers.junction.JunctionCircleMarkerWrapper;
import main.domain.marker_settings_wrappers.CircleMarkerSettingsWrapper;
import main.domain.marker_wrappers.junction.JunctionContourWrapper;
import main.domain.marker_wrappers.junction.JunctionRulerWrapper;
import main.junctions.data_types.handlers.JunctionCircleMarkersListHandler;

public class CircleMarkersJunctionImageView extends JunctionImageView {
    public static final int RESULTS = 4;

    @Override
    public void initialize(JunctionRulerWrapper junctionRulerWrapper, RulerSettingsWrapper rulerSettingsWrapper, ObservableList<JunctionContourWrapper> junctionContourWrapper, ContourSettingsWrapper contourSettingsWrapper, AbstractJunction abstractJunction) {
        super.initialize(junctionRulerWrapper, rulerSettingsWrapper, junctionContourWrapper, contourSettingsWrapper, abstractJunction);
        setSelectionModel(new CircleMarkersSelectionModel());
    }

    public void add(ObservableList<JunctionCircleMarkerWrapper> junctionMarkers, CircleMarkersJunction junction, LogParameters logParameters) {
        add(JunctionCircleMarkersListHandler.KEY, new JunctionCircleMarkersListHandler(junctionMarkers, this, junction, logParameters));
    }

    public ObservableList<JunctionCircleMarkerWrapper> getMarkers() {
        return ((ObservableList<JunctionCircleMarkerWrapper>) getList(JunctionCircleMarkersListHandler.KEY));
    }


    private CircleMarkerSettingsWrapper circleMarkerSettingsWrapper;

    public void setCircleMarkerSettings(CircleMarkerSettingsWrapper circleMarkerSettingsWrapper) {
        this.circleMarkerSettingsWrapper = circleMarkerSettingsWrapper;
    }

    public CircleMarkerSettingsWrapper getCircleMarkerSettingsWrapper() {
        return circleMarkerSettingsWrapper;
    }


    public void setEnabled(int type) {
        switch (type) {
            case RULER:
                getRulerSettingsWrapper().setEnabled(true);
                getContourSettingsWrapper().setEnabled(false);
                getCircleMarkerSettingsWrapper().setEnabled(false);
                break;
            case CONTOURS:
                getRulerSettingsWrapper().setEnabled(false);
                getContourSettingsWrapper().setEnabled(true);
                getCircleMarkerSettingsWrapper().setEnabled(false);
                break;
            case MARKERS:
                getRulerSettingsWrapper().setEnabled(false);
                getContourSettingsWrapper().setEnabled(false);
                getCircleMarkerSettingsWrapper().setEnabled(true);
                break;
            case GEOMETRY:
            case RESULTS:
                getCircleMarkerSettingsWrapper().setEnabled(false);
                getRulerSettingsWrapper().setEnabled(false);
                getContourSettingsWrapper().setEnabled(false);
                break;
        }
    }

    public class CircleMarkersSelectionModel extends SingleSelectionModel<Object> {

        @Override
        protected Object getModelItem(int index) {
            if(index == -1) return null;
            else return getMarkers().get(index);
        }

        @Override
        protected int getItemCount() {
            return getMarkers().size();
        }
    }
}
