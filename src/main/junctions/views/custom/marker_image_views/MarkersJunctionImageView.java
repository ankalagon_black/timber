package main.junctions.views.custom.marker_image_views;

import javafx.collections.ObservableList;
import main.core.data_types.junctions.MarkersJunction;
import main.domain.marker_wrappers.junction.JunctionMarkerWrapper;
import main.domain.marker_settings_wrappers.MarkerSettingsWrapper;
import main.junctions.data_types.handlers.JunctionMarkersListHandler;

public class MarkersJunctionImageView extends JunctionImageView {

    public void add(ObservableList<JunctionMarkerWrapper> junctionMarkers, MarkersJunction junction) {
        add(JunctionMarkersListHandler.KEY, new JunctionMarkersListHandler(junctionMarkers, this, junction));
    }

    public ObservableList<JunctionMarkerWrapper> getMarkers() {
        return ((ObservableList<JunctionMarkerWrapper>) getList(JunctionMarkersListHandler.KEY));
    }


    private MarkerSettingsWrapper markerSettingsWrapper;

    public MarkerSettingsWrapper getMarkerSettingsWrapper() {
        return markerSettingsWrapper;
    }

    public void setMarkerSettingsWrapper(MarkerSettingsWrapper markerSettingsWrapper) {
        this.markerSettingsWrapper = markerSettingsWrapper;
    }


    public void setEnabled(int type) {
        switch (type) {
            case RULER:
                getRulerSettingsWrapper().setEnabled(true);
                getContourSettingsWrapper().setEnabled(false);
                getMarkerSettingsWrapper().setEnabled(false);
                break;
            case CONTOURS:
                getRulerSettingsWrapper().setEnabled(false);
                getContourSettingsWrapper().setEnabled(true);
                getMarkerSettingsWrapper().setEnabled(false);
                break;
            case MARKERS:
                getRulerSettingsWrapper().setEnabled(false);
                getContourSettingsWrapper().setEnabled(false);
                getMarkerSettingsWrapper().setEnabled(true);
                break;
            case GEOMETRY:
                getMarkerSettingsWrapper().setEnabled(false);
                getRulerSettingsWrapper().setEnabled(false);
                getContourSettingsWrapper().setEnabled(false);
                break;
        }
    }
}
