package main.junctions.data_types;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import main.core.data_types.markers.contour_branch.LogicalContour;

public class LCCubageWrapper {

    public LCCubageWrapper(LogicalContour logicalContour, int index) {
        this.logicalContour = logicalContour;

        logicalContour.logicalAreaProperty().addListener(new WeakChangeListener<>(changeListener));

        setIndex(index);
    }


    private LogicalContour logicalContour;

    public LogicalContour getLogicalContour() {
        return logicalContour;
    }


    private IntegerProperty index = new SimpleIntegerProperty(-1);

    public int getIndex() {
        return index.get();
    }

    public IntegerProperty indexProperty() {
        return index;
    }

    public void setIndex(int index) {
        this.index.set(index);
    }


    private DoubleProperty volumeStandard = new SimpleDoubleProperty(0);

    public Double getVolumeStandard() {
        return volumeStandard.get();
    }

    public DoubleProperty volumeStandardProperty() {
        return volumeStandard;
    }

    public void setVolumeStandard(Double volumeStandard) {
        this.volumeStandard.set(volumeStandard);
    }


    private ChangeListener<Number> changeListener = (observable, oldValue, newValue) -> {
        //todo decide what to do later
        setVolumeStandard(newValue.doubleValue());
    };
}
