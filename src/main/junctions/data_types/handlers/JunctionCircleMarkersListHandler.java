package main.junctions.data_types.handlers;

import javafx.collections.ObservableList;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.settings.LogParameters;
import main.domain.marker_wrappers.junction.JunctionCircleMarkerWrapper;
import main.core.views.data_types.ListHandler;
import main.core.views.MarkerImageView;
import main.junctions.views.custom.markers.JunctionCircleMarkerView;

public class JunctionCircleMarkersListHandler extends ListHandler<JunctionCircleMarkerWrapper, JunctionCircleMarkerView> {
    public static final String KEY = "JUNCTION_CIRCLE_MARKERS";

    public JunctionCircleMarkersListHandler(ObservableList<JunctionCircleMarkerWrapper> list, MarkerImageView markerImageView, CircleMarkersJunction junction, LogParameters logParameters) {
        super(list);
        setCallback(param -> new JunctionCircleMarkerView(param, markerImageView, junction, logParameters));
    }
}
