package main.junctions.data_types.handlers;

import javafx.collections.ObservableList;
import main.core.data_types.junctions.MarkersJunction;
import main.domain.marker_wrappers.junction.JunctionMarkerWrapper;
import main.core.views.data_types.ListHandler;
import main.core.views.MarkerImageView;
import main.junctions.views.custom.markers.JunctionMarkerView;

public class JunctionMarkersListHandler extends ListHandler<JunctionMarkerWrapper, JunctionMarkerView> {
    public static final String KEY = "JUNCTION_MARKERS";

    public JunctionMarkersListHandler(ObservableList<JunctionMarkerWrapper> list, MarkerImageView markerImageView, MarkersJunction markersJunction) {
        super(list);
        setCallback(param -> new JunctionMarkerView(param, markerImageView, markersJunction));
    }
}
