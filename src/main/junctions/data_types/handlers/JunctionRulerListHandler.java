package main.junctions.data_types.handlers;

import javafx.collections.ObservableList;
import main.domain.marker_wrappers.junction.JunctionRulerWrapper;
import main.core.views.data_types.ListHandler;
import main.core.views.MarkerImageView;
import main.junctions.views.custom.markers.JunctionRulerView;

public class JunctionRulerListHandler extends ListHandler<JunctionRulerWrapper, JunctionRulerView> {
    public static final String KEY = "JUNCTION_RULERS";

    public JunctionRulerListHandler(ObservableList<JunctionRulerWrapper> list, MarkerImageView markerImageView) {
        super(list);
        setCallback(param -> new JunctionRulerView(param, markerImageView));
    }
}
