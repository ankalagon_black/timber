package main.junctions.data_types.handlers;

import javafx.collections.ObservableList;
import main.core.data_types.junctions.AbstractJunction;
import main.domain.marker_wrappers.junction.JunctionContourWrapper;
import main.core.views.MarkerImageView;
import main.core.views.data_types.ListHandler;
import main.junctions.views.custom.markers.JunctionContourView;

public class JunctionContoursListHandler extends ListHandler<JunctionContourWrapper, JunctionContourView> {
    public static final String KEY = "JUNCTION_CONTOURS";

    public JunctionContoursListHandler(ObservableList<JunctionContourWrapper> list, MarkerImageView markerImageView, AbstractJunction abstractJunction) {
        super(list);
        setCallback(param -> new JunctionContourView(param, markerImageView, abstractJunction));
    }
}
