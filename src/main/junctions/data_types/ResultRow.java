package main.junctions.data_types;

import javafx.beans.property.*;
import main.core.data_types.base.LogicalValue;

public class ResultRow {

    public ResultRow(LogicalValue diameterStandard, double cubage, int count, double sumVolumeStandard, double percentage) {
        setDiameterStandard(diameterStandard);
        setCubage(cubage);
        setCount(count);
        setSumVolumeStandard(sumVolumeStandard);
        setPercentage(percentage);
    }

    private ObjectProperty<LogicalValue> diameterStandard = new SimpleObjectProperty<>();

    public LogicalValue getDiameterStandard() {
        return diameterStandard.get();
    }

    public ObjectProperty<LogicalValue> diameterStandardProperty() {
        return diameterStandard;
    }

    public void setDiameterStandard(LogicalValue diameterStandard) {
        this.diameterStandard.set(diameterStandard);
    }


    private DoubleProperty cubage = new SimpleDoubleProperty();

    public double getCubage() {
        return cubage.get();
    }

    public DoubleProperty cubageProperty() {
        return cubage;
    }

    public void setCubage(double cubage) {
        this.cubage.set(cubage);
    }


    private IntegerProperty count = new SimpleIntegerProperty();

    public int getCount() {
        return count.get();
    }

    public IntegerProperty countProperty() {
        return count;
    }

    public void setCount(int count) {
        this.count.set(count);
    }


    private DoubleProperty sumVolumeStandard = new SimpleDoubleProperty();

    public double getSumVolumeStandard() {
        return sumVolumeStandard.get();
    }

    public DoubleProperty sumVolumeStandardProperty() {
        return sumVolumeStandard;
    }

    public void setSumVolumeStandard(double sumVolumeStandard) {
        this.sumVolumeStandard.set(sumVolumeStandard);
    }

    public void addSumVolumeStandard(double value) {
        sumVolumeStandard.set(sumVolumeStandard.get() + value);
    }


    private DoubleProperty percentage = new SimpleDoubleProperty();

    public double getPercentage() {
        return percentage.get();
    }

    public DoubleProperty percentageProperty() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage.set(percentage);
    }

    public void addPercentage(double value) {
        percentage.set(percentage.get() + value);
    }
}
