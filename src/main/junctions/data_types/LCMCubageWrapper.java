package main.junctions.data_types;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import main.core.data_types.junctions.CircleMarkersJunction;
import main.core.data_types.markers.circle_markers_branch.LogicalCircleMarker;
import main.repository.database.CubageDatabase;
import main.truck.data_types.proxys.CubageProxy;

public class LCMCubageWrapper {
    private CircleMarkersJunction circleMarkersJunction;
    private CubageDatabase cubageDatabase;

    public LCMCubageWrapper(LogicalCircleMarker logicalCircleMarker, CircleMarkersJunction circleMarkersJunction,
                            CubageDatabase cubageDatabase,
                            int index) {
        this.logicalCircleMarker = logicalCircleMarker;
        this.circleMarkersJunction = circleMarkersJunction;

        this.cubageDatabase = cubageDatabase;

        logicalCircleMarker.logicalDiameterProperty().addListener(new WeakChangeListener<>(changeListener));
        circleMarkersJunction.cubageTypeProperty().addListener(new WeakChangeListener<>(changeListener));
        circleMarkersJunction.lengthProperty().addListener(new WeakChangeListener<>(changeListener));

        update();
        setIndex(index);
    }

    private void update() {
        CubageProxy cubageProxy = cubageDatabase.getCubageProxy(circleMarkersJunction.getCubageType(), circleMarkersJunction.getLength(), logicalCircleMarker.getLogicalDiameter());

        if(cubageProxy != null) lcmCubage.set(LCMCubage.toLCMCubage(cubageProxy));
        else lcmCubage.set(new LCMCubage(logicalCircleMarker.getLogicalDiameter()));
    }


    private LogicalCircleMarker logicalCircleMarker;

    public LogicalCircleMarker getLogicalCircleMarker() {
        return logicalCircleMarker;
    }


    private IntegerProperty index = new SimpleIntegerProperty(-1);

    public int getIndex() {
        return index.get();
    }

    public IntegerProperty indexProperty() {
        return index;
    }

    public void setIndex(int index) {
        this.index.set(index);
    }


    private ObjectProperty<LCMCubage> lcmCubage = new SimpleObjectProperty<>();

    public LCMCubage getLcmCubage() {
        return lcmCubage.get();
    }

    public ObjectProperty<LCMCubage> lcmCubageProperty() {
        return lcmCubage;
    }

    public void setLcmCubage(LCMCubage lcmCubage) {
        this.lcmCubage.set(lcmCubage);
    }


    private ChangeListener<Object> changeListener = (observable, oldValue, newValue) -> {
        update();
    };


}
