package main.junctions.data_types;

import main.core.data_types.base.LogicalValue;
import main.truck.data_types.proxys.CubageProxy;

public class LCMCubage {

    public static LCMCubage toLCMCubage(CubageProxy cubageProxy) {
        return new LCMCubage(cubageProxy.getDiameter(), cubageProxy.getDiameterStandard(), cubageProxy.getCubage(), cubageProxy.getCubage());
    }


    public LCMCubage(LogicalValue diameter, LogicalValue diameterStandard, double cubage, double volumeStandard) {
        this.diameter = diameter;
        this.diameterStandard = diameterStandard;
        this.cubage = cubage;
        this.volumeStandard = volumeStandard;
    }

    public LCMCubage(LogicalValue diameter) {
        this(diameter, new LogicalValue(), 0, 0);
    }


    private LogicalValue diameter;

    public LogicalValue getDiameter() {
        return diameter;
    }

    public void setDiameter(LogicalValue diameter) {
        this.diameter = diameter;
    }


    private LogicalValue diameterStandard;

    public LogicalValue getDiameterStandard() {
        return diameterStandard;
    }

    public void setDiameterStandard(LogicalValue diameterStandard) {
        this.diameterStandard = diameterStandard;
    }


    private double cubage;

    public double getCubage() {
        return cubage;
    }

    public void setCubage(double cubage) {
        this.cubage = cubage;
    }


    private double volumeStandard;

    public double getVolumeStandard() {
        return volumeStandard;
    }

    public void setVolumeStandard(double volumeStandard) {
        this.volumeStandard = volumeStandard;
    }
}
